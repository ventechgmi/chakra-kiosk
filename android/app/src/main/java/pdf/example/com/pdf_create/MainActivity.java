package pdf.example.com.pdf_create;

import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

  /*   if (ContextCompat.checkSelfPermission(getActivity(),
    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
      // REQUEST_CODE_LOCATION should be defined on your app level
      ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_CODE_LOCATION);
    } */
  }

 /*  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    if (requestCode == REQUEST_CODE_LOCATION && grantResults.length > 0
        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
      throw new RuntimeException("Location services are required in order to " +
                  "connect to a reader.");
    }
  } */
}

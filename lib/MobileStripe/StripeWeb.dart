import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:interactive_webview/interactive_webview.dart';

class StripeWeb extends StatefulWidget {
  final double cartAmount;
  final String userToken;

  const StripeWeb(
      {Key key, @required this.cartAmount, @required this.userToken})
      : super(key: key);

  @override
  _StripeWebState createState() => _StripeWebState();
}

class _StripeWebState extends State<StripeWeb> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _webView = InteractiveWebView();
  String stripeMsg = 'Stripe Payment';
  String userToken = '';

  @override
  void initState() {
    super.initState();
    userToken = widget.userToken;
    _webViewHandler();
  }

  _webViewHandler() async {
    _webView.didReceiveMessage.listen((message) {
      setState(() {
        stripeMsg = message.data;
        print('from javaScript : $stripeMsg');

        if (stripeMsg == 'payment_done') {
          goToPrevious();
        }
      });
    });

    _webView.stateChanged.listen((state) {
      if (state.type == WebViewState.didFinish) {
        _webView.evalJavascript(
            'setAmount(${widget.cartAmount}, "userToken", "", "Chakra", "wildcat-tawny-beige", undefined , undefined)');
        print('Page Loaded');
      }
    });

    final html = await rootBundle.loadString("assets/test.html", cache: false);
    _webView.loadHTML(html, baseUrl: "http://192.168.10.6:3000/test.html");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Center(
        child: Text(stripeMsg),
      ),
    );
  }

  void goToPrevious() {
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        // Here you can write your code for open new view
        Navigator.of(context).pop();
      });
    });
  }
}

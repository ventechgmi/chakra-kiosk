import 'dart:io';

import 'package:chakra/admin/DatatableDemo.dart';
import 'package:chakra/admin/DevoteeRegistration.dart';
import 'package:chakra/models/ClosingReports/BillCollectors.dart';
import 'package:chakra/models/ClosingReports/ClosingReceiptNew.dart'
    as collectedReceiptsObj;
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart';
import 'package:chakra/utils/DateHelper.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:chakra/utils/Printer.dart';
import 'package:chakra/utils/ReceiptTemplate.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;

import 'ReturnAndRefunds.dart';

class AdminWidget extends StatefulWidget {
  final bool isAdminRoleActive;
  final String selectedAdmin;
  final String token;
  final int reportPrivilege;
  final int actionPrevilege;
  final String userId;
  final bool isClosingReceiptGroupBy;
  final bool isSignatureCopy;
  final String minAmntReceipt;

  AdminWidget(
      {Key key,
      @required this.isAdminRoleActive,
      @required this.selectedAdmin,
      @required this.token,
      @required this.userId,
      @required this.reportPrivilege,
      @required this.actionPrevilege,
      this.isClosingReceiptGroupBy,
      this.isSignatureCopy,
      this.minAmntReceipt})
      : super(key: key);

  @override
  _AdminWidgetState createState() => _AdminWidgetState();
}

class _AdminWidgetState extends State<AdminWidget> {
  String selectedDate = DateHelper().getCurrentDate();
  NetworkHelper networkHelper = NetworkHelper();
  List<BillCollectors> users = [];
  bool isDataLoaded = false;
  String receiptsString = '';
  String selectedUser = '';
  TenantInfo tenantInfo;
  bool isUserList = false;
  bool firstym = true;
  bool isWidget = false;

  Widget widgetForClosingReceipt;
  @override
  void initState() {
    super.initState();
    if (widget.selectedAdmin == 'Closing Receipt') {
      getCollectorsListMain();
    }

    getTenantInfo();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget container;

    switch (widget.selectedAdmin) {
      case 'Devotee Enrollment':
        container = DevoteeRegistration(
          token: widget.token,
          context: context,
        );
        break;
      case 'Return / Refund':
        container = DataTableDemo(
          token: widget.token,
          context: context,
          actionPrevilege: widget.actionPrevilege,
          isSignatureCopy: widget.isSignatureCopy,
          minAmntReceipt: widget.minAmntReceipt,
        );
        break;
      case 'Closing Receipt':
        container = Container(
            padding: EdgeInsets.all(10),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/reports.png',
                      width: 40,
                      height: 40,
                    ),
                    Text(
                      'Closing Receipt',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    OutlineButton(
                      borderSide: BorderSide(color: Colors.deepOrange),
                      onPressed: () {
                        setState(() {
                          _selectDate(context);
                        });
                      },
                      child: Text(selectedDate,
                          style: TextStyle(color: Colors.black)),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: DropdownButton<String>(
                        isDense: true,
                        hint: Text(
                          "Select a User",
                          style: TextStyle(color: Colors.black),
                        ),
                        value: selectedUser.isNotEmpty ? selectedUser : null,
                        onChanged: (String newValue) {
                          print('new Value : $newValue');
                          setState(() {
                            selectedUser = newValue;
                            if (selectedUser != null && selectedUser != '') {
                              users.forEach((element) {
                                if (element.userCollectedName == selectedUser) {
                                  getRecordsForUser(
                                      selectedDate, element.userCollectedId);
                                }
                              });
                            }
                          });
                        },
                        items: users.map((BillCollectors user) {
                          return new DropdownMenuItem<String>(
                            value: user.userCollectedName.toString(),
                            child: new Text(
                              user.userCollectedName,
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    //),
                    Visibility(
                      visible: isDataLoaded ? true : false,
                      child: IconButton(
                        icon: Icon(Icons.print),
                        onPressed: () {
                          printReceipt('');
                        },
                      ),
                    )
                  ],
                ),
                users.length == 0
                    ? Expanded(
                        child: Padding(
                            padding: EdgeInsets.all(20),
                            child: isUserList
                                ? Text('No Transactions Available')
                                : Text('')))
                    : Expanded(
                        child: Center(
                            child: Container(
                                padding: EdgeInsets.all(10),
                                constraints: BoxConstraints(maxWidth: 360),
                                child: SingleChildScrollView(
                                  child:
                                      widgetForClosingReceipt /*Html(
                                    data: receiptsString,
                                    defaultTextStyle: TextStyle(fontSize: 13),
                                  )*/
                                  ,
                                ))),
                      )
              ],
            )));
        break;
      default:
        return Container(
          height: 0,
          width: 0,
        );
        break;
    }
    return container;
  }

  Widget getWidgetForClosingReceipt(
      List<collectedReceiptsObj.ClosingReportDetails> itemsList,
      bool isClosinReceipt) {
    return Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                tenantInfo.tenantDetails.additionalProperties.receiptLogo,
                width: 50,
                height: 50,
                fit: BoxFit.fitHeight,
              ),
              SizedBox(
                width: 15,
              ),
              Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    Text(
                      tenantInfo.tenantDetails.name,
                      style: TextStyle(fontSize: 14),
                    ),
                    Text(
                      tenantInfo.tenantDetails.address.address,
                      style: TextStyle(fontSize: 11),
                    ),
                    Text(
                        tenantInfo.tenantDetails.address.city +
                            ', ' +
                            ' - ' +
                            tenantInfo.tenantDetails.address.zipCode,
                        style: TextStyle(
                          fontSize: 11,
                        )),
                    Text(
                        'Tel - ' +
                            tenantInfo
                                .tenantDetails.additionalProperties.phoneNumber,
                        style: TextStyle(
                          fontSize: 11,
                        )),
                    Text(
                        tenantInfo
                            .tenantDetails.additionalProperties.websiteUrl,
                        style: TextStyle(
                          fontSize: 11,
                        )),
                    SizedBox(height: 20),
                  ])),
            ],
          ),
          SingleChildScrollView(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: getFromPaginatedTable(itemsList, isClosinReceipt),
          )),
        ],
      ),
    );
  }

  List<Widget> getFromPaginatedTable(
      List<collectedReceiptsObj.ClosingReportDetails> itemsList,
      bool isClosinReceipt) {
    List<Widget> tableWidget = new List();
    for (int i = 0; i < itemsList.length; i++) {
      List<collectedReceiptsObj.ItemDetails> itemDetails =
          itemsList[i].itemDetails;

      if (isClosinReceipt) {
        tableWidget.add(DataTable(
            onSelectAll: (b) {},
            sortAscending: true,
            columns: <DataColumn>[
              DataColumn(
                label: Center(
                  child: Text("Item",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 12),
                      softWrap: true),
                ),
                numeric: false,
                onSort: (i, b) {},
              ),
              DataColumn(
                label: Center(
                    child: Text("Quantity",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 12),
                        softWrap: true)),
                numeric: false,
              ),
              DataColumn(
                label: Center(
                  child: Text("Amount(\$)",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 12),
                      softWrap: false),
                ),
                numeric: false,
              ),
            ],
            rows: itemDetails
                .map(
                  (item) => DataRow(
                    cells: [
                      DataCell(
                        Container(
                          child: Text(item.name),
                        ),
                        showEditIcon: false,
                        placeholder: false,
                      ),
                      DataCell(
                        Center(
                            child: Container(
                          child: Text(item.itemCount.toString()),
                        )),
                      ),
                      DataCell(
                        Center(child: Container(child: Text(item.total))),
                        showEditIcon: false,
                        placeholder: false,
                      ),
                    ],
                  ),
                )
                .toList()));
      } else {
        tableWidget.add(DataTable(
            onSelectAll: (b) {},
            sortAscending: true,
            columns: <DataColumn>[
              DataColumn(
                label: Center(
                  child: Text("Receipt Number ",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 12),
                      softWrap: true),
                ),
                numeric: false,
                onSort: (i, b) {},
              ),
              DataColumn(
                label: Center(
                    child: Text("Amount (\$)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 12),
                        softWrap: true)),
                numeric: false,
              ),
            ],
            rows: itemDetails
                .map(
                  (item) => DataRow(
                    cells: [
                      DataCell(
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              item.receiptNumber,
                              textAlign: TextAlign.left,
                            )),
                        showEditIcon: false,
                        placeholder: false,
                      ),
                      DataCell(
                        Center(
                            child: Container(
                          child: Text(item.total),
                        )),
                      ),
                    ],
                  ),
                )
                .toList()));
      }

      tableWidget.add(Row(
        children: [
          Text('Cash transaction :'),
          Text('${itemsList[i].cashTotal}')
        ],
      ));
      tableWidget.add(Row(
        children: [
          Text('Check transaction :'),
          Text('${itemsList[i].checkTotal}')
        ],
      ));

      tableWidget.add(Row(
        children: [
          Text('Card Transaction :'),
          Text('${itemsList[i].cardTotal}')
        ],
      ));

      tableWidget.add(Row(
        children: [
          Text('Total Transaction :'),
          Text('${itemsList[i].grandTotal.toString()}')
        ],
      ));
    }

    return tableWidget;
  }

  Widget getDataTable() {}

  _selectDate(BuildContext context) async {
    print(selectedDate);
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateFormat('MM-dd-yyyy').parse(selectedDate),
        firstDate: new DateTime(1990),
        lastDate: new DateTime(2099));
    if (picked != null) users.clear();
    selectedDate = DateFormat('MM-dd-yyyy').format(picked);

    await getCollectorsList();
  }

  Future<dynamic> getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var tenantInfos = await localStorage.getTenantInfoFull();
    setState(() {
      tenantInfo = tenantInfos;
    });
  }

  void getRecordsForUserOld(String recordDate, String userId) async {
    var reportsBody = {};
    reportsBody["transactionDate"] = recordDate;
    reportsBody["userId"] = userId;
    String reportsJson = convert.jsonEncode(reportsBody);
    print(reportsJson);
    var response =
        await networkHelper.getRecordsForUser(reportsJson, widget.token);

    print(response.toString());
    var collectedReceipts =
        collectedReceiptsObj.ClosingReceipt.fromJson(response);

    List<collectedReceiptsObj.ClosingReportDetails> itemsList =
        collectedReceipts.closingReceiptDetails.closingReportDetails;

    String state =
        collectedReceipts.closingReceiptDetails.tenantInfo.address.state.name;

    String htmlString = "<html><body><div id=\"invoice\"><center id=\"top\">" +
        "<div style=height:100%; width:100%>" +
        "<div style='width:25%'><img src=${tenantInfo.tenantDetails.additionalProperties.receiptLogo} width=80, height=80></div>" +
        "<div  style='width:75%'><h5>${tenantInfo.tenantDetails.name}</h5>" +
        "<p>${tenantInfo.tenantDetails.address.address}" +
        "<br>${tenantInfo.tenantDetails.address.city}," +
        " $state" +
        "<br>${tenantInfo.tenantDetails.address.zipCode},"
            "<br>Tel: ${tenantInfo.tenantDetails.additionalProperties.phoneNumber}" +
        //"<br>${tenantInfo.tenantDetails.additionalProperties.templeMail}" +
        "<br>${tenantInfo.tenantDetails.additionalProperties.websiteUrl} <br></p>" +
        "</div></div>" +
        "</center>";

    String htmlStrinhNew = "<html><body><div id=\"invoice\"><center id=\"top\">" +
        "<div style=height:100%; width:100%>" +
        "<div style='width:25%'><img src=${tenantInfo.tenantDetails.additionalProperties.receiptLogo} width=80, height=80></div>" +
        "<div  style='width:75%'><h5>${tenantInfo.tenantDetails.name}</h5>" +
        "<p>${tenantInfo.tenantDetails.address.address}" +
        "<br>${tenantInfo.tenantDetails.address.city}," +
        " $state" +
        "<br>${tenantInfo.tenantDetails.address.zipCode},"
            "<br>Tel: ${tenantInfo.tenantDetails.additionalProperties.phoneNumber}" +
        //"<br>${tenantInfo.tenantDetails.additionalProperties.templeMail}" +
        "<br>${tenantInfo.tenantDetails.additionalProperties.websiteUrl} <br></p>" +
        "</div></div>" +
        "</center>";

    if (widget.isClosingReceiptGroupBy) {
      htmlString = htmlString +
          "<div id=\"bot\"><div id=\"table\"><table class=\"itemtable\"><tr><th>Item</th><th>Quantity</th><th>Amount (\$)</th></tr>";
    } else {
      htmlString = htmlString +
          "<div id=\"bot\"><div id=\"table\"><table class=\"itemtable\"><tr><th>Receipt No.</th><th>Amount (\$)</th></tr>";
    }
    itemsList.forEach((items) {
      htmlString =
          htmlString + '<div>Collected by : ${items.collectedBy}</div>';
      List<collectedReceiptsObj.ItemDetails> itemDetails = items.itemDetails;
      double grandTotal = items.grandTotal.toDouble();
      itemDetails.forEach((i) {
        //grandTotal = grandTotal + int.parse('${i.total}');
        if (widget.isClosingReceiptGroupBy) {
          htmlString = htmlString +
              '<tr><td width=\"70%\">${i.name}</td><td width=\"20%\"> ${i.itemCount} </td><td>  ${i.total} </td></tr>';
        } else {
          htmlString = htmlString +
              '<tr><td width=\"70%\">${i.receiptNumber}</td><td> ${i.total}</td></tr>';
        }
      });

      htmlString = htmlString +
          '</table><div>Cash transaction : \$ ${items.cashTotal}</div>' +
          '<div>Check transaction : \$ ${items.checkTotal}</div>' +
          '<div>Card Transaction : \$ ${items.cardTotal}</div>' +
          '<div>Total Transaction : \$ ${grandTotal.toString()}</div>';
    });

    htmlString = htmlString + '</div></div></div></body></html>';

    print(htmlString);

    //widForClosingReceipt = getWidgetForClosingReceipt(itemsList);

    setState(() {
      isDataLoaded = true;
      //receiptsString = htmlString;
    });
  }

  void getRecordsForUser(String recordDate, String userId) async {
    var reportsBody = {};
    reportsBody["transactionDate"] = recordDate;
    reportsBody["userId"] = userId;
    String reportsJson = convert.jsonEncode(reportsBody);
    print(reportsJson);
    var response =
        await networkHelper.getRecordsForUser(reportsJson, widget.token);

    print(response.toString());
    var collectedReceipts =
        collectedReceiptsObj.ClosingReceipt.fromJson(response);

    List<collectedReceiptsObj.ClosingReportDetails> itemsList =
        collectedReceipts.closingReceiptDetails.closingReportDetails;

    String state =
        collectedReceipts.closingReceiptDetails.tenantInfo.address.state.name;

    bool isClosingReceiptGroupBy =
        collectedReceipts.closingReceiptDetails.closingReceiptGroupbyService;

    String htmlResponseToDisplay = "<html><body><div id=\"invoice\"><center id=\"top\">" +
        "<div style=height:100%; width:100%>" +
        "<div style='width:25%'><img src=${tenantInfo.tenantDetails.additionalProperties.receiptLogo} width=80, height=80></div>" +
        "<div  style='width:75%'><h5>${tenantInfo.tenantDetails.name}</h5>" +
        "<p>${tenantInfo.tenantDetails.address.address}" +
        "<br>${tenantInfo.tenantDetails.address.city}," +
        " $state" +
        "<br>${tenantInfo.tenantDetails.address.zipCode},"
            "<br>Tel: ${tenantInfo.tenantDetails.additionalProperties.phoneNumber}" +
        //"<br>${tenantInfo.tenantDetails.additionalProperties.templeMail}" +
        "<br>${tenantInfo.tenantDetails.additionalProperties.websiteUrl} <br></p>" +
        "</div></div>" +
        "</center>";

    String htmlResponseToPrint =
        "<html><head><style type=\"text/css\">	html {margin: 0;padding: 0;}</style></head><body>" +
            "<div style=\"height:auto;\"><table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: sans-serif;font-weight: 400;'>" +
            "<tr><td><table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${tenantInfo.tenantDetails.additionalProperties.receiptLogo} /></td>" +
            "<td style='font-size: 10pt;'><b>${tenantInfo.tenantDetails.name}</b><br>${tenantInfo.tenantDetails.address.address}<br>${tenantInfo.tenantDetails.address.city},<br>Tel: $state" +
            "<br>${tenantInfo.tenantDetails.address.zipCode},<br>Tel: ${tenantInfo.tenantDetails.additionalProperties.phoneNumber},<br>${tenantInfo.tenantDetails.additionalProperties.websiteUrl}</td></tr></table></td></tr>";

    if (isClosingReceiptGroupBy) {
      htmlResponseToDisplay = htmlResponseToDisplay +
          "<td style='padding-left:10px;word-wrap: break-word;'>" +
          "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
          "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Item</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000;'>Quantity</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
          "</tr>";

      htmlResponseToPrint = htmlResponseToPrint +
          "<td style='padding-left:10px;word-wrap: break-word;'>" +
          "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
          "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Item</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000;'>Quantity</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
          "</tr>";
    } else {
      htmlResponseToDisplay = htmlResponseToDisplay +
          "<td style='padding-left:10px;word-wrap: break-word;'>" +
          "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
          "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Receipt No.</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
          "</tr>";

      htmlResponseToPrint = htmlResponseToPrint +
          "<td style='padding-left:10px;word-wrap: break-word;'>" +
          "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
          "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Receipt No.</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
          "</tr>";
    }

    itemsList.forEach((items) {
      htmlResponseToDisplay = htmlResponseToDisplay +
          '<div>Collected by : ${items.collectedBy}</div>';
      htmlResponseToPrint = htmlResponseToPrint +
          '<div>Collected by : ${items.collectedBy}</div></br>';
      List<collectedReceiptsObj.ItemDetails> itemDetails = items.itemDetails;
      double grandTotal = items.grandTotal.toDouble();
      itemDetails.forEach((i) {
        //grandTotal = grandTotal + int.parse('${i.total}');
        if (isClosingReceiptGroupBy) {
          htmlResponseToDisplay = htmlResponseToDisplay +
              "<tr><td style='border: 1px solid #000000'>${i.name}</td>" +
              "<td style='text-align:right; border: 1px solid #000000;'>${i.itemCount}</td>" +
              "<td style='text-align:right; border: 1px solid #000000'>${i.total}</td>" +
              "</tr></br>";
          htmlResponseToPrint = htmlResponseToPrint +
              "<tr><td style='border: 1px solid #000000'>${i.name}</td>" +
              "<td style='text-align:right; border: 1px solid #000000;'>${i.itemCount}</td>" +
              "<td style='text-align:right; border: 1px solid #000000'>${i.total}</td>" +
              "</tr>";
        } else {
          htmlResponseToDisplay = htmlResponseToDisplay +
              "<tr><td style='border: 1px solid #000000'>${i.receiptNumber}</td>" +
              "<td style='text-align:right; border: 1px solid #000000'>${i.total}</td>" +
              "</tr></br>";

          htmlResponseToPrint = htmlResponseToPrint +
              "<tr><td style='border: 1px solid #000000'>${i.receiptNumber}</td>" +
              "<td style='text-align:right; border: 1px solid #000000'>${i.total}</td>" +
              "</tr>";
        }
      });

      htmlResponseToDisplay = htmlResponseToDisplay +
          '</table><div></br>Cash transaction : \$ ${items.cashTotal}</div></br>' +
          '<div>Check transaction : \$ ${items.checkTotal}</div></br>' +
          '<div>Card Transaction : \$ ${items.cardTotal}</div></br>' +
          '<div>Total Transaction : \$ ${grandTotal.toString()}</div></br>';

      htmlResponseToPrint = htmlResponseToPrint +
          '</table><div></br>Cash transaction : \$ ${items.cashTotal}</div></br>' +
          '<div>Check transaction : \$ ${items.checkTotal}</div></br>' +
          '<div>Card Transaction : \$ ${items.cardTotal}</div></br>' +
          '<div>Total Transaction : \$ ${grandTotal.toString()}</div></br>';
    });

    htmlResponseToDisplay =
        htmlResponseToDisplay + "</table></div></body></html>";
    htmlResponseToPrint = htmlResponseToPrint + "</table></div></body></html>";

    print(htmlResponseToPrint);
    print('**************');
    print(htmlResponseToDisplay);
    //widForClosingReceipt = getWidgetForClosingReceipt(itemsList);

    Printer().writeAndPrintText(htmlResponseToPrint);
    Widget widgForClosingReceipt =
        getWidgetForClosingReceipt(itemsList, isClosingReceiptGroupBy);

    setState(() {
      isDataLoaded = true;
      widgetForClosingReceipt = widgForClosingReceipt;
    });
  }

  getCollectorsList() async {
    receiptsString = '';
    users.clear();
    var response =
        await networkHelper.getBillCollectors(selectedDate, widget.token);

    selectedUser = '';
    isUserList = false;
    print('Inside getCollectorsList');
    print(selectedDate);
    print(users.length);
    isWidget = false;

    if (response != null) {
      List billCollectors = response['collectedByUserList'];

      if (billCollectors.length > 0) {
        print('billCollectors ***');
        billCollectors.forEach((user) {
          if (widget.reportPrivilege == 1) {
            if (BillCollectors.fromJson(user).userCollectedId ==
                widget.userId) {
              users.add(BillCollectors.fromJson(user));
              selectedUser = user['collectedByUserName'];

              isWidget = true;

              print('******** ' + user['collectedByUserName']);
            }
          } else {
            users.add(BillCollectors.fromJson(user));
            selectedUser = '';

            // print('----- ' + user.userCollectedName);
          }
        });
      } else {
        isUserList = true;
        selectedUser = '';
      }

      if (firstym) {
        firstym = false;
        if (selectedUser != null &&
            selectedUser != '' &&
            selectedUser.length > 0) {
          if (users.length > 0) {
            print(users.length);
            users.forEach((element) {
              if (element.userCollectedName == selectedUser) {
                getRecordsForUser(selectedDate, element.userCollectedId);
              }
            });
          } else {
            isUserList = true;
          }
        } else {
          isUserList = true;
        }
      }
      print('yada yyada');
      print(selectedUser);
      print(users.length);
      print(isUserList);

      print('Before setState');
      print('After setState');
      if (selectedUser != null &&
          selectedUser != '' &&
          selectedUser.length > 0) {
        if (users.length > 0) {
          print(users.length);
          users.forEach((element) {
            if (element.userCollectedName == selectedUser) {
              getRecordsForUser(selectedDate, element.userCollectedId);
            }
          });
        } else {
          isUserList = true;
        }
      } else {
        isUserList = true;
      }
      setState(() {});
    }
  }

  void printReceipt(String orderJson) {
    Socket.connect("localhost", 2500).then((socket) async {
      print('Connected to: '
          '${socket.remoteAddress.address}:${socket.remotePort}');

      //Establish the onData, and onDone callbacks
      socket.listen((data) {
        print('listening');
        print(new String.fromCharCodes(data).trim());
      }, onDone: () {
        print("Done");
        socket.destroy();
      });
      socket.write(receiptsString);
    });
  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  void getCollectorsListMain() async {
    await getCollectorsList();
  }
}

import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/models/Products/Items.dart';
import 'package:chakra/models/ReturnRefunds/Donations.dart';
import 'package:chakra/models/ReturnRefunds/OrderDetails.dart';
import 'package:chakra/models/ReturnRefunds/RefundOrderByCash.dart';
import 'package:chakra/models/ReturnRefunds/RefundOrderByCheck.dart';
import 'package:chakra/models/ReturnRefunds/RepaymentByCash.dart';
import 'package:chakra/models/ReturnRefunds/RepaymentByCheck.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'package:chakra/utils/DateHelper.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:data_tables/data_tables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;

class ReturnAndRefunds extends StatefulWidget {
  final String token;
  final BuildContext context;

  @override
  _ReturnAndRefundsState createState() => _ReturnAndRefundsState();
  const ReturnAndRefunds(
      {Key key, @required this.token, @required this.context})
      : super(key: key);
}

class _ReturnAndRefundsState extends State<ReturnAndRefunds> {
  NetworkHelper networkHelper = new NetworkHelper();
  final _formKey = GlobalKey<FormState>();
  List<DonationItems> items = [];
  List<DonationItems> filterItems = [];
  DateTime selectedDate = DateTime.now();
  DateTime newCheckDate = DateTime.now();
  TextEditingController nameController = TextEditingController();
  TextEditingController receiptNumController = TextEditingController();
  var dateHelper = new DateHelper();
  bool cashVal = false;
  bool checkVal = false;
  bool cardVal = false;
  String tenantName;
  String tenantAddress;
  String tenantLogo;
  TenantInfo tenantInfo;
  List<OrderItems> selectedItems;
  int defaultRowsPerPage = 5;
  var dataForRows;

  Widget bodyData() => DataTable(
      sortAscending: true,
      sortColumnIndex: 0,
      columns: <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Date",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          numeric: true,
          onSort: (i, b) {
            setState(() {
              items.sort(
                  (a, b) => a.transactionDate.compareTo(b.transactionDate));
            });
          },
          /*   onSort: (i, b) {
            setState(() {
              items.sort(
                  (b, a) => a.transactionDate.compareTo(b.transactionDate));
            });
          }, */
        ),
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Receipt Num",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          numeric: true,
          onSort: (i, b) {
            print("$i $b");
            setState(() {
              items.sort((a, b) => a.receiptNumber.compareTo(b.receiptNumber));
            });
          },
        ),
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Name",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          numeric: false,
          onSort: (i, b) {
            print("$i $b");
            setState(() {
              items.sort((a, b) => a.firstName.compareTo(b.firstName));
            });
          },
        ),
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Service",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          numeric: false,
          onSort: (i, b) {
            print("$i $b");
            setState(() {
              items.sort((a, b) => a.serviceName.compareTo(b.serviceName));
            });
          },
        ),
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Payment mode",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          numeric: false,
          onSort: (i, b) {
            print("$i $b");
            setState(() {
              items.sort((a, b) => a.paymentType.compareTo(b.paymentType));
            });
          },
        ),
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Amount",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          onSort: (i, b) {
            print("$i $b");
            setState(() {
              items.sort((a, b) => a.amount.compareTo(b.amount));
            });
          },
          numeric: true,
        ),
        DataColumn(
          label: Expanded(
            child: Center(
                child: Text("Action",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                        fontSize: 12),
                    softWrap: true)),
          ),
          numeric: false,
        ),
      ],
      rows: items
          .map(
            (item) => DataRow(
              cells: [
                DataCell(
                  Text(
                    item.transactionDate,
                    textAlign: TextAlign.center,
                  ),
                  showEditIcon: false,
                  placeholder: false,
                ),
                DataCell(
                  Center(
                    child: FlatButton(
                      child: Text(
                        item.receiptNumber.toString(),
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      onPressed: () {
                        getPrintReceiptDetails(item.orderId,
                            item.receiptNumber.toString(), context);
                      },
                    ),
                  ),
                  showEditIcon: false,
                  placeholder: false,
                ),
                DataCell(
                  Center(
                    child: Text(item.firstName + ' ' + item.lastName),
                  ),
                  showEditIcon: false,
                  placeholder: false,
                ),
                DataCell(
                  Center(
                    child: Text(toBeginningOfSentenceCase(
                        item.serviceName.toString().toLowerCase())),
                  ),
                  showEditIcon: false,
                  placeholder: false,
                ),
                DataCell(
                  Center(
                    child: Text(item.paymentType),
                  ),
                  showEditIcon: false,
                  placeholder: false,
                ),
                DataCell(
                  Center(
                    child: Text('\$ ${item.totalAmount}.00'),
                  ),
                  showEditIcon: false,
                  placeholder: false,
                ),
                DataCell(Center(
                    child: Row(
                  children: <Widget>[
                    item.paymentType == "Check"
                        ? Padding(
                            padding: EdgeInsets.all(10),
                            child: FlatButton(
                              textColor: Colors.deepOrange,
                              child: (item.type == '' ||
                                      item.type == 'Repayment')
                                  ? Text(
                                      'Return',
                                      style: TextStyle(
                                          fontSize: 11,
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.underline),
                                    )
                                  : Text(''),
                              onPressed: () {
                                if (item.type != 'Returned') {
                                  getOrderDetails(item.orderId, true, true,
                                      item.paymentType);
                                }
                              },
                            ),
                          )
                        : SizedBox(
                            height: 0,
                            width: 0,
                          ),
                    Visibility(
                      visible: (item.type == '' || item.type == 'Repayment')
                          ? true
                          : false,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                          textColor: Colors.deepOrange,
                          child: Text(
                            'Refund',
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {
                            getOrderDetails(
                                item.orderId, false, false, item.paymentType);
                          },
                        ),
                      ),
                    ),
                    Visibility(
                      visible: item.type == 'Refunded' ? true : false,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                          textColor: Colors.green,
                          child: Text(
                            'Refunded',
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {
                            /*  getOrderDetails(
                                                                                item.orderId, false, false, item.paymentType); */
                          },
                        ),
                      ),
                    ),
                    Visibility(
                      visible: item.type == 'Returned' ? true : false,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                          textColor: Colors.deepOrange,
                          child: Text(
                            'Repay',
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {
                            getOrderDetails(
                                item.orderId, false, true, item.paymentType);
                          },
                        ),
                      ),
                    )
                  ],
                ))),
              ],
            ),
          )
          .toList());

  @override
  void initState() {
    //_items = _desserts;
    super.initState();
    getAllDonations();
    getTenantDetails();
    selectedItems = [];
  }

  onSelectedRow(bool selected, OrderItems orderItems) async {
    setState(() {
      if (selected) {
        selectedItems.add(orderItems);
      } else {
        selectedItems.remove(orderItems);
      }

      print(selectedItems);
    });
  }

  getTenantDetails() async {
    await getTenantInfo();
  }

  getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var response = await localStorage.getTenantInfo();

    print('tenanat Info : ' + response.toString());

    //tenantInfo = TenantInfo.fromJson(response);

    /*   setState(() {
      tenantName = tenantInfo.tenantDetails.name;
      tenantAddress = tenantInfo.tenantDetails.address.address;
      tenantLogo = tenantInfo.tenantDetails.additionalProperties.receiptLogo;

      print('tenant Logo : $tenantLogo'); 

      /*  selectedState = tenantInfo.tenantDetails.address.stateId;
                              //selectedCountryCode = tenantInfo.tenantDetails.address.countryCode;
                              zipController.text = tenantInfo.tenantDetails.address.zipCode;
                              cityController.text = tenantInfo.tenantDetails.address.city;
                              getStateList(selectedCountry); */
    });*/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/refund.png',
                      width: 40,
                      height: 40,
                    ),
                    Text(
                      'Return and Refunds',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
                /*  Padding(
                                          padding: EdgeInsets.only(left: 20, right: 20),
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              height: 60,
                                              color: Colors.deepOrange,
                                              child: Form(
                                                key: _formKey,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(
                                                      'Search by : ',
                                                      style: TextStyle(
                                                          color: Colors.white54,
                                                          fontSize: 16,
                                                          fontWeight: FontWeight.w500),
                                                    ),
                                                    Container(
                                                      width: 180,
                                                      padding: EdgeInsets.all(5),
                                                      child: TextFormField(
                                                          controller: nameController,
                                                          style: TextStyle(
                                                              color: Colors.white,
                                                              fontSize: 13,
                                                              fontWeight: FontWeight.w500),
                                                          cursorColor: Colors.white,
                                                          decoration: InputDecoration(
                                                              contentPadding: EdgeInsets.all(8),
                                                              hintText: 'Name',
                                                              hintStyle: TextStyle(
                                                                  color: Colors.white60,
                                                                  fontSize: 13,
                                                                  fontWeight: FontWeight.w500),
                                                              //border: InputBorder.none
                                                              border: UnderlineInputBorder(
                                                                  borderSide: BorderSide(
                                                                      color: Colors.white60)))),
                        
                                                      /*   decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          border: Border.all(color: Colors.white),
                                                          borderRadius: BorderRadius.circular(30)), */
                                                    ),
                                                    SizedBox(
                                                      width: 20,
                                                    ),
                                                    Container(
                                                      width: 180,
                                                      padding: EdgeInsets.all(5),
                                                      child: TextFormField(
                                                        keyboardType: TextInputType.number,
                                                        inputFormatters: <TextInputFormatter>[
                                                          WhitelistingTextInputFormatter.digitsOnly
                                                        ],
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 13,
                                                            fontWeight: FontWeight.w500),
                                                        cursorColor: Colors.white,
                                                        controller: receiptNumController,
                                                        decoration: InputDecoration(
                                                            contentPadding: EdgeInsets.all(8),
                                                            hintText: 'Receipt Number',
                                                            hintStyle: TextStyle(
                                                                color: Colors.white60,
                                                                fontSize: 13,
                                                                fontWeight: FontWeight.w500),
                        
                                                            //focusedBorder: InputBorder( borderSide: )
                                                            border: UnderlineInputBorder(
                                                                borderSide:
                                                                    BorderSide(color: Colors.white60))),
                                                      ),
                                                      /*  decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          border: Border.all(color: Colors.white),
                                                          borderRadius: BorderRadius.circular(30)), */
                                                    ),
                                                    Theme(
                                                        data: Theme.of(context).copyWith(
                                                          unselectedWidgetColor: Colors.white,
                                                        ),
                                                        child: Checkbox(
                                                          checkColor: Colors.white,
                                                          value: cashVal,
                                                          onChanged: (bool value) {
                                                            setState(() {
                                                              cashVal = value;
                                                            });
                                                          },
                                                        )),
                                                    Text(
                                                      'Only cash',
                                                      style: TextStyle(color: Colors.white),
                                                    ),
                                                    Theme(
                                                        data: Theme.of(context).copyWith(
                                                          unselectedWidgetColor: Colors.white,
                                                        ),
                                                        child: Checkbox(
                                                          checkColor: Colors.white,
                                                          value: checkVal,
                                                          onChanged: (bool value) {
                                                            setState(() {
                                                              checkVal = value;
                                                            });
                                                          },
                                                        )),
                                                    Text(
                                                      'only check',
                                                      style: TextStyle(color: Colors.white),
                                                    ),
                                                    Theme(
                                                      data: Theme.of(context).copyWith(
                                                        unselectedWidgetColor: Colors.white,
                                                      ),
                                                      child: Checkbox(
                                                        checkColor: Colors.white,
                                                        value: cardVal,
                                                        onChanged: (bool value) {
                                                          setState(() {
                                                            cardVal = value;
                                                          });
                                                        },
                                                      ),
                                                    ),
                                                    Text(
                                                      'only card',
                                                      style: TextStyle(color: Colors.white),
                                                    ),
                                                    SizedBox(
                                                      width: 20,
                                                    ),
                                                    Visibility(
                                                      visible: nameController.text.isNotEmpty ||
                                                              receiptNumController.text.isNotEmpty ||
                                                              checkVal ||
                                                              cardVal ||
                                                              cashVal
                                                          ? true
                                                          : false,
                                                      child: OutlineButton(
                                                          padding: EdgeInsets.all(2),
                                                          color: Colors.white,
                                                          onPressed: () {
                                                            setState(() {
                                                              nameController.clear();
                                                              receiptNumController.clear();
                                                              checkVal = false;
                                                              cashVal = false;
                                                              cardVal = false;
                                                            });
                                                          },
                                                          borderSide: BorderSide(
                                                            color: Colors.white,
                                                          ),
                                                          shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius.circular(30.0),
                                                          ),
                                                          child: Text(
                                                            'Clear',
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: 13,
                                                                fontWeight: FontWeight.bold),
                                                          )),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                        ),
                                        */
                Expanded(
                    child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Padding(
                            child: bodyData(), padding: EdgeInsets.all(20))))
              ],
            )));
  }

  getAllDonations() async {
    NetworkHelper networkHelper = NetworkHelper();
    var response = await networkHelper.getAllDonations(widget.token);
    Donations donations = Donations.fromJson(response);
    setState(() {
      items = donations.donationItems;
      //dataForRows = DTS(items, context, widget.token);
      //dataForRows = items;
    });
  }

  getFilterItems(List<DonationItems> items, String searchText) {
    //print(items.contains(searchText));
    items.forEach((item) {
      if (items.contains(searchText)) {
        filterItems.add(item);
      }
    });
  }

  Future<void> getOrderDetails(String orderId, bool isShowDialog,
      bool isCheckReturned, String paymentType) async {
    NetworkHelper networkHelper = new NetworkHelper();
    var response = await networkHelper.getOrderDetails(widget.token, orderId);

    OrderDetails orderDetails = OrderDetails.fromJson(response);
    List<OrderItems> orderItems = orderDetails.orderDetails;
    //orderItems.forEach((f) {});

    if (isShowDialog) {
      _showDialog(orderItems);
    } else {
      if (isCheckReturned) {
        _showDialogRepay(orderItems, false, paymentType);
      } else {
        _showDialogRepay(orderItems, true, paymentType);
      }
    }
  }

  _showDialogRepay(List<OrderItems> items, bool isRefund, String paymentType) {
    String dropdownValue = paymentType;
    TextEditingController reason = TextEditingController();
    TextEditingController payeeName = TextEditingController();
    TextEditingController bankName = TextEditingController();
    //TextEditingController dateOnTheCehck = TextEditingController();
    TextEditingController refundCheckNumber = TextEditingController();
    TextEditingController bankCharges = TextEditingController();
    bool _validPayeeName = false;
    bool _validBankName = false;
    bool _validCheckNumber = false;
    bool _isValidReason = false;

    double totalAmount = 0.00;
    bankCharges.text = '0.00';

    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '${items[0].firstName} ${items[0].lastName}',
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                  child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    DataTable(
                        showCheckboxColumn: true,
                        //sortColumnIndex: 0,
                        //sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Date",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Receipt Num",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Service Name",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Mode",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                            /*  onSort: (i, b) {
                                                                                  print("$i $b");
                                                                                  setState(() {
                                                                                    names.sort((a, b) => a.lastName.compareTo(b.lastName));
                                                                                  });
                                                                                }, */
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Amount",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                        ],
                        rows: items
                            .map(
                              (item) => DataRow(
                                selected: selectedItems.contains(item),
                                onSelectChanged: (b) {
                                  setState(() {
                                    onSelectedRow(b, item);
                                  });
                                },
                                cells: [
                                  DataCell(
                                    Center(
                                      child: Text(
                                        dateHelper.getFromattedDate(
                                            item.transactionDate),
                                        //item.transactionDate.substring(0, 10),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.receiptNumber.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.serviceName,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.paymentType,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        '\$ ${item.totalAmount}',
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                ],
                              ),
                            )
                            .toList()),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 600),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Payment Type',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: DropdownButton<String>(
                                            isExpanded: true,
                                            value: dropdownValue,
                                            icon: Icon(Icons.arrow_drop_down),
                                            iconSize: 22,
                                            elevation: 16,
                                            hint: Text(
                                              'Select payment type',
                                              style: TextStyle(fontSize: 12),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                dropdownValue = newValue;
                                              });
                                            },
                                            items: <String>['Check', 'Cash']
                                                .map<DropdownMenuItem<String>>(
                                                    (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(
                                                  value,
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              );
                                            }).toList(),
                                          ))
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: (isRefund &&
                                                items[0].refundDetails == null)
                                            ? Text('Refunded On',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey))
                                            : Text('Transaction Date',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: OutlineButton(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                selectedDate
                                                    .toString()
                                                    .substring(0, 10),
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(5),
                                                child: Icon(
                                                  FontAwesomeIcons
                                                      .solidCalendarAlt,
                                                  color: Colors.deepOrange,
                                                  size: 14,
                                                ),
                                              )
                                            ],
                                          ),
                                          onPressed: () async {
                                            DateTime picked =
                                                await showDatePicker(
                                              context: context,
                                              initialDate: selectedDate,
                                              firstDate: DateTime(2000, 1),
                                              lastDate: DateTime.now(),
                                            );
                                            setState(() {
                                              if (picked != null) {
                                                selectedDate = picked;
                                              } else {
                                                selectedDate = DateTime.now();
                                              }
                                            });
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Amount',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          //items[0].totalAmount,
                                          '\$ ${totalAmount.toStringAsFixed(2)}',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Reason *',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        /*   child: Container(
                                                                    padding: EdgeInsets.all(5),
                                                                    decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5),
                                                                        border: Border.all(
                                                                            color: Colors.grey)), */
                                        child: TextField(
                                          controller: reason,
                                          style: TextStyle(fontSize: 12),
                                          keyboardType: TextInputType.multiline,
                                          decoration: InputDecoration(
                                            errorText: _isValidReason
                                                ? 'Reason mandatory'
                                                : null,
                                            hintStyle: TextStyle(fontSize: 12),
                                            hintText: 'Reason',
                                          ),
                                        ),
                                      )
                                      //)
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible: (isRefund &&
                                          items[0].refundDetails == null)
                                      ? false
                                      : true,
                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: Text('Bank charges (\$)',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.grey)),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                            flex: 2,
                                            child: Container(
                                              child: TextField(
                                                controller: bankCharges,
                                                decoration: InputDecoration(
                                                    hintText: '\$ 0.00',
                                                    border: UnderlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        borderSide: BorderSide(
                                                            color:
                                                                Colors.black))),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Total amount to be paid',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Text(
                                                  '\$ ${(totalAmount + double.parse(bankCharges.text)).toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.w500))))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 40,
                          ),
                          Expanded(
                              child: Column(
                            children: <Widget>[
                              Visibility(
                                visible:
                                    dropdownValue == "Check" ? true : false,
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Name on Check *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: TextField(
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        100),
                                                  ],
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                  controller: payeeName,
                                                  decoration: InputDecoration(
                                                      errorText: _validPayeeName
                                                          ? 'Name cannot be empty'
                                                          : null,
                                                      hintText: 'Name on Check',
                                                      hintStyle: TextStyle(
                                                          fontSize: 12),
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Check Number *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                /*  decoration: BoxDecoration(
                                                                                                    borderRadius:
                                                                                                        BorderRadius.circular(
                                                                                                            5),
                                                                                                    border: Border.all(
                                                                                                        color: Colors.grey)), */
                                                child: TextField(
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        10),
                                                    WhitelistingTextInputFormatter
                                                        .digitsOnly,
                                                  ],
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                  controller: refundCheckNumber,
                                                  decoration: InputDecoration(
                                                      errorText: _validCheckNumber
                                                          ? 'Check number cannot be empty'
                                                          : null,
                                                      hintText: 'Check Number',
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Check Date *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: OutlineButton(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    newCheckDate
                                                        .toString()
                                                        .substring(0, 10),
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.all(5),
                                                    child: Icon(
                                                      FontAwesomeIcons
                                                          .solidCalendarAlt,
                                                      color: Colors.deepOrange,
                                                      size: 14,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              onPressed: () async {
                                                DateTime picked =
                                                    await showDatePicker(
                                                  context: context,
                                                  initialDate: newCheckDate,
                                                  firstDate: DateTime.now(),
                                                  lastDate: DateTime(2100, 12),
                                                );
                                                setState(() {
                                                  if (picked != null) {
                                                    newCheckDate = picked;
                                                  } else {
                                                    newCheckDate =
                                                        DateTime.now();
                                                  }
                                                });
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Bank Name *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: TextField(
                                                  controller: bankName,
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        100),
                                                  ],
                                                  decoration: InputDecoration(
                                                      errorText: _validBankName
                                                          ? 'Bank name cannot be empty'
                                                          : null,
                                                      hintText: 'Bank Name',
                                                      hintStyle: TextStyle(
                                                          fontSize: 12),
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Visibility(
                                visible: dropdownValue == "Card" ? true : false,
                                child: Center(
                                  child: RaisedButton(
                                    color: Theme.of(context).primaryColor,
                                    child: Text(
                                      'Click and swipe a card',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              )
                            ],
                          ))
                        ],
                      ),
                    ),
                    Center(
                      child: OutlineButton(
                        borderSide:
                            BorderSide(color: Theme.of(context).primaryColor),
                        child: isRefund
                            ? Text(
                                'Refund',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              )
                            : Text(
                                'Repay',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              ),
                        onPressed: () {
                          if (isRefund) {
                            if (dropdownValue == 'Cash') {
                              if (reason.text.isEmpty) {
                                setState(() {
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                });
                              } else {
                                refundAnOrderByCash(
                                    items[0].orderId,
                                    items[0].totalAmount,
                                    reason.text,
                                    selectedItems);
                              }
                            } else if (dropdownValue == 'Check') {
                              if (payeeName.text.isEmpty ||
                                  refundCheckNumber.text.isEmpty ||
                                  bankName.text.isEmpty ||
                                  reason.text.isEmpty) {
                                setState(() {
                                  payeeName.text.isEmpty
                                      ? _validPayeeName = true
                                      : _validPayeeName = false;
                                  refundCheckNumber.text.isEmpty
                                      ? _validCheckNumber = true
                                      : _validCheckNumber = false;
                                  bankName.text.isEmpty
                                      ? _validBankName = true
                                      : _validBankName = false;
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                });
                              } else {
                                refundAnOrderByCheck(
                                    items[0].orderId,
                                    items[0].totalAmount,
                                    reason.text,
                                    selectedItems,
                                    payeeName.text,
                                    bankName.text,
                                    refundCheckNumber.text,
                                    newCheckDate.toString().substring(0, 10));
                              }
                            }
                          } else {
                            if (dropdownValue == 'Cash') {
                              if (reason.text.isEmpty) {
                                reason.text.isEmpty
                                    ? _isValidReason = true
                                    : _isValidReason = false;
                              }
                              repayByCash(items[0].orderId,
                                  items[0].totalAmount, reason.text, items);
                            } else if (dropdownValue == 'Check') {
                              if (payeeName.text.isEmpty ||
                                  refundCheckNumber.text.isEmpty ||
                                  bankName.text.isEmpty ||
                                  reason.text.isEmpty) {
                                setState(() {
                                  payeeName.text.isEmpty
                                      ? _validPayeeName = true
                                      : _validPayeeName = false;
                                  refundCheckNumber.text.isEmpty
                                      ? _validCheckNumber = true
                                      : _validCheckNumber = false;
                                  bankName.text.isEmpty
                                      ? _validBankName = true
                                      : _validBankName = false;
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                });
                              } else {
                                repayByCheck(
                                    items[0].orderId,
                                    items[0].totalAmount,
                                    reason.text,
                                    items,
                                    payeeName.text,
                                    bankName.text,
                                    refundCheckNumber.text,
                                    newCheckDate.toString().substring(0, 10));
                              }
                            }
                          }
                        },
                      ),
                    )
                  ],
                ),
              ));
            }));
      },
    );
  }

  void _showDialog(
    List<OrderItems> items,
  ) {
    TextEditingController reasonController = TextEditingController();
    double totalAmount = 0.00;
    bool _isReasonValid = false;

    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                constraints: BoxConstraints(minWidth: 1200),
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '${items[0].firstName} ${items[0].lastName}',
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Padding(
                  padding: EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        DataTable(
                            onSelectAll: (b) {},
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Date",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Receipt Num",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Service Name",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Mode",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Amount",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                            ],
                            rows: items
                                .map(
                                  (item) => DataRow(
                                    cells: [
                                      DataCell(
                                        Text(
                                          dateHelper.getFromattedDate(
                                              item.transactionDate),
                                          //item.transactionDate.substring(0, 10),
                                          textAlign: TextAlign.center,
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(
                                            item.receiptNumber.toString(),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(item.serviceName),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(item.paymentType),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text('\$ ${item.totalAmount}'),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                    ],
                                  ),
                                )
                                .toList()),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Check Number : ',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    '${items[0].approvalNumber}',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Check Date : ',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    '${items[0].paymentDetails.dateOnTheCheck}'
                                        .substring(0, 10),
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Bank Name : ',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    '${items[0].paymentDetails.bankName}'
                                            .isEmpty
                                        ? '${items[0].paymentDetails.bankName}'
                                        : 'N/A',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Text('Check return date',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey)),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      OutlineButton(
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              dateHelper.getFromattedDate(
                                                  selectedDate.toString()),
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(
                                                FontAwesomeIcons
                                                    .solidCalendarAlt,
                                                color: Colors.deepOrange,
                                                size: 14,
                                              ),
                                            )
                                          ],
                                        ),
                                        onPressed: () async {
                                          DateTime picked =
                                              await showDatePicker(
                                            context: context,
                                            initialDate: selectedDate,
                                            firstDate: DateTime(2000, 1),
                                            lastDate: DateTime.now(),
                                          );
                                          setState(() {
                                            selectedDate = picked;
                                          });
                                        },
                                      ),
                                    ],
                                  )),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: <Widget>[
                                    Text('Reason',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey)),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey
                                                    .withOpacity(0.5)),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        width: 300,
                                        height: 60,
                                        child: TextField(
                                          controller: reasonController,
                                          keyboardType: TextInputType.multiline,
                                          maxLines: 3,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.black,
                                          ),
                                          decoration: InputDecoration(
                                              errorText: _isReasonValid
                                                  ? 'Enter Reason'
                                                  : null,
                                              hintText: 'Reason for return',
                                              hintStyle:
                                                  TextStyle(fontSize: 12),
                                              border: InputBorder.none),
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Text('Amount to be paid',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey)),
                                  Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                        '\$ ${totalAmount.toStringAsFixed(2)}',
                                        style: TextStyle(
                                          fontSize: 26,
                                        )),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Visibility(
                                    visible: items[0].refundDetails == null
                                        ? true
                                        : false,
                                    child: RaisedButton(
                                      child: Text(
                                        'Mark check return',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      color: Theme.of(context).primaryColor,
                                      onPressed: () {
                                        if (reasonController.text.isEmpty) {
                                          setState(() {
                                            reasonController.text.isEmpty
                                                ? _isReasonValid = true
                                                : _isReasonValid = false;
                                          });
                                        } else {
                                          markCheckReturn(
                                              selectedDate,
                                              items[0].orderId,
                                              reasonController.text,
                                              '0');
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  RaisedButton(
                                    color: Theme.of(context).primaryColor,
                                    child: Text(
                                      'Mark check return and collect payment',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context, true);
                                      markCheckReturn(
                                          selectedDate,
                                          items[0].orderId,
                                          reasonController.text,
                                          '0');
                                      _showDialogRepay(items, false, 'Check');
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ));
            }));
      },
    );
  }

  markCheckReturn(DateTime selectedDate, String orderId, String reasonText,
      String bankCharges) async {
    String checkReturnDate = selectedDate.toString().substring(0, 10);

    var returnDetails = {};
    returnDetails["checkReturnDate"] = checkReturnDate;
    returnDetails["reason"] = reasonText;
    returnDetails["bankCharges"] = bankCharges;

    var checkReturnBody = {};
    checkReturnBody["orderId"] = orderId;
    checkReturnBody["returnDetails"] = returnDetails;

    String checkReturnJson = convert.jsonEncode(checkReturnBody);
    //print(checkReturnJson);

    NetworkHelper networkHelper = NetworkHelper();
    var jsonResponse =
        await networkHelper.markChcekReturn(checkReturnJson, widget.token);
    if (jsonResponse != null) {
      Scaffold.of(context).showSnackBar(new SnackBar(
        backgroundColor: Colors.green,
        content: new Text(
            "The selected transaction status has been successfully marked to check returned"),
      ));
      Navigator.of(context).pop();
    }

    getAllDonations();

    //print(jsonResponse);
  }

  refundAnOrderByCash(String orderId, String totalAmount, String reasonText,
      List<OrderItems> items) async {
    List<RefundOrderDetails> refundsList = [];
    items.forEach((items) {
      RefundOrderDetails refundDetails = RefundOrderDetails();
      refundDetails.templeServiceId = items.templeServiceId;
      refundDetails.amount = items.amount;
      refundsList.add(refundDetails);
    });

    RefundOrderByCash refundOrderByCash = RefundOrderByCash();
    refundOrderByCash.orderId = orderId;
    refundOrderByCash.refundPaymentType = 'Cash';
    refundOrderByCash.refundedOn = selectedDate.toString().substring(0, 10);
    refundOrderByCash.refundAmount = totalAmount;
    refundOrderByCash.reason = reasonText;
    refundOrderByCash.refundDetails = refundsList;
    String refundJson = convert.jsonEncode(refundOrderByCash);

    if (reasonText.isNotEmpty) {
      NetworkHelper networkHelper = new NetworkHelper();
      var jsonResponse =
          await networkHelper.orderRefund(refundJson, widget.token);

      Navigator.of(context).pop();

      getAllDonations();
      print(jsonResponse);

      Scaffold.of(context).showSnackBar(new SnackBar(
        backgroundColor: Colors.green,
        content: new Text("Refunded succesfully"),
      ));
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        backgroundColor: Colors.green,
        content: new Text("Please enter the reason"),
      ));
    }

    //print(refundJson);
  }

  refundAnOrderByCheck(
      String orderId,
      String totalAmount,
      String reasonText,
      List<OrderItems> items,
      String payeeName,
      String bankName,
      String checkNumber,
      String dateOnTheCheck) async {
    List<RefundOrderDetails> refundsList = [];
    items.forEach((items) {
      RefundOrderDetails refundDetails = RefundOrderDetails();
      refundDetails.templeServiceId = items.templeServiceId;
      refundDetails.amount = items.amount;
      refundsList.add(refundDetails);
    });

    RefundOrderByCheck refundOrderByCheck = RefundOrderByCheck();
    refundOrderByCheck.orderId = orderId;
    refundOrderByCheck.refundPaymentType = 'Check';
    //refundOrderByCheck.refundedOn = DateTime.now().toString().substring(0,10);
    refundOrderByCheck.refundedOn = selectedDate.toString().substring(0, 10);
    refundOrderByCheck.refundAmount = totalAmount;
    refundOrderByCheck.reason = reasonText;
    refundOrderByCheck.payeeName = payeeName;
    refundOrderByCheck.bankName = bankName;
    refundOrderByCheck.dateOnTheCheck = dateOnTheCheck;
    refundOrderByCheck.refundCheckNumber = checkNumber;
    refundOrderByCheck.refundDetails = refundsList;
    String refundJson = convert.jsonEncode(refundOrderByCheck);

    //print(refundJson);

    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse =
        await networkHelper.orderRefund(refundJson, widget.token);
    print(jsonResponse);

    Navigator.of(context).pop();

    getAllDonations();

    Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: Colors.green,
      content: new Text("Refunded succesfully"),
    ));
  }

  repayByCash(String orderId, String totalAmount, String reasonText,
      List<OrderItems> items) async {
    List<RepaymentDetails> repayList = [];
    items.forEach((items) {
      RepaymentDetails repaymentDetails = RepaymentDetails();
      repaymentDetails.templeServiceId = items.templeServiceId;
      repaymentDetails.totalAmount = items.amount;
      repayList.add(repaymentDetails);
    });

    RepaymentByCash repaymentByCash = RepaymentByCash();

    repaymentByCash.orderId = orderId;
    repaymentByCash.paymentType = 'Cash';
    //repaymentByCash.transactionDate = DateTime.now().toString().substring(0,10);
    repaymentByCash.transactionDate = selectedDate.toString().substring(0, 10);
    repaymentByCash.amount = totalAmount;
    repaymentByCash.reason = reasonText;
    repaymentByCash.notes = '';
    repaymentByCash.checkReturnedDate = '';
    repaymentByCash.bankcharges = '';
    repaymentByCash.repaymentDetails = repayList;
    String repayJson = convert.jsonEncode(repaymentByCash);
    //print(repayJson);

    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.orderRepay(repayJson, widget.token);
    print(jsonResponse);

    Navigator.of(context).pop();

    getAllDonations();

    Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: Colors.green,
      content: new Text("Repaid succesfully"),
    ));
  }

  repayByCheck(
      String orderId,
      String totalAmount,
      String reasonText,
      List<OrderItems> items,
      String payeeName,
      String bankName,
      String checkNumber,
      String dateOnTheCheck) async {
    List<RepaymentDetails> repayList = [];
    items.forEach((items) {
      RepaymentDetails repaymentDetails = RepaymentDetails();
      repaymentDetails.templeServiceId = items.templeServiceId;
      repaymentDetails.totalAmount = items.amount;
      repayList.add(repaymentDetails);
    });

    RepaymentByCheck repaymentByCheck = RepaymentByCheck();

    repaymentByCheck.orderId = orderId;
    repaymentByCheck.paymentType = 'Check';
    repaymentByCheck.transactionDate =
        DateTime.now().toString().substring(0, 10);
    repaymentByCheck.amount = totalAmount;
    repaymentByCheck.reason = items[0].refundDetails.reason;
    repaymentByCheck.notes = reasonText;
    repaymentByCheck.checkReturnedDate = items[0].refundDetails.checkReturnDate;
    repaymentByCheck.bankcharges = items[0].refundDetails.bankCharges;
    repaymentByCheck.payeeName = payeeName;
    repaymentByCheck.bankName = bankName;
    repaymentByCheck.approvalNumber = checkNumber;
    repaymentByCheck.dateOnTheCheck = dateOnTheCheck;
    repaymentByCheck.repaymentDetails = repayList;

    String repayJson = convert.jsonEncode(repaymentByCheck);
    //print(repayJson);

    var jsonResponse = await networkHelper.orderRepay(repayJson, widget.token);
    print(jsonResponse);

    Navigator.of(context).pop();

    getAllDonations();

    Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: Colors.green,
      content: new Text("Repaid succesfully"),
    ));
  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  getPrintReceiptDetails(
      String orderId, var receiptNumber, BuildContext context) async {
    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse =
        await networkHelper.getPrintReceiptDetails(widget.token, orderId);

    PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
    List<OrderItemDetails> orderDetails =
        printReceipt.receiptDetails.order.orderItemDetails;

    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
            //contentPadding: EdgeInsets.zero,
            titlePadding: EdgeInsets.zero,
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Receipt - $receiptNumber',
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.w500)),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: Container(
                constraints: BoxConstraints(maxWidth: 360),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Order Details",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 14),
                          ),
                          Image.network(
                            tenantLogo,
                            width: 60,
                            height: 60,
                            fit: BoxFit.fitHeight,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            tenantName,
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 16),
                          ),
                          Text(
                            tenantAddress,
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 12),
                          ),
                          /*  Text(
                                                                  '${printReceipt.receiptDetails.billTenantInfo.billTenantAddres.city}, ${printReceipt.receiptDetails.billTenantInfo.tenantState.name}',
                                                                  style:
                                                                      TextStyle(fontFamily: 'Roboto', fontSize: 12),
                                                                ),
                                                                Text(
                                                                  printReceipt.receiptDetails.billTenantInfo
                                                                      .tenantCountry.name,
                                                                  style:
                                                                      TextStyle(fontFamily: 'Roboto', fontSize: 12),
                                                                ),
                                                                Text(
                                                                  'Tel : ${tenantInfo.tenantDetails.additionalProperties.phoneNumber}',
                                                                  style:
                                                                      TextStyle(fontFamily: 'Roboto', fontSize: 12),
                                                                ),
                                                                Text(
                                                                  tenantInfo
                                                                      .tenantDetails.additionalProperties.websiteUrl,
                                                                  style:
                                                                      TextStyle(fontFamily: 'Roboto', fontSize: 12),
                                                                ) */
                        ],
                      ),
                    ),
                    SingleChildScrollView(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        DataTable(
                            //showCheckboxColumn: true,
                            //onSelectAll: (b) {},
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Item",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                                onSort: (i, b) {
                                  /* setState(() {
                                    items.sort((b, a) => a.transactionDate
                                        .compareTo(b.transactionDate));
                                  }); */
                                },
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Quantity",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Amount (\$)",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                            ],
                            rows: orderDetails
                                .map(
                                  (orderDetails) => DataRow(
                                    cells: [
                                      DataCell(
                                        Text(
                                          toBeginningOfSentenceCase(orderDetails
                                              .name
                                              .toString()
                                              .toLowerCase()),
                                          textAlign: TextAlign.center,
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(
                                            orderDetails.quantity.toString(),
                                          ),
                                        ),
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(orderDetails.totalAmount
                                              .toString()),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                    ],
                                  ),
                                )
                                .toList()),
                        SizedBox(
                          height: 20,
                        ),
                        /*  Text(printReceipt.receiptDetails.orders.user
                                                                      .nakshatraDetails[0].nakshatra +
                                                                  printReceipt.receiptDetails.orders.user
                                                                  
                                                                      .nakshatraDetails[0].name), */

                        /*  getDevoteeNakshatraInfo(printReceipt
                                                                  .receiptDetails.orders.user.nakshatraDetails), */
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Text(
                                ' Payment Type : ${printReceipt.receiptDetails.orderInfo.dRRPaymentDetails.paymentType}',
                                style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                                textAlign: TextAlign.start,
                              ),
                            )),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Text(
                                ' Total Amount : \$ ${printReceipt.receiptDetails.orderInfo.receiptTotalAmount}',
                                style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                                textAlign: TextAlign.start,
                              ),
                            ))
                      ],
                    ))
                  ],
                ))));
  }

  /* getDevoteeNakshatraInfo(List<NakshatraDetails> nakshatraDetails) {
    //List<Widget> nakshatraWidget;

    nakshatraDetails.forEach((user) {});
  } */
}

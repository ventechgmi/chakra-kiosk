import 'dart:async';
import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:chakra/utils/ReceiptTemplate.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/models/ReturnRefunds/Donations.dart';
import 'package:chakra/models/ReturnRefunds/OrderDetails.dart';
import 'package:chakra/models/ReturnRefunds/RefundOrderByCash.dart';
import 'package:chakra/models/ReturnRefunds/RefundOrderByCheck.dart';
import 'package:chakra/models/ReturnRefunds/RepaymentByCash.dart';
import 'package:chakra/models/ReturnRefunds/RepaymentByCheck.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'package:chakra/utils/DateHelper.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;

import '../widgets/common_components.dart';
import '../models/ReturnRefunds/OrderDetails.dart';
import '../utils/DateHelper.dart';
import '../utils/NetworkHelper.dart';

/*Future<List<Result>> fetchResults(http.Client client) async {
  final response = await client.get('https://api.myjson.com/bins/j5xau');

  // Use the compute function to run parseResults in a separate isolate
  return compute(parseResults, response.body);
}*/

Future<List<DonationItems>> fetchResults(String token) async {
  // final response = await client.get('https://api.myjson.com/bins/j5xau');

  NetworkHelper networkHelper = NetworkHelper();
  var response = await networkHelper.getAllDonations(token);

  // Use the compute function to run parseResults in a separate isolate
  return compute(parseResults, response);
}

// A function that will convert a response body into a List<Result>
/*List<Result> parseResults(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Result>((json) => Result.fromJson(json)).toList();
}*/

List<DonationItems> parseResults(var response) {
  // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  Donations donations = Donations.fromJson(response);

  //return parsed.map<Result>((json) => DonationItems.fromJson(json)).toList();

  return donations.donationItems;
}

class Result {
  final String sex;
  final String region;
  final int year;
  final String statistic;
  final String value;

  Result({this.sex, this.region, this.year, this.statistic, this.value});

  bool selected = false;

  factory Result.fromJson(Map<String, dynamic> json) {
    return Result(
      sex: json['sex'] as String,
      region: json['region'] as String,
      year: json['year'] as int,
      statistic: json['statistic'] as String,
      value: json['value'] as String,
    );
  }
}

class ResultsDataSource extends DataTableSource {
  final List<DonationItems> _results;
  final BuildContext context;
  final String token;
  final StateSetter setStateforData;
  int actionPrevilege;
  final bool isSignatureCopy;
  final String minAmntReceipt;
  tenant.TenantInfo tenantInfo;
  ResultsDataSource(this._results,
      {this.context,
      this.token,
      this.setStateforData,
      this.tenantInfo,
      this.actionPrevilege,
      this.isSignatureCopy,
      this.minAmntReceipt});

  void _sort<T>(Comparable<T> getField(DonationItems d), bool ascending) {
    _results.sort((DonationItems a, DonationItems b) {
      if (!ascending) {
        final DonationItems c = a;
        a = b;
        b = c;
      }
      final Comparable<T> aValue = getField(a);
      final Comparable<T> bValue = getField(b);
      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  int _selectedCount = 0;
  DateFormat dateFormat = DateFormat("MM-dd-yyyy");

  DateTime selectedDate = DateTime.now();
  DateTime newCheckDate = DateTime.now();
  List<OrderItems> selectedItems = [];

  var dateHelper = new DateHelper();

  var commonComponents = new CommonComponents();

  NetworkHelper networkHelper = new NetworkHelper();

  String tenantName;
  String tenantAddress;
  String tenantLogo;
  String tenantWeb;
  String tenantPhone;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _results.length) return null;
    final DonationItems result = _results[index];
    print(result.totalAmount);
    String amt;
    if (result.totalAmount.contains(".")) {
      amt = result.totalAmount;
    } else {
      amt = int.parse(result.totalAmount).toStringAsFixed(2).toString();
    }

    return DataRow.byIndex(index: index,
        /* selected: result.selected,
        onSelectChanged: (bool value) {
          if (result.selected != value) {
            _selectedCount += value ? 1 : -1;
            assert(_selectedCount >= 0);
            result.selected = value;
            notifyListeners();
          }
        }, */
        cells: <DataCell>[
          DataCell(
            Container(
                alignment: Alignment.centerLeft,
                width: 100,
                //color: Colors.yellow,
                child: Text(
                  '${result.transactionDate}',
                  textAlign: TextAlign.center,
                )),
            showEditIcon: false,
            placeholder: false,
          ),
          DataCell(
            Container(
              width: 100,
              //color: Colors.yellow,
              child: FlatButton(
                child: Text(
                  result.receiptNumber.toString(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                  ),
                ),
                onPressed: () {
                  getPrintReceiptDetails(
                      result.orderId, result.receiptNumber.toString(), context);
                },
              ),
            ),
            showEditIcon: false,
            placeholder: false,
          ),
          DataCell(Container(
            alignment: Alignment.centerLeft,
            //color: Colors.yellow,
            width: 200,
            child: Text('${result.firstName}  ${result.lastName}',
                textAlign: TextAlign.left, softWrap: true),
          )),
          DataCell(
            Container(
              //color: Colors.yellow,
              alignment: Alignment.centerLeft,
              width: 200,
              child: AutoSizeText(
                '${result.serviceName}',
                maxLines: 3,
                textAlign: TextAlign.left,
              ),
            ),
          ),
          DataCell(Container(
              //color: Colors.yellow,
              width: 100,
              alignment: Alignment.centerLeft,
              child: Text(
                '${result.paymentType}',
              ))),
          DataCell(Container(
              width: 80,
              //color: Colors.yellow,
              alignment: Alignment.centerLeft,
              child: Text(amt))),
          actionPrevilege == 2
              ? DataCell(Container(
                  alignment: Alignment.centerLeft,
                  //color: Colors.yellow,
                  width: 120,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      result.paymentType == "Check" &&
                              (result.type == '' || result.type == 'Repayment')
                          ? InkWell(
                              //textColor: Colors.deepOrange,
                              child: (result.type == '' ||
                                      result.type == 'Repayment')
                                  ? Padding(
                                      padding: EdgeInsets.all(5),
                                      child: Text(
                                        'Return',
                                        style: TextStyle(
                                            color: Colors.deepOrange,
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold,
                                            decoration:
                                                TextDecoration.underline),
                                      ))
                                  : Text(''),
                              onTap: () {
                                if (result.type != 'Returned') {
                                  RefundDetails refundDetails =
                                      result.refundDetails;
                                  getOrderDetails(
                                      result.orderId,
                                      true,
                                      true,
                                      result.paymentType,
                                      context,
                                      token,
                                      refundDetails != null
                                          ? refundDetails.reason
                                          : null);
                                }
                              },
                            )
                          : SizedBox(
                              height: 0,
                              width: 0,
                            ),
                      Visibility(
                        visible:
                            (result.type == '' || result.type == 'Repayment')
                                ? true
                                : false,
                        child: InkWell(
                          child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Text(
                                'Refund',
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              )),
                          onTap: () {
                            RefundDetails refundDetails = result.refundDetails;
                            if (result.paymentType == 'Credit/Debit Card') {
                              getOrderDetails(
                                  result.orderId,
                                  false,
                                  false,
                                  'Cash',
                                  context,
                                  token,
                                  refundDetails != null
                                      ? refundDetails.reason
                                      : null);
                            } else {
                              getOrderDetails(
                                  result.orderId,
                                  false,
                                  false,
                                  result.paymentType,
                                  context,
                                  token,
                                  refundDetails != null
                                      ? refundDetails.reason
                                      : null);
                            }
                          },
                        ),
                      ),
                      Visibility(
                        visible: result.type == 'Refunded' ? true : false,
                        child: InkWell(
                          child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Text(
                                'Refunded',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              )),
                          onTap: () {
                            /*  getOrderDetails(
                                                                                                                item.orderId, false, false, item.paymentType); */
                          },
                        ),
                      ),
                      Visibility(
                        visible: result.type == 'Returned' ? true : false,
                        child: InkWell(
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                'Repay',
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              )),
                          onTap: () {
                            getOrderDetails(
                                result.orderId,
                                false,
                                true,
                                result.paymentType,
                                context,
                                token,
                                result.refundDetails.reason);
                          },
                        ),
                      )
                    ],
                  )))
              : DataCell(Container(width: 100, child: Text('')))
        ]);
  }

  @override
  int get rowCount => _results.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  void _selectAll(bool checked) {
    for (DonationItems result in _results) result.selected = checked;
    _selectedCount = checked ? _results.length : 0;
    //notifyListeners();
  }

  Future<void> getOrderDetails(
      String orderId,
      bool isShowDialog,
      bool isCheckReturned,
      String paymentType,
      BuildContext context,
      String token,
      String reason) async {
    NetworkHelper networkHelper = new NetworkHelper();
    var response = await networkHelper.getOrderDetails(token, orderId);

    OrderDetails orderDetails = OrderDetails.fromJson(response);
    List<OrderItems> orderItems = orderDetails.orderDetails;
    //orderItems.forEach((f) {});

    if (isShowDialog) {
      _showDialogScrap(orderItems, context);
    } else {
      if (isCheckReturned) {
        _showDialogRepay(orderItems, false, paymentType, context, reason);
      } else {
        _showDialogRepay(orderItems, true, paymentType, context, reason);
      }
    }
  }

  void _showDialog(List<OrderItems> items, BuildContext context) {
    TextEditingController reasonController = TextEditingController();
    double totalAmount = 0.00;
    bool _isReasonValid = false;

    FocusNode textSecondFocusNode = new FocusNode();

    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                width: 300,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      toBeginningOfSentenceCase(
                          '${items[0].firstName} ${items[0].lastName}'),
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    DataTable(
                        onSelectAll: (b) {},
                        sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Date",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Receipt Num",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Service Name",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Mode",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Amount (\$)",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                        ],
                        rows: items
                            .map((item) => DataRow(
                                  cells: [
                                    DataCell(
                                      Text(
                                        dateHelper.getFromattedDate(
                                            item.transactionDate),
                                        //item.transactionDate.substring(0, 10),
                                        textAlign: TextAlign.center,
                                      ),
                                      showEditIcon: false,
                                      placeholder: false,
                                    ),
                                    DataCell(
                                      Center(
                                        child: Text(
                                          item.receiptNumber.toString(),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      showEditIcon: false,
                                      placeholder: false,
                                    ),
                                    DataCell(
                                      Container(
                                        width: 200,
                                        child: Center(
                                          child: AutoSizeText(
                                            '${item.serviceName}',
                                            maxLines: 3,
                                          ),
                                        ),
                                      ),
                                      showEditIcon: false,
                                      placeholder: false,
                                    ),
                                    DataCell(
                                      Center(
                                        child: Text(item.paymentType),
                                      ),
                                      showEditIcon: false,
                                      placeholder: false,
                                    ),
                                    DataCell(
                                      Center(
                                        child: Text('${item.totalAmount}'),
                                      ),
                                      showEditIcon: false,
                                      placeholder: false,
                                    )
                                  ],
                                ))
                            .toList()),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 700),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 2,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Check Number : ',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey),
                                      ),
                                      Text(
                                        '${items[0].approvalNumber}',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 50,
                                      ),
                                      Text('Amount to be paid',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey)),
                                      Text(
                                          '\$ ${totalAmount.toStringAsFixed(2)}',
                                          style: TextStyle(
                                            fontSize: 12,
                                          )),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [],
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: 20,
                            width: 5,
                          ),
                          Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Check Date : ',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey),
                                      ),
                                      Text(
                                        '${items[0].paymentDetails.dateOnTheCheck}'
                                            .substring(0, 10),
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 40,
                                      ),
                                      Text('Check return date',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey)),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      OutlineButton(
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              dateHelper.getFromattedDate(
                                                  selectedDate.toString()),
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(
                                                FontAwesomeIcons
                                                    .solidCalendarAlt,
                                                color: Colors.deepOrange,
                                                size: 14,
                                              ),
                                            )
                                          ],
                                        ),
                                        onPressed: () async {
                                          DateTime picked =
                                              await showDatePicker(
                                            context: context,
                                            initialDate: selectedDate,
                                            firstDate: DateTime(2000, 1),
                                            lastDate: DateTime.now(),
                                          );
                                          setState(() {
                                            if (picked != null) {
                                              selectedDate = picked;
                                            } else {
                                              selectedDate = DateTime.now();
                                            }
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[],
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: 20,
                            width: 10,
                          ),
                          Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Bank Name : ',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey),
                                      ),
                                      Text(
                                        '${items[0].paymentDetails.bankName}'
                                                .isEmpty
                                            ? '${items[0].paymentDetails.bankName}'
                                            : 'N/A',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 40,
                                      ),
                                      Text('Reason',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey)),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                          padding: EdgeInsets.all(2),
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.grey
                                                      .withOpacity(0.5)),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          width: 180,
                                          height: 60,
                                          child: TextField(
                                            controller: reasonController,
                                            keyboardType:
                                                TextInputType.multiline,
                                            focusNode: textSecondFocusNode,
                                            maxLines: 3,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.black,
                                            ),
                                            onChanged: (text) {
                                              if (text.length > 0) {
                                                setState(() {
                                                  _isReasonValid = false;
                                                });
                                              }
                                            },
                                            decoration: InputDecoration(
                                                errorText: _isReasonValid
                                                    ? 'Reason is required.'
                                                    : null,
                                                errorStyle:
                                                    TextStyle(fontSize: 10),
                                                hintText: 'Reason for return',
                                                hintStyle:
                                                    TextStyle(fontSize: 12),
                                                border: InputBorder.none),
                                          ))
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[],
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Visibility(
                              visible:
                                  items[0].refundDetails == null ? true : true,
                              child: RaisedButton(
                                child: Text(
                                  'Mark check return',
                                  style: TextStyle(color: Colors.white),
                                ),
                                color: Theme.of(context).primaryColor,
                                onPressed: () {
                                  FocusScope.of(context)
                                      .requestFocus(textSecondFocusNode);

                                  if (reasonController.text.isEmpty) {
                                    setState(() {
                                      reasonController.text.isEmpty
                                          ? _isReasonValid = true
                                          : _isReasonValid = false;
                                    });
                                  } else {
                                    Navigator.pop(context, true);
                                    setState(() {
                                      markCheckReturn(
                                          selectedDate,
                                          items[0].orderId,
                                          reasonController.text,
                                          '0',
                                          items,
                                          false);
                                    });
                                  }
                                },
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            RaisedButton(
                              color: Theme.of(context).primaryColor,
                              child: Text(
                                'Mark check return and collect payment',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12),
                              ),
                              onPressed: () {
                                FocusScope.of(context)
                                    .requestFocus(textSecondFocusNode);

                                if (reasonController.text.isEmpty) {
                                  setState(() {
                                    reasonController.text.isEmpty
                                        ? _isReasonValid = true
                                        : _isReasonValid = false;
                                  });
                                } else {
                                  Navigator.pop(context, true);
                                  setState(() {
                                    markCheckReturn(
                                      selectedDate,
                                      items[0].orderId,
                                      reasonController.text,
                                      '0',
                                      items,
                                      true,
                                    );
                                  });
                                }
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              );
            }));
      },
    );
  }

  void _showDialogNew(List<OrderItems> items, BuildContext context) {
    TextEditingController reasonController = TextEditingController();
    double totalAmount = 0.00;
    bool _isReasonValid = false;

    var focusNode = new FocusNode();

    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      toBeginningOfSentenceCase(
                          '${items[0].firstName} ${items[0].lastName}'),
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Padding(
                  padding: EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        DataTable(
                            onSelectAll: (b) {},
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Date",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Receipt Num",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Service Name",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Mode",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Amount (\$)",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                            ],
                            rows: items
                                .map((item) => DataRow(
                                      cells: [
                                        DataCell(
                                          Text(
                                            dateHelper.getFromattedDate(
                                                item.transactionDate),
                                            //item.transactionDate.substring(0, 10),
                                            textAlign: TextAlign.center,
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text(
                                              item.receiptNumber.toString(),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text(item.serviceName),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text(item.paymentType),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text('${item.totalAmount}'),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        )
                                      ],
                                    ))
                                .toList()),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Check Number : ',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                    Text(
                                      '${items[0].approvalNumber}',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                Row(
                                  children: [
                                    Text('Amount to be paid',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey)),
                                    Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Text(
                                          '\$ ${totalAmount.toStringAsFixed(2)}',
                                          style: TextStyle(
                                            fontSize: 12,
                                          )),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Check Date : ',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                    Text(
                                      '${items[0].paymentDetails.dateOnTheCheck}'
                                          .substring(0, 10),
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text('Check return date',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey)),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    OutlineButton(
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            dateHelper.getFromattedDate(
                                                selectedDate.toString()),
                                            style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(5),
                                            child: Icon(
                                              FontAwesomeIcons.solidCalendarAlt,
                                              color: Colors.deepOrange,
                                              size: 14,
                                            ),
                                          )
                                        ],
                                      ),
                                      onPressed: () async {
                                        DateTime picked = await showDatePicker(
                                          context: context,
                                          initialDate: selectedDate,
                                          firstDate: DateTime(2000, 1),
                                          lastDate: DateTime.now(),
                                        );
                                        setState(() {
                                          if (picked != null) {
                                            selectedDate = picked;
                                          } else {
                                            selectedDate = DateTime.now();
                                          }
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 35,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Bank Name : ',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                    Text(
                                      '${items[0].paymentDetails.bankName}'
                                              .isEmpty
                                          ? '${items[0].paymentDetails.bankName}'
                                          : 'N/A',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text('Reason',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey)),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      children: [
                                        Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.grey
                                                        .withOpacity(0.5)),
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            width: 280,
                                            height: 60,
                                            child: TextField(
                                              controller: reasonController,
                                              focusNode: focusNode,
                                              keyboardType:
                                                  TextInputType.multiline,
                                              maxLines: 3,
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black,
                                              ),
                                              onChanged: (text) {
                                                if (text.length > 0) {
                                                  setState(() {
                                                    _isReasonValid = false;
                                                  });
                                                }
                                              },
                                              decoration: InputDecoration(
                                                  /* errorText: _isReasonValid
                                                  ? 'Enter Reason'
                                                  : null,*/
                                                  errorStyle:
                                                      TextStyle(fontSize: 10),
                                                  hintText: 'Reason for return',
                                                  hintStyle:
                                                      TextStyle(fontSize: 12),
                                                  border: InputBorder.none),
                                            )),
                                        Container(
                                          child: Padding(
                                              padding: EdgeInsets.all(10),
                                              child: Text(
                                                _isReasonValid
                                                    ? 'Reason is required.'
                                                    : '',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.red),
                                              )),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Visibility(
                                  visible: items[0].refundDetails == null
                                      ? true
                                      : true,
                                  child: RaisedButton(
                                    child: Text(
                                      'Mark check return',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Theme.of(context).primaryColor,
                                    onPressed: () {
                                      if (reasonController.text.isEmpty) {
                                        setState(() {
                                          focusNode.requestFocus();

                                          reasonController.text.isEmpty
                                              ? _isReasonValid = true
                                              : _isReasonValid = false;
                                        });
                                      } else {
                                        Navigator.pop(context, true);
                                        setState(() {
                                          markCheckReturn(
                                              selectedDate,
                                              items[0].orderId,
                                              reasonController.text,
                                              '0',
                                              items,
                                              false);
                                        });
                                      }
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                RaisedButton(
                                  color: Theme.of(context).primaryColor,
                                  child: Text(
                                    'Mark check return and collect payment',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12),
                                  ),
                                  onPressed: () {
                                    if (reasonController.text.isEmpty) {
                                      focusNode.requestFocus();

                                      setState(() {
                                        reasonController.text.isEmpty
                                            ? _isReasonValid = true
                                            : _isReasonValid = false;
                                      });
                                    } else {
                                      Navigator.pop(context, true);
                                      setState(() {
                                        markCheckReturn(
                                          selectedDate,
                                          items[0].orderId,
                                          reasonController.text,
                                          '0',
                                          items,
                                          true,
                                        );
                                      });
                                    }
                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ));
            }));
      },
    );
  }

  void _showDialogScrap(List<OrderItems> items, BuildContext context) {
    TextEditingController reasonController = TextEditingController();
    double totalAmount = 0.00;
    bool _isReasonValid = false;

    var focusNode = new FocusNode();

    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      toBeginningOfSentenceCase(
                          '${items[0].firstName} ${items[0].lastName}'),
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Container(
                  constraints: BoxConstraints(maxWidth: 700),
                  child: Padding(
                      padding: EdgeInsets.all(10),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            DataTable(
                                onSelectAll: (b) {},
                                sortAscending: true,
                                columns: <DataColumn>[
                                  DataColumn(
                                    label: Expanded(
                                      child: Center(
                                          child: Text("Date",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.deepOrange,
                                                  fontSize: 12),
                                              softWrap: true)),
                                    ),
                                    numeric: false,
                                  ),
                                  DataColumn(
                                    label: Expanded(
                                      child: Center(
                                          child: Text("Receipt Num",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.deepOrange,
                                                  fontSize: 12),
                                              softWrap: true)),
                                    ),
                                    numeric: false,
                                  ),
                                  DataColumn(
                                    label: Expanded(
                                      child: Center(
                                          child: Text("Service Name",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.deepOrange,
                                                  fontSize: 12),
                                              softWrap: true)),
                                    ),
                                    numeric: false,
                                  ),
                                  DataColumn(
                                    label: Expanded(
                                      child: Center(
                                          child: Text("Mode",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.deepOrange,
                                                  fontSize: 12),
                                              softWrap: true)),
                                    ),
                                    numeric: false,
                                  ),
                                  DataColumn(
                                    label: Expanded(
                                      child: Center(
                                          child: Text("Amount (\$)",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.deepOrange,
                                                  fontSize: 12),
                                              softWrap: true)),
                                    ),
                                    numeric: false,
                                  ),
                                ],
                                rows: items
                                    .map((item) => DataRow(
                                          cells: [
                                            DataCell(
                                              Text(
                                                dateHelper.getFromattedDate(
                                                    item.transactionDate),
                                                //item.transactionDate.substring(0, 10),
                                                textAlign: TextAlign.center,
                                              ),
                                              showEditIcon: false,
                                              placeholder: false,
                                            ),
                                            DataCell(
                                              Center(
                                                child: Text(
                                                  item.receiptNumber.toString(),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                              showEditIcon: false,
                                              placeholder: false,
                                            ),
                                            DataCell(
                                              Center(
                                                child: Text(item.serviceName),
                                              ),
                                              showEditIcon: false,
                                              placeholder: false,
                                            ),
                                            DataCell(
                                              Center(
                                                child: Text(item.paymentType),
                                              ),
                                              showEditIcon: false,
                                              placeholder: false,
                                            ),
                                            DataCell(
                                              Center(
                                                child:
                                                    Text('${item.totalAmount}'),
                                              ),
                                              showEditIcon: false,
                                              placeholder: false,
                                            )
                                          ],
                                        ))
                                    .toList()),
                            SizedBox(
                              height: 20,
                            ),
                            Column(children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Check Number : ',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            '${items[0].approvalNumber}',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          )
                                        ],
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Check Date : ',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            '${items[0].paymentDetails.dateOnTheCheck}'
                                                .substring(0, 10),
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Bank Name : ',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            '${items[0].paymentDetails.bankName}'
                                                    .isEmpty
                                                ? '${items[0].paymentDetails.bankName}'
                                                : 'N/A',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                                width: 30,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('Amount to be paid',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.grey)),
                                          Text(
                                              '\$ ${totalAmount.toStringAsFixed(2)}',
                                              style: TextStyle(
                                                fontSize: 12,
                                              )),
                                        ],
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('Check return date',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.grey)),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Container(
                                              width: 150,
                                              child: OutlineButton(
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(
                                                      dateHelper
                                                          .getFromattedDate(
                                                              selectedDate
                                                                  .toString()),
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.all(5),
                                                      child: Icon(
                                                        FontAwesomeIcons
                                                            .solidCalendarAlt,
                                                        color:
                                                            Colors.deepOrange,
                                                        size: 14,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                onPressed: () async {
                                                  DateTime picked =
                                                      await showDatePicker(
                                                    context: context,
                                                    initialDate: selectedDate,
                                                    firstDate:
                                                        DateTime(2000, 1),
                                                    lastDate: DateTime.now(),
                                                  );
                                                  setState(() {
                                                    if (picked != null) {
                                                      selectedDate = picked;
                                                    } else {
                                                      selectedDate =
                                                          DateTime.now();
                                                    }
                                                  });
                                                },
                                              )),
                                        ],
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('Reason',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.grey)),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Column(
                                            children: [
                                              Container(
                                                  padding: EdgeInsets.all(5),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Colors.grey
                                                              .withOpacity(
                                                                  0.5)),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  width: 280,
                                                  height: 60,
                                                  child: TextField(
                                                    controller:
                                                        reasonController,
                                                    focusNode: focusNode,
                                                    keyboardType:
                                                        TextInputType.multiline,
                                                    maxLines: 3,
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black,
                                                    ),
                                                    onChanged: (text) {
                                                      if (text.length > 0) {
                                                        setState(() {
                                                          _isReasonValid =
                                                              false;
                                                        });
                                                      }
                                                    },
                                                    decoration: InputDecoration(
                                                        /* errorText: _isReasonValid
                                                  ? 'Enter Reason'
                                                  : null,*/
                                                        errorStyle: TextStyle(
                                                            fontSize: 10),
                                                        hintText:
                                                            'Reason for return',
                                                        hintStyle: TextStyle(
                                                            fontSize: 12),
                                                        border:
                                                            InputBorder.none),
                                                  )),
                                              Container(
                                                child: Padding(
                                                    padding: EdgeInsets.all(10),
                                                    child: Text(
                                                      _isReasonValid
                                                          ? 'Reason is required.'
                                                          : '',
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          color: Colors.red),
                                                    )),
                                              )
                                            ],
                                          ),
                                        ],
                                      )),
                                ],
                              )
                            ]),
                            SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Visibility(
                                      visible: items[0].refundDetails == null
                                          ? true
                                          : true,
                                      child: RaisedButton(
                                        child: Text(
                                          'Mark check return',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        color: Theme.of(context).primaryColor,
                                        onPressed: () {
                                          if (reasonController.text.isEmpty) {
                                            setState(() {
                                              focusNode.requestFocus();

                                              reasonController.text.isEmpty
                                                  ? _isReasonValid = true
                                                  : _isReasonValid = false;
                                            });
                                          } else {
                                            Navigator.pop(context, true);
                                            setState(() {
                                              markCheckReturn(
                                                  selectedDate,
                                                  items[0].orderId,
                                                  reasonController.text,
                                                  '0',
                                                  items,
                                                  false);
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    RaisedButton(
                                      color: Theme.of(context).primaryColor,
                                      child: Text(
                                        'Mark check return and collect payment',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                      onPressed: () {
                                        if (reasonController.text.isEmpty) {
                                          focusNode.requestFocus();

                                          setState(() {
                                            reasonController.text.isEmpty
                                                ? _isReasonValid = true
                                                : _isReasonValid = false;
                                          });
                                        } else {
                                          Navigator.pop(context, true);
                                          setState(() {
                                            markCheckReturn(
                                              selectedDate,
                                              items[0].orderId,
                                              reasonController.text,
                                              '0',
                                              items,
                                              true,
                                            );
                                          });
                                        }
                                      },
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                      )));
            }));
      },
    );
  }

  _showDialogRepay(List<OrderItems> items, bool isRefund, String paymentType,
      BuildContext context, String reasonValue) {
    String dropdownValue = paymentType;
    TextEditingController reason = TextEditingController();
    TextEditingController payeeName = TextEditingController();
    TextEditingController bankName = TextEditingController();
    //TextEditingController dateOnTheCehck = TextEditingController();
    TextEditingController refundCheckNumber = TextEditingController();
    TextEditingController bankCharges = TextEditingController();
    bool _validPayeeName = false;
    //bool _validBankName = false;
    bool _validCheckNumber = false;
    bool _isValidReason = false;

    double totalAmount = 0.00;
    double refunsAmt = 0.00;
    bankCharges.text = '0.00';
    // if (!isRefund) {
    print('isRefund' + isRefund.toString());
    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });
    // }

    try {
      if (reasonValue != null && reasonValue != '' && isRefund == false) {
        reason.text = reasonValue;
      } else {
        reason.text = '';
      }
    } catch (e) {
      reason.text = '';
    }

    print('totalAmount $totalAmount');

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      toBeginningOfSentenceCase(
                              '${items[0].firstName} ${items[0].lastName}') +
                          ' - ' +
                          items[0].receiptNumber.toString(),
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                  child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    DataTable(
                        showCheckboxColumn: isRefund ? true : false,
                        //sortColumnIndex: 0,
                        //sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Transaction Date",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Receipt No.",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Service Name",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Mode",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                            /*  onSort: (i, b) {
                                                                                                                              print("$i $b");
                                                                                                                              setState(() {
                                                                                                                                names.sort((a, b) => a.lastName.compareTo(b.lastName));
                                                                                                                              });
                                                                                                                            }, */
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Amount (\$)",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                        ],
                        rows: items
                            .map(
                              (item) => DataRow(
                                selected: selectedItems.contains(item),
                                onSelectChanged: (b) {
                                  refunsAmt = 0;
                                  setState(() {
                                    onSelectedRow(b, item);

                                    selectedItems.forEach((item) {
                                      refunsAmt = refunsAmt +
                                          double.parse(item.totalAmount);
                                    });
                                  });
                                },
                                cells: [
                                  DataCell(
                                    Center(
                                      child: Text(
                                        dateHelper.getFromattedDate(
                                            item.transactionDate),
                                        //item.transactionDate.substring(0, 10),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.receiptNumber.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Container(
                                      width: 200,
                                      child: Center(
                                        child: AutoSizeText(
                                          '${item.serviceName}',
                                          maxLines: 3,
                                        ),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.paymentType,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        '${item.totalAmount}',
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                ],
                              ),
                            )
                            .toList()),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 600),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Refund Payment Type',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: DropdownButton<String>(
                                            isExpanded: true,
                                            value: dropdownValue,
                                            icon: Icon(Icons.arrow_drop_down),
                                            iconSize: 22,
                                            elevation: 16,
                                            hint: Text(
                                              'Select payment type',
                                              style: TextStyle(fontSize: 12),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                dropdownValue = newValue;
                                              });
                                            },
                                            items: <String>['Check', 'Cash']
                                                .map<DropdownMenuItem<String>>(
                                                    (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(
                                                  value,
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              );
                                            }).toList(),
                                          ))
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: (isRefund &&
                                                items[0].refundDetails == null)
                                            ? Text('Refunded On',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey))
                                            : Text('Transaction Date',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: OutlineButton(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                dateHelper.getFromattedDate(
                                                    selectedDate.toString()),
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(5),
                                                child: Icon(
                                                  FontAwesomeIcons
                                                      .solidCalendarAlt,
                                                  color: Colors.deepOrange,
                                                  size: 14,
                                                ),
                                              )
                                            ],
                                          ),
                                          onPressed: () async {
                                            DateTime picked =
                                                await showDatePicker(
                                              context: context,
                                              initialDate: selectedDate,
                                              firstDate: DateTime(2000, 1),
                                              lastDate: DateTime.now(),
                                            );
                                            setState(() {
                                              if (picked != null) {
                                                selectedDate = picked;
                                              } else {
                                                selectedDate = DateTime.now();
                                              }
                                            });
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Refund Amount (\$)',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          //items[0].totalAmount,
                                          '\$ ${refunsAmt.toStringAsFixed(2)}',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Reason *',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: TextField(
                                          controller: reason,
                                          enabled: isRefund ? true : false,
                                          style: TextStyle(fontSize: 12),
                                          keyboardType: TextInputType.multiline,
                                          onChanged: (text) {
                                            if (text.length > 0) {
                                              setState(() {
                                                _isValidReason = false;
                                              });
                                            }
                                          },
                                          decoration: InputDecoration(
                                            errorText: _isValidReason
                                                ? 'Reason is required'
                                                : null,
                                            hintStyle: TextStyle(fontSize: 12),
                                            hintText: 'Reason',
                                          ),
                                        ),
                                      )
                                      //)
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible: (isRefund &&
                                          items[0].refundDetails == null)
                                      ? false
                                      : true,
                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: Text('Bank charges (\$)',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.grey)),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                            flex: 2,
                                            child: Container(
                                              child: TextField(
                                                controller: bankCharges,
                                                decoration: InputDecoration(
                                                    hintText: '\$ 0.00',
                                                    border: UnderlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        borderSide: BorderSide(
                                                            color:
                                                                Colors.black))),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Total Amount',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              /* decoration: BoxDecoration(
                                                                                                                                                                                                                          borderRadius:
                                                                                                                                                                                                                              BorderRadius.circular(5),
                                                                                                                                                                                                                          border: Border.all(
                                                                                                                                                                                                                              color: Colors.grey)), */
                                              child: Text(
                                                  '\$ ${(totalAmount + double.parse(bankCharges.text)).toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.w500))))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 40,
                          ),
                          Expanded(
                              child: Column(
                            children: <Widget>[
                              Visibility(
                                visible:
                                    dropdownValue == "Check" ? true : false,
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Payee Name*',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: TextField(
                                                  onChanged: (text) {
                                                    if (text.length > 0) {
                                                      setState(() {
                                                        _validPayeeName = false;
                                                      });
                                                    }
                                                  },
                                                  inputFormatters: [
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-z A-Z]")),
                                                    new LengthLimitingTextInputFormatter(
                                                        100),
                                                  ],
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                  controller: payeeName,
                                                  decoration: InputDecoration(
                                                      errorText: _validPayeeName
                                                          ? 'Payee Name is required'
                                                          : null,
                                                      errorStyle: TextStyle(
                                                          fontSize: 10),
                                                      hintText: 'Payee Name',
                                                      hintStyle: TextStyle(
                                                          fontSize: 12),
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Check Number *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                /*  decoration: BoxDecoration(
                                                                                                                                                                                                                              borderRadius:
                                                                                                                                                                                                                                  BorderRadius.circular(
                                                                                                                                                                                                                                      5),
                                                                                                                                                                                                                              border: Border.all(
                                                                                                                                                                                                                                  color: Colors.grey)), */
                                                child: TextField(
                                                  onChanged: (text) {
                                                    if (text.length > 0) {
                                                      setState(() {
                                                        _validCheckNumber =
                                                            false;
                                                      });
                                                    }
                                                  },
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        10),
                                                    WhitelistingTextInputFormatter
                                                        .digitsOnly,
                                                  ],
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                  controller: refundCheckNumber,
                                                  decoration: InputDecoration(
                                                      errorText: _validCheckNumber
                                                          ? 'Check Number is required'
                                                          : null,
                                                      errorStyle: TextStyle(
                                                          fontSize: 10),
                                                      hintText: 'Check Number',
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Check Date *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: OutlineButton(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    dateHelper.getFromattedDate(
                                                        newCheckDate
                                                            .toString()),
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.all(5),
                                                    child: Icon(
                                                      FontAwesomeIcons
                                                          .solidCalendarAlt,
                                                      color: Colors.deepOrange,
                                                      size: 14,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              onPressed: () async {
                                                DateTime picked =
                                                    await showDatePicker(
                                                  context: context,
                                                  initialDate: newCheckDate,
                                                  firstDate: DateTime.now(),
                                                  lastDate: DateTime(2100, 12),
                                                );
                                                setState(() {
                                                  if (picked != null) {
                                                    newCheckDate = picked;
                                                  } else {
                                                    newCheckDate =
                                                        DateTime.now();
                                                  }
                                                });
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Bank Name',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: TextField(
                                                  controller: bankName,
                                                  inputFormatters: [
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-z A-Z]")),
                                                    new LengthLimitingTextInputFormatter(
                                                        100),
                                                  ],
                                                  decoration: InputDecoration(
                                                      /*  errorText: _validBankName
                                                                                                                                                                                                            ? 'Bank name cannot be empty'
                                                                                                                                                                                                            : null, */
                                                      hintText: 'Bank Name',
                                                      hintStyle: TextStyle(
                                                          fontSize: 12),
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Visibility(
                                visible: dropdownValue == "Card" ? true : false,
                                child: Center(
                                  child: RaisedButton(
                                    color: Theme.of(context).primaryColor,
                                    child: Text(
                                      'Click and swipe a card',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              )
                            ],
                          ))
                        ],
                      ),
                    ),
                    Center(
                      child: OutlineButton(
                          borderSide:
                              BorderSide(color: Theme.of(context).primaryColor),
                          child: isRefund
                              ? Text(
                                  'Refund',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                )
                              : Text(
                                  'Repay',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                          onPressed: () {
                            if (isRefund) {
                              if (dropdownValue == 'Cash') {
                                if (reason.text.isEmpty) {
                                  setState(() {
                                    reason.text.isEmpty
                                        ? _isValidReason = true
                                        : _isValidReason = false;
                                  });
                                } else if (selectedItems.length == 0) {
                                  commonComponents.showOverlayMessage(
                                      context,
                                      'Please select a item',
                                      Colors.deepOrange,
                                      Colors.white);
                                } else {
                                  refundAnOrderByCash(
                                      items[0].orderId,
                                      items[0].totalAmount,
                                      reason.text,
                                      selectedItems);
                                }
                              } else if (dropdownValue == 'Check') {
                                if (payeeName.text.isEmpty ||
                                    refundCheckNumber.text.isEmpty ||
                                    //bankName.text.isEmpty ||
                                    reason.text.isEmpty) {
                                  setState(() {
                                    payeeName.text.isEmpty
                                        ? _validPayeeName = true
                                        : _validPayeeName = false;
                                    refundCheckNumber.text.isEmpty
                                        ? _validCheckNumber = true
                                        : _validCheckNumber = false;
                                    /*  bankName.text.isEmpty
                                                                                                                                                                                                                        ? _validBankName = true
                                                                                                                                                                                                                        : _validBankName = false; */
                                    reason.text.isEmpty
                                        ? _isValidReason = true
                                        : _isValidReason = false;
                                  });
                                } else if (selectedItems.length == 0) {
                                  commonComponents.showOverlayMessage(
                                      context,
                                      'Please select a item',
                                      Colors.deepOrange,
                                      Colors.white);
                                } else {
                                  setState(() {
                                    refundAnOrderByCheck(
                                        items[0].orderId,
                                        items[0].totalAmount,
                                        reason.text,
                                        selectedItems,
                                        payeeName.text,
                                        bankName.text != null
                                            ? bankName.text
                                            : '',
                                        refundCheckNumber.text,
                                        newCheckDate
                                            .toString()
                                            .substring(0, 10));
                                  });
                                }
                              }
                            } else {
                              if (dropdownValue == 'Cash') {
                                if (reason.text.isEmpty) {
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                }

                                repayByCash(items[0].orderId,
                                    items[0].totalAmount, reason.text, items);
                              } else if (dropdownValue == 'Check') {
                                if (payeeName.text.isEmpty ||
                                    refundCheckNumber.text.isEmpty ||
                                    //bankName.text.isEmpty ||
                                    reason.text.isEmpty) {
                                  setState(() {
                                    payeeName.text.isEmpty
                                        ? _validPayeeName = true
                                        : _validPayeeName = false;
                                    refundCheckNumber.text.isEmpty
                                        ? _validCheckNumber = true
                                        : _validCheckNumber = false;
                                    /*  bankName.text.isEmpty
                                                                                                                                                                                                                        ? _validBankName = true
                                                                                                                                                                                                                        : _validBankName = false; */
                                    reason.text.isEmpty
                                        ? _isValidReason = true
                                        : _isValidReason = false;
                                  });
                                } else {
                                  setState(() {
                                    repayByCheck(
                                        items[0].orderId,
                                        items[0].totalAmount,
                                        reason.text,
                                        items,
                                        payeeName.text,
                                        bankName.text != null
                                            ? bankName.text
                                            : '',
                                        refundCheckNumber.text,
                                        newCheckDate
                                            .toString()
                                            .substring(0, 10));
                                  });
                                }
                              }
                            }
                          }),
                    )
                  ],
                ),
              ));
            }));
      },
    );
  }

  markCheckReturn(
    DateTime selectedDate,
    String orderId,
    String reasonText,
    String bankCharges,
    List<OrderItems> items,
    bool condition,
  ) async {
    String checkReturnDate = selectedDate.toString().substring(0, 10);

    var returnDetails = {};
    returnDetails["checkReturnDate"] = checkReturnDate;
    returnDetails["reason"] = reasonText;
    returnDetails["bankCharges"] = bankCharges;

    var checkReturnBody = {};
    checkReturnBody["orderId"] = orderId;
    checkReturnBody["returnDetails"] = returnDetails;

    String checkReturnJson = convert.jsonEncode(checkReturnBody);
    //print(checkReturnJson);

    NetworkHelper networkHelper = NetworkHelper();
    var jsonResponse =
        await networkHelper.markChcekReturn(checkReturnJson, token);
    if (jsonResponse != null) {
      if (condition == false) {
        commonComponents.showOverlayMessage(
            context,
            'The selected transaction status has been successfully marked to check returned',
            Colors.green,
            Colors.white);
      }

      if (condition) {
        _showDialogRepay(items, false, 'Check', context, reasonText);
      }

      setStateforData(() {});

      notifyListeners();
    }

    //print(jsonResponse);
  }

  refundAnOrderByCheck(
      String orderId,
      String totalAmount,
      String reasonText,
      List<OrderItems> items,
      String payeeName,
      String bankName,
      String checkNumber,
      String dateOnTheCheck) async {
    List<RefundOrderDetails> refundsList = [];
    items.forEach((items) {
      RefundOrderDetails refundDetails = RefundOrderDetails();
      refundDetails.templeServiceId = items.templeServiceId;
      refundDetails.amount = items.amount;
      refundsList.add(refundDetails);
    });

    RefundOrderByCheck refundOrderByCheck = RefundOrderByCheck();
    refundOrderByCheck.orderId = orderId;
    refundOrderByCheck.refundPaymentType = 'Check';
    //refundOrderByCheck.refundedOn = DateTime.now().toString().substring(0,10);
    refundOrderByCheck.refundedOn = selectedDate.toString().substring(0, 10);
    refundOrderByCheck.refundAmount = totalAmount;
    refundOrderByCheck.reason = reasonText;
    refundOrderByCheck.payeeName = payeeName;
    refundOrderByCheck.bankName = bankName;
    refundOrderByCheck.dateOnTheCheck = dateOnTheCheck;
    refundOrderByCheck.refundCheckNumber = checkNumber;
    refundOrderByCheck.refundDetails = refundsList;
    String refundJson = convert.jsonEncode(refundOrderByCheck);

    //print(refundJson);

    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.orderRefund(refundJson, token);
    print(jsonResponse);

    Navigator.of(context).pop();
    selectedItems.clear();
    commonComponents.showOverlayMessage(
        context,
        'The selected transaction has been refunded successfully',
        Colors.green,
        Colors.white);

    notifyListeners();
    setStateforData(() {});

    /* setState(() {
                                  getAllDonations();
                                  getAllDonationsItems();
                                });
                          */
  }

  repayByCash(String orderId, String totalAmount, String reasonText,
      List<OrderItems> items) async {
    List<RepaymentDetails> repayList = [];
    items.forEach((items) {
      RepaymentDetails repaymentDetails = RepaymentDetails();
      repaymentDetails.templeServiceId = items.templeServiceId;
      repaymentDetails.totalAmount = items.amount;
      repayList.add(repaymentDetails);
    });

    RepaymentByCash repaymentByCash = RepaymentByCash();

    repaymentByCash.orderId = orderId;
    repaymentByCash.paymentType = 'Cash';
    //repaymentByCash.transactionDate = DateTime.now().toString().substring(0,10);
    repaymentByCash.transactionDate = selectedDate.toString().substring(0, 10);
    repaymentByCash.amount = totalAmount;
    repaymentByCash.reason = reasonText;
    repaymentByCash.notes = '';
    repaymentByCash.checkReturnedDate = '';
    repaymentByCash.bankcharges = '';
    repaymentByCash.repaymentDetails = repayList;
    String repayJson = convert.jsonEncode(repaymentByCash);
    //print(repayJson);

    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.orderRepay(repayJson, token);
    print(jsonResponse);

    Navigator.of(context).pop();
    commonComponents.showOverlayMessage(context,
        'Transaction processed successfully', Colors.green, Colors.white);

    setStateforData(() {});

    notifyListeners();
  }

  onSelectedRow(bool selected, OrderItems orderItems) async {
    // print(isRefundOrder);

    //print(' returnRefundDate ***'+orderItems.returnRefundDate);
    print('isRefundOrder ***' + orderItems.isRefundOrder.toString());
    if (orderItems.returnRefundDate == null && orderItems.isRefundOrder) {
      if (selected) {
        selectedItems.add(orderItems);
      } else {
        selectedItems.remove(orderItems);
      }
    } else {
      commonComponents.showOverlayMessage(context,
          'This item is already returned', Colors.deepOrange, Colors.white);
    }

    setStateforData(() {});
    notifyListeners();
  }

  refundAnOrderByCash(String orderId, String totalAmount, String reasonText,
      List<OrderItems> items) async {
    List<RefundOrderDetails> refundsList = [];
    items.forEach((items) {
      RefundOrderDetails refundDetails = RefundOrderDetails();
      refundDetails.templeServiceId = items.templeServiceId;
      refundDetails.amount = items.amount;
      refundsList.add(refundDetails);
    });

    RefundOrderByCash refundOrderByCash = RefundOrderByCash();
    refundOrderByCash.orderId = orderId;
    refundOrderByCash.refundPaymentType = 'Cash';
    refundOrderByCash.refundedOn = selectedDate.toString().substring(0, 10);
    refundOrderByCash.refundAmount = totalAmount;
    refundOrderByCash.reason = reasonText;
    refundOrderByCash.refundDetails = refundsList;
    String refundJson = convert.jsonEncode(refundOrderByCash);

    if (reasonText.isNotEmpty) {
      NetworkHelper networkHelper = new NetworkHelper();
      var jsonResponse = await networkHelper.orderRefund(refundJson, token);

      Navigator.of(context).pop();
      selectedItems.clear();
      /*  Scaffold.of(context).showSnackBar(new SnackBar(
                                                                                                  backgroundColor: Colors.green,
                                                                                                  content: new Text("Refunded succesfully"),
                                                                                                )); */
      commonComponents.showOverlayMessage(
          context,
          'The selected transaction has been refunded successfully',
          Colors.green,
          Colors.white);

      /*setState(() {
                                  getAllDonations();
                                  getAllDonationsItems();
                                  getTenantDetails();
                                });*/

      setStateforData(() {});

      notifyListeners();
    } else {
      commonComponents.showOverlayMessage(
          context, 'Please enter the reason', Colors.deepOrange, Colors.white);
    }

    //print(refundJson);
  }

  repayByCheck(
      String orderId,
      String totalAmount,
      String reasonText,
      List<OrderItems> items,
      String payeeName,
      String bankName,
      String checkNumber,
      String dateOnTheCheck) async {
    List<RepaymentDetails> repayList = [];
    items.forEach((items) {
      RepaymentDetails repaymentDetails = RepaymentDetails();
      repaymentDetails.templeServiceId = items.templeServiceId;
      repaymentDetails.totalAmount = items.amount;
      repayList.add(repaymentDetails);
    });

    RepaymentByCheck repaymentByCheck = RepaymentByCheck();

    repaymentByCheck.orderId = orderId;
    repaymentByCheck.paymentType = 'Check';
    repaymentByCheck.transactionDate =
        DateTime.now().toString().substring(0, 10);
    repaymentByCheck.amount = totalAmount;
    if (items[0].refundDetails != null)
      repaymentByCheck.reason = items[0].refundDetails.reason != null
          ? items[0].refundDetails.reason
          : '';
    else
      repaymentByCheck.reason = '';

    repaymentByCheck.notes = reasonText;
    if (items[0].refundDetails != null)
      repaymentByCheck.checkReturnedDate =
          items[0].refundDetails.checkReturnDate;
    else
      repaymentByCheck.checkReturnedDate = '';

    if (items[0].refundDetails != null)
      repaymentByCheck.bankcharges = items[0].refundDetails.bankCharges;
    else
      repaymentByCheck.bankcharges = '';
    repaymentByCheck.payeeName = payeeName;
    repaymentByCheck.bankName = bankName;
    repaymentByCheck.approvalNumber = checkNumber;
    repaymentByCheck.dateOnTheCheck = dateOnTheCheck;
    repaymentByCheck.repaymentDetails = repayList;

    String repayJson = convert.jsonEncode(repaymentByCheck);
    print(repayJson);

    var jsonResponse = await networkHelper.orderRepay(repayJson, token);
    print(jsonResponse);

    Navigator.of(context).pop();
    commonComponents.showOverlayMessage(context,
        'Transaction processed successfully', Colors.green, Colors.white);
    /*  Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: Colors.green,
      content: new Text("Repaid succesfully"),
    ));

     setState(() {
                                  getAllDonations();
                                  getAllDonationsItems();
                                });
                          */

    setStateforData(() {});

    notifyListeners();
  }

  getPrintReceiptDetails(
      String orderId, var receiptNumber, BuildContext context) async {
    var jsonResponse =
        await networkHelper.getPrintReceiptDetails(token, orderId);
    int eventCount = 0;
    bool canShowNakshatra = false;
    if (jsonResponse != null) {
      PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
      print(printReceipt.toString());
      List<OrderItemDetails> orderDetails =
          printReceipt.receiptDetails.order.orderItemDetails;

      List<OrderItemDetails> templeTokenList = [];

      tenantName = tenantInfo.tenantDetails.name;
      tenantAddress = tenantInfo.tenantDetails.address.address;
      tenantLogo = tenantInfo.tenantDetails.additionalProperties.receiptLogo;
      tenantWeb = tenantInfo.tenantDetails.additionalProperties.websiteUrl;
      tenantPhone = tenantInfo.tenantDetails.additionalProperties.phoneNumber;

      orderDetails.forEach((element) {
        if (element.templeToken != null && element.templeToken) {
          templeTokenList.add(element);
        }
        if (element.serviceType == 'Event') {
          eventCount++;
        }
      });
      canShowNakshatra = !(eventCount == orderDetails.length);

      showDialog(
          context: context,
          barrierDismissible: false,
          child: new AlertDialog(
              titlePadding: EdgeInsets.zero,
              title: Container(
                  color: Theme.of(context).primaryColor,
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Receipt - $receiptNumber',
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w500)),
                      Align(
                        alignment: Alignment.centerRight,
                        child: IconButton(
                            icon: Icon(
                              Icons.close,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                      )
                    ],
                  )),
              content: Container(
                  constraints: BoxConstraints(maxWidth: 450),
                  child: SingleChildScrollView(
                      child: Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.network(
                                  tenantLogo,
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.fitHeight,
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Container(
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                      Text(
                                        tenantName,
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      Text(
                                        tenantAddress,
                                        style: TextStyle(fontSize: 11),
                                      ),
                                      Text(
                                          printReceipt.receiptDetails.tenantInfo
                                                  .address.city +
                                              ', ' +
                                              printReceipt
                                                  .receiptDetails
                                                  .tenantInfo
                                                  .address
                                                  .state
                                                  .name +
                                              ' - ' +
                                              tenantInfo.tenantDetails.address
                                                  .zipCode,
                                          style: TextStyle(
                                            fontSize: 11,
                                          )),
                                      Text('Tel - ' + tenantPhone,
                                          style: TextStyle(
                                            fontSize: 11,
                                          )),
                                      Text(tenantWeb,
                                          style: TextStyle(
                                            fontSize: 11,
                                          )),
                                      SizedBox(height: 20),
                                    ])),
                              ],
                            ),
                            Text('Receipt for Services',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w500)),
                            SizedBox(height: 5),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Receipt No : ' + receiptNumber,
                                  style: TextStyle(fontSize: 11),
                                ),
                                Text(
                                  getFormattedDateTime(printReceipt
                                      .receiptDetails.order.transactionDate),
                                  style: TextStyle(fontSize: 11),
                                )
                              ],
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      printReceipt.receiptDetails.order.user
                                              .firstName +
                                          ' ' +
                                          printReceipt.receiptDetails.order.user
                                              .lastName,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 11,
                                      )),
                                  Text(
                                      printReceipt.receiptDetails.order.user
                                          .userAddress.address,
                                      style: TextStyle(
                                        fontSize: 11,
                                      )),
                                  Text(
                                      printReceipt.receiptDetails.order.user
                                              .userAddress.city +
                                          ', ' +
                                          printReceipt.receiptDetails.order.user
                                              .userAddress.state.name +
                                          ' - ' +
                                          printReceipt.receiptDetails.order.user
                                              .userAddress.zipCode,
                                      style: TextStyle(
                                        fontSize: 11,
                                      )),
                                  Text(
                                      printReceipt.receiptDetails.order.user
                                          .userAddress.country.name,
                                      style: TextStyle(
                                        fontSize: 11,
                                      ))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SingleChildScrollView(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          DataTable(
                              onSelectAll: (b) {},
                              sortAscending: true,
                              dataRowHeight: 65,
                              columns: <DataColumn>[
                                DataColumn(
                                  label: Center(
                                    child: Text("Item",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: 12),
                                        softWrap: true),
                                  ),
                                  numeric: false,
                                  onSort: (i, b) {
                                    setStateforData(() {
                                      _results.sort((b, a) => a.transactionDate
                                          .compareTo(b.transactionDate));
                                    });
                                  },
                                ),
                                DataColumn(
                                  label: Center(
                                      child: Text("Quantity",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: true)),
                                  numeric: false,
                                ),
                                DataColumn(
                                  label: Center(
                                    child: Text("Amt(\$)",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: 12),
                                        softWrap: false),
                                  ),
                                  numeric: false,
                                ),
                              ],
                              rows: orderDetails
                                  .map(
                                    (orderDetails) => DataRow(
                                      cells: [
                                        DataCell(
                                          SizedBox(
                                            width: 200,
                                            height: 550,
                                            child: Center(
                                              child: Container(
                                                  child: AutoSizeText(
                                                orderDetails.name[0]
                                                        .toUpperCase() +
                                                    orderDetails.name
                                                        .substring(1),
                                                maxLines: 5,
                                              )),
                                            ),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                              child: Container(
                                            child: getColumnCell(
                                                orderDetails.name,
                                                orderDetails.quantity),
                                          )),
                                        ),
                                        DataCell(
                                          Center(
                                              child: Container(
                                                  child: Text((orderDetails
                                                          .totalAmount)
                                                      .toStringAsFixed(2)))),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                      ],
                                    ),
                                  )
                                  .toList()),
                          SizedBox(
                            height: 20,
                          ),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Row(
                                  children: [
                                    Text('Total Amount :',
                                        style: TextStyle(
                                          fontSize: 14,
                                        )),
                                    SizedBox(width: 10),
                                    Text(
                                      '\$ ${printReceipt.receiptDetails.orderInfo.receiptTotalAmount.toStringAsFixed(2).toString()}',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.start,
                                    )
                                  ],
                                ),
                              )),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Row(
                                  children: [
                                    Text('Payment Type : ',
                                        style: TextStyle(
                                          fontSize: 14,
                                        )),
                                    SizedBox(width: 10),
                                    Text(
                                      '${printReceipt.receiptDetails.order.paymentDetails.paymentType}',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.start,
                                    )
                                  ],
                                ),
                              )),
                          Visibility(
                            visible: (printReceipt.receiptDetails.order
                                            .paymentDetails.paymentType ==
                                        'Check' ||
                                    printReceipt.receiptDetails.order
                                            .paymentDetails.paymentType ==
                                        'Credit/Debit Card')
                                ? true
                                : false,
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: EdgeInsets.all(5),
                                  child: Row(
                                    children: [
                                      Text('Reference Number : ',
                                          style: TextStyle(
                                            fontSize: 14,
                                          )),
                                      SizedBox(width: 10),
                                      Text(
                                        '${printReceipt.receiptDetails.order.paymentDetails.approvalNumber}',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.start,
                                      )
                                    ],
                                  ),
                                )),
                          ),
                          Visibility(
                              visible: printReceipt.receiptDetails.order.user
                                              .nakshatraDetails.length >
                                          0 &&
                                      canShowNakshatra
                                  ? true
                                  : false,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(height: 40),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.network(
                                        tenantLogo,
                                        width: 50,
                                        height: 50,
                                        fit: BoxFit.fitHeight,
                                      ),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Container(
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                            Text(
                                              tenantName,
                                              style: TextStyle(fontSize: 14),
                                            ),
                                            Text(
                                              tenantAddress,
                                              style: TextStyle(fontSize: 11),
                                            ),
                                            Text(
                                                printReceipt.receiptDetails
                                                        .tenantInfo.address.city +
                                                    ', ' +
                                                    printReceipt
                                                        .receiptDetails
                                                        .tenantInfo
                                                        .address
                                                        .state
                                                        .name +
                                                    ' - ' +
                                                    tenantInfo.tenantDetails
                                                        .address.zipCode,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                )),
                                            Text('Tel - ' + tenantPhone,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                )),
                                            Text(tenantWeb,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                )),
                                            SizedBox(height: 20),
                                          ])),
                                    ],
                                  ),
                                  Text('Nakshatra Details',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500)),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Receipt No : ' + receiptNumber,
                                        style: TextStyle(fontSize: 11),
                                      ),
                                      Text(
                                        getFormattedDateTime(printReceipt
                                            .receiptDetails
                                            .order
                                            .transactionDate),
                                        style: TextStyle(fontSize: 11),
                                      )
                                    ],
                                  ),
                                  getNakshatraWidget(printReceipt.receiptDetails
                                      .order.user.nakshatraDetails)
                                ],
                              )),
                          Visibility(
                              visible:
                                  templeTokenList.length > 0 ? true : false,
                              child: Column(
                                children: [
                                  SizedBox(height: 40),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.network(
                                        tenantLogo,
                                        width: 50,
                                        height: 50,
                                        fit: BoxFit.fitHeight,
                                      ),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Container(
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                            Text(
                                              tenantName,
                                              style: TextStyle(fontSize: 14),
                                            ),
                                            Text(
                                              tenantAddress,
                                              style: TextStyle(fontSize: 11),
                                            ),
                                            Text(
                                                printReceipt.receiptDetails
                                                        .tenantInfo.address.city +
                                                    ', ' +
                                                    printReceipt
                                                        .receiptDetails
                                                        .tenantInfo
                                                        .address
                                                        .state
                                                        .name +
                                                    ' - ' +
                                                    tenantInfo.tenantDetails
                                                        .address.zipCode,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                )),
                                            Text('Tel - ' + tenantPhone,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                )),
                                            Text(tenantWeb,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                )),
                                            SizedBox(height: 20),
                                          ])),
                                    ],
                                  ),
                                  Text('Token Receipt',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500)),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Receipt No : ' + receiptNumber,
                                        style: TextStyle(fontSize: 11),
                                      ),
                                      Text(
                                        getFormattedDateTime(printReceipt
                                            .receiptDetails
                                            .order
                                            .transactionDate),
                                        style: TextStyle(fontSize: 11),
                                      )
                                    ],
                                  ),
                                  getTokenReceipt(templeTokenList)
                                ],
                              )),
                          RaisedButton(
                              color: Colors.deepOrange,
                              textColor: Colors.white,
                              child: Text('Print'),
                              onPressed: () async {
                                new ReceiptTemplate().getHtmlFromResponse(
                                    printReceipt,
                                    isSignatureCopy,
                                    minAmntReceipt,
                                    false,
                                    printReceipt.receiptDetails.orderInfo
                                                .receiptTotalAmount <=
                                            0
                                        ? true
                                        : false,
                                    canShowNakshatra);
                              }),
                        ],
                      ))
                    ],
                  )))));
    } else {
      CommonComponents().showOverlayMessage(
          context,
          'Oops, someting went wrong. Try again later',
          Colors.deepOrange,
          Colors.white);
    }
  }

  getTokenReceipt(List<OrderItemDetails> templeTokenList) {
    return new Column(
      children: [
        Padding(
          padding: EdgeInsets.all(10),
        ),
        Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Expanded(
                  child: Text(toBeginningOfSentenceCase('Name'),
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                Expanded(
                  child: Row(
                    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Qty',
                          style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          )),
                      SizedBox(width: 10),
                      SizedBox(width: 10),
                      Text('Amt(\$)',
                          style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          )),
                    ],
                  ),
                ),
                Text('Total (\$)',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            )),
        Column(
            children: templeTokenList
                .map((nakshatra) => new Padding(
                    padding: EdgeInsets.all(10),
                    child: new Row(
                      children: [
                        Expanded(
                          child: Text(toBeginningOfSentenceCase(nakshatra.name),
                              style: TextStyle(fontSize: 13)),
                        ),
                        Expanded(
                          child: Row(
                            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(nakshatra.quantity.toString(),
                                  style: TextStyle(fontSize: 13)),
                              SizedBox(width: 10),
                              Text(' '),
                              SizedBox(width: 10),
                              Text(nakshatra.fee,
                                  style: TextStyle(fontSize: 13)),
                            ],
                          ),
                        ),

                        //SizedBox(width: 20),
                        Text(nakshatra.totalAmount.toStringAsFixed(2),
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500))
                      ],
                    )))
                .toList())
      ],
    );
  }

  String getFormattedDateTime(String transactionDate) {
    return DateFormat('MM-dd-yyyy hh:mm aa')
        .format(DateTime.parse(transactionDate));
  }

  Widget getNakshatraWidget(List<NakshatraDetails> nakshatraDetails) {
    return new Column(
      children: [
        Padding(
            padding: EdgeInsets.all(10),
            child: new Row(
              children: [
                Expanded(
                    child: Text(toBeginningOfSentenceCase('Name'),
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                        ))),
                SizedBox(width: 20),
                Expanded(
                  child: Text(
                    'Naksthra',
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                    ),
                    softWrap: true,
                    //overflow: TextOverflow.ellipsis,
                  ),
                )
              ],
            )),
        Column(
            children: nakshatraDetails
                .map((nakshatra) => new Padding(
                    padding: EdgeInsets.all(10),
                    child: new Row(
                      children: [
                        Expanded(
                            child: Text(
                                toBeginningOfSentenceCase(nakshatra.name),
                                style: TextStyle(fontSize: 13))),
                        SizedBox(width: 20),
                        Expanded(
                          child: Text(
                            nakshatra.nakshatra,
                            style: TextStyle(fontSize: 13),
                            softWrap: true,
                            //overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    )))
                .toList())
      ],
    );
  }

  getColumnCell(String orderName, int amt) {
    //'(repayment-'

    if ((toBeginningOfSentenceCase(orderName.toLowerCase())
            .contains('(check returned - ')) ||
        (toBeginningOfSentenceCase(orderName.toLowerCase())
            .contains('(repayment - ')) ||
        (toBeginningOfSentenceCase(orderName.toLowerCase())
            .contains('refunded - '))) {
      return Text(
        '',
      );
    } else {
      return Text(
        amt.toString(),
        textAlign: TextAlign.center,
      );
    }
  }
}

class DataTableDemo extends StatefulWidget {
  final String token;
  final BuildContext context;
  final actionPrevilege;
  final bool isSignatureCopy;
  final String minAmntReceipt;
  @override
  _DataTableDemoState createState() => _DataTableDemoState();
  const DataTableDemo(
      {Key key,
      @required this.token,
      @required this.context,
      @required this.actionPrevilege,
      this.isSignatureCopy,
      this.minAmntReceipt})
      : super(key: key);
}

class _DataTableDemoState extends State<DataTableDemo> {
  ResultsDataSource _resultsDataSource = ResultsDataSource([]);
  bool isLoaded = false;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  int _sortColumnIndex;
  bool _sortAscending = true;

  int i = 0;

  NetworkHelper networkHelper = new NetworkHelper();
  final _formKey = GlobalKey<FormState>();
  List<DonationItems> items = [];

  tenant.TenantInfo tenantInfo;

  TextEditingController nameController = TextEditingController();
  TextEditingController receiptNumController = TextEditingController();
  var dateHelper = new DateHelper();
  bool cashVal = false;
  bool checkVal = false;
  bool cardVal = false;

  bool firstTym = true;

  List<DonationItems> itemsToView = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (firstTym) {
      getData(context, widget.token, false);
      firstTym = false;
    } else {}
  }

  void _sort<T>(Comparable<T> getField(DonationItems d), int columnIndex,
      bool ascending) {
    _resultsDataSource._sort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  Future<void> getData(
      BuildContext context, String token, bool isFilter) async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    tenantInfo = await localStorage.getTenantInfoFull();
    var results;
    i++;
    if (itemsToView.length > 0 && isLoaded) {
      if (isFilter) {
        print('condition one');
        List<DonationItems> itemToViewDuplicate = [];
        results = await fetchResults(widget.token);
        items = await fetchResults(widget.token);
        for (DonationItems donationItemsObj in itemsToView) {
          for (DonationItems donationItems in results) {
            if (donationItemsObj.receiptNumber == donationItems.receiptNumber &&
                donationItemsObj.paymentType == donationItems.paymentType) {
              if (!itemToViewDuplicate.contains(donationItems))
                itemToViewDuplicate.add(donationItems);
            }
          }
        }
        if (itemsToView.length > 0) {
          itemsToView.clear();
          itemsToView = itemToViewDuplicate;
          results = itemsToView;
        }
      } else {
        results = itemsToView;
        itemsToView = results;
      }
    } else if (itemsToView.length == 0 && isLoaded) {
      results = itemsToView;
      itemsToView = results;
    } else {
      results = await fetchResults(widget.token);
      items = await fetchResults(widget.token);
      itemsToView = await fetchResults(widget.token);
      print('condition 3');
    }
    print(' results $i and' + results.length.toString());

    setState(() {
      _resultsDataSource = ResultsDataSource(results,
          context: context,
          token: token,
          setStateforData: refresh,
          tenantInfo: tenantInfo,
          actionPrevilege: widget.actionPrevilege,
          isSignatureCopy: widget.isSignatureCopy,
          minAmntReceipt: widget.minAmntReceipt);
      // isLoaded = true;
    });

    /* NetworkHelper networkHelper = NetworkHelper();
                var response = await networkHelper.getAllDonations(widget.token);
                Donations donations = Donations.fromJson(response);
                if (!isLoaded) {
                  setState(() {
                    items = donations.donationItems;
                    isLoaded = true;
                  });
                }*/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/refund.png',
              width: 40,
              height: 40,
            ),
            Text(
              'Return and Refund',
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Container(
              padding: EdgeInsets.all(10),
              height: 70,
              color: Colors.deepOrange,
              child: Form(
                key: _formKey,
                child: Row(
                  children: <Widget>[
                    Text(
                      'Search by : ',
                      style: TextStyle(
                          color: Colors.white54,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    Expanded(
                        flex: 1,
                        child: Container(
                          width: 180,
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                              controller: nameController,
                              onChanged: (editedName) {
                                if (editedName.length > 0) {
                                  print(items.length);
                                  //getFilterItems(items, editedName);
                                  getItemsBasedOnSearch();
                                } else {
                                  if (receiptNumController.text.length == 0) {
                                    itemsToView.clear();
                                    addItemsToView(items);
                                    setState(() {});
                                  } else {
                                    getItemsBasedOnSearch();
                                  }
                                }
                              },
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500),
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(8),
                                  hintText: 'Name',
                                  hintStyle: TextStyle(
                                      color: Colors.white60,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500),
                                  //border: InputBorder.none
                                  border: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white)))),

                          /*   decoration: BoxDecoration(
                                                                                                                                                      color: Colors.white,
                                                                                                                                                      border: Border.all(color: Colors.white),
                                                                                                                                                      borderRadius: BorderRadius.circular(30)), */
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                        flex: 1,
                        child: Container(
                          width: 180,
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontWeight: FontWeight.w500),
                            cursorColor: Colors.white,
                            controller: receiptNumController,
                            onChanged: (editedName) {
                              if (editedName.length > 0) {
                                print(items.length);
                                //getFilterItemsForReceipt(items, editedName);
                                getItemsBasedOnSearch();
                              } else {
                                if (nameController.text.length == 0) {
                                  itemsToView.clear();
                                  addItemsToView(items);
                                  setState(() {});
                                } else {
                                  getItemsBasedOnSearch();
                                }
                              }
                            },
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(8),
                                hintText: 'Receipt Number',
                                hintStyle: TextStyle(
                                    color: Colors.white60,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500),

                                //focusedBorder: InputBorder( borderSide: )
                                border: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white60))),
                          ),
                          /*  decoration: BoxDecoration(
                                                                                                                                                                                            color: Colors.white,
                                                                                                                                                                                            border: Border.all(color: Colors.white),
                                                                                                                                                                                            borderRadius: BorderRadius.circular(30)), */
                        )),
                    Expanded(
                        flex: 4,
                        child: Row(
                          children: [
                            Theme(
                                data: Theme.of(context).copyWith(
                                  unselectedWidgetColor: Colors.white,
                                ),
                                child: Checkbox(
                                  checkColor: Colors.white,
                                  value: cashVal,
                                  onChanged: (bool value) {
                                    if (nameController.text.length > 0 ||
                                        receiptNumController.text.length > 0) {
                                      setState(() {
                                        cashVal = value;
                                        getItemsBasedOnSearch();
                                      });
                                    } else {
                                      print('inside elsedddddddddddd');
                                      print(items.length);

                                      setState(() {
                                        cashVal = value;
                                        checkOnlyCheckBoxesNew();
                                      });
                                    }
                                  },
                                )),
                            Text(
                              'Only cash',
                              style: TextStyle(color: Colors.white),
                            ),
                            Theme(
                                data: Theme.of(context).copyWith(
                                  unselectedWidgetColor: Colors.white,
                                ),
                                child: Checkbox(
                                  checkColor: Colors.white,
                                  value: checkVal,
                                  onChanged: (bool value) {
                                    if (nameController.text.length > 0 ||
                                        receiptNumController.text.length > 0) {
                                      setState(() {
                                        checkVal = value;
                                        getItemsBasedOnSearch();
                                      });
                                    } else {
                                      print('inside elsedddddddddddd');
                                      print(items.length);

                                      setState(() {
                                        checkVal = value;
                                        checkOnlyCheckBoxesNew();
                                      });
                                    }
                                  },
                                )),
                            Text(
                              'Only check',
                              style: TextStyle(color: Colors.white),
                            ),
                            Theme(
                              data: Theme.of(context).copyWith(
                                unselectedWidgetColor: Colors.white,
                              ),
                              child: Checkbox(
                                checkColor: Colors.white,
                                value: cardVal,
                                onChanged: (bool value) {
                                  if (nameController.text.length > 0 ||
                                      receiptNumController.text.length > 0) {
                                    setState(() {
                                      cardVal = value;
                                      getItemsBasedOnSearch();
                                    });
                                  } else {
                                    print('inside elsedddddddddddd');
                                    print(items.length);

                                    setState(() {
                                      cardVal = value;
                                      checkOnlyCheckBoxesNew();
                                    });
                                  }
                                },
                              ),
                            ),
                            Text(
                              'Only card',
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Visibility(
                              visible: nameController.text.isNotEmpty ||
                                      receiptNumController.text.isNotEmpty ||
                                      checkVal ||
                                      cardVal ||
                                      cashVal
                                  ? true
                                  : false,
                              child: OutlineButton(
                                  padding: EdgeInsets.all(2),
                                  color: Colors.white,
                                  onPressed: () {
                                    print('length on clear' +
                                        itemsToView.length.toString());

                                    print('length on clear items' +
                                        items.length.toString());
                                    itemsToView.clear();
                                    nameController.clear();
                                    receiptNumController.clear();
                                    checkVal = false;
                                    cashVal = false;
                                    cardVal = false;

                                    addItemsToView(items);
                                    print('length on clear' +
                                        itemsToView.length.toString());
                                    isLoaded = true;

                                    setState(() {
                                      //itemsToView.addAll(items);
                                    });
                                  },
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  ),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                  child: Text(
                                    'Clear',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                          ],
                        ))
                  ],
                ),
              )),
        ),
        Expanded(
            child: ListView(children: <Widget>[
          PaginatedDataTable(
              showCheckboxColumn: false,
              columnSpacing: 0,
              horizontalMargin: 10,
              header: Container(),
              rowsPerPage: _rowsPerPage,
              //showCheckboxColumn: false,
              onRowsPerPageChanged: (int value) {
                setState(() {
                  _rowsPerPage = value;
                });
              },
              sortColumnIndex: _sortColumnIndex,
              sortAscending: _sortAscending,
              onSelectAll: _resultsDataSource._selectAll,
              columns: <DataColumn>[
                DataColumn(
                    label: Container(
                      alignment: Alignment.centerLeft,
                      width: 100,
                      //padding: EdgeInsets.symmetric(horizontal: 10),
                      //color: Colors.yellow,
                      child: Text(
                        'Transaction Date',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    numeric: true,
                    onSort: (int columnIndex, bool ascending) => _sort<String>(
                        (DonationItems d) => d.transactionDate,
                        columnIndex,
                        ascending)),
                DataColumn(
                    label: Container(
                      width: 100,
                      alignment: Alignment.center,
                      //color: Colors.yellow,
                      child: Text('Receipt No',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    numeric: true,
                    onSort: (int columnIndex, bool ascending) => _sort<num>(
                        (DonationItems d) => d.receiptNumber,
                        columnIndex,
                        ascending)),
                DataColumn(
                    label: Container(
                        width: 200,
                        alignment: Alignment.centerLeft,
                        //color: Colors.yellow,
                        child: Text('Devotee Name',
                            textAlign: TextAlign.left,
                            style: TextStyle(fontWeight: FontWeight.bold))),
                    numeric: true,
                    onSort: (int columnIndex, bool ascending) => _sort<String>(
                        (DonationItems d) => d.firstName,
                        columnIndex,
                        ascending)),
                DataColumn(
                    label: Container(
                        //color: Colors.yellow,
                        alignment: Alignment.centerLeft,
                        width: 200,
                        child: Text('Service',
                            style: TextStyle(fontWeight: FontWeight.bold))),
                    numeric: true,
                    onSort: (int columnIndex, bool ascending) => _sort<String>(
                        (DonationItems d) => d.serviceType,
                        columnIndex,
                        ascending)),
                DataColumn(
                    label: Container(
                        alignment: Alignment.centerLeft,
                        width: 100,
                        //color: Colors.yellow,
                        child: Text('Payment mode',
                            style: TextStyle(fontWeight: FontWeight.bold))),
                    numeric: true,
                    onSort: (int columnIndex, bool ascending) => _sort<String>(
                        (DonationItems d) => d.paymentType,
                        columnIndex,
                        ascending)),
                DataColumn(
                    label: Container(
                        alignment: Alignment.centerLeft,
                        width: 80,
                        //color: Colors.yellow,
                        child: Text('Amount (\$)',
                            style: TextStyle(fontWeight: FontWeight.bold))),
                    numeric: true,
                    onSort: (int columnIndex, bool ascending) => _sort<String>(
                        (DonationItems d) => d.amount, columnIndex, ascending)),
                widget.actionPrevilege == 2
                    ? DataColumn(
                        label: Container(
                            alignment: Alignment.centerLeft,
                            width: 120,
                            //color: Colors.yellow,
                            child: Text('Actions',
                                style: TextStyle(fontWeight: FontWeight.bold))),
                        numeric: true,
                        onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((DonationItems d) => d.amount,
                                columnIndex, ascending))
                    : DataColumn(
                        label: Container(
                            alignment: Alignment.centerLeft,
                            width: 100,
                            //color: Colors.yellow,
                            child: Text(''))),
              ],
              source: _resultsDataSource)
        ]))
      ],
    ));
  }

  void refresh(fn) {
    bool isfilter = false;
    if (nameController.text.isNotEmpty ||
        receiptNumController.text.isNotEmpty) {
      if (checkVal || cardVal || cashVal) {
        isLoaded = true;
        isfilter = true;
      } else {
        isLoaded = true;
        isfilter = false;
      }
    } else if (nameController.text.isEmpty ||
        receiptNumController.text.isEmpty) {
      if (checkVal || cardVal || cashVal) {
        isLoaded = true;
        isfilter = true;
      } else {
        isLoaded = false;
        isfilter = false;
        itemsToView.clear();
      }
    } else {
      isLoaded = false;
      isfilter = false;
      itemsToView.clear();
    }

    print(isLoaded);
    print(isfilter);
    print('************INSIDE refresh');

    getData(context, widget.token, isfilter);
    setState(() {
      print('************INSIDE SETSTATE');
    });
  }

  void getItemsBasedOnSearch() {
    List<DonationItems> filteredItems = [];
    itemsToView.clear();
    isLoaded = true;
    print(nameController.text.length);
    if (nameController.text.length > 0) {
      items.forEach((item) {
        print(item.firstName.compareTo(nameController.text));
        String fullName = item.firstName + ' ' + item.lastName;
        if (fullName
            .toLowerCase()
            .trim()
            .contains(nameController.text.toLowerCase().trim())) {
          filteredItems.add(item);
        }
      });
      commonMethodToCheck(filteredItems);
    } else {
      commonMethodToCheck(filteredItems);
    }
  }

  void commonMethodToCheck(List<DonationItems> filteredItems) {
    isLoaded = true;
    List<DonationItems> finalfilteredItems = [];
    itemsToView.clear();

    if (receiptNumController.text.length > 0) {
      if (filteredItems.length > 0) {
        filteredItems.forEach((item) {
          print(item.receiptNumber
              .toString()
              .contains(receiptNumController.text));

          if (item.receiptNumber
              .toString()
              .contains(receiptNumController.text)) {
            finalfilteredItems.add(item);
          }
        });
      } else {
        if (nameController.text.length == 0)
          items.forEach((item) {
            if (item.receiptNumber
                .toString()
                .contains(receiptNumController.text)) {
              finalfilteredItems.add(item);
            }
          });
      }
    } else {
      if (filteredItems.length > 0) {
        finalfilteredItems.addAll(filteredItems);
      }
    }

    if ((finalfilteredItems.length > 0 &&
            receiptNumController.text.length > 0) ||
        (finalfilteredItems.length > 0 &&
            receiptNumController.text.length == 0 &&
            nameController.text.length == 0)) {
      itemsToView.addAll(finalfilteredItems);
    } else if ((filteredItems.length > 0 &&
            nameController.text.length > 0 &&
            receiptNumController.text.length == 0) ||
        (filteredItems.length > 0 &&
            nameController.text.length == 0 &&
            receiptNumController.text.length > 0)) {
      itemsToView.addAll(filteredItems);
    } else {
      itemsToView.clear();
    }

    checkOnlyCheckBoxes(itemsToView);
  }

  void checkOnlyCheckBoxes(List<DonationItems> itemsToView) {
    isLoaded = true;
    List<DonationItems> filterItemsForTypeOfCash = [];

    print(itemsToView.length.toString() + ' before multi if');
    if (itemsToView.length > 0) {
      if (cashVal && checkVal && cardVal) {
        print('1111111111');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Cash' ||
              item.paymentType == 'Check' ||
              item.paymentType == 'Card') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (checkVal && cashVal) {
        print('2222222222222');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Check' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && cashVal) {
        print('33333333333');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && checkVal) {
        print('44444444444444');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Check') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else {
        if (cashVal) {
          print('55555555555');
          itemsToView.forEach((item) {
            if (item.paymentType == 'Cash') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (cardVal) {
          print('6666666666');
          itemsToView.forEach((item) {
            if (item.paymentType == 'Card') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (checkVal) {
          print('7777777777');
          itemsToView.forEach((item) {
            if (item.paymentType == 'Check') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else {}
      }

      print(filterItemsForTypeOfCash.length);

      if (filterItemsForTypeOfCash.length > 0) {
        itemsToView.clear();
        itemsToView.addAll(filterItemsForTypeOfCash);
      } else {
        if (cashVal || checkVal || cardVal) {
          itemsToView.clear();
        }
      }

      getData(context, widget.token, false);

      setState(() {});
    } else {
      setState(() {});
    }
  }

  void checkOnlyCheckBoxesNew() {
    isLoaded = true;

    List<DonationItems> filterItemsForTypeOfCash = [];

    print(items.length.toString() + ' before multi if');
    if (items.length > 0) {
      if (cashVal && checkVal && cardVal) {
        print('1111111111');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Cash' ||
              item.paymentType == 'Check' ||
              item.paymentType == 'Card') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (checkVal && cashVal) {
        print('2222222222222');
        items.forEach((item) {
          if (item.paymentType == 'Check' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && cashVal) {
        print('33333333333');
        items.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && checkVal) {
        print('44444444444444');
        items.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Check') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else {
        if (cashVal) {
          print('55555555555');
          items.forEach((item) {
            if (item.paymentType == 'Cash') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (cardVal) {
          print('6666666666');
          items.forEach((item) {
            if (item.paymentType == 'Card') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (checkVal) {
          print('7777777777');
          items.forEach((item) {
            if (item.paymentType == 'Check') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else {}
      }

      print(filterItemsForTypeOfCash.length);

      if (filterItemsForTypeOfCash.length > 0) {
        itemsToView.clear();
        itemsToView.addAll(filterItemsForTypeOfCash);
      } else {
        if (cashVal || checkVal || cardVal) {
          itemsToView.clear();
        } else if (!cardVal && !cashVal && !checkVal) {
          itemsToView.clear();
          itemsToView.addAll(items);
        }
      }

      getData(context, widget.token, false);

      setState(() {});
    } else {
      setState(() {});
    }
  }

  void addItemsToView(List<DonationItems> items) {
    for (DonationItems itemObj in items) {
      itemsToView.add(itemObj);
    }
    checkOnlyCheckBoxesNew();
  }
}

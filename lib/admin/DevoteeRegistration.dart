import 'package:chakra/models/DevoteeRegistration/DevoteeRegResponse.dart';
import 'package:chakra/models/Setup/AppcodeItems.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart';
import 'package:chakra/utils/FormValidations.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:flutter/services.dart';

import '../widgets/common_components.dart';

class DevoteeRegistration extends StatefulWidget {
  final String token;
  final BuildContext context;

  @override
  _DevoteeRegistrationState createState() => _DevoteeRegistrationState();
  const DevoteeRegistration({
    Key key,
    @required this.token,
    @required this.context,
  }) : super(key: key);
}

class _DevoteeRegistrationState extends State<DevoteeRegistration> {
  final _formKey = GlobalKey<FormState>();
  var formValidations = new FormValidations();
  String selectedCountry;
  String selectedState;

  String selectedCountryDuplicate;
  String selectedStateDuplicate;

  String selectedCountryCode;
  String phoneCode;
  String tenantState;

  var networkHelper = new NetworkHelper();
  List<AppcodeItems> appCodeItems = [];
  List<AppcodeItems> stateList = [];
  List<String> countryCodeList = [];
  List<String> countryCodeIdList = [];
  List<AppcodeItems> countryList = [];
  List<AppcodeItems> selectedStateList = [];
  TextEditingController fNameController = TextEditingController();
  TextEditingController lNameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController zipController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  bool isPhoneExists = false;
  bool isEmailExists = false;
  String phoneExistsErrorText = 'Mobile number already exists';
  String emailExistsErrorText = 'Email already exists';

  bool validPhoneNumber = false;
  bool validEmail = false;
  bool validFirstName = false;
  bool validLastName = false;
  bool validAddress = false;
  bool isEmailError = false;
  bool validZipCode = false;
  bool validCity = false;
  bool mobileNumberLen = false;

  var commonComponents = new CommonComponents();

  Map<String, String> countryMap = new Map();

  @override
  void initState() {
    super.initState();
    getTenantData();
    phoneController.addListener(() {
      checkIfMobileNumExists(
          selectedCountryCode + '-' + phoneController.text, false);
    });
    emailController.addListener(() {
      checkIfMobileNumExists(emailController.text, true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        padding: EdgeInsets.only(left: 40, right: 40, top: 0, bottom: 20),
        scrollDirection: Axis.vertical,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/registration.png',
                    width: 40,
                    height: 40,
                  ),
                  Text(
                    'Devotee Enrollment',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Row(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.only(right: 10, left: 10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(color: Colors.grey))),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: false,
                                hint: new Text(
                                  "Country Code",
                                  style: TextStyle(fontSize: 13),
                                ),
                                value: selectedCountryCode,
                                onChanged: (String newValue) {
                                  setState(() {
                                    selectedCountryCode = newValue.toString();
                                  });
                                },
                                items:
                                    countryCodeList.map((String countryCode) {
                                  return new DropdownMenuItem<String>(
                                    child: new Text(
                                      countryCode,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                    value: countryCode,
                                  );
                                }).toList(),
                              ),
                            )),
                        Expanded(
                          child: TextFormField(
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            onChanged: (text) {
                              if (text.length < 10) {
                                setState(() {
                                  mobileNumberLen = true;
                                  validPhoneNumber = false;
                                });
                              } else if (text.length == 10) {
                                setState(() {
                                  mobileNumberLen = false;
                                  validPhoneNumber = false;
                                });
                              } else if (text.length == 0) {
                                setState(() {
                                  validPhoneNumber = true;
                                });
                              }
                            },
                            controller: phoneController,
                            style: TextStyle(fontSize: 13),
                            onEditingComplete: () {
                              if (phoneController.text.length == 10) {
                                setState(() {
                                  mobileNumberLen = false;
                                });

                                checkIfMobileNumExists(
                                    selectedCountryCode +
                                        '-' +
                                        phoneController.text,
                                    false);
                              }
                            },
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(10),
                                hintText: 'Mobile Number',
                                hintStyle: TextStyle(fontSize: 13),
                                errorText: getPhoneNumberErrorText(),
                                errorBorder: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 1),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide:
                                      BorderSide(color: Colors.black, width: 1),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide:
                                      BorderSide(color: Colors.black, width: 1),
                                )),
                            /* validator: (value) {
                                                              return formValidations.validateTextField(
                                                                  value, 'Mobile Number');
                                                            },*/
                          ),
                        ),
                      ],
                    )),
                    SizedBox(
                      width: 40,
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: emailController,
                        style: TextStyle(fontSize: 13),
                        onChanged: (text) {
                          if (text.length > 0) {
                            setState(() {
                              validEmail = false;

                              if (formValidations
                                      .validateEmail(emailController.text) ==
                                  null) {
                                isEmailError = false;
                              } else {
                                isEmailError = true;
                              }
                            });
                          }
                        },
                        onEditingComplete: () {
                          checkIfMobileNumExists(emailController.text, true);
                        },
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: 'Email',
                            errorText: getEmailError(),
                            hintStyle: TextStyle(fontSize: 13),
                            errorBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            )),
                        /*   validator: (value) {
                                                                                      return formValidations.validateEmail(value);
                                                                                    },*/
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(25),
                          WhitelistingTextInputFormatter(RegExp("[a-zA-Z. ]")),
                        ],
                        controller: fNameController,
                        onChanged: (text) {
                          if (text.length > 0) {
                            setState(() {
                              validFirstName = false;
                            });
                          }
                        },
                        style: TextStyle(fontSize: 14),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(8),
                            hintText: 'First Name',
                            hintStyle: TextStyle(fontSize: 14),
                            errorText: validFirstName
                                ? 'First Name is required.'
                                : null,
                            errorBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1)),
                            focusedBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            ),
                            enabledBorder: UnderlineInputBorder(
                                //borderRadius: BorderRadius.circular(10.0),
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1))),
                        validator: (value) {
                          return formValidations.validateTextField(
                              value, 'First Name');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Expanded(
                      child: TextFormField(
                        onChanged: (text) {
                          if (text.length > 0) {
                            setState(() {
                              validLastName = false;
                            });
                          }
                        },
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(25),
                          WhitelistingTextInputFormatter(RegExp("[a-zA-Z. ]")),
                        ],
                        controller: lNameController,
                        style: TextStyle(fontSize: 14),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(8),
                            hintText: 'Last Name',
                            hintStyle: TextStyle(fontSize: 14),
                            errorText:
                                validLastName ? 'Last Name is required.' : null,
                            errorBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1)),
                            focusedBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            ),
                            enabledBorder: UnderlineInputBorder(
                                //borderRadius: BorderRadius.circular(10.0),
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1))),
                        validator: (value) {
                          return formValidations.validateTextField(
                              value, 'Last Name');
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: addressController,
                        onChanged: (text) {
                          if (text.length > 0) {
                            setState(() {
                              validAddress = false;
                            });
                          }
                        },
                        style: TextStyle(fontSize: 14),
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(100),
                        ],
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(8),
                            hintText: 'Address',
                            hintStyle: TextStyle(fontSize: 14),
                            errorText:
                                validAddress ? 'Address is required.' : null,
                            errorBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1)),
                            focusedBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            ),
                            enabledBorder: UnderlineInputBorder(
                                //borderRadius: BorderRadius.circular(10.0),
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1))),
                        validator: (value) {
                          return formValidations.validateTextField(
                              value, 'Address');
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        hint: new Text(
                          "Country",
                          style: TextStyle(fontSize: 13),
                        ),
                        value: selectedCountry,
                        onChanged: (String newValue) {
                          if (newValue != selectedCountry) {
                            setState(() {
                              selectedCountry = newValue;
                              selectedState = null;
                              getStateList(selectedCountry);
                              zipController.text = '';
                              cityController.text = '';
                            });
                          }
                        },
                        items: countryList.map((AppcodeItems appItems) {
                          return new DropdownMenuItem<String>(
                            value: appItems.applicationCodeId.toString(),
                            child: new Text(
                              appItems.name,
                              style: TextStyle(fontSize: 13),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Expanded(
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        //underline:
                        hint: new Text(
                          "State",
                          style: TextStyle(fontSize: 13),
                        ),
                        value: selectedState,
                        onChanged: (String newValue) {
                          if (newValue != selectedState) {
                            setState(() {
                              selectedState = newValue;
                              cityController.text = '';
                              zipController.text = '';
                            });
                          }
                        },
                        items: selectedStateList.map((AppcodeItems appItems) {
                          return new DropdownMenuItem<String>(
                            value: appItems.applicationCodeId.toString(),
                            child: new Text(
                              appItems.name,
                              style: TextStyle(fontSize: 13),
                            ),
                          );
                        }).toList(),
                        validator: (value) {
                          print('DropDownValue');
                          return formValidations.validateTextField(
                              value ?? '', 'State');
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(100),
                          WhitelistingTextInputFormatter(RegExp("[a-zA-Z. -]")),
                        ],
                        controller: cityController,
                        onChanged: (String text) {
                          if (text.length > 0) {
                            setState(() {
                              validCity = false;
                            });
                          }
                        },
                        style: TextStyle(fontSize: 14),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(8),
                            hintText: 'City',
                            errorText: validCity ? 'City is required' : null,
                            hintStyle: TextStyle(fontSize: 14),
                            errorBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1)),
                            focusedBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            ),
                            enabledBorder: UnderlineInputBorder(
                                //borderRadius: BorderRadius.circular(10.0),
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1))),
                        validator: (value) {
                          return formValidations.validateTextField(
                              value, 'City');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Expanded(
                      child: TextFormField(
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(15),
                          WhitelistingTextInputFormatter(RegExp("[0-9 -]")),
                        ],
                        controller: zipController,
                        onChanged: (String text) {
                          if (text.length > 0) {
                            setState(() {
                              validZipCode = false;
                            });
                          }
                        },
                        style: TextStyle(fontSize: 13),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(8),
                            hintText: 'Zip code',
                            hintStyle: TextStyle(fontSize: 13),
                            errorText:
                                validZipCode ? 'Zip code is required.' : null,
                            errorBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.red, width: 1),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1),
                            )),
                        validator: (value) {
                          return formValidations.validateTextField(
                              value, 'Zip code');
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        elevation: 0,
                        color: Colors.red,
                        textColor: Colors.white,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          "Register",
                          style: TextStyle(fontSize: 12),
                        ),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        onPressed: () {
                          if (!_formKey.currentState.validate() ||
                              phoneController.text.isEmpty ||
                              emailController.text.isEmpty ||
                              fNameController.text.isEmpty ||
                              lNameController.text.isEmpty ||
                              addressController.text.isEmpty) {
                            setState(() {
                              phoneController.text.isEmpty
                                  ? validPhoneNumber = true
                                  : validPhoneNumber = false;

                              emailController.text.isEmpty
                                  ? validEmail = true
                                  : validEmail = false;

                              fNameController.text.isEmpty
                                  ? validFirstName = true
                                  : validFirstName = false;

                              lNameController.text.isEmpty
                                  ? validLastName = true
                                  : validLastName = false;

                              addressController.text.isEmpty
                                  ? validAddress = true
                                  : validAddress = false;

                              cityController.text.isEmpty
                                  ? validCity = true
                                  : validCity = false;

                              zipController.text.isEmpty
                                  ? validZipCode = true
                                  : validZipCode = false;
                              if (formValidations
                                      .validateEmail(emailController.text) ==
                                  null) {
                                isEmailError = false;
                              } else {
                                isEmailError = true;
                              }
                            });
                          } else if (_formKey.currentState.validate()) {
                            var createUserBody = {};
                            createUserBody["firstName"] = fNameController.text;
                            createUserBody["lastName"] = lNameController.text;
                            createUserBody["mobileNumber"] =
                                selectedCountryCode +
                                    '-' +
                                    phoneController.text;
                            createUserBody["email"] = emailController.text;
                            createUserBody["address"] = addressController.text;
                            createUserBody["city"] = "Chennai";
                            createUserBody["stateId"] = selectedState;
                            createUserBody["countryId"] = selectedCountry;
                            createUserBody["zipCode"] = zipController.text;

                            String createUserJson =
                                convert.jsonEncode(createUserBody);

                            print(createUserJson);

                            createNewUser(createUserJson);
                          }
                        },
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      RaisedButton(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        textColor: Colors.black,
                        color: Colors.grey[200],
                        elevation: 0,
                        child: Text(
                          "Clear",
                          style: TextStyle(fontSize: 12),
                        ),
                        onPressed: () {
                          clearForm();
                        },
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ),
      constraints: BoxConstraints(
          minWidth: 240, maxWidth: MediaQuery.of(context).size.width - 400),
    );
  }

  clearForm() async {
    fNameController.clear();
    lNameController.clear();
    emailController.clear();
    phoneController.clear();
    addressController.clear();
    cityController.clear();
    zipController.clear();

    print(selectedCountry + ' selectedCountry');
    print(selectedState + ' selectedState');

    print(selectedCountryDuplicate + ' selectedCountryDup');
    print(selectedStateDuplicate + ' selectedStateDup');

    selectedCountry = null;
    selectedState = null;
    //selectedCountryCode = null;

    selectedCountry = selectedCountryDuplicate;
    selectedState = selectedStateDuplicate;

    getStateList(selectedCountry);

    print(selectedCountry + ' selectedCountry');
    print(selectedState + ' selectedState');
  }

  getAppCodes() async {
    var jsonResponse = await networkHelper.getAppCodeList(widget.token);

    print('app codes :' + jsonResponse.toString());
    jsonResponse.forEach((item) {
      appCodeItems.add(AppcodeItems.fromJson(item));
    });

    appCodeItems.forEach((appCode) {
      if (appCode.applicationCodeType == 'Country') {
        countryList.add(appCode);
      }
    });

    appCodeItems.forEach((appCode) {
      if (appCode.applicationCodeType == 'State') {
        stateList.add(appCode);
      }
    });

    appCodeItems.forEach((appCode) {
      if (appCode.applicationCodeType == 'CountryCode') {
        if (!countryCodeList.contains(appCode.name)) {
          countryCodeList.add(appCode.name);
          countryCodeIdList.add(appCode.applicationCodeId);
        }

        if (countryMap.containsKey(appCode.applicationCodeId)) {
          countryMap[appCode.applicationCodeId] = appCode.name;
        } else {
          countryMap[appCode.applicationCodeId] = appCode.name;
        }
      }
      /*  countryCodeList.sort((a, b) {
                                        return a.toString().toLowerCase().compareTo(b.toString().toLowerCase());
                                      }); */
    });

    print(countryMap.toString() + 'map of country');
  }

  getStateList(String countryId) async {
    selectedStateList.clear();
    setState(() {
      stateList.forEach((item) {
        if (item.referenceId == countryId) {
          selectedStateList.add(item);
        }
      });
      selectedStateList.sort((a, b) {
        return a.name
            .toString()
            .toLowerCase()
            .compareTo(b.name.toString().toLowerCase());
      });
    });
  }

  createNewUser(String createUserJson) async {
    var jsonResponse =
        await networkHelper.createNewUser(createUserJson, widget.token);
    DevoteeRegResponse rsm = DevoteeRegResponse.fromJson(jsonResponse);
    if (rsm.success) {
      /* Scaffold.of(context).showSnackBar(new SnackBar(
                                        backgroundColor: Colors.green,
                                        content: new Text("Devotee registered succesfully"),
                                      ));*/

      commonComponents.showOverlayMessage(context,
          'Devotee registered successfully', Colors.green, Colors.white);
      clearForm();
    } else {
      /* Scaffold.of(context).showSnackBar(new SnackBar(
                                        backgroundColor: Colors.red,
                                        content: new Text(rsm.message),
                                      ));*/

      commonComponents.showOverlayMessage(
          context, rsm.message, Colors.deepOrange, Colors.white);
    }
  }

  Future<dynamic> getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();

    //print('Get tenant info response : ' + response.toString());

    //TenantInfo tenantInfo = TenantInfo.fromJson(response);
    TenantInfo tenantInfo = await localStorage.getTenantInfoFull();

    countryMap.forEach((key, value) {
      if (key == tenantInfo.tenantDetails.address.countryCode) {
        selectedCountryCode = value;
        print('inside country code');
        print(key);
      }
    });

    selectedCountry = tenantInfo.tenantDetails.address.countryId;
    selectedState = tenantInfo.tenantDetails.address.stateId;
    selectedCountryDuplicate = tenantInfo.tenantDetails.address.countryId;
    selectedStateDuplicate = tenantInfo.tenantDetails.address.stateId;

    setState(() {
      //print('country code' + tenantInfo.tenantDetails.address.countryCode);
      //selectedCountryCode = tenantInfo.tenantDetails.address.countryCode;
      //zipController.text = tenantInfo.tenantDetails.address.zipCode;
      //cityController.text = tenantInfo.tenantDetails.address.city;
      getStateList(selectedCountry);
    });
  }

  getTenantData() async {
    await getAppCodes();
    await getTenantInfo();
  }

  void checkIfMobileNumExists(String emailOrMobileNumber, bool isEmail) async {
    if (isEmail) {
      if (FormValidations().validateEmail(emailOrMobileNumber) == null) {
        checkUserExists(emailOrMobileNumber, true);
      }
    } else {
      if (emailOrMobileNumber.length >= 10) {
        checkUserExists(emailOrMobileNumber, false);
      }
    }
  }

  checkUserExists(
    String emailOrMobileNumber,
    bool isEmail,
  ) async {
    print('entered mobile number' + emailOrMobileNumber);

    var checkMemberExists = {};
    checkMemberExists["userName"] = emailOrMobileNumber;

    String loginJson = convert.jsonEncode(checkMemberExists);
    var jsonResponse =
        await networkHelper.toCheckEmailIdExists(loginJson, widget.token);
    print(jsonResponse);

    if (jsonResponse != null) {
      if (jsonResponse['success']) {
        if (jsonResponse['message'] != '') {
          setState(() {
            if (isEmail) {
              isEmailExists = true;
            } else {
              isPhoneExists = true;
            }
          });
        } else {
          setState(() {
            if (isEmail) {
              isEmailExists = false;
            } else {
              print('getting into phone exists');
              isPhoneExists = false;
            }
          });
        }

        //return true;
      } else {
        setState(() {
          if (isEmail) {
            isEmailExists = true;
          } else {
            print('getting into phone exists');
            isPhoneExists = true;
          }
        });
      }
    }
  }

  getEmailError() {
    if (isEmailExists) {
      return emailExistsErrorText;
    } else if (validEmail) {
      return 'Email is required';
    } else if (isEmailError) {
      return 'Enter Valid email';
    } else {
      return null;
    }
  }

  String getPhoneNumberErrorText() {
    String error;

    if (isPhoneExists) {
      error = phoneExistsErrorText;
    } else if (validPhoneNumber) {
      error = 'Mobile Number is required.';
    } else if (mobileNumberLen) {
      error = 'Mobile Number should be 10 digits.';
    } else {
      error = null;
    }

    return error;
  }
}

import 'dart:io';

import 'package:chakra/admin/DatatableDemo.dart';
import 'package:chakra/admin/DevoteeRegistration.dart';
import 'package:chakra/models/ClosingReports/BillCollectors.dart';
import 'package:chakra/models/ClosingReports/CollectedReceipts.dart'
    as closingReceiptObj;
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart';
import 'package:chakra/utils/DateHelper.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:chakra/widgets/common_components.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;

import 'ReturnAndRefunds.dart';

class AdminWidget extends StatefulWidget {
  final bool isAdminRoleActive;
  final String selectedAdmin;
  final String token;
  final int reportPrivilege;
  final String userId;

  AdminWidget(
      {Key key,
      @required this.isAdminRoleActive,
      @required this.selectedAdmin,
      @required this.token,
      @required this.userId,
      @required this.reportPrivilege})
      : super(key: key);

  @override
  _AdminWidgetState createState() => _AdminWidgetState();
}

class _AdminWidgetState extends State<AdminWidget> {
  String selectedDate = DateHelper().getCurrentDate();
  NetworkHelper networkHelper = NetworkHelper();
  List<BillCollectors> users = [];
  bool isDataLoaded = false;
  String receiptsString = '';
  String selectedUser = '';
  TenantInfo tenantInfo;
  bool isUserList = false;
  bool firstym = true;
  bool isWidget = false;
  @override
  void initState() {
    super.initState();
    if (widget.selectedAdmin == 'Closing Receipt') {
      getCollectorsListMain();
    }
    getTenantInfo();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget container;

    switch (widget.selectedAdmin) {
      case 'Devotee Enrollment':
        container = DevoteeRegistration(
          token: widget.token,
          context: context,
        );
        break;
      case 'Return / Refund':
        container = DataTableDemo(
          token: widget.token,
          context: context,
          actionPrevilege: widget.reportPrivilege,
        );
        break;
      case 'Closing Receipt':
        container = Container(
            padding: EdgeInsets.all(10),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/reports.png',
                      width: 40,
                      height: 40,
                    ),
                    Text(
                      'Closing Receipt',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    OutlineButton(
                      borderSide: BorderSide(color: Colors.deepOrange),
                      onPressed: () {
                        setState(() {
                          _selectDate(context);
                        });
                      },
                      child: Text(selectedDate,
                          style: TextStyle(color: Colors.black)),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: DropdownButton<String>(
                        isDense: true,
                        hint: Text(
                          "Select a User",
                          style: TextStyle(color: Colors.black),
                        ),
                        value: selectedUser.isNotEmpty ? selectedUser : null,
                        onChanged: (String newValue) {
                          print('new Value : $newValue');
                          setState(() {
                            selectedUser = newValue;
                            if (selectedUser != null && selectedUser != '') {
                              users.forEach((element) {
                                if (element.userCollectedName == selectedUser) {
                                  getRecordsForUser(
                                      selectedDate, element.userCollectedId);
                                }
                              });
                            }
                          });
                        },
                        items: users.map((BillCollectors user) {
                          return new DropdownMenuItem<String>(
                            value: user.userCollectedName.toString(),
                            child: new Text(
                              user.userCollectedName,
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    //),
                    Visibility(
                      visible: isDataLoaded ? true : false,
                      child: IconButton(
                        icon: Icon(Icons.print),
                        onPressed: () {
                          printReceipt('');
                        },
                      ),
                    )
                  ],
                ),
                users.length == 0
                    ? Expanded(
                        child: Padding(
                            padding: EdgeInsets.all(20),
                            child: isUserList
                                ? Text('No transactions available')
                                : Text('')))
                    : Expanded(
                        child: Center(
                            child: Container(
                                padding: EdgeInsets.all(10),
                                constraints: BoxConstraints(maxWidth: 360),
                                child: SingleChildScrollView(
                                  child: Html(
                                    data: receiptsString,
                                    defaultTextStyle: TextStyle(fontSize: 13),
                                  ),
                                ))),
                      )
              ],
            )));
        break;
      default:
        return Container(
          height: 0,
          width: 0,
        );
        break;
    }
    return container;
  }

  _selectDate(BuildContext context) async {
    print(selectedDate);
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateFormat('MM-dd-yyyy').parse(selectedDate),
        firstDate: new DateTime(1990),
        lastDate: new DateTime(2099));
    if (picked != null) await getCollectorsList();
    print('Before setState');
    print('After setState');
    selectedDate = DateFormat('MM-dd-yyyy').format(picked);
    if (selectedUser != null && selectedUser != '' && selectedUser.length > 0) {
      if (users.length > 0) {
        print(users.length);
        users.forEach((element) {
          if (element.userCollectedName == selectedUser) {
            getRecordsForUser(selectedDate, element.userCollectedId);
          }
        });
      } else {
        isUserList = true;
      }
    } else {
      isUserList = true;
    }
    setState(() {});
  }

  Future<dynamic> getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var tenantInfos = await localStorage.getTenantInfoFull();
    setState(() {
      tenantInfo = tenantInfos;
    });
  }

  void getRecordsForUser(String recordDate, String userId) async {
    var reportsBody = {};
    reportsBody["transactionDate"] = recordDate;
    reportsBody["userId"] = userId;
    String reportsJson = convert.jsonEncode(reportsBody);
    var response =
        await networkHelper.getRecordsForUser(reportsJson, widget.token);
    var collectedReceipts =
        closingReceiptObj.CollectedReceipts.fromJson(response);
    List<closingReceiptObj.ClosingReceiptDetails> itemsList =
        collectedReceipts.closingReceipt;

    String htmlString = "<html><body><div id=\"invoice\"><center id=\"top\">" +
        "<div style=height:100%; width:100%>" +
        "<div style='width:25%'><img src=${tenantInfo.tenantDetails.additionalProperties.receiptLogo} width=80, height=80></div>" +
        "<div  style='width:75%'><h5>${tenantInfo.tenantDetails.name}</h5>" +
        "<p>${tenantInfo.tenantDetails.address.address}" +
        "<br>${tenantInfo.tenantDetails.address.city}," +
        "<br>Tel: ${tenantInfo.tenantDetails.additionalProperties.phoneNumber}" +
        //"<br>${tenantInfo.tenantDetails.additionalProperties.templeMail}" +
        "<br>${tenantInfo.tenantDetails.additionalProperties.websiteUrl} <br></p>" +
        "</div></div>" +
        "</center>";
    //htmlString = htmlString +
    //</center>";
    htmlString = htmlString +
        "<div id=\"bot\"><div id=\"table\"><table class=\itemtable\"><tr><th>Item</th><th>Quantity</th><th>Amount (\$)</th></tr>";
    itemsList.forEach((items) {
      htmlString =
          htmlString + '<div>Collected by : ${items.collectedBy}</div>';
      List<closingReceiptObj.ItemDetails> itemDetails = items.itemDetails;
      double grandTotal = items.grandTotal;
      itemDetails.forEach((i) {
        //grandTotal = grandTotal + int.parse('${i.total}');
        htmlString = htmlString +
            '<tr><td width=\"50%\">${i.name}</td><td>${i.itemCount}</td><td> ${i.total}</td></tr>';
      });

      htmlString = htmlString +
          '</table><div>Cash transaction : \$ ${items.cashTotal}</div>' +
          '<div>Check transaction : \$ ${items.checkTotal}</div>' +
          '<div>Card Transaction : \$ ${items.cardTotal}</div>' +
          '<div>Total Transaction : \$ ${grandTotal.toString()}</div>';
    });

    htmlString = htmlString + '</div></div></div></body></html>';

    setState(() {
      isDataLoaded = true;
      receiptsString = htmlString;
    });
  }

  getCollectorsList() async {
    receiptsString = '';
    users.clear();
    var response =
        await networkHelper.getBillCollectors(selectedDate, widget.token);

    selectedUser = '';
    isUserList = false;
    print('Inside getCollectorsList');

    print(users.length);
    isWidget = false;

    if (response != null) {
      List billCollectors = response['collectedByUserList'];

      if (billCollectors.length > 0) {
        print('billCollectors ***');
        billCollectors.forEach((user) {
          if (widget.reportPrivilege == 1) {
            if (BillCollectors.fromJson(user).userCollectedId ==
                widget.userId) {
              users.add(BillCollectors.fromJson(user));
              selectedUser = user['collectedByUserName'];

              isWidget = true;

              print('******** ' + user['collectedByUserName']);
            }
          } else {
            users.add(BillCollectors.fromJson(user));
            selectedUser = '';

            // print('----- ' + user.userCollectedName);
          }
        });
      } else {
        isUserList = true;
        selectedUser = '';
      }

      if (firstym) {
        firstym = false;
        if (selectedUser != null &&
            selectedUser != '' &&
            selectedUser.length > 0) {
          if (users.length > 0) {
            print(users.length);
            users.forEach((element) {
              if (element.userCollectedName == selectedUser) {
                getRecordsForUser(selectedDate, element.userCollectedId);
              }
            });
          } else {
            isUserList = true;
          }
        } else {
          isUserList = true;
        }
      }

      print(selectedUser);
      print(users.length);
      print(isUserList);
    }
  }

  void printReceipt(String orderJson) {
    Socket.connect("localhost", 2500).then((socket) async {
      print('Connected to: '
          '${socket.remoteAddress.address}:${socket.remotePort}');

      //Establish the onData, and onDone callbacks
      socket.listen((data) {
        print('listening');
        print(new String.fromCharCodes(data).trim());
      }, onDone: () {
        print("Done");
        socket.destroy();
      });
      socket.write(receiptsString);
    });
  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  void getCollectorsListMain() async {
    await getCollectorsList();
  }
}

import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/models/ReturnRefunds/Donations.dart';
import 'package:chakra/models/ReturnRefunds/OrderDetails.dart';
import 'package:chakra/models/ReturnRefunds/RefundOrderByCash.dart';
import 'package:chakra/models/ReturnRefunds/RefundOrderByCheck.dart';
import 'package:chakra/models/ReturnRefunds/RepaymentByCash.dart';
import 'package:chakra/models/ReturnRefunds/RepaymentByCheck.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'package:chakra/utils/DateHelper.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;

import '../widgets/common_components.dart';

class ReturnAndRefunds extends StatefulWidget {
  final String token;
  final BuildContext context;

  @override
  _ReturnAndRefundsState createState() => _ReturnAndRefundsState();
  const ReturnAndRefunds(
      {Key key, @required this.token, @required this.context})
      : super(key: key);
}

class _ReturnAndRefundsState extends State<ReturnAndRefunds> {
  NetworkHelper networkHelper = new NetworkHelper();
  final _formKey = GlobalKey<FormState>();
  List<DonationItems> items = [];
  List<DonationItems> filterItems = [];
  DateTime selectedDate = DateTime.now();
  DateTime newCheckDate = DateTime.now();

  var commonComponents = new CommonComponents();

  TextEditingController nameController = TextEditingController();
  TextEditingController receiptNumController = TextEditingController();
  var dateHelper = new DateHelper();
  bool cashVal = false;
  bool checkVal = false;
  bool cardVal = false;
  String tenantName;
  String tenantAddress;
  String tenantLogo;
 tenant.TenantInfo tenantInfo;

  List<DonationItems> itemsToView = [];
  List<OrderItems> selectedItems;

  bool loadiing = false;

  Widget bodyData(List<DonationItems> itemsToView) {
    return DataTable(
        sortAscending: true,
        sortColumnIndex: 0,
        columns: <DataColumn>[
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Date",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            numeric: true,
            onSort: (i, b) {
              setState(() {
                itemsToView.sort(
                    (a, b) => a.transactionDate.compareTo(b.transactionDate));
              });
            },
            /*   onSort: (i, b) {
            setState(() {
              items.sort(
                  (b, a) => a.transactionDate.compareTo(b.transactionDate));
            });
          }, */
          ),
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Receipt Num",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            numeric: true,
            onSort: (i, b) {
              print("$i $b");
              setState(() {
                itemsToView
                    .sort((a, b) => a.receiptNumber.compareTo(b.receiptNumber));
              });
            },
          ),
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Name",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            numeric: false,
            onSort: (i, b) {
              print("$i $b");
              setState(() {
                itemsToView.sort((a, b) => a.firstName.compareTo(b.firstName));
              });
            },
          ),
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Service",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            numeric: false,
            onSort: (i, b) {
              print("$i $b");
              setState(() {
                itemsToView
                    .sort((a, b) => a.serviceName.compareTo(b.serviceName));
              });
            },
          ),
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Payment mode",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            numeric: false,
            onSort: (i, b) {
              print("$i $b");
              setState(() {
                itemsToView
                    .sort((a, b) => a.paymentType.compareTo(b.paymentType));
              });
            },
          ),
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Amount (\$)",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            onSort: (i, b) {
              print("$i $b");
              setState(() {
                itemsToView.sort((a, b) => a.amount.compareTo(b.amount));
              });
            },
            numeric: true,
          ),
          DataColumn(
            label: Expanded(
              child: Center(
                  child: Text("Action",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrange,
                          fontSize: 12),
                      softWrap: true)),
            ),
            numeric: false,
          ),
        ],
        rows: itemsToView
            .map(
              (item) => DataRow(
                cells: [
                  DataCell(
                    Text(
                      //item.transactionDate.substring(0, 10),
                      //dateHelper.getFromattedDate(item.transactionDate),
                      item.transactionDate,
                      textAlign: TextAlign.center,
                    ),
                    showEditIcon: false,
                    placeholder: false,
                  ),
                  DataCell(
                    Center(
                      child: FlatButton(
                        child: Text(
                          item.receiptNumber.toString(),
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        onPressed: () {
                          getPrintReceiptDetails(item.orderId,
                              item.receiptNumber.toString(), context);
                        },
                      ),
                    ),
                    showEditIcon: false,
                    placeholder: false,
                  ),
                  DataCell(
                    Center(
                      child: Text(item.firstName + ' ' + item.lastName),
                    ),
                    showEditIcon: false,
                    placeholder: false,
                  ),
                  DataCell(
                    Center(
                      child: Text(toBeginningOfSentenceCase(
                          item.serviceName.toString().toLowerCase())),
                    ),
                    showEditIcon: false,
                    placeholder: false,
                  ),
                  DataCell(
                    Center(
                      child: Text(item.paymentType),
                    ),
                    showEditIcon: false,
                    placeholder: false,
                  ),
                  DataCell(
                    Center(
                      child: Text('${item.totalAmount}.00'),
                    ),
                    showEditIcon: false,
                    placeholder: false,
                  ),
                  DataCell(Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      item.paymentType == "Check" &&
                              (item.type == '' || item.type == 'Repayment')
                          ? InkWell(
                              //textColor: Colors.deepOrange,
                              child:
                                  (item.type == '' || item.type == 'Repayment')
                                      ? Padding(
                                          padding: EdgeInsets.all(5),
                                          child: Text(
                                            'Return',
                                            style: TextStyle(
                                                color: Colors.deepOrange,
                                                fontSize: 11,
                                                fontWeight: FontWeight.bold,
                                                decoration:
                                                    TextDecoration.underline),
                                          ))
                                      : Text(''),
                              onTap: () {
                                if (item.type != 'Returned') {
                                  getOrderDetails(item.orderId, true, true,
                                      item.paymentType);
                                }
                              },
                            )
                          : SizedBox(
                              height: 0,
                              width: 0,
                            ),
                      Visibility(
                        visible: (item.type == '' || item.type == 'Repayment')
                            ? true
                            : false,
                        child: InkWell(
                          child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Text(
                                'Refund',
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              )),
                          onTap: () {
                            if (item.paymentType == 'Credit/Debit Card') {
                              getOrderDetails(
                                  item.orderId, false, false, 'Cash');
                            } else {
                              getOrderDetails(
                                  item.orderId, false, false, item.paymentType);
                            }
                          },
                        ),
                      ),
                      Visibility(
                        visible: item.type == 'Refunded' ? true : false,
                        child: InkWell(
                          child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Text(
                                'Refunded',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              )),
                          onTap: () {
                            /*  getOrderDetails(
                                                        item.orderId, false, false, item.paymentType); */
                          },
                        ),
                      ),
                      Visibility(
                        visible: item.type == 'Returned' ? true : false,
                        child: InkWell(
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                'Repay',
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              )),
                          onTap: () {
                            getOrderDetails(
                                item.orderId, false, true, item.paymentType);
                          },
                        ),
                      )
                    ],
                  ))),
                ],
              ),
            )
            .toList());
  }

  @override
  void initState() {
    super.initState();
    getAllDonations();
    getAllDonationsItems();
    selectedItems = [];

    getTenantDetails();
  }

  onSelectedRow(bool selected, OrderItems orderItems) async {
    //print(isRefundOrder);
    setState(() {
      if (orderItems.returnRefundDate == null) {
        if (selected) {
          selectedItems.add(orderItems);
        } else {
          selectedItems.remove(orderItems);
        }
      } else {
        commonComponents.showOverlayMessage(context,
            'This item is already returned', Colors.deepOrange, Colors.white);
      }
    });
  }

  getTenantDetails() async {
    await getTenantInfo();
  }

  /*  getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var response = await localStorage.getTenantInfo(); */

  Future<dynamic> getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    tenant.TenantInfo tenantInfo = await localStorage.getTenantInfoFull();

    //tenantInfo = TenantInfo.fromJson(response);

    setState(() {
      tenantName = tenantInfo.tenantDetails.name;
      tenantAddress = tenantInfo.tenantDetails.address.address;
      tenantLogo = tenantInfo.tenantDetails.additionalProperties.receiptLogo;

      print('tenant Logo : $tenantLogo');

      /*  selectedState = tenantInfo.tenantDetails.address.stateId;
      //selectedCountryCode = tenantInfo.tenantDetails.address.countryCode;
      zipController.text = tenantInfo.tenantDetails.address.zipCode;
      cityController.text = tenantInfo.tenantDetails.address.city;
      getStateList(selectedCountry); */
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/refund.png',
                      width: 40,
                      height: 40,
                    ),
                    Text(
                      'Return and Refunds',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Container(
                      padding: EdgeInsets.all(10),
                      height: 60,
                      color: Colors.deepOrange,
                      child: Form(
                        key: _formKey,
                        child: Row(
                          children: <Widget>[
                            Text(
                              'Search by : ',
                              style: TextStyle(
                                  color: Colors.white54,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                            ),
                            Container(
                              width: 180,
                              padding: EdgeInsets.all(5),
                              child: TextFormField(
                                  controller: nameController,
                                  onChanged: (editedName) {
                                    if (editedName.length > 0) {
                                      print(items.length);
                                      //getFilterItems(items, editedName);
                                      getItemsBasedOnSearch();
                                    } else {
                                      if (receiptNumController.text.length ==
                                          0) {
                                        itemsToView.clear();
                                        addItemsToView(items);
                                        setState(() {});
                                      } else {
                                        getItemsBasedOnSearch();
                                      }
                                    }
                                  },
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500),
                                  cursorColor: Colors.white,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(8),
                                      hintText: 'Name',
                                      hintStyle: TextStyle(
                                          color: Colors.white60,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w500),
                                      //border: InputBorder.none
                                      border: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white)))),

                              /*   decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.white),
                                  borderRadius: BorderRadius.circular(30)), */
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 180,
                              padding: EdgeInsets.all(5),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  WhitelistingTextInputFormatter.digitsOnly
                                ],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500),
                                cursorColor: Colors.white,
                                controller: receiptNumController,
                                onChanged: (editedName) {
                                  if (editedName.length > 0) {
                                    print(items.length);
                                    //getFilterItemsForReceipt(items, editedName);
                                    getItemsBasedOnSearch();
                                  } else {
                                    if (nameController.text.length == 0) {
                                      itemsToView.clear();
                                      addItemsToView(items);
                                      setState(() {});
                                    } else {
                                      getItemsBasedOnSearch();
                                    }
                                  }
                                },
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    hintText: 'Receipt Number',
                                    hintStyle: TextStyle(
                                        color: Colors.white60,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w500),

                                    //focusedBorder: InputBorder( borderSide: )
                                    border: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white60))),
                              ),
                              /*  decoration: BoxDecoration(
                                                                        color: Colors.white,
                                                                        border: Border.all(color: Colors.white),
                                                                        borderRadius: BorderRadius.circular(30)), */
                            ),
                            Theme(
                                data: Theme.of(context).copyWith(
                                  unselectedWidgetColor: Colors.white,
                                ),
                                child: Checkbox(
                                  checkColor: Colors.white,
                                  value: cashVal,
                                  onChanged: (bool value) {
                                    if (nameController.text.length > 0 ||
                                        receiptNumController.text.length > 0) {
                                      setState(() {
                                        cashVal = value;
                                        getItemsBasedOnSearch();
                                      });
                                    } else {
                                      print('inside elsedddddddddddd');
                                      print(items.length);

                                      setState(() {
                                        cashVal = value;
                                        checkOnlyCheckBoxesNew();
                                      });
                                    }
                                  },
                                )),
                            Text(
                              'Only cash',
                              style: TextStyle(color: Colors.white),
                            ),
                            Theme(
                                data: Theme.of(context).copyWith(
                                  unselectedWidgetColor: Colors.white,
                                ),
                                child: Checkbox(
                                  checkColor: Colors.white,
                                  value: checkVal,
                                  onChanged: (bool value) {
                                    if (nameController.text.length > 0 ||
                                        receiptNumController.text.length > 0) {
                                      setState(() {
                                        checkVal = value;
                                        getItemsBasedOnSearch();
                                      });
                                    } else {
                                      print('inside elsedddddddddddd');
                                      print(items.length);

                                      setState(() {
                                        checkVal = value;
                                        checkOnlyCheckBoxesNew();
                                      });
                                    }
                                  },
                                )),
                            Text(
                              'Only check',
                              style: TextStyle(color: Colors.white),
                            ),
                            Theme(
                              data: Theme.of(context).copyWith(
                                unselectedWidgetColor: Colors.white,
                              ),
                              child: Checkbox(
                                checkColor: Colors.white,
                                value: cardVal,
                                onChanged: (bool value) {
                                  if (nameController.text.length > 0 ||
                                      receiptNumController.text.length > 0) {
                                    setState(() {
                                      cardVal = value;
                                      getItemsBasedOnSearch();
                                    });
                                  } else {
                                    print('inside elsedddddddddddd');
                                    print(items.length);

                                    setState(() {
                                      cardVal = value;
                                      checkOnlyCheckBoxesNew();
                                    });
                                  }
                                },
                              ),
                            ),
                            Text(
                              'Only card',
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Visibility(
                              visible: nameController.text.isNotEmpty ||
                                      receiptNumController.text.isNotEmpty ||
                                      checkVal ||
                                      cardVal ||
                                      cashVal
                                  ? true
                                  : false,
                              child: OutlineButton(
                                  padding: EdgeInsets.all(2),
                                  color: Colors.white,
                                  onPressed: () {
                                    print('length on clear' +
                                        itemsToView.length.toString());

                                    print('length on clear items' +
                                        items.length.toString());
                                    itemsToView.clear();

                                    addItemsToView(items);
                                    print('length on clear' +
                                        itemsToView.length.toString());

                                    setState(() {
                                      nameController.clear();
                                      receiptNumController.clear();
                                      checkVal = false;
                                      cashVal = false;
                                      cardVal = false;

                                      //itemsToView.addAll(items);
                                    });
                                  },
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  ),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                  child: Text(
                                    'Clear',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                          ],
                        ),
                      )),
                ),
                Expanded(
                    child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: loadiing
                            ? itemsToView.length > 0
                                ? bodyData(itemsToView)
                                : Container(
                                    child: Text('No Data Found'),
                                  )
                            : Center(child: CircularProgressIndicator())))
              ],
            )));
  }

  getAllDonations() async {
    NetworkHelper networkHelper = NetworkHelper();
    var response = await networkHelper.getAllDonations(widget.token);
    Donations donations = Donations.fromJson(response);
    setState(() {
      items = donations.donationItems;
    });
  }

  getAllDonationsItems() async {
    NetworkHelper networkHelper = NetworkHelper();
    var response = await networkHelper.getAllDonations(widget.token);
    Donations donations = Donations.fromJson(response);

    setState(() {
      itemsToView = donations.donationItems;
      loadiing = true;
      // items = donations.donationItems;
    });
  }

  getFilterItems(List<DonationItems> items, String searchText) {
    List<DonationItems> filteredItems = [];

    if (receiptNumController.text.length > 0) {
      items.forEach((item) {
        print(item.serviceType.compareTo(searchText));
        if (item.serviceType.compareTo(searchText) == 1) {
          filteredItems.add(item);
        }
      });
    } else {
      items.forEach((item) {
        print(item.serviceType.compareTo(searchText));
        if (item.serviceType.compareTo(searchText) == 1) {
          filteredItems.add(item);
        }
      });
    }

    if (filteredItems.length > 0) {
      setState(() {
        itemsToView.clear();
        itemsToView.addAll(List.from(filteredItems));
      });
    } else {
      itemsToView.clear();

      addItemsToView(items);

      setState(() {});
    }

    //print(filterItems);

    //return items.contains(searchText);
  }

  void getFilterItemsForReceipt(List<DonationItems> items, String searchText) {
    print('inside getFilterItems');

    print(items.length);
    List<DonationItems> filteredItems = [];

    if (nameController.text.length > 0) {
      items.forEach((item) {
        print(item.receiptNumber.toString().contains(searchText));

        if (item.receiptNumber.toString().contains(searchText)) {
          filteredItems.add(item);
        }
      });
    } else {
      items.forEach((item) {
        print(item.receiptNumber.toString().contains(searchText));

        if (item.receiptNumber.toString().contains(searchText)) {
          filteredItems.add(item);
        }
      });
    }

    if (filteredItems.length > 0) {
      setState(() {
        itemsToView.clear();
        itemsToView.addAll(List.from(filteredItems));
      });
    } else {
      itemsToView.clear();

      addItemsToView(items);

      setState(() {
        // itemsToView.clear();
      });
    }
  }

  void getItemsBasedOnSearch() {
    List<DonationItems> filteredItems = [];
    itemsToView.clear();

    print(nameController.text.length);
    if (nameController.text.length > 0) {
      items.forEach((item) {
        print(item.firstName.compareTo(nameController.text));
        String fullName = item.firstName + ' ' + item.lastName;
        if (fullName
            .toLowerCase()
            .trim()
            .contains(nameController.text.toLowerCase().trim())) {
          filteredItems.add(item);
        }
      });
      commonMethodToCheck(filteredItems);
    } else {
      commonMethodToCheck(filteredItems);
    }
  }

  void commonMethodToCheck(List<DonationItems> filteredItems) {
    List<DonationItems> finalfilteredItems = [];

    if (receiptNumController.text.length > 0) {
      if (filteredItems.length > 0) {
        filteredItems.forEach((item) {
          print(item.receiptNumber
              .toString()
              .contains(receiptNumController.text));

          if (item.receiptNumber
              .toString()
              .contains(receiptNumController.text)) {
            finalfilteredItems.add(item);
          }
        });
      } else {
        if (nameController.text.length == 0)
          items.forEach((item) {
            if (item.receiptNumber
                .toString()
                .contains(receiptNumController.text)) {
              finalfilteredItems.add(item);
            }
          });
      }
    } else {
      if (filteredItems.length > 0) {
        finalfilteredItems.addAll(filteredItems);
      }
    }

    if (finalfilteredItems.length > 0 && receiptNumController.text.length > 0) {
      itemsToView.addAll(finalfilteredItems);
      //setState(() {});
    } else if ((filteredItems.length > 0 &&
            nameController.text.length > 0 &&
            receiptNumController.text.length == 0) ||
        (filteredItems.length > 0 &&
            nameController.text.length == 0 &&
            receiptNumController.text.length > 0)) {
      itemsToView.addAll(filteredItems);
      //setState(() {});
    } else {
      itemsToView.clear();
      // setState(() {});
    }

    checkOnlyCheckBoxes(itemsToView);
  }

  Future<void> getOrderDetails(String orderId, bool isShowDialog,
      bool isCheckReturned, String paymentType) async {
    NetworkHelper networkHelper = new NetworkHelper();
    var response = await networkHelper.getOrderDetails(widget.token, orderId);

    OrderDetails orderDetails = OrderDetails.fromJson(response);
    List<OrderItems> orderItems = orderDetails.orderDetails;
    //orderItems.forEach((f) {});

    if (isShowDialog) {
      _showDialog(orderItems);
    } else {
      if (isCheckReturned) {
        _showDialogRepay(orderItems, false, paymentType);
      } else {
        _showDialogRepay(orderItems, true, paymentType);
      }
    }
  }

  _showDialogRepay(List<OrderItems> items, bool isRefund, String paymentType) {
    String dropdownValue = paymentType;
    TextEditingController reason = TextEditingController();
    TextEditingController payeeName = TextEditingController();
    TextEditingController bankName = TextEditingController();
    //TextEditingController dateOnTheCehck = TextEditingController();
    TextEditingController refundCheckNumber = TextEditingController();
    TextEditingController bankCharges = TextEditingController();
    bool _validPayeeName = false;
    //bool _validBankName = false;
    bool _validCheckNumber = false;
    bool _isValidReason = false;

    double totalAmount = 0.00;
    bankCharges.text = '0.00';
/* 
    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    }); */

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      toBeginningOfSentenceCase(
                          '${items[0].firstName} ${items[0].lastName}'),
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                  child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    DataTable(
                        showCheckboxColumn: isRefund ? true : false,
                        //sortColumnIndex: 0,
                        //sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Date",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Receipt Num",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Service Name",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Mode",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                            /*  onSort: (i, b) {
                                                          print("$i $b");
                                                          setState(() {
                                                            names.sort((a, b) => a.lastName.compareTo(b.lastName));
                                                          });
                                                        }, */
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Center(
                                  child: Text("Amount (\$)",
                                      style: TextStyle(
                                          //fontWeight: FontWeight.bold,
                                          color: Colors.deepOrange,
                                          fontSize: 12),
                                      softWrap: true)),
                            ),
                            numeric: false,
                          ),
                        ],
                        rows: items
                            .map(
                              (item) => DataRow(
                                selected: selectedItems.contains(item),
                                onSelectChanged: (b) {
                                  totalAmount = 0;
                                  setState(() {
                                    onSelectedRow(b, item);

                                    selectedItems.forEach((item) {
                                      totalAmount = totalAmount +
                                          double.parse(item.totalAmount);
                                    });
                                  });
                                },
                                cells: [
                                  DataCell(
                                    Center(
                                      child: Text(
                                        dateHelper.getFromattedDate(
                                            item.transactionDate),
                                        //item.transactionDate.substring(0, 10),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.receiptNumber.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.serviceName,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        item.paymentType,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                  DataCell(
                                    Center(
                                      child: Text(
                                        '${item.totalAmount}',
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      ),
                                    ),
                                    showEditIcon: false,
                                    placeholder: false,
                                  ),
                                ],
                              ),
                            )
                            .toList()),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 600),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Payment Type',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: DropdownButton<String>(
                                            isExpanded: true,
                                            value: dropdownValue,
                                            icon: Icon(Icons.arrow_drop_down),
                                            iconSize: 22,
                                            elevation: 16,
                                            hint: Text(
                                              'Select payment type',
                                              style: TextStyle(fontSize: 12),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                dropdownValue = newValue;
                                              });
                                            },
                                            items: <String>['Check', 'Cash']
                                                .map<DropdownMenuItem<String>>(
                                                    (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(
                                                  value,
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              );
                                            }).toList(),
                                          ))
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: (isRefund &&
                                                items[0].refundDetails == null)
                                            ? Text('Refunded On',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey))
                                            : Text('Transaction Date',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: OutlineButton(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                selectedDate
                                                    .toString()
                                                    .substring(0, 10),
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(5),
                                                child: Icon(
                                                  FontAwesomeIcons
                                                      .solidCalendarAlt,
                                                  color: Colors.deepOrange,
                                                  size: 14,
                                                ),
                                              )
                                            ],
                                          ),
                                          onPressed: () async {
                                            DateTime picked =
                                                await showDatePicker(
                                              context: context,
                                              initialDate: selectedDate,
                                              firstDate: DateTime(2000, 1),
                                              lastDate: DateTime.now(),
                                            );
                                            setState(() {
                                              if (picked != null) {
                                                selectedDate = picked;
                                              } else {
                                                selectedDate = DateTime.now();
                                              }
                                            });
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Amount (\$)',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          //items[0].totalAmount,
                                          '\$ ${totalAmount.toStringAsFixed(2)}',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Reason *',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: TextField(
                                          controller: reason,
                                          style: TextStyle(fontSize: 12),
                                          keyboardType: TextInputType.multiline,
                                          onChanged: (text) {
                                            if (text.length > 0) {
                                              setState(() {
                                                _isValidReason = false;
                                              });
                                            }
                                          },
                                          decoration: InputDecoration(
                                            errorText: _isValidReason
                                                ? 'Reason required'
                                                : null,
                                            hintStyle: TextStyle(fontSize: 12),
                                            hintText: 'Reason',
                                          ),
                                        ),
                                      )
                                      //)
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible: (isRefund &&
                                          items[0].refundDetails == null)
                                      ? false
                                      : true,
                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: Text('Bank charges (\$)',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.grey)),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                            flex: 2,
                                            child: Container(
                                              child: TextField(
                                                controller: bankCharges,
                                                decoration: InputDecoration(
                                                    hintText: '\$ 0.00',
                                                    border: UnderlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        borderSide: BorderSide(
                                                            color:
                                                                Colors.black))),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Text('Total amount to be paid',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              /* decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5),
                                                                        border: Border.all(
                                                                            color: Colors.grey)), */
                                              child: Text(
                                                  '\$ ${(totalAmount + double.parse(bankCharges.text)).toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.w500))))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 40,
                          ),
                          Expanded(
                              child: Column(
                            children: <Widget>[
                              Visibility(
                                visible:
                                    dropdownValue == "Check" ? true : false,
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Payee Name*',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: TextField(
                                                  onChanged: (text) {
                                                    if (text.length > 0) {
                                                      setState(() {
                                                        _validPayeeName = false;
                                                      });
                                                    }
                                                  },
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        100),
                                                  ],
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                  controller: payeeName,
                                                  decoration: InputDecoration(
                                                      errorText: _validPayeeName
                                                          ? 'Payee name required'
                                                          : null,
                                                      hintText: 'Payee Name',
                                                      hintStyle: TextStyle(
                                                          fontSize: 12),
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Check Number *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                /*  decoration: BoxDecoration(
                                                                            borderRadius:
                                                                                BorderRadius.circular(
                                                                                    5),
                                                                            border: Border.all(
                                                                                color: Colors.grey)), */
                                                child: TextField(
                                                  onChanged: (text) {
                                                    if (text.length > 0) {
                                                      setState(() {
                                                        _validCheckNumber =
                                                            false;
                                                      });
                                                    }
                                                  },
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        10),
                                                    WhitelistingTextInputFormatter
                                                        .digitsOnly,
                                                  ],
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                  controller: refundCheckNumber,
                                                  decoration: InputDecoration(
                                                      errorText: _validCheckNumber
                                                          ? 'Check number required'
                                                          : null,
                                                      hintText: 'Check Number',
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Check Date *',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: OutlineButton(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    newCheckDate
                                                        .toString()
                                                        .substring(0, 10),
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.all(5),
                                                    child: Icon(
                                                      FontAwesomeIcons
                                                          .solidCalendarAlt,
                                                      color: Colors.deepOrange,
                                                      size: 14,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              onPressed: () async {
                                                DateTime picked =
                                                    await showDatePicker(
                                                  context: context,
                                                  initialDate: newCheckDate,
                                                  firstDate: DateTime.now(),
                                                  lastDate: DateTime(2100, 12),
                                                );
                                                setState(() {
                                                  if (picked != null) {
                                                    newCheckDate = picked;
                                                  } else {
                                                    newCheckDate =
                                                        DateTime.now();
                                                  }
                                                });
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text('Bank Name',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey)),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: TextField(
                                                  controller: bankName,
                                                  inputFormatters: [
                                                    new LengthLimitingTextInputFormatter(
                                                        100),
                                                  ],
                                                  decoration: InputDecoration(
                                                      /*  errorText: _validBankName
                                                          ? 'Bank name cannot be empty'
                                                          : null, */
                                                      hintText: 'Bank Name',
                                                      hintStyle: TextStyle(
                                                          fontSize: 12),
                                                      border: UnderlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .black))),
                                                ),
                                              ))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Visibility(
                                visible: dropdownValue == "Card" ? true : false,
                                child: Center(
                                  child: RaisedButton(
                                    color: Theme.of(context).primaryColor,
                                    child: Text(
                                      'Click and swipe a card',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              )
                            ],
                          ))
                        ],
                      ),
                    ),
                    Center(
                      child: OutlineButton(
                        borderSide:
                            BorderSide(color: Theme.of(context).primaryColor),
                        child: isRefund
                            ? Text(
                                'Refund',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              )
                            : Text(
                                'Repay',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              ),
                        onPressed: () {
                          if (isRefund) {
                            if (dropdownValue == 'Cash') {
                              if (reason.text.isEmpty) {
                                setState(() {
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                });
                              } else {
                                refundAnOrderByCash(
                                    items[0].orderId,
                                    items[0].totalAmount,
                                    reason.text,
                                    selectedItems);
                              }
                            } else if (dropdownValue == 'Check') {
                              if (payeeName.text.isEmpty ||
                                  refundCheckNumber.text.isEmpty ||
                                  //bankName.text.isEmpty ||
                                  reason.text.isEmpty) {
                                setState(() {
                                  payeeName.text.isEmpty
                                      ? _validPayeeName = true
                                      : _validPayeeName = false;
                                  refundCheckNumber.text.isEmpty
                                      ? _validCheckNumber = true
                                      : _validCheckNumber = false;
                                  /*  bankName.text.isEmpty
                                      ? _validBankName = true
                                      : _validBankName = false; */
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                });
                              } else {
                                refundAnOrderByCheck(
                                    items[0].orderId,
                                    items[0].totalAmount,
                                    reason.text,
                                    selectedItems,
                                    payeeName.text,
                                    bankName.text != null ? bankName.text : '',
                                    refundCheckNumber.text,
                                    newCheckDate.toString().substring(0, 10));
                              }
                            }
                          } else {
                            if (dropdownValue == 'Cash') {
                              if (reason.text.isEmpty) {
                                reason.text.isEmpty
                                    ? _isValidReason = true
                                    : _isValidReason = false;
                              }
                              repayByCash(items[0].orderId,
                                  items[0].totalAmount, reason.text, items);
                            } else if (dropdownValue == 'Check') {
                              if (payeeName.text.isEmpty ||
                                  refundCheckNumber.text.isEmpty ||
                                  //bankName.text.isEmpty ||
                                  reason.text.isEmpty) {
                                setState(() {
                                  payeeName.text.isEmpty
                                      ? _validPayeeName = true
                                      : _validPayeeName = false;
                                  refundCheckNumber.text.isEmpty
                                      ? _validCheckNumber = true
                                      : _validCheckNumber = false;
                                  /*  bankName.text.isEmpty
                                      ? _validBankName = true
                                      : _validBankName = false; */
                                  reason.text.isEmpty
                                      ? _isValidReason = true
                                      : _isValidReason = false;
                                });
                              } else {
                                repayByCheck(
                                    items[0].orderId,
                                    items[0].totalAmount,
                                    reason.text,
                                    items,
                                    payeeName.text,
                                    bankName.text != null ? bankName.text : '',
                                    refundCheckNumber.text,
                                    newCheckDate.toString().substring(0, 10));
                              }
                            }
                          }
                        },
                      ),
                    )
                  ],
                ),
              ));
            }));
      },
    );
  }

  void _showDialog(
    List<OrderItems> items,
  ) {
    TextEditingController reasonController = TextEditingController();
    double totalAmount = 0.00;
    bool _isReasonValid = false;

    items.forEach((item) {
      totalAmount = totalAmount + double.parse(item.totalAmount);
    });

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      toBeginningOfSentenceCase(
                          '${items[0].firstName} ${items[0].lastName}'),
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Padding(
                  padding: EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        DataTable(
                            onSelectAll: (b) {},
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Date",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Receipt Num",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Service Name",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Mode",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Amount (\$)",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.deepOrange,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                            ],
                            rows: items
                                .map((item) => DataRow(
                                      cells: [
                                        DataCell(
                                          Text(
                                            dateHelper.getFromattedDate(
                                                item.transactionDate),
                                            //item.transactionDate.substring(0, 10),
                                            textAlign: TextAlign.center,
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text(
                                              item.receiptNumber.toString(),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text(item.serviceName),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text(item.paymentType),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        ),
                                        DataCell(
                                          Center(
                                            child: Text('${item.totalAmount}'),
                                          ),
                                          showEditIcon: false,
                                          placeholder: false,
                                        )
                                      ],
                                    ))
                                .toList()),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Check Number : ',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    '${items[0].approvalNumber}',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Check Date : ',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    '${items[0].paymentDetails.dateOnTheCheck}'
                                        .substring(0, 10),
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Bank Name : ',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    '${items[0].paymentDetails.bankName}'
                                            .isEmpty
                                        ? '${items[0].paymentDetails.bankName}'
                                        : 'N/A',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Text('Check return date',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey)),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      OutlineButton(
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              dateHelper.getFromattedDate(
                                                  selectedDate.toString()),
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(
                                                FontAwesomeIcons
                                                    .solidCalendarAlt,
                                                color: Colors.deepOrange,
                                                size: 14,
                                              ),
                                            )
                                          ],
                                        ),
                                        onPressed: () async {
                                          DateTime picked =
                                              await showDatePicker(
                                            context: context,
                                            initialDate: selectedDate,
                                            firstDate: DateTime(2000, 1),
                                            lastDate: DateTime.now(),
                                          );
                                          setState(() {
                                            if (picked != null) {
                                              selectedDate = picked;
                                            } else {
                                              selectedDate = DateTime.now();
                                            }
                                          });
                                        },
                                      ),
                                    ],
                                  )),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: <Widget>[
                                    Text('Reason',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey)),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey
                                                    .withOpacity(0.5)),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        width: 300,
                                        height: 60,
                                        child: TextField(
                                          controller: reasonController,
                                          keyboardType: TextInputType.multiline,
                                          maxLines: 3,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.black,
                                          ),
                                          onChanged: (text) {
                                            if (text.length > 0) {
                                              setState(() {
                                                _isReasonValid = false;
                                              });
                                            }
                                          },
                                          decoration: InputDecoration(
                                              errorText: _isReasonValid
                                                  ? 'Enter Reason'
                                                  : null,
                                              hintText: 'Reason for return',
                                              hintStyle:
                                                  TextStyle(fontSize: 12),
                                              border: InputBorder.none),
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Text('Amount to be paid',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey)),
                                  Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                        '\$ ${totalAmount.toStringAsFixed(2)}',
                                        style: TextStyle(
                                          fontSize: 26,
                                        )),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Visibility(
                                    visible: items[0].refundDetails == null
                                        ? true
                                        : true,
                                    child: RaisedButton(
                                      child: Text(
                                        'Mark check return',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      color: Theme.of(context).primaryColor,
                                      onPressed: () {
                                        if (reasonController.text.isEmpty) {
                                          setState(() {
                                            reasonController.text.isEmpty
                                                ? _isReasonValid = true
                                                : _isReasonValid = false;
                                          });
                                        } else {
                                          Navigator.pop(context, true);

                                          markCheckReturn(
                                              selectedDate,
                                              items[0].orderId,
                                              reasonController.text,
                                              '0',
                                              items,
                                              false);
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  RaisedButton(
                                    color: Theme.of(context).primaryColor,
                                    child: Text(
                                      'Mark check return and collect payment',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                    onPressed: () {
                                      if (reasonController.text.isEmpty) {
                                        setState(() {
                                          reasonController.text.isEmpty
                                              ? _isReasonValid = true
                                              : _isReasonValid = false;
                                        });
                                      } else {
                                        Navigator.pop(context, true);
                                        markCheckReturn(
                                            selectedDate,
                                            items[0].orderId,
                                            reasonController.text,
                                            '0',
                                            items,
                                            true);
                                      }
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ));
            }));
      },
    );
  }

  markCheckReturn(DateTime selectedDate, String orderId, String reasonText,
      String bankCharges, List<OrderItems> items, bool condition) async {
    String checkReturnDate = selectedDate.toString().substring(0, 10);

    var returnDetails = {};
    returnDetails["checkReturnDate"] = checkReturnDate;
    returnDetails["reason"] = reasonText;
    returnDetails["bankCharges"] = bankCharges;

    var checkReturnBody = {};
    checkReturnBody["orderId"] = orderId;
    checkReturnBody["returnDetails"] = returnDetails;

    String checkReturnJson = convert.jsonEncode(checkReturnBody);
    //print(checkReturnJson);

    NetworkHelper networkHelper = NetworkHelper();
    var jsonResponse =
        await networkHelper.markChcekReturn(checkReturnJson, widget.token);
    if (jsonResponse != null) {
      //Navigator.of(context).pop();

      /*  Scaffold.of(context).showSnackBar(new SnackBar(
        backgroundColor: Colors.green,
        content: new Text(
            "The selected transaction status has been successfully marked to check returned"),
      )); */

      commonComponents.showOverlayMessage(
          context,
          'The selected transaction status has been successfully marked to check returned',
          Colors.green,
          Colors.white);

      if (condition) {
        _showDialogRepay(items, false, 'Check');
      }

      setState(() {
        getAllDonations();
        getAllDonationsItems();
      });
    }

    //print(jsonResponse);
  }

  refundAnOrderByCash(String orderId, String totalAmount, String reasonText,
      List<OrderItems> items) async {
    List<RefundOrderDetails> refundsList = [];
    items.forEach((items) {
      RefundOrderDetails refundDetails = RefundOrderDetails();
      refundDetails.templeServiceId = items.templeServiceId;
      refundDetails.amount = items.amount;
      refundsList.add(refundDetails);
    });

    RefundOrderByCash refundOrderByCash = RefundOrderByCash();
    refundOrderByCash.orderId = orderId;
    refundOrderByCash.refundPaymentType = 'Cash';
    refundOrderByCash.refundedOn = selectedDate.toString().substring(0, 10);
    refundOrderByCash.refundAmount = totalAmount;
    refundOrderByCash.reason = reasonText;
    refundOrderByCash.refundDetails = refundsList;
    String refundJson = convert.jsonEncode(refundOrderByCash);

    if (reasonText.isNotEmpty) {
      NetworkHelper networkHelper = new NetworkHelper();
      var jsonResponse =
          await networkHelper.orderRefund(refundJson, widget.token);

      Navigator.of(context).pop();
      selectedItems.clear();
      /*  Scaffold.of(context).showSnackBar(new SnackBar(
        backgroundColor: Colors.green,
        content: new Text("Refunded succesfully"),
      )); */
      commonComponents.showOverlayMessage(
          context, 'Refunded succesfully', Colors.green, Colors.white);
      setState(() {
        getAllDonations();
        getAllDonationsItems();
        getTenantDetails();
      });

      setState(() {
        getAllDonations();
        getAllDonationsItems();
        getTenantDetails();
      });
    } else {
      commonComponents.showOverlayMessage(
          context, 'Please enter the reason', Colors.deepOrange, Colors.white);
    }

    //print(refundJson);
  }

  refundAnOrderByCheck(
      String orderId,
      String totalAmount,
      String reasonText,
      List<OrderItems> items,
      String payeeName,
      String bankName,
      String checkNumber,
      String dateOnTheCheck) async {
    List<RefundOrderDetails> refundsList = [];
    items.forEach((items) {
      RefundOrderDetails refundDetails = RefundOrderDetails();
      refundDetails.templeServiceId = items.templeServiceId;
      refundDetails.amount = items.amount;
      refundsList.add(refundDetails);
    });

    RefundOrderByCheck refundOrderByCheck = RefundOrderByCheck();
    refundOrderByCheck.orderId = orderId;
    refundOrderByCheck.refundPaymentType = 'Check';
    //refundOrderByCheck.refundedOn = DateTime.now().toString().substring(0,10);
    refundOrderByCheck.refundedOn = selectedDate.toString().substring(0, 10);
    refundOrderByCheck.refundAmount = totalAmount;
    refundOrderByCheck.reason = reasonText;
    refundOrderByCheck.payeeName = payeeName;
    refundOrderByCheck.bankName = bankName;
    refundOrderByCheck.dateOnTheCheck = dateOnTheCheck;
    refundOrderByCheck.refundCheckNumber = checkNumber;
    refundOrderByCheck.refundDetails = refundsList;
    String refundJson = convert.jsonEncode(refundOrderByCheck);

    //print(refundJson);

    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse =
        await networkHelper.orderRefund(refundJson, widget.token);
    print(jsonResponse);

    Navigator.of(context).pop();
    selectedItems.clear();
    commonComponents.showOverlayMessage(
        context, 'Refunded succesfully', Colors.green, Colors.white);

    setState(() {
      getAllDonations();
      getAllDonationsItems();
    });
  }

  repayByCash(String orderId, String totalAmount, String reasonText,
      List<OrderItems> items) async {
    List<RepaymentDetails> repayList = [];
    items.forEach((items) {
      RepaymentDetails repaymentDetails = RepaymentDetails();
      repaymentDetails.templeServiceId = items.templeServiceId;
      repaymentDetails.totalAmount = items.amount;
      repayList.add(repaymentDetails);
    });

    RepaymentByCash repaymentByCash = RepaymentByCash();

    repaymentByCash.orderId = orderId;
    repaymentByCash.paymentType = 'Cash';
    //repaymentByCash.transactionDate = DateTime.now().toString().substring(0,10);
    repaymentByCash.transactionDate = selectedDate.toString().substring(0, 10);
    repaymentByCash.amount = totalAmount;
    repaymentByCash.reason = reasonText;
    repaymentByCash.notes = '';
    repaymentByCash.checkReturnedDate = '';
    repaymentByCash.bankcharges = '';
    repaymentByCash.repaymentDetails = repayList;
    String repayJson = convert.jsonEncode(repaymentByCash);
    //print(repayJson);

    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.orderRepay(repayJson, widget.token);
    print(jsonResponse);

    Navigator.of(context).pop();
    commonComponents.showOverlayMessage(
        context, 'Repaid succesfully', Colors.green, Colors.white);

    setState(() {
      getAllDonations();
      getAllDonationsItems();
    });
  }

  repayByCheck(
      String orderId,
      String totalAmount,
      String reasonText,
      List<OrderItems> items,
      String payeeName,
      String bankName,
      String checkNumber,
      String dateOnTheCheck) async {
    List<RepaymentDetails> repayList = [];
    items.forEach((items) {
      RepaymentDetails repaymentDetails = RepaymentDetails();
      repaymentDetails.templeServiceId = items.templeServiceId;
      repaymentDetails.totalAmount = items.amount;
      repayList.add(repaymentDetails);
    });

    RepaymentByCheck repaymentByCheck = RepaymentByCheck();

    repaymentByCheck.orderId = orderId;
    repaymentByCheck.paymentType = 'Check';
    repaymentByCheck.transactionDate =
        DateTime.now().toString().substring(0, 10);
    repaymentByCheck.amount = totalAmount;
    if (items[0].refundDetails != null)
      repaymentByCheck.reason = items[0].refundDetails.reason != null
          ? items[0].refundDetails.reason
          : '';
    else
      repaymentByCheck.reason = '';

    repaymentByCheck.notes = reasonText;
    if (items[0].refundDetails != null)
      repaymentByCheck.checkReturnedDate =
          items[0].refundDetails.checkReturnDate;
    else
      repaymentByCheck.checkReturnedDate = '';

    if (items[0].refundDetails != null)
      repaymentByCheck.bankcharges = items[0].refundDetails.bankCharges;
    else
      repaymentByCheck.bankcharges = '';
    repaymentByCheck.payeeName = payeeName;
    repaymentByCheck.bankName = bankName;
    repaymentByCheck.approvalNumber = checkNumber;
    repaymentByCheck.dateOnTheCheck = dateOnTheCheck;
    repaymentByCheck.repaymentDetails = repayList;

    String repayJson = convert.jsonEncode(repaymentByCheck);
    print(repayJson);

    var jsonResponse = await networkHelper.orderRepay(repayJson, widget.token);
    print(jsonResponse);

    Navigator.of(context).pop();
    Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: Colors.green,
      content: new Text("Repaid succesfully"),
    ));

    setState(() {
      getAllDonations();
      getAllDonationsItems();
    });
  }

  getPrintReceiptDetails(
      String orderId, var receiptNumber, BuildContext context) async {
    var jsonResponse =
        await networkHelper.getPrintReceiptDetails(widget.token, orderId);

    PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
    List<OrderItemDetails> orderDetails =
        printReceipt.receiptDetails.order.orderItemDetails;

    List<OrderItemDetails> templeTokenList = [];

    orderDetails.forEach((element) {
      if (element.templeToken != null && element.templeToken) {
        templeTokenList.add(element);
      }
    });

    showDialog(
        context: context,
        barrierDismissible: false,
        child: new AlertDialog(
            titlePadding: EdgeInsets.zero,
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Receipt - $receiptNumber',
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.w500)),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: Container(
                constraints: BoxConstraints(maxWidth: 320),
                child: SingleChildScrollView(
                    child: Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Image.network(
                                  tenantLogo,
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                              /*  SizedBox(
                                width: 20,
                              ), */
                              Expanded(
                                  flex: 6,
                                  child: Container(
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                        Text(
                                          tenantName,
                                          style: TextStyle(fontSize: 14),
                                        ),
                                        Text(
                                          tenantAddress,
                                          style: TextStyle(fontSize: 11),
                                        ),
                                        Text(
                                            printReceipt
                                                    .receiptDetails
                                                    .tenantInfo
                                                    .address
                                                    .city +
                                                ',' +
                                                printReceipt
                                                    .receiptDetails
                                                    .tenantInfo
                                                    .address.state
                                                    .name,
                                            style: TextStyle(
                                              fontSize: 11,
                                            )),
                                        SizedBox(height: 20),
                                      ]))),
                              Expanded(flex: 2, child: Container())
                            ],
                          ),
                          Text('Receipt for Services',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500)),
                          SizedBox(height: 5),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Receipt No : ' + receiptNumber,
                                style: TextStyle(fontSize: 11),
                              ),
                              Text(
                                getFormattedDateTime(printReceipt
                                    .receiptDetails.order.transactionDate),
                                style: TextStyle(fontSize: 11),
                              )
                            ],
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    printReceipt.receiptDetails.order.user
                                            .firstName +
                                        ' ' +
                                        printReceipt.receiptDetails.order.user
                                            .lastName,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 11,
                                    )),
                                Text(
                                    printReceipt.receiptDetails.order.user
                                        .userAddress.address,
                                    style: TextStyle(
                                      fontSize: 11,
                                    )),
                                Text(
                                    printReceipt.receiptDetails.order.user
                                            .userAddress.city +
                                        ', ' +
                                        printReceipt.receiptDetails.order.user
                                            .userAddress.state.name +
                                        ' - ' +
                                        printReceipt.receiptDetails.order.user
                                            .userAddress.zipCode,
                                    style: TextStyle(
                                      fontSize: 11,
                                    )),
                                Text(
                                    printReceipt.receiptDetails.order.user
                                        .userAddress.country.name,
                                    style: TextStyle(
                                      fontSize: 11,
                                    ))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SingleChildScrollView(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        DataTable(
                            onSelectAll: (b) {},
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Item",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                                onSort: (i, b) {
                                  setState(() {
                                    items.sort((b, a) => a.transactionDate
                                        .compareTo(b.transactionDate));
                                  });
                                },
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Quantity",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: true)),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Expanded(
                                  child: Center(
                                      child: Text("Amount (\$)",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12),
                                          softWrap: false)),
                                ),
                                numeric: false,
                              ),
                            ],
                            rows: orderDetails
                                .map(
                                  (orderDetails) => DataRow(
                                    cells: [
                                      DataCell(
                                        Padding(
                                          padding: EdgeInsets.all(5),
                                          child: Text(
                                            toBeginningOfSentenceCase(
                                                orderDetails.name
                                                    .toString()
                                                    .toLowerCase()),
                                            textAlign: TextAlign.left,
                                            softWrap: true,
                                          ),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(
                                            orderDetails.quantity.toString(),
                                          ),
                                        ),
                                      ),
                                      DataCell(
                                        Center(
                                          child: Text(orderDetails.totalAmount
                                              .toString()),
                                        ),
                                        showEditIcon: false,
                                        placeholder: false,
                                      ),
                                    ],
                                  ),
                                )
                                .toList()),
                        SizedBox(
                          height: 20,
                        ),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  Text('Total Amount :',
                                      style: TextStyle(
                                        fontSize: 14,
                                      )),
                                  SizedBox(width: 10),
                                  Text(
                                    '\$ ${printReceipt.receiptDetails.orderInfo.receiptTotalAmount}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                    textAlign: TextAlign.start,
                                  )
                                ],
                              ),
                            )),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  Text('Payment Type : ',
                                      style: TextStyle(
                                        fontSize: 14,
                                      )),
                                  SizedBox(width: 10),
                                  Text(
                                    '${printReceipt.receiptDetails.order.paymentDetails.paymentType}',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                    textAlign: TextAlign.start,
                                  )
                                ],
                              ),
                            )),
                        Visibility(
                            visible: printReceipt.receiptDetails.order.user
                                        .nakshatraDetails.length >
                                    0
                                ? true
                                : false,
                            child: Column(
                              children: [
                                SizedBox(height: 40),
                                Text('Nakshatra Details',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                getNakshatraWidget(printReceipt.receiptDetails
                                    .order.user.nakshatraDetails)
                                /*  Text(printReceipt.receiptDetails.orders.user
                                                                                              .nakshatraDetails[0].nakshatra +
                                                                                          printReceipt.receiptDetails.orders.user
                                                                                              .nakshatraDetails[0].name) */
                              ],
                            )),
                        Visibility(
                            visible: templeTokenList.length > 0 ? true : false,
                            child: Column(
                              children: [
                                SizedBox(height: 40),
                                Text('Token Receipt',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                getTokenReceipt(templeTokenList)
                                /*  Text(printReceipt.receiptDetails.orders.user
                                                                                                                              .nakshatraDetails[0].nakshatra +
                                                                                                                          printReceipt.receiptDetails.orders.user
                                                                                                                              .nakshatraDetails[0].name) */
                              ],
                            )),
                      ],
                    ))
                  ],
                )))));
  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  void addItemsToView(List<DonationItems> items) {
    for (DonationItems itemObj in items) {
      itemsToView.add(itemObj);
    }
  }

  void checkOnlyCheckBoxes(List<DonationItems> itemsToView) {
    List<DonationItems> filterItemsForTypeOfCash = [];

    print(itemsToView.length.toString() + ' before multi if');
    if (itemsToView.length > 0) {
      if (cashVal && checkVal && cardVal) {
        print('1111111111');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Cash' ||
              item.paymentType == 'Check' ||
              item.paymentType == 'Card') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (checkVal && cashVal) {
        print('2222222222222');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Check' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && cashVal) {
        print('33333333333');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && checkVal) {
        print('44444444444444');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Check') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else {
        if (cashVal) {
          print('55555555555');
          itemsToView.forEach((item) {
            if (item.paymentType == 'Cash') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (cardVal) {
          print('6666666666');
          itemsToView.forEach((item) {
            if (item.paymentType == 'Card') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (checkVal) {
          print('7777777777');
          itemsToView.forEach((item) {
            if (item.paymentType == 'Check') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else {}
      }

      print(filterItemsForTypeOfCash.length);

      if (filterItemsForTypeOfCash.length > 0) {
        itemsToView.clear();
        itemsToView.addAll(filterItemsForTypeOfCash);
      } else {
        if (cashVal || checkVal || cardVal) {
          itemsToView.clear();
        }
      }

      setState(() {});
    } else {
      setState(() {});
    }
  }

  void checkOnlyCheckBoxesNew() {
    List<DonationItems> filterItemsForTypeOfCash = [];

    print(items.length.toString() + ' before multi if');
    if (items.length > 0) {
      if (cashVal && checkVal && cardVal) {
        print('1111111111');
        itemsToView.forEach((item) {
          if (item.paymentType == 'Cash' ||
              item.paymentType == 'Check' ||
              item.paymentType == 'Card') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (checkVal && cashVal) {
        print('2222222222222');
        items.forEach((item) {
          if (item.paymentType == 'Check' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && cashVal) {
        print('33333333333');
        items.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Cash') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else if (cardVal && checkVal) {
        print('44444444444444');
        items.forEach((item) {
          if (item.paymentType == 'Card' || item.paymentType == 'Check') {
            filterItemsForTypeOfCash.add(item);
          }
        });
      } else {
        if (cashVal) {
          print('55555555555');
          items.forEach((item) {
            if (item.paymentType == 'Cash') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (cardVal) {
          print('6666666666');
          items.forEach((item) {
            if (item.paymentType == 'Card') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else if (checkVal) {
          print('7777777777');
          items.forEach((item) {
            if (item.paymentType == 'Check') {
              filterItemsForTypeOfCash.add(item);
            }
          });
        } else {}
      }

      print(filterItemsForTypeOfCash.length);

      if (filterItemsForTypeOfCash.length > 0) {
        itemsToView.clear();
        itemsToView.addAll(filterItemsForTypeOfCash);
      } else {
        if (cashVal || checkVal || cardVal) {
          itemsToView.clear();
        } else if (!cardVal && !cashVal && !checkVal) {
          itemsToView.clear();
          itemsToView.addAll(items);
        }
      }

      setState(() {});
    } else {
      setState(() {});
    }
  }

  Widget getNakshatraWidget(List<NakshatraDetails> nakshatraDetails) {
    return new Column(
        children: nakshatraDetails
            .map((nakshatra) => new Padding(
                padding: EdgeInsets.all(10),
                child: new Row(
                  children: [
                    Text(toBeginningOfSentenceCase(nakshatra.name),
                        style: TextStyle(fontSize: 13)),
                    SizedBox(width: 20),
                    Text(nakshatra.nakshatra, style: TextStyle(fontSize: 13))
                  ],
                )))
            .toList());
  }

  getTokenReceipt(List<OrderItemDetails> templeTokenList) {
    return new Column(
        children: templeTokenList
            .map((nakshatra) => new Padding(
                padding: EdgeInsets.all(10),
                child: new Row(
                  children: [
                    Expanded(
                      child: Text(toBeginningOfSentenceCase(nakshatra.name),
                          style: TextStyle(fontSize: 13)),
                    ),
                    Expanded(
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(nakshatra.quantity.toString(),
                              style: TextStyle(fontSize: 13)),
                          SizedBox(width: 10),
                          Text('*'),
                          SizedBox(width: 10),
                          Text(nakshatra.fee, style: TextStyle(fontSize: 13)),
                        ],
                      ),
                    ),

                    //SizedBox(width: 20),
                    Text(nakshatra.totalAmount.toStringAsFixed(2),
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500))
                  ],
                )))
            .toList());
  }

  String getFormattedDateTime(String transactionDate) {
    String formattedDate = DateFormat('mm-dd-yyyy hh:mm aa')
        .format(DateTime.parse(transactionDate));
    return formattedDate;
  }
}

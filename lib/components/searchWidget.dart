import 'package:chakra/models/Users/SearchMaster.dart';
import 'package:chakra/models/Users/UsersList.dart';
import 'package:chakra/utils/FormValidations.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:flutter/material.dart';

class SearchWidget extends StatefulWidget {
  final String userToken;

  const SearchWidget({
    Key key,
    @required this.userToken,
  }) : super(key: key);

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  FormValidations formValidations = new FormValidations();
  final searchController = TextEditingController();
  NetworkHelper networkHelper = new NetworkHelper();
  String searchEmptyText = '';
  List<UsersList> searchUserList = [];

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 10, right: 10, bottom: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      width: 200,
                      child: TextFormField(
                        controller: searchController,
                        validator: (value) {
                          return _isNumeric(value)
                              ? formValidations.validateMobile(value)
                              : formValidations.validateEmail(value);
                        },
                        style: TextStyle(color: Colors.black, fontSize: 14),
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(left: 10, right: 10),
                            focusedErrorBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1.0)),
                            errorBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 1.0)),
                            focusedBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                    color: Colors.black, width: 1.0)),
                            enabledBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1)),
                            hintText: 'Phone number or Email',
                            hintStyle: TextStyle(
                                color: Colors.white.withOpacity(0.8))),
                      )),
                  OutlineButton(
                    child: Text('Search'),
                    onPressed: () {
                      getUserInfo(searchController.text, widget.userToken);

                      /*  if (_formKey.currentState.validate()) {
                    setState(() {
                      searchUserList.clear();
                    }); */

                      /*   if (_formKey.currentState.validate()) {
                    setState(() {
                      isAdmin = false;
                      searchUserList.clear();
                    });
                    userLogin(isAdmin, adminEmail, adminPassword, true);
                  } */
                    },
                  )
                ],
              ),
            ),
            Expanded(
                child: Container(
                    width: 600,
                    //color: Colors.red,
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(color: Colors.yellow, offset: Offset(0, 5))
                    ]
                        //boxShadow: Box
                        ),
                    child: searchUserList.isEmpty
                        ? Center(
                            child: Text(searchEmptyText,
                                style: TextStyle(
                                  color: Colors.red[400],
                                  fontSize: 13,
                                )))
                        : ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount: searchUserList.length,
                            itemBuilder: (context, index) {
                              return new GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      print(searchUserList[index].firstName);

                                      /*  moveToNextScreen(
                                                            userToken,
                                                            isAdmin,
                                                            loggedInUser); */
                                    });
                                  },
                                  child: Card(
                                    elevation: 4,
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 10),
                                    color: Colors.white,
                                    child: Container(
                                        padding: EdgeInsets.all(10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              '${searchUserList[index].firstName} ${searchUserList[index].lastName}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12),
                                            ),
                                            Text(
                                              searchUserList[index].email,
                                              style: TextStyle(
                                                  color: Colors.black
                                                      .withOpacity(0.5),
                                                  fontSize: 11),
                                            ),
                                          ],
                                        )),
                                  ));
                            },
                          ))),
          ],
        ));
  }

  void getUserInfo(String searchString, String tokenString) async {
    //NetworkHelper networkHelper = new NetworkHelper();
    var response = await networkHelper.getUsersInfo(searchString, tokenString);
    SearchMaster searchMaster = SearchMaster.fromJson(response);

    setState(() {
      if (searchMaster.userList.isEmpty) {
        searchEmptyText = 'No user found';
      }
      searchUserList = searchMaster.userList;
    });
  }
}

bool _isNumeric(String str) {
  if (str == null) {
    return false;
  }
  return double.tryParse(str) != null;
}

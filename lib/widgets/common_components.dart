import 'package:flutter/material.dart';

class CommonComponents {
  getOutlineButton(String buttonText, onPressed) {
    return OutlineButton(
        child: Text(buttonText),
        onPressed: () {
          onPressed();
        });
  }

  getTextField(String hintString) {
    return TextField(
      keyboardType: TextInputType.phone,
      decoration:
          InputDecoration(hintText: hintString, border: InputBorder.none),
    );
  }

  showSnackBar(String message, BuildContext context, Color color) {
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(message, style: TextStyle(color: Colors.white)),
    ));
  }

  showOverlayMessage(BuildContext context, String message, Color color,
      Color textColor) async {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = OverlayEntry(
        builder: (context) => Positioned(
            //top: MediaQuery.of(context).size.height - 50,
            top: 60,
            left: MediaQuery.of(context).size.width / 2 - 150,
            height: 120,
            width: 300,
            child: Material(
              color: Colors.transparent,
              child: Container(
                alignment: Alignment.center,
                constraints: BoxConstraints(maxWidth: 320),
                padding: EdgeInsets.all(10.0),
                margin: new EdgeInsets.all(20.0),
                decoration: new BoxDecoration(
                  color: color,
                  shape: BoxShape.rectangle,
                  borderRadius: new BorderRadius.circular(10.0),
                  boxShadow: <BoxShadow>[
                    new BoxShadow(
                      color: Colors.black12,
                      blurRadius: 20.0,
                      offset: new Offset(0.0, 5.0),
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    /*  Icon(
                      icon,
                      color: Colors.white,
                      size: 20,
                    ),
                    SizedBox(width: 10), */
                    Flexible(
                      child: Text(
                        message,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: textColor,
                            fontSize: 14,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
              ),
            )));
    overlayState.insert(overlayEntry);
    await Future.delayed(Duration(seconds: 3));
    overlayEntry.remove();
  }

  showTenantOverlayMessage(BuildContext context, String message, Color color,
      Color textColor) async {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = OverlayEntry(
        builder: (context) => Positioned(
            //top: MediaQuery.of(context).size.height - 50,
            top: 100,
            left: MediaQuery.of(context).size.width / 2 - 150,
            height: 120,
            width: 300,
            child: Material(
              color: Colors.transparent,
              child: Container(
                alignment: Alignment.center,
                constraints: BoxConstraints(maxWidth: 320),
                padding: EdgeInsets.all(10.0),
                margin: new EdgeInsets.all(20.0),
                decoration: new BoxDecoration(
                  color: color,
                  shape: BoxShape.rectangle,
                  borderRadius: new BorderRadius.circular(10.0),
                  boxShadow: <BoxShadow>[
                    new BoxShadow(
                      color: Colors.black12,
                      blurRadius: 20.0,
                      offset: new Offset(0.0, 5.0),
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    /*  Icon(
                      icon,
                      color: Colors.white,
                      size: 20,
                    ),
                    SizedBox(width: 10), */
                    Flexible(
                      child: Text(
                        message,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: textColor,
                            fontSize: 14,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
              ),
            )));
    overlayState.insert(overlayEntry);
    await Future.delayed(Duration(seconds: 3));
    overlayEntry.remove();
  }

  userSearchDialog() {
    return AlertDialog(
      title: Text('Devotee Search'),
    );
  }
}

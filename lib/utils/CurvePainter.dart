import 'package:flutter/material.dart';
import 'dart:math' as math;

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    //Path path = Path();
    //Paint paint = Paint();

    /* path.lineTo(0, size.height);
    path.arcTo(rect, startAngle, sweepAngle, forceMoveTo)
    path.quadraticBezierTo(size.width, size.height, size.width, size.height);
    /*  path.quadraticBezierTo(
        size.width * 0.20, size.height, size.width * 0.25, size.height * 0.90);
    path.quadraticBezierTo(size.width * 0.40, size.height * 0.40,
        size.width * 0.50, size.height * 0.70);
    path.quadraticBezierTo(size.width * 0.60, size.height * 0.85,
        size.width * 0.65, size.height * 0.65);
    path.quadraticBezierTo(
        size.width * 0.70, size.height * 0.90, size.width, 0); */
    path.close();

    paint.color = Colors.amber;
    canvas.drawPath(path, paint); */

    final rect = Rect.fromLTRB(20, 10, 205, 100);
    final startAngle = math.pi / 2;
    final sweepAngle = math.pi;
    final useCenter = false;
    final paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 20;
    canvas.drawArc(rect, startAngle, sweepAngle, useCenter, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import '../models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'dart:convert';



class LocalStorage {
  final storage = new FlutterSecureStorage();
  Database db;
  var store = StoreRef.main();

  setUpLocalDB() async {
    String dbPath = 'sample.db';
    DatabaseFactory dbFactory = databaseFactoryIo;
    db = await dbFactory.openDatabase(dbPath);
  }

  saveLocalForDesktop() {}
  saveLocalForMobile() {}

  saveStripeInfo(dynamic deviceInfo) async {
    if (Platform.isWindows) {
      await setUpLocalDB();
     // await store.delete(db);
      await store.record('stripeDeviceInfo').put(db, deviceInfo);
    }
  }

  saveKioskTempleSettings(dynamic templeSettings) async {
    if (Platform.isWindows) {
      await setUpLocalDB();
     // await store.delete(db);
      await store.record('templeSettings').put(db, templeSettings);
      //await store.record('isSignatureCopy').put(db, isSignatureCopy);
    }
  }

  getKioskTempleSettings() async {
    return await store.record('templeSettings').get(db);
  }

  getStripeInfo() async {
    return await store.record('stripeDeviceInfo').get(db);
  }

  saveTokenToLocal(String userToken, bool isAnonymous) async {
    if (Platform.isAndroid || Platform.isIOS) {
      if (isAnonymous) {
        await storage.write(key: 'anonymous_token', value: userToken);
      } else {
        await storage.write(key: 'auth_token', value: userToken);
      }
    } else {
      await setUpLocalDB();
      if (isAnonymous) {
        //await store.delete(db,userToken);
        await store.record('anonymous_token').put(db, userToken);
      } else {
        await store.record('auth_token').put(db, userToken);
      }
    }
  }

  getUserTokenFromLocal(bool isAdmin) async {
    var tokenString = '';
    if (isAdmin) {
      tokenString = await store.record('auth_token').get(db) as String;
      print('admin token $tokenString');
    } else {
      tokenString = await store.record('anonymous_token').get(db) as String;
      print('anonymous token $tokenString');
    }
    return tokenString;
  }

  saveTenantInfo(dynamic tenantInfo) async {
    print('saving tenant info : ' + tenantInfo.toString());
    await store.record('tenantInfo').put(db, tenantInfo);
  }

   saveTenantInfoOne(dynamic tenantInfo) async {
    print('saving tenant info : ' + tenantInfo.toString());
    await store.record('tenantInfo').put(db, tenantInfo);
  }

  getTenantInfoFull() async {
    print('get tenant info called');
    var tenantInfo = await store.record('tenantInfo').get(db);
   // print('Tenant info in local storage' + tenantInfo.toString());

    //Map valueMap = json.decode(tenantInfo[_map][source]);

    tenant.TenantInfo tenantObj=tenant.TenantInfo.fromJson(tenantInfo);
    return tenantObj;
  }

  
    saveTenantInfoOneByOne(tenant.TenantInfo tenantInfo) async {
    print('saving tenant info : ' + tenantInfo.toString());
    await store.record('tenantId').put(db, tenantInfo.tenantDetails.tenantId);
    await store.record('name').put(db, tenantInfo.tenantDetails.name);
    await store.record('city').put(db, tenantInfo.tenantDetails.address.city);
    await store
        .record('address')
        .put(db, tenantInfo.tenantDetails.address.address);
    await store
        .record('stateId')
        .put(db, tenantInfo.tenantDetails.address.stateId);
    await store
        .record('zipCode')
        .put(db, tenantInfo.tenantDetails.address.zipCode);
    await store
        .record('countryId')
        .put(db, tenantInfo.tenantDetails.address.countryId);
    await store
        .record('countryCode')
        .put(db, tenantInfo.tenantDetails.address.countryCode);

    await store
        .record('password')
        .put(db, tenantInfo.tenantDetails.additionalProperties.password);
    await store
        .record('mailFooter')
        .put(db, tenantInfo.tenantDetails.additionalProperties.mailFooter);
    await store
        .record('mailHeader')
        .put(db, tenantInfo.tenantDetails.additionalProperties.mailHeader);
    await store
        .record('templeMail')
        .put(db, tenantInfo.tenantDetails.additionalProperties.templeMail);
    await store
        .record('websiteUrl')
        .put(db, tenantInfo.tenantDetails.additionalProperties.websiteUrl);
    await store
        .record('phoneNumber')
        .put(db, tenantInfo.tenantDetails.additionalProperties.phoneNumber);
    await store
        .record('receiptLogo')
        .put(db, tenantInfo.tenantDetails.additionalProperties.receiptLogo);

    print('TENANT SAVED SUCCESSFULLY **************');
  }

  Future<tenant.TenantInfo> getTenantInfo() async {
    print('get tenant info called');

    tenant.TenantInfo tenantInfoObj = new tenant.TenantInfo();
    print(await store.record('city').get(db) as String);

    tenant.TenantDetails tenantDetails = new tenant.TenantDetails();
    tenantDetails.tenantId = await store.record('tenantId').get(db) as String;
    tenantDetails.name = await store.record('name').get(db) as String;

    tenant.TenantAddress tenantAddress = new tenant.TenantAddress();
    tenantAddress.city = await store.record('city').get(db) as String;
    tenantAddress.address = await store.record('address').get(db) as String;
    tenantAddress.countryCode = await store.record('countryCode').get(db) as String;
    tenantAddress.countryId = await store.record('countryId').get(db) as String;
    tenantAddress.stateId = await store.record('stateId').get(db) as String;
    tenantAddress.zipCode = await store.record('zipCode').get(db) as String;

    tenantDetails.address = tenantAddress;

    tenant.AdditionalProperties additionalProperties =
        new tenant.AdditionalProperties();
    additionalProperties.password = await store.record('password').get(db) as String;
    additionalProperties.phoneNumber =
        await store.record('phoneNumber').get(db) as String;
    additionalProperties.receiptLogo =
        await store.record('receiptLogo').get(db) as String;
    additionalProperties.templeMail = await store.record('templeMail').get(db) as String;
    additionalProperties.mailFooter = await store.record('mailFooter').get(db) as String;
    additionalProperties.mailHeader = await store.record('mailHeader').get(db) as String;
    additionalProperties.websiteUrl = await store.record('websiteUrl').get(db) as String;

    tenantDetails.additionalProperties = additionalProperties;

    // var tenantInfo = await store.record('tenantInfo').get(db);
    //print('Tenant info in local storage' + tenantInfo.toString());
    tenantInfoObj.tenantDetails = tenantDetails;
    print('TENANT RECEIVED SUCCESSFULLY **************');
    return tenantInfoObj;
  }

}

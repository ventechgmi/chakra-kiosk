class FormValidations {
  String validateTextField(String value, String textFieldType) {
    if (value.isEmpty) {
      return '$textFieldType is required.';
    } else {
      return null;
    }
  }

  bool validateZeroFeeText(String value) {
    if (value.isEmpty) {
      return false;
    } else if (double.parse(value) <= 0) {
      return false;
    } else {
      return true;
    }
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty) {
      return 'Mobile Number / Email is required.';
    } else if (!regex.hasMatch(value))
      return 'Enter Valid Email.';
    else
      return null;
  }

  String validateMobile(String value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return 'Enter valid Mobile Number.';
    } else if (!regExp.hasMatch(value)) {
      return 'Enter valid Mobile Number.';
    }
    return null;
  }

  String validateEMailAndMobile(String value) {
    Pattern patternEmail =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regexEmail = new RegExp(patternEmail);
    String pattternMobile = r'/^(\+\d{1,3}[- ]?)?\d{10}$/';
    //r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExpMobile = new RegExp(pattternMobile);

    if (value.length == 0) {
      return 'Mobile Number / Email is required.';
    } else if (regexEmail.hasMatch(value) || !regExpMobile.hasMatch(value)) {
      return 'Enter Valid Email or Mobile Number.';
    } else if (!regexEmail.hasMatch(value) || regExpMobile.hasMatch(value)) {
      return 'Enter Valid Email or Mobile Number.';
    } else
      return null;
  }

  static String validateDate(String value) {
    if (value.isEmpty) {
      return 'Card Expiry is required.';
    }

    int year;
    int month;
    if (value.contains(new RegExp(r'(\/)'))) {
      var split = value.split(new RegExp(r'(\/)'));
      month = int.parse(split[0]);
      year = int.parse(split[1]);
    } else {
      month = int.parse(value.substring(0, (value.length)));
      year = -1;
    }

    if ((month < 1) || (month > 12)) {
      return 'Expiry month is invalid';
    }

    var fourDigitsYear = convertYearTo4Digits(year);
    if ((fourDigitsYear < 1) || (fourDigitsYear > 2099)) {
      return 'Expiry year is invalid';
    }

    if (!hasDateExpired(month, year)) {
      return "Invalid Expiry Date";
    }
    return null;
  }

  static int convertYearTo4Digits(int year) {
    if (year < 100 && year >= 0) {
      var now = DateTime.now();
      String currentYear = now.year.toString();
      String prefix = currentYear.substring(0, currentYear.length - 2);
      year = int.parse('$prefix${year.toString().padLeft(2, '0')}');
    }
    return year;
  }

  static bool hasDateExpired(int month, int year) {
    return !(month == null || year == null) && isNotExpired(year, month);
  }

  static bool isNotExpired(int year, int month) {
    return !hasYearPassed(year) && !hasMonthPassed(year, month);
  }

  static bool hasMonthPassed(int year, int month) {
    var now = DateTime.now();
    return hasYearPassed(year) ||
        convertYearTo4Digits(year) == now.year && (month < now.month + 1);
  }

  static bool hasYearPassed(int year) {
    int fourDigitsYear = convertYearTo4Digits(year);
    var now = DateTime.now();
    return fourDigitsYear < now.year;
  }

  static List<int> getExpiryDate(String value) {
    var split = value.split(new RegExp(r'(\/)'));
    return [int.parse(split[0]), int.parse(split[1])];
  }

  static String validateCVV(String value) {
    if (value.isEmpty) {
      return "CVV is required.";
    }

    if (value.length < 3 || value.length > 4) {
      return "CVV is invalid";
    }
    return null;
  }
}

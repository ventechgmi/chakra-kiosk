import 'dart:io';
import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'package:chakra/models/TokenItems.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/widgets/common_components.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
//import 'package:path_provider/path_provider.dart';
import 'NetworkHelper.dart';
import 'dart:io' show Directory, File, Platform;
import 'package:path/path.dart' as path;
import 'package:flutter/services.dart' show ByteData, rootBundle;

class Printer {
  void printCustomerReceipt(String printString) async {
    Socket.connect("localhost", 2500).then((socket) async {
      print('Connected to: '
          '${socket.remoteAddress.address}:${socket.remotePort}');

      socket.listen((data) {
        print('listening');
        print(new String.fromCharCodes(data).trim());
      }, onDone: () {
        print("Done");
        socket.destroy();
      });
      print(printString);
      socket.write(printString);
    });
  }

  writeAndPrintText(String html) async {
    final directory = await getApplicationDocumentsDirectory();
    final printerFilePath = directory.path + Platform.pathSeparator + 'chakra';
    final savedDir = Directory(printerFilePath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      await savedDir.create();
    }

    final file = new File('$printerFilePath/print.txt');

    //final fileSample = await File('assets/sample.txt');

    //await fileSample.writeAsString(html);
    /*
    var bytes = await rootBundle.load('assets/sample.txt');
    final buffer = await bytes.buffer;

    await file.writeAsBytes(
        buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
    */

    await file.writeAsString(html);

    String newPath = path.join(printerFilePath, 'print.html');
    file.renameSync(newPath);
  }

  Future<void> checkForPrinterNew(
      String authToken,
      String orderId,
      List<TokenItems> tokenItemsList,
      bool isSignatureCopy,
      String minAmntForReceipt) async {
    bool isNakshatraReceipt = false;
    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.getPrintReceiptDetails(
        'Bearer ' + authToken, orderId);

    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var tenantInfos = await localStorage.getTenantInfoFull();

    var htmlString =
        "<html><body style='max-width: 220px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'><h2>RECEIPT</div>" +
            "<div><h3>${tenantInfos.tenantDetails.name}</h3></div>" +
            "<div>${tenantInfos.tenantDetails.address.address}<br>${tenantInfos.tenantDetails.address.city}<br>${tenantInfos.tenantDetails.address.zipCode}</div>";

    PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
    List<OrderItemDetails> orderDetails =
        printReceipt.receiptDetails.order.orderItemDetails;

    htmlString = htmlString +
        "</br>" +
        "<div><b>Receipt for services</b></div>" +
        "</br>" +
        "<div>Receipt no :" +
        printReceipt.receiptDetails.order.receiptNumber.toString() +
        "</div>" +
        "</br>" +
        "<div>Date :" +
        printReceipt.receiptDetails.order.transactionDate +
        "</div>" +
        "</br>" +
        "<div>" +
        printReceipt.receiptDetails.order.user.firstName +
        " " +
        printReceipt.receiptDetails.order.user.lastName +
        "</div>" +
        "<div>" +
        printReceipt.receiptDetails.order.user.userAddress.address +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.city +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.state.name +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.country.name +
        "</div>" +
        "<table style='max-width: 120px'><tr><td>Item</td><td>Qty</td><td>Price</td></tr></br></br>";

    orderDetails.forEach((orderItem) {
      htmlString = htmlString +
          "<tr><td>" +
          "${toBeginningOfSentenceCase(orderItem.name.toString().toLowerCase())} " +
          "</td><td>X ${orderItem.quantity.toString()} nos</td><td>${orderItem.totalAmount.toStringAsFixed(2)}</td></tr>";

      if (orderItem.serviceType == 'Payment Service') {
        isNakshatraReceipt = true;
      }
    });

    htmlString = htmlString +
        "</table>" +
        "<p>Receipt Total (\$) : <b> ${printReceipt.receiptDetails.orderInfo.receiptTotalAmount.toStringAsFixed(2)}</b></p>" +
        "<p>Payment mode: ${printReceipt.receiptDetails.order.paymentDetails.paymentType}</p>" +
        "<p>Thank you for a visit !!!</p>"
            "</div></body></html>";

    printCustomerReceipt(htmlString);

    if (isSignatureCopy) {
      //print('min amount' + minAmntForReceipt);
      if (double.parse(minAmntForReceipt) >=
          printReceipt.receiptDetails.orderInfo.receiptTotalAmount) {
        print('isSignature copy');
        printCustomerReceipt(htmlString);
      }
    }

    if (isNakshatraReceipt) {
      printNakshatraReceipt(
          printReceipt.receiptDetails.order.user.nakshatraDetails, htmlString);
    }

    if (tokenItemsList.length != 0) {
      printTokenReceipt(tokenItemsList, htmlString);
    }
  }

  Future<void> checkForPrinter(
      String authToken,
      String orderId,
      List<TokenItems> tokenItemsList,
      bool isSignatureCopy,
      String minAmntForReceipt) async {
    bool isNakshatraReceipt = false;
    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.getPrintReceiptDetails(
        'Bearer ' + authToken, orderId);
    PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
    List<OrderItemDetails> orderDetails =
        printReceipt.receiptDetails.order.orderItemDetails;
    /*  var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<div>CHAKRA</div>" +
            "<div>Global Mantra Innovations<br>Chennai<br>603103</div>";

  

    htmlString = htmlString +
        "<div>Receipt for services</div>" +
        "<div>Receipt no :" +
        printReceipt.receiptDetails.order.receiptNumber.toString() +
        "</div>" +
        "<div>Date :" +
        printReceipt.receiptDetails.order.transactionDate +
        "</div>" +
        "<div>" +
        printReceipt.receiptDetails.order.user.firstName +
        printReceipt.receiptDetails.order.user.lastName +
        "</div>" +
        "<div>" +
        printReceipt.receiptDetails.order.user.userAddress.address +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.city +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.state.name +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.country.name +
        "</div>" +
        "<table style='max-width: 120px'><tr><td>Item</td><td>Price</td></tr>";

    orderDetails.forEach((orderItem) {
      htmlString = htmlString +
          "<tr><td>" +
          "${toBeginningOfSentenceCase(orderItem.name.toString().toLowerCase())} X ${orderItem.quantity.toString()} nos" +
          "</td><td>${orderItem.totalAmount.toStringAsFixed(2)}</td></tr>";

      if (orderItem.serviceType == 'Payment Service') {
        isNakshatraReceipt = true;
      }
    });

    htmlString = htmlString +
        "</table>" +
        "<p>Receipt Total (\$) : <b> ${printReceipt.receiptDetails.orderInfo.receiptTotalAmount.toStringAsFixed(2)}</b></p>" +
        "<p>Payment mode: ${printReceipt.receiptDetails.order.paymentDetails.paymentType}</p>" +
        "<p>Thank you for a visit !!!</p>"
            "</div></body></html>";*/

    //printCustomerReceipt(htmlString);

    var htmlString = "<html><body style='max-width: 120px;'>" +
        "<div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;border:1px solid black;'>" +
        "<div><b>CHAKRA</b></div><div style='font-size: 8pt;'>Global Mantra Innovations<br>Chennai<br>603103</div>" +
        "<br><div><b>Receipt for services</b></div>" +
        "<br><div>Receipt no :273</div>" +
        "<br><div>Date: 2020-06-16 07.30 AM</div> <br><div>saranyaDuraisamy</div><div>CBE<br>CBE<br>Tamil Nadu<br>India</div>" +
        "<table style='max-width: 120px; font-size: 8pt;border: 1px solid black;'>" +
        "<tr ><td ><b>Service</b></td><td><b>Amount</b></td></tr><tr><td>Donation</td><td>45.00</td></tr></table><p style='font-size: 8pt;'><b>Receipt Total (\$) : 45.00</b></p><p>Payment mode: Cash</p><p>Thank you for a visit !!!</p></div></body></html>";
    print(htmlString);

    printCustomerReceipt(htmlString);

    /*if (isSignatureCopy) {
      //print('min amount' + minAmntForReceipt);
      if (double.parse(minAmntForReceipt) ==
          printReceipt.receiptDetails.orderInfo.receiptTotalAmount) {
        //printCustomerReceipt(htmlString);
      }
    }

    if (isNakshatraReceipt) {
      htmlString = printNakshatraReceipt(
          printReceipt.receiptDetails.order.user.nakshatraDetails, htmlString);
    }

    print(htmlString);
   
    if (tokenItemsList.length != 0) {
      printTokenReceipt(tokenItemsList, htmlString);
    } else {
      printCustomerReceipt(htmlString);
    }*/
  }

  Future<String> printNakshatraReceipt(
      List<NakshatraDetails> nakshatraDetails, String htmlString) async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var tenantInfo = await localStorage.getTenantInfoFull();
    htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'><h2>RECEIPT</h2></div></br>" +
            "<div><h5>${tenantInfo.tenantDetails.name}</h5></div>" +
            "<div>${tenantInfo.tenantDetails.address.address}<br>${tenantInfo.tenantDetails.address.city}<br>${tenantInfo.tenantDetails.address.zipCode}</div></br>";

    nakshatraDetails.forEach((item) {
      htmlString =
          htmlString + "<p> ${item.name}</p>" + "<p> ${item.nakshatra}</p>";
    });
    htmlString = htmlString + "</div></body></html>";
    return htmlString;
    //printCustomerReceipt(htmlString);
  }

  void printTokenReceipt(
      List<TokenItems> tokenItemsList, String htmlString) async {
    var tenantInfo = await getTenantInfo();

    /*  var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<div>CHAKRA</div>" +
            "<div>Global Mantra Innovations<br>Chennai<br>603103</div><div>"; */

    htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<img src='${tenantInfo.tenantDetails.additionalProperties.receiptLogo}' width='60'; height='60'>" +
            "<div>${tenantInfo.tenantDetails.name}</div>" +
            "<div>${tenantInfo.tenantDetails.address.address}<br>${tenantInfo.tenantDetails.address.city}<br>${tenantInfo.tenantDetails.address.zipCode}</div></br>";

    tokenItemsList.forEach((item) {
      htmlString = htmlString +
          "<p>${item.name} \t ${item.quantity.toString()} * ${item.price.toStringAsFixed(2)} = ${item.totalAmount.toStringAsFixed(2)}</p>";

      print(htmlString);
      printCustomerReceipt(htmlString);
    });
  }

  Future<dynamic> getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    return await localStorage.getTenantInfoFull();
  }

  /*  getTenantInfo(String orderJson) async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var response = await localStorage.getTenantInfo();
    TenantInfo tenantInfo = TenantInfo.fromJson(response);

    var jsonString = convert.jsonDecode(orderJson);
    PrinterItems printerItems = PrinterItems.fromJson(jsonString);
    List<PrintOrderItems> orderedItems = printerItems.orderedItems;

    print(tenantInfo.tenantDetails.name);

    var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<img src='${tenantInfo.tenantDetails.additionalProperties.receiptLogo}' width='60'; height='60'>" +
            "<div>${tenantInfo.tenantDetails.name}</div>" +
            "<div>${tenantInfo.tenantDetails.address.address}<br>${tenantInfo.tenantDetails.address.city}<br>${tenantInfo.tenantDetails.address.zipCode}</div>" +
            "<table style='max-width: 120px'><tr><td>Item</td><td>Price</td></tr>";

    double totalBillAmount = 0;
    orderedItems.forEach((item) {
      htmlString = htmlString +
          "<tr><td>" +
          "${toBeginningOfSentenceCase(item.name.toString().toLowerCase())} X ${item.quantity} nos" +
          "</td><td>${item.totalAmount}</td></tr>";
      totalBillAmount = totalBillAmount + item.totalAmount;
    });
    htmlString = htmlString +
        "</table>" +
        "<p>Total Amount : <b> \$ ${totalBillAmount.toStringAsFixed(2)}</b></p>" +
        "<p>Thank you for a visit !!!</p>"
            "</div></body></html>";
    return htmlString;
  } */
}

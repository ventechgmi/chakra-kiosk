import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Clock extends StatefulWidget {
  @override
  _ClockState createState() => _ClockState();
}

class _ClockState extends State<Clock> {
  var currentDateTime = "";
  var currentDate = "";

  @override
  void initState() {
    super.initState();
    _getCurrentDate();
    _delayForTime();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Padding(
              padding: EdgeInsets.only(right: 20),
              child: Text(currentDateTime,
                  style: new TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.red,
                  )))),
    );
  }

  _delayForTime() {
    const oneSecond = const Duration(seconds: 1);
    new Timer.periodic(oneSecond, (Timer t) async {
      _getCurrentDate();
    });
  }

  _getCurrentDate() {
    DateTime now = DateTime.now();
    String formattedDateTime = DateFormat('MMM dd, hh:mm:ss a').format(now);
    String formattedDate = DateFormat('MMM dd,').format(now);

    setState(() {
      currentDate = formattedDate;
      currentDateTime = formattedDateTime;
    });
  }
}

import 'dart:io';
import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'package:chakra/models/TokenItems.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:intl/intl.dart';
import 'NetworkHelper.dart';

class Printer {
  void printCustomerReceipt(String printString) async {
    Socket.connect("localhost", 2500).then((socket) async {
      print('Connected to: '
          '${socket.remoteAddress.address}:${socket.remotePort}');

      socket.listen((data) {
        print('listening');
        print(new String.fromCharCodes(data).trim());
      }, onDone: () {
        print("Done");
        socket.destroy();
      });
      print(printString);
      socket.write(printString);
    });
  }

  Future<void> checkForPrinter(
      String authToken,
      String orderId,
      List<TokenItems> tokenItemsList,
      bool isSignatureCopy,
      String minAmntForReceipt) async {
    bool isNakshatraReceipt = false;
    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.getPrintReceiptDetails(
        'Bearer ' + authToken, orderId);

    var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<div>CHAKRA</div>" +
            "<div>Global Mantra Innovations<br>Chennai<br>603103</div>";

    PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
    List<OrderItemDetails> orderDetails =
        printReceipt.receiptDetails.order.orderItemDetails;

    htmlString = htmlString +
        "<div>Receipt for services</div>" +
        "<div>Receipt no :" +
        printReceipt.receiptDetails.order.receiptNumber.toString() +
        "</div>" +
        "<div>Date :" +
        printReceipt.receiptDetails.order.transactionDate +
        "</div>" +
        "<div>" +
        printReceipt.receiptDetails.order.user.firstName +
        printReceipt.receiptDetails.order.user.lastName +
        "</div>" +
        "<div>" +
        printReceipt.receiptDetails.order.user.userAddress.address +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.city +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.state.name +
        "</br>" +
        printReceipt.receiptDetails.order.user.userAddress.country.name +
        "</div>" +
        "<table style='max-width: 120px'><tr><td>Item</td><td>Price</td></tr>";

    orderDetails.forEach((orderItem) {
      htmlString = htmlString +
          "<tr><td>" +
          "${toBeginningOfSentenceCase(orderItem.name.toString().toLowerCase())} X ${orderItem.quantity.toString()} nos" +
          "</td><td>${orderItem.totalAmount.toStringAsFixed(2)}</td></tr>";

      if (orderItem.serviceType == 'Payment Service') {
        isNakshatraReceipt = true;
      }
    });

    htmlString = htmlString +
        "</table>" +
        "<p>Receipt Total (\$) : <b> ${printReceipt.receiptDetails.orderInfo.receiptTotalAmount.toStringAsFixed(2)}</b></p>" +
        "<p>Payment mode: ${printReceipt.receiptDetails.order.paymentDetails.paymentType}</p>" +
        "<p>Thank you for a visit !!!</p>"
            "</div></body></html>";

    printCustomerReceipt(htmlString);

    if (isSignatureCopy) {
      //print('min amount' + minAmntForReceipt);
      if (double.parse(minAmntForReceipt) ==
          printReceipt.receiptDetails.orderInfo.receiptTotalAmount) {
        printCustomerReceipt(htmlString);
      }
    }

    if (isNakshatraReceipt) {
      printNakshatraReceipt(
          printReceipt.receiptDetails.order.user.nakshatraDetails);
    }

    if (tokenItemsList.length != 0) {
      printTokenReceipt(tokenItemsList);
    }
  }

  void printNakshatraReceipt(List<NakshatraDetails> nakshatraDetails) {
    var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<div>CHAKRA</div>" +
            "<div>Global Mantra Innovations<br>Chennai<br>603103</div><div>";

    nakshatraDetails.forEach((item) {
      htmlString = htmlString +
          "<p>Name: ${item.name}</p>" +
          "<p>Name: ${item.nakshatra}</p>";
    });
    htmlString = htmlString + "</div></body></html>";
    printCustomerReceipt(htmlString);
  }

  void printTokenReceipt(List<TokenItems> tokenItemsList) async {
    var tenantInfo = await getTenantInfo();

    /*  var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<div>CHAKRA</div>" +
            "<div>Global Mantra Innovations<br>Chennai<br>603103</div><div>"; */

    var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<img src='${tenantInfo.tenantDetails.additionalProperties.receiptLogo}' width='60'; height='60'>" +
            "<div>${tenantInfo.tenantDetails.name}</div>" +
            "<div>${tenantInfo.tenantDetails.address.address}<br>${tenantInfo.tenantDetails.address.city}<br>${tenantInfo.tenantDetails.address.zipCode}</div>";

    tokenItemsList.forEach((item) {
      htmlString = htmlString +
          "<p>${item.name} \t ${item.quantity.toString()} * ${item.price.toStringAsFixed(2)} = ${item.totalAmount.toStringAsFixed(2)}</p>";

      printCustomerReceipt(htmlString);
    });
  }

  Future<dynamic> getTenantInfo() async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    return await localStorage.getTenantInfoFull();
  }

  /*  getTenantInfo(String orderJson) async {
    LocalStorage localStorage = LocalStorage();
    await localStorage.setUpLocalDB();
    var response = await localStorage.getTenantInfo();
    TenantInfo tenantInfo = TenantInfo.fromJson(response);

    var jsonString = convert.jsonDecode(orderJson);
    PrinterItems printerItems = PrinterItems.fromJson(jsonString);
    List<PrintOrderItems> orderedItems = printerItems.orderedItems;

    print(tenantInfo.tenantDetails.name);

    var htmlString =
        "<html><body style='max-width: 120px'><div style='font-family:Arial, Helvetica, sans-serif; font-size:11px;'>" +
            "<div style='padding-left= 30px; max-width: 120px'>RECEIPT</div>" +
            "<img src='${tenantInfo.tenantDetails.additionalProperties.receiptLogo}' width='60'; height='60'>" +
            "<div>${tenantInfo.tenantDetails.name}</div>" +
            "<div>${tenantInfo.tenantDetails.address.address}<br>${tenantInfo.tenantDetails.address.city}<br>${tenantInfo.tenantDetails.address.zipCode}</div>" +
            "<table style='max-width: 120px'><tr><td>Item</td><td>Price</td></tr>";

    double totalBillAmount = 0;
    orderedItems.forEach((item) {
      htmlString = htmlString +
          "<tr><td>" +
          "${toBeginningOfSentenceCase(item.name.toString().toLowerCase())} X ${item.quantity} nos" +
          "</td><td>${item.totalAmount}</td></tr>";
      totalBillAmount = totalBillAmount + item.totalAmount;
    });
    htmlString = htmlString +
        "</table>" +
        "<p>Total Amount : <b> \$ ${totalBillAmount.toStringAsFixed(2)}</b></p>" +
        "<p>Thank you for a visit !!!</p>"
            "</div></body></html>";
    return htmlString;
  } */
}

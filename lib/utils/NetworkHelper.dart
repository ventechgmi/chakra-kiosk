import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class NetworkHelper {
  //ChakraTest
  static String baseURL = "https://chakratest.vsolgmi.com/api/kiosk/";
  static String payBaseURL = "https://chakratest.vsolgmi.com/api/payment/";
  static String appCodesURL =
      'https://chakratest.vsolgmi.com/api/configuration/getApplicationCodeList';

  //static String baseURL = "http://192.168.10.6:3001/api/kiosk/";

  //SVTR
//  static String baseURL = "https://balajiusa.vsolgmi.com/api/kiosk/";
//  static String payBaseURL = "https://balajiusa.vsolgmi.com/api/payment/";
//  static String appCodesURL =
//      'https://balajiusa.vsolgmi.com/api/configuration/getApplicationCodeList';

  String loginURL = "${baseURL}login";
  String productURL = "${baseURL}getProductItems";
  String userSearchURL = "${baseURL}searchUserInfo/";
  String tenantVerificationURL = "${baseURL}tenantVerification";
  String createUserURL = "${baseURL}createUser";
  String billCollectorsURL = "${baseURL}getCollectedByUser/";
  String reportsURL = "${baseURL}getCollectedByReceipt";
  String donationsURL = "${baseURL}getDonations";
  String orderDetailURL = "${baseURL}getOrderDetails/";
  String printReceiptURL = "${baseURL}getPrintReceiptDetails/";
  String orderRepayURL = "${baseURL}orderRepayment";
  String placeOrderURL = "${baseURL}placeOrder";
  String updateOrderUserURL = "${baseURL}updateOrderUser";
  String markCheckReturnURL = "${payBaseURL}markCheckReturn";
  String orderRefundURL = "${payBaseURL}orderRefund";
  String templeSettingsUrl = baseURL + 'getTempleSettings';
  String checkEmailExistsUrl = baseURL + 'tocheckEmailOrMobileExists';

  static var tenantIdText = 'CHAKRA';
  static var tenantPasswordText = 'chakra@123';

  getAppCodeList(String tokenValue) async {
    final headerParams = {
      "Authorization": tokenValue,
    };

    var response = await http.post(appCodesURL, headers: headerParams);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      print('unauthorized');
      print('server issue');
    }
  }

  getTempleSettings(String tokenValue) async {
    final headerParams = {
      "Authorization": tokenValue,
    };

    print('temple settings token' + tokenValue);
    print(templeSettingsUrl);

    var response = await http.get(templeSettingsUrl, headers: headerParams);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print('unauthorized');
      print('server issue');
    }
  }

  tenantVerification(String jsonData) async {
    var response = await http.post(
      tenantVerificationURL,
      headers: {HttpHeaders.contentTypeHeader: "application/json"},
      body: jsonData,
    );

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      return jsonResponse;
    } else if (response.statusCode == 502) {
      return {'errors': 'Server Down'};
    } else {
      var jsonResponse = convert.jsonDecode(response.body);

      return jsonResponse;
    }
  }

  userLogin(String jsonData) async {
    var response = await http.post(loginURL,
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
        body: jsonData);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else if (response.statusCode == 500) {
      var loginBody = {};
      loginBody["success"] = false;
      loginBody["message"] = "Invalid Email/Mobile Number and Password";
      String loginJson = convert.jsonEncode(loginBody);
      var jsonRespnse = convert.jsonDecode(loginJson);
      return jsonRespnse;
    } else if (response.statusCode == 400) {
      var loginBody = {};
      loginBody["success"] = false;
      loginBody["message"] = "Invalid Email/Mobile Number and Password";
      String loginJson = convert.jsonEncode(loginBody);
      var jsonRespnse = convert.jsonDecode(loginJson);
      return jsonRespnse;
    } else {
      print('server issue');
      var jsonResponse = convert.jsonEncode(response.body);
      return jsonResponse;
    }
  }

  getAnonymousToken() async {
    var tokenURL = '${baseURL}getAnonymousToken/' + tenantIdText;
    print(tokenURL);

    var response = await http.get(tokenURL);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print("Request failed with status: ${response.statusCode}.");
    }
  }

  placeOrder(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + tokenValue
    };

    print(placeOrderURL);

    var response = await http.post(
      placeOrderURL,
      body: jsonData,
      headers: requestHeaders,
    );

    print(response.toString());
    print(response.statusCode);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print('server issue');
    }
  }

  updateOrderUser(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + tokenValue
    };
    print(updateOrderUserURL);

    var response = await http.post(
      updateOrderUserURL,
      body: jsonData,
      headers: requestHeaders,
    );

    print(response.toString());
    print(response.statusCode);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print('server issue');
    }
  }

  markChcekReturn(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenValue
    };

    print(markCheckReturnURL);
    print(jsonData);

    var response = await http.post(
      markCheckReturnURL,
      body: jsonData,
      headers: requestHeaders,
    );

    print(response.toString());
    print(response.statusCode);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print('server issue');
    }
  }

  orderRefund(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenValue
    };

    print(orderRefundURL);
    print(jsonData);

    var response = await http.post(
      orderRefundURL,
      body: jsonData,
      headers: requestHeaders,
    );

    print(response.toString());
    print(response.statusCode);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print('server issue');
    }
  }

  orderRepay(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenValue
    };

    print(orderRepayURL);
    print(jsonData);

    var response = await http.post(
      orderRepayURL,
      body: jsonData,
      headers: requestHeaders,
    );

    print(response.toString());
    print(response.statusCode);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print(jsonResponse);
      return jsonResponse;
    } else {
      print('server issue');
    }
  }

  createNewUser(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenValue
    };

    var response = await http.post(
      createUserURL,
      body: jsonData,
      headers: requestHeaders,
    );

    //print(response.toString());
    print(response.body);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      print('server issue');
    }
  }

  getUsersInfo(String searchString, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + tokenValue
    };
    var searchUrl = '$userSearchURL$searchString';
    print('search url : $searchUrl');
    print('auth token string : $tokenValue');
    var response = await http.get(searchUrl, headers: requestHeaders);

    print(response.statusCode);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      print("Request failed with status: ${response.statusCode}.");
    }
  }

  getAllProductItems(String tokenValue) async {
    final headerParams = {
      "Authorization": tokenValue,
    };

    var response = await http.post(productURL, headers: headerParams);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      print('unauthorized');
      print('server issue');
    }
  }

  getBillCollectors(String billDate, String tokenString) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenString
    };
    print('collectors list' + tokenString);
    var urlString = '$billCollectorsURL$billDate';
    var response = await http.get(urlString, headers: requestHeaders);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      print("Request failed with status: ${response.statusCode}.");
    }
  }

  getRecordsForUser(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenValue
    };

    var response = await http.post(
      reportsURL,
      body: jsonData,
      headers: requestHeaders,
    );

    print(reportsURL);
    print(jsonData);
    print(tokenValue);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return convert.jsonDecode(response.body);
    }
  }

  getAllDonations(String tokenString) async {
    final headerParams = {
      "Authorization": tokenString,
    };

    var response = await http.post(donationsURL, headers: headerParams);

    if (response.statusCode == 200) {
      return convert.jsonDecode(response.body);
    } else {
      print('server issue');
    }
  }

  getOrderDetails(String tokenString, String orderId) async {
    final headerParams = {
      "Authorization": tokenString,
    };

    var response =
        await http.get('$orderDetailURL$orderId', headers: headerParams);

    if (response.statusCode == 200) {
      return convert.jsonDecode(response.body);
    } else {
      print('server issue');
    }
  }

  getPrintReceiptDetails(String tokenString, String orderId) async {
    final headerParams = {
      "Authorization": tokenString,
    };

    var response =
        await http.get('$printReceiptURL$orderId', headers: headerParams);

    if (response.statusCode == 200) {
      print(response.body);
      return convert.jsonDecode(response.body);
    } else {
      print('server issue');
    }
  }

  toCheckEmailIdExists(String jsonData, String tokenValue) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': tokenValue
    };

    print(jsonData);
    print(tokenValue);

    print(checkEmailExistsUrl);
    var response = await http.post(checkEmailExistsUrl,
        body: jsonData, headers: requestHeaders);
    print(response.statusCode);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    }
  }

  assignValueToTenantUserAndPassword() {
    if (baseURL == "https://chakratest.vsolgmi.com/api/kiosk/") {
      tenantIdText = 'CHAKRA';
      tenantPasswordText = 'chakra@123';
    } else if (baseURL == "https://balajiusa.vsolgmi.com/api/kiosk/") {
      tenantIdText = 'SVTR';
      tenantPasswordText = 'chakra@123';
    }
  }
}

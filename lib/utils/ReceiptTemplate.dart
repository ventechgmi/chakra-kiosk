import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/utils/Printer.dart';

class ReceiptTemplate {
  //ChakraTest
  static String logoURL =
      'https://chakra-app-files.s3.ap-south-1.amazonaws.com/app-files/chakra-receipt.png';
  //SVTR
//  static String logoURL =
//      'https://chakra-app-files.s3.ap-south-1.amazonaws.com/SVTR/Logo.jpg';

  static String htmlString = '<html><head> <style>' +
      '.invoice-POS { width:100px; min-height: 5cm; padding: 1cm; margin: 1cm auto; border: 1px #D3D3D3 solid; border-radius: 5px; background: white; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);}' +
      'h1{font-size: 1.5em; color: #222;}' +
      'h2{font-size: .9em;}h3{ font-size: 1.2em; font-weight: 300; line-height: 2em;}' +
      'p{font-size: .7em; color: #666; line-height: 1.2em;} #top, #mid,#bot{border-bottom: 1px solid #EEE;}' +
      '#top{min-height: 100px;} #mid{min-height: 80px;} #bot{ min-height: 50px;}' +
      '#top .logo{height: 60px;width: 60px;background: url(http://michaeltruong.ca/images/logo1.png) no-repeat;background-size: 60px 60px;}' +
      '.clientlogo{float: left;height: 60px;width: 60px;background: url(http://michaeltruong.ca/images/client.jpg) no-repeat;background-size: 60px 60px;border-radius: 50px;}' +
      '.info{display: block;margin-left: 0;}.title{float: right;}.title p{text-align: right;} table{width: 100%;border-collapse: collapse;}' +
      '.tabletitle{font-size: .5em;background: #EEE;}.service{border-bottom: 1px solid #EEE;}.item{width: 24mm;}.itemtext{font-size: .5em;}' +
      '#legalcopy{margin-top: 5mm;} </style></head><body>' +
      '<div class="invoice-POS"><center id="top"><div class="logo"></div><div class="info"> <h2>Closing Report</h2></div></center><div id="mid">' +
      '<div class="info"><h2>Contact Info</h2><p>Address : street city, state 0000</br>Email   : JohnDoe@gmail.com</br>Phone   : 555-555-5555</br></p></div></div>' +
      '<div id="bot"><div id="table"><table><tr class="tabletitle"><td class="item"><h2>Item</h2></td><td class="Hours"><h2>Qty</h2></td><td class="Rate"><h2>Sub Total</h2></td></tr>';

  String getHtmlFromResponse(
      PrintReceipt printReceipt,
      bool isSignatureCopy,
      String minAmntForReceipt,
      bool isCardPayment,
      bool totalAmountEmpty,
      bool canShowNakshatra) {
    Order order = printReceipt.receiptDetails.order;
    // User user=printReceipt.receiptDetails.

    List<OrderItemDetails> orderDetails = order.orderItemDetails;
    TenantInfo tenantInfo = printReceipt.receiptDetails.tenantInfo;
    String htmlResponse = '';

    htmlResponse =
        "<html><head><style type=\"text/css\">	html {margin: 0;padding: 0;}</style></head><body>" +
            "<div style=\"height:auto;\"><table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: sans-serif;font-weight: 400; ";

    if (totalAmountEmpty) {
      htmlResponse = htmlResponse +
          'background-size: 265px 540px;background-repeat: no-repeat; background-image: url("https://chakra-app-files.s3.ap-south-1.amazonaws.com/receipt-void.jpg")';
    }
    htmlResponse = htmlResponse +
        "'>" +
        "<tr><td><table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${logoURL} /></td>" +
        "<td style='text-align:center;font-size: 10pt;'><b>${tenantInfo.name}</b><br>${tenantInfo.address.address},<br>${tenantInfo.address.city}, ${tenantInfo.address.state.name} - ${tenantInfo.address.zipCode}<br>Tel: ${tenantInfo.additionalProperties.phoneNumber}" +
        "<br>${tenantInfo.additionalProperties.websiteUrl}</td></tr></table></td></tr>" +
        "<tr><td style='text-align:center'><b>Receipt for Services</b></td></tr><tr>" +
        "<td style='font-size: 9pt'><b>Receipt No: ${order.receiptNumber} <span style='font-size: 9pt;float:right'>${order.transactionDateDisplay}</span></b></td></tr>" +
        "<tr><td>${order.user.firstName} ${order.user.lastName} </td></tr><tr><td>${order.user.userAddress.address},</td></tr><tr><td>${order.user.userAddress.city}, ${order.user.userAddress.state.name} - ${order.user.userAddress.zipCode}</td></tr><tr><td>${order.user.userAddress.country.name}</td></tr>";
    htmlResponse = htmlResponse +
        "<td style='padding-left:10px;word-wrap: break-word;'>" +
        "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
        "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Service</th>" +
        "<th style='background-color: #FFFFF; border: 1px solid #000000;'>Qty</th>" +
        "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
        "<th style='background-color: #FFFFF; border: 1px solid #000000'>Total Amount (\$)</th>" +
        "</tr>";
    orderDetails.forEach((orderItem) {
      htmlResponse = htmlResponse +
          "<tr><td style='border: 1px solid #000000'>${orderItem.name}</td>" +
          "<td style='text-align:right; border: 1px solid #000000;'>${orderItem.quantity}</td>" +
          "<td style='text-align:right; border: 1px solid #000000'>${orderItem.fee}</td>" +
          "<td style=' text-align:right; border: 1px solid #000000'>${orderItem.totalAmount.toStringAsFixed(2)}</td>" +
          "</tr>";
    });

    htmlResponse = htmlResponse +
        "</table></td></tr><tr><td><b>Receipt Total (\$) : ${order.amount}</b></td></tr><tr><td>Payment Mode : ${printReceipt.receiptDetails.order.paymentDetails.paymentType}</td></tr>";

    if ((printReceipt.receiptDetails.order.paymentDetails.paymentType ==
            'Check' ||
        printReceipt.receiptDetails.order.paymentDetails.paymentType ==
            'Credit/Debit Card')) {
      htmlResponse = htmlResponse +
          "<tr><td>Reference Number : ${printReceipt.receiptDetails.order.paymentDetails.approvalNumber}</td></tr>";
    }

    htmlResponse = htmlResponse +
        "<tr><td>${order.thankYouMessage}</td></tr></table></div>";

    if (isSignatureCopy) {
      isCardPayment =
          printReceipt.receiptDetails.order.paymentDetails.paymentType ==
              'Credit/Debit Card';
      if (printReceipt.receiptDetails.orderInfo.receiptTotalAmount >=
              double.parse(minAmntForReceipt) &&
          isCardPayment) {
        htmlResponse = htmlResponse +
            "<div class=\"underLineStlye\">.............................................................................</div><div style=\"height:auto;\">";

        htmlResponse = htmlResponse +
            "<table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: sans-serif;font-weight: 400;'>" +
            "<tr><td><table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${logoURL} /></td><td style='text-align:center;font-size: 10pt;'>" +
            "<br><b>${tenantInfo.name}</b>><br>${tenantInfo.address.address},<br>${tenantInfo.address.city}, ${tenantInfo.address.state.name} - ${tenantInfo.address.zipCode}<br>Tel: ${tenantInfo.additionalProperties.phoneNumber}<br>${tenantInfo.additionalProperties.websiteUrl}</td></tr></table></td></tr>" +
            "<tr><td style='text-align:center'><h4 style='font-size: 9pt;text-align: center;'><b>Token Receipt</b></h4></td>" +
            "</tr><tr><td style='font-size: 9pt'><b>Receipt No: ${order.receiptNumber} <span style='font-size: 9pt;float:right'>${order.transactionDateDisplay}</span></b></td></tr>";

        htmlResponse = htmlResponse +
            "<td style='padding-left:10px;word-wrap: break-word;'>" +
            "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
            "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Service</th>" +
            "<th style='background-color: #FFFFF; border: 1px solid #000000;'>Qty</th>" +
            "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
            "<th style='background-color: #FFFFF; border: 1px solid #000000'>Total Amount (\$)</th>" +
            "</tr>";
        orderDetails.forEach((orderItem) {
          htmlResponse = htmlResponse +
              "<tr><td style='border: 1px solid #000000'>${orderItem.name}</td>" +
              "<td style='text-align:right; border: 1px solid #000000;'>${orderItem.quantity}</td>" +
              "<td style='text-align:right; border: 1px solid #000000'>${orderItem.fee}</td>" +
              "<td style=' text-align:right; border: 1px solid #000000'>${orderItem.totalAmount.toStringAsFixed(2)}</td>" +
              "</tr>";
        });

        htmlResponse = htmlResponse +
            "</tr></table></td></tr><tr><td><b>Receipt Total (\$) : ${order.amount}</b></td></tr><tr><td>Payment Mode: ${printReceipt.receiptDetails.order.paymentDetails.paymentType}</td></tr>" +
            "<tr><td>Reference Number : ${printReceipt.receiptDetails.order.paymentDetails.approvalNumber}</td></tr>" +
            "<tr><td></br></br> Signature</td></tr><tr><td></br></br> </br></br> </td></tr>";

        htmlResponse = htmlResponse +
            "<tr><td> <div class=\"underLineStlye\">.............................................................................</div><div style=\"height:auto;\"></td></tr>";
        htmlResponse = htmlResponse +
            "<tr><td>I agree to pay the above total amount according to the card issuer agreement.</td></tr>" +
            "<tr><td>ALL PERSONAL DATA ARE SECURED AND NEVER SHARED</td></tr></table></div>";
      }
    }

    if (order.user.nakshatraDetails.length > 0 && canShowNakshatra) {
      htmlResponse = htmlResponse +
          "<div class=\"underLineStlye\">.............................................................................</div><div style=\"height:auto;\">";
      htmlResponse = htmlResponse +
          "<table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: sans-serif;font-weight: 400;'>" +
          "<tr><td><table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${logoURL} /></td><td style='text-align:center;font-size: 10pt;'>" +
          "<br><b>${tenantInfo.name}</b>><br>${tenantInfo.address.address},<br>${tenantInfo.address.city}, ${tenantInfo.address.state.name} - ${tenantInfo.address.zipCode}<br>Tel: ${tenantInfo.additionalProperties.phoneNumber}<br>${tenantInfo.additionalProperties.websiteUrl}</td></tr></table></td></tr>" +
          "<tr><td style='text-align:center'><h4 style='font-size: 9pt;text-align: center;'><b> Name &amp; Nakshatra Details</b></h4></td>" +
          "</tr><tr><td style='font-size: 9pt'><b>Receipt No: ${order.receiptNumber}</b></td></tr>";

      order.user.nakshatraDetails.forEach((nakshtra) {
        htmlResponse = htmlResponse +
            "<tr><td style='font-size: 9pt;'><b>${nakshtra.name} ${nakshtra.nakshatra}</b></td></tr>";
      });
      htmlResponse = htmlResponse + "</table></div>";
    }

    orderDetails.forEach((orderItem) {
      htmlResponse = htmlResponse +
          "<div style=\"height:auto;\"><table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: sans-serif;'>" +
          "<tr><td><table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${logoURL} /></td><td style='text-align:center;font-size: 10pt;'>" +
          "<br><b>${tenantInfo.name}</b><br>${tenantInfo.address.address},<br>${tenantInfo.address.city}, ${tenantInfo.address.state.name} - ${tenantInfo.address.zipCode}<br>Tel: ${tenantInfo.additionalProperties.phoneNumber}<br>${tenantInfo.additionalProperties.websiteUrl}</td></tr></table></td></tr><tr><td style='text-align:center'><h4 style='font-size: 9pt;text-align: center;'><b>Token Receipt</b></h4></td></tr>" +
          "<tr><td style='font-size: 9pt'><b>Receipt No: ${order.receiptNumber} <span style='font-size: 9pt;float:right'>${order.transactionDateDisplay}</span></b></td></tr>";

      htmlResponse = htmlResponse +
          "<td style='padding-left:10px;word-wrap: break-word;'>" +
          "<table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
          "<tr><th style='background-color: #FFFFF; border: 1px solid #000000'>Service</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000;'>Qty</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000'>Amount (\$)</th>" +
          "<th style='background-color: #FFFFF; border: 1px solid #000000'>Total Amount (\$)</th>" +
          "</tr>";
      htmlResponse = htmlResponse +
          "<tr><td style='border: 1px solid #000000'>${orderItem.name}</td>" +
          "<td style='text-align:right; border: 1px solid #000000;'>${orderItem.quantity}</td>" +
          "<td style='text-align:right; border: 1px solid #000000'>${orderItem.fee}</td>" +
          "<td style=' text-align:right; border: 1px solid #000000'>${orderItem.totalAmount.toStringAsFixed(2)}</td>" +
          "</tr>";
      htmlResponse = htmlResponse + "</tr></table></td></tr>";
      htmlResponse = htmlResponse +
          "<div class=\"underLineStlye\">.............................................................................</div>";
    });

    /*htmlResponse = htmlResponse +
      "<div class=\"underLineStlye\">.............................................................................</div>";
  htmlResponse = htmlResponse +
      "<div style=\"height:auto;\"><table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: sans-serif;'>" +
      "<tr><td><table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${logoURL} /></td><td style='font-size: 10pt;'>" +
      "<br>${tenantInfo.name}<br>${tenantInfo.address.address}<br>${tenantInfo.address.city},<br>Tel: ${tenantInfo.address.state.name}</td></tr></table></td></tr><tr><td style='text-align:center'><h4 style='font-size: 9pt;text-align: center;'><b>Token Receipt</b></h4></td></tr>" +
      "<tr><td style='font-size: 9pt'><b>Receipt No : 4</b></td></tr><tr><td style='font-size: 9pt;text-align:right;'><b>2020-06-16 07:30 AM</b></td></tr><tr><td><table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
      "<tr><td>Gokulastami Celebrations</td><td style='text-align:right;'>1</td><td style='text-align:right;'>\$ 10.00</td></tr></table></td></tr></table></div>";
  htmlResponse = htmlResponse +
      "<div class=\"underLineStlye\">.............................................................................</div>";
  htmlResponse = htmlResponse +
      "<div style=\"height:auto;\"><table cellspacing='0' cellpadding='4' style='border-collapse: collapse; border: 2px solid #000000;width:250px; font-size: 11pt; font-family: Tahoma;font-weight: 400;'><tr><td>" +
      "<table style='width:300px; '><tr><td style='padding-left:20px;'><img height='38' src=${logoURL} /></td><td style='font-size: 10pt;'>" +
      "<br>${tenantInfo.name}<br>${tenantInfo.address.address}<br>${tenantInfo.address.city},<br>Tel: ${tenantInfo.address.state.name}</td></tr></table></td></tr><tr><td style='text-align:center'><h4 style='font-size: 9pt;text-align: center;'><b>Token Receipt</b></h4></td></tr><tr><td style='font-size: 9pt'><b>Receipt No : 4</b></td>" +
      "</tr><tr><td style='font-size: 9pt;text-align:right;'><b>2020-06-16 07:30 AM</b></td></tr><tr><td><table cellspacing='0' cellpadding='2' style='width:280px;border-collapse: collapse; font-size: 11pt;'>" +
      "<tr><td>Ganesha Abhisekam</td><td style='text-align:right; '>2</td><td style='text-align:right;'>\$ 20.00</td></tr></table></td></tr></table></div>" +*/
    htmlResponse = htmlResponse + "</div></body></html>";

    //print(htmlResponse);

    Printer().writeAndPrintText(htmlResponse);
  }
}

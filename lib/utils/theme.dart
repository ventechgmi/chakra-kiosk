import 'package:flutter/material.dart';

ThemeData applicationTheme() {
  /*  return Theme.of(context).copyWith(
      textTheme: Theme.of(context).textTheme.apply(fontFamily: 'Poppins'),
      primaryTextTheme:
          Theme.of(context).textTheme.apply(fontFamily: 'Poppins'),
      accentTextTheme: Theme.of(context).textTheme.apply(fontFamily: 'Poppins'),
      primaryColor: Colors.red,
      accentColor: Colors.redAccent,
      buttonTheme: ButtonThemeData(buttonColor: Colors.redAccent)); */

  return ThemeData(
    primarySwatch: Colors.red,
    primaryColor: Colors.red,
    accentColor: Colors.redAccent,
    fontFamily: 'Poppins',
    textTheme: TextTheme().apply(fontFamily: 'Poppins'),
    primaryTextTheme: TextTheme().apply(fontFamily: 'Poppins'),
    accentTextTheme: TextTheme().apply(fontFamily: 'Poppins'),
  );
}

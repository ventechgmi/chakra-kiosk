import 'package:intl/date_symbol_data_file.dart';
import 'package:intl/intl.dart';
import 'package:devicelocale/devicelocale.dart';

class DateHelper {
  getCurrentDateTime() {
    DateTime now = DateTime.now();
    return DateFormat('MMM dd, hh:mm:ss a').format(now);
  }

  getCurrentDate() {
    DateTime now = DateTime.now();
    return DateFormat('MM-dd-yyyy').format(now);
  }

  getFromattedDate(String dateTime) {
    //print(dateTime);
    return DateFormat('MM-dd-yyyy').format(DateTime.parse(dateTime));
  }

  getDeviceLocale() async {
    String currentLocale;
    try {
      currentLocale = await Devicelocale.currentLocale;
      print(currentLocale);
    } catch (e) {
      print("Error obtaining current locale");
      print(e.toString());
    }
  }

  getLocaleDate() async {
    //print('Get local data called');
    await initializeDateFormatting("en_US", null);
    var now = DateTime.now();
    var formatter = DateFormat.yMMMd('en_US');
    print(formatter.locale);
    String formatted = formatter.format(now);
    print('formatted date :' + formatted);
  }
}

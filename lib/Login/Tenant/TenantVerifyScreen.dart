import 'package:chakra/Login/User/LoginScreen.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart';
import 'package:chakra/utils/FormValidations.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:chakra/widgets/common_components.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:sembast/sembast.dart';
import 'dart:io' show Platform;

class TenantVerifyScreen extends StatefulWidget {
  @override
  _TenantVerifyScreenState createState() => _TenantVerifyScreenState();
}

class _TenantVerifyScreenState extends State<TenantVerifyScreen> {
  var networkHelper = new NetworkHelper();
  var commonComponents = new CommonComponents();
  TextEditingController tenantId = TextEditingController();
  TextEditingController tenantPassCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Database db;

  @override
  void initState() {
    super.initState();
    new NetworkHelper().assignValueToTenantUserAndPassword();
    tenantId.text = NetworkHelper.tenantIdText;
    tenantPassCode.text = NetworkHelper.tenantPasswordText;
    //setDB();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Color(0xffff7d6b), Color(0xffff3939)])),
            child: Center(
                child: Container(
                    constraints: BoxConstraints(maxHeight: 500, maxWidth: 380),
                    padding: EdgeInsets.all(40),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            'assets/chakra_logo.png',
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: 260,
                            child: TextFormField(
                              controller: tenantId,
                              style: TextStyle(fontSize: 13),
                              validator: (value) {
                                return FormValidations()
                                    .validateTextField(value, 'Tenant Code');
                              },
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5),
                                  hintText: 'Tenant Code',
                                  hintStyle: TextStyle(fontSize: 13),
                                  border: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide:
                                          BorderSide(color: Colors.black))),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: 260,
                            child: TextFormField(
                              controller: tenantPassCode,
                              style: TextStyle(fontSize: 13),
                              obscureText: true,
                              validator: (value) {
                                return FormValidations()
                                    .validateTextField(value, 'Passcode');
                              },
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5),
                                  hintText: 'Passcode',
                                  hintStyle: TextStyle(fontSize: 13),
                                  border: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.black),
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: RaisedButton(
                                elevation: 0,
                                color: Colors.red,
                                textColor: Colors.white,
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                ),
                                child: Text('Verify Tenant'),
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    verifyTenant();
                                  }
                                }),
                          ),
                        ],
                      ),
                    )))));
  }

  void verifyTenant() async {
    var tenantBody = {};
    tenantBody["shortCode"] = tenantId.text;
    tenantBody["password"] = tenantPassCode.text;

    String tenantJson = convert.jsonEncode(tenantBody);
    var response = await networkHelper.tenantVerification(tenantJson);

    if (response.toString().contains("error")) {
      if (response['errors'] != null) {
        commonComponents.showTenantOverlayMessage(
            context,
            response['errors'].toString(),
            Colors.black.withOpacity(0.5),
            Colors.white);
      } else {
        commonComponents.showTenantOverlayMessage(
            context,
            'Invalid Tenant Id or Passcode.',
            Colors.black.withOpacity(0.5),
            Colors.white);
      }
    } else {
      if (response['tenantDetails'].length > 0) {
        TenantInfo tenantInfo = TenantInfo.fromJson(response);
        String kioskBanner =
            tenantInfo.tenantDetails.additionalProperties.kioskBanner;
        String receiptLogo =
            tenantInfo.tenantDetails.additionalProperties.receiptLogo;
        String tennatName = tenantInfo.tenantDetails.name;
        if (tenantInfo.success) {
          if (Platform.isAndroid || Platform.isIOS) {
            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return new LoginScreen(
                kioskBanner: kioskBanner,
                tenantCode: tenantId.text,
                receiptLogo: receiptLogo,
                tennatName: tennatName,
              );
            }));
          } else {
            LocalStorage localStorage = LocalStorage();
            await localStorage.setUpLocalDB();
            await localStorage.saveTenantInfo(response);

            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return new LoginScreen(
                kioskBanner: kioskBanner,
                tenantCode: tenantId.text,
                receiptLogo: receiptLogo,
                tennatName: tennatName,
              );
            }));
          }
        } else {
          commonComponents.showTenantOverlayMessage(
              context,
              'Invalid Tenant Id or Passcode.',
              Colors.black.withOpacity(0.5),
              Colors.white);
        }
      } else {
        commonComponents.showTenantOverlayMessage(
            context,
            'Invalid Tenant Id or Passcode.',
            Colors.black.withOpacity(0.5),
            Colors.white);
      }
    }
  }
}

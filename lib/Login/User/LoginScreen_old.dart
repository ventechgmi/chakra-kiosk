import 'dart:async';
import 'package:chakra/home/HomeScreen.dart';
import 'package:chakra/models/Setup/UserAuthentication.dart';
import 'package:chakra/models/Users/SearchMaster.dart';
import 'package:chakra/models/Users/UsersList.dart';
import 'package:chakra/models/temple/TempleSettings.dart';
import 'package:chakra/utils/FormValidations.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:chakra/widgets/common_components.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:io' show Platform;
import 'dart:convert' as convert;

class LoginScreen extends StatefulWidget {
  String kioskBanner = '';
  LoginScreen({this.kioskBanner});
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  CommonComponents commonComponents = new CommonComponents();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  //TabController _tabController;
  final searchController = TextEditingController();
  final adminEmailString = TextEditingController();
  final adminPasswordString = TextEditingController();
  var formValidations = new FormValidations();
  final _formKey = GlobalKey<FormState>();
  List<UsersList> searchUserList = [];
  var textFieldSize = 280.0;
  var buttonSize = 280.0;
  var buttonHeight = 50.0;
  var currentDateTime = "";
  var currentDate = "";
  var adminEmail = 'chakra.team@ventechsolutions.com';
  //var adminPassword = 'C4temples\$';
  var adminPassword = 'chakra@123';
  UserAuthentication userAuthentication;
  var loggedInUser = '';
  var devoteeMail = '';
  var devoteeId = '';
  var userToken = '';
  String searchEmptyText = '';
  bool isAdmin = false;
  var networkHelper = new NetworkHelper();
  int _state = 0;
  Timer _timer;
  LocalStorage _localStorage = LocalStorage();
  bool isAdminSelected = false;

  @override
  void initState() {
    super.initState();
    _getCurrentDate();
    _delayForTime();
    adminEmailString.text = adminEmail;
    adminPasswordString.text = adminPassword;
  }

  @override
  void dispose() {
    _state = 0;
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    /*  _tabController =
        new TabController(length: isAdminSelected ? 2 : 1, vsync: this); */

    if (Platform.isMacOS || Platform.isWindows || Platform.isLinux) {
      textFieldSize = 200.0;
      buttonSize = 200.0;
      buttonHeight = 50.0;
    } else {
      textFieldSize = 160;
      buttonSize = 160;
      buttonHeight = 30.0;
    }

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Image.asset(
              'assets/chakra_logo.png',
              height: 44,
            ),
          ),
          actions: <Widget>[
            Center(
                child: Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Text(currentDateTime,
                        style: new TextStyle(
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        ))))
          ],
        ),
        body: Stack(
          //alignment: Alignment.center,
          children: <Widget>[
            Stack(children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                  fit: BoxFit.cover,
                  image:
                      (widget.kioskBanner == null && widget.kioskBanner == '')
                          ? AssetImage('assets/login_bg.jpg')
                          : NetworkImage(widget.kioskBanner),
                )),
              ),
              Container(
                color: Colors.deepOrange.withOpacity(0.6),
              )
            ]),
            Container(
              padding: (Platform.isAndroid || Platform.isIOS)
                  ? EdgeInsets.all(20)
                  : EdgeInsets.all(50),
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerRight,
                    child: IconButton(
                      icon: isAdminSelected
                          ? Icon(Icons.lock_open, color: Colors.white)
                          : Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                      onPressed: () {
                        setState(() {
                          searchUserList.clear();
                          searchController.clear();
                          adminEmailString.clear();
                          adminPasswordString.clear();

                          if (isAdminSelected) {
                            isAdminSelected = false;
                          } else {
                            isAdminSelected = true;
                          }
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  isAdminSelected
                      ? Form(
                          key: _formKey,
                          child: loginWidget(),
                        )
                      : Form(
                          key: _formKey,
                          child: searchDevotee(),
                        ),
                  /*  Expanded(
                      child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              isAdminSelected ? loginWidget() : searchDevotee(),
                            ],
                          ))), */
                  isAdminSelected
                      ? Expanded(
                          child: Container(),
                        )
                      : Expanded(
                          child: searchResults(),
                        ),
                  Container(
                    margin: EdgeInsets.all(20),
                    height: 1,
                    color: Colors.white.withOpacity(0.3),
                    width: MediaQuery.of(context).size.width - 100,
                  ),
                  Container(
                    width: 320,
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: OutlineButton(
                      onPressed: () {
                        setState(() {
                          isAdmin = false;
                          loggedInUser = 'Guest';
                        });
                        getAnonymousToken(true);
                      },
                      borderSide: BorderSide(color: Colors.white, width: 0.2),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                      child: Text('Continue as Guest User',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                          )),
                    ),
                  ),
                ],
              ),
            ),
            //),
          ],
        ));
  }

  void animateButton(BuildContext context) {
    setState(() {
      _state = 1;
    });
    userLogin(isAdmin, adminEmailString.text, adminPasswordString.text, false,
        context);
  }

  _delayForTime() {
    const oneSecond = const Duration(seconds: 1);
    _timer = Timer.periodic(oneSecond, (Timer t) async {
      _getCurrentDate();
    });
  }

  _getCurrentDate() {
    DateTime now = DateTime.now();
    String formattedDateTime = DateFormat('MMM dd, hh:mm:ss a').format(now);
    String formattedDate = DateFormat('MMM dd,').format(now);

    setState(() {
      currentDate = formattedDate;
      currentDateTime = formattedDateTime;
    });
  }

  void userLogin(bool isAdmin, String username, String password, bool isSearch,
      BuildContext loginContext) async {
    if (isSearch) {
      await getAnonymousToken(false);
      getUserInfo(searchController.text);
    } else {
      var loginBody = {};
      loginBody["tenantCode"] = "chakra";
      loginBody["userName"] = username;
      loginBody["password"] = password;

      String loginJson = convert.jsonEncode(loginBody);
      var jsonResponse = await networkHelper.userLogin(loginJson);

      if (jsonResponse != null) {
        if (jsonResponse['success']) {
          userAuthentication = UserAuthentication.fromJson(jsonResponse);

          print('token in login : ${userAuthentication.token}');

          setState(() {
            userToken = 'Bearer ${userAuthentication.token}';
          });

          if (Platform.isWindows) {
            saveTokenToLocal(userToken, false);
          }

          if (isSearch) {
            getUserInfo(searchController.text);
          } else {
            if (isAdmin) {
              setState(() {
                _state = 2;
              });
            }

            if (_state == 2) {
              Future.delayed(const Duration(milliseconds: 500), () {
                userAuthentication.claims.forEach((item) {
                  if (item.name == 'Kiosk Role') {
                    if (item.privilege != 0) {
                      moveToNextScreen(
                          userAuthentication,
                          isAdmin,
                          userAuthentication.loggedinUser,
                          devoteeMail,
                          devoteeId);
                    } else {}
                  }
                });

                /* setState(() {
                moveToNextScreen(userAuthentication, isAdmin, loggedInUser,
                    devoteeMail, devoteeId);
              }); */
              });
            } else {
              userAuthentication.claims.forEach((item) {
                if (item.name == 'Kiosk Role') {
                  if (item.privilege != 0) {
                    moveToNextScreen(userAuthentication, isAdmin, loggedInUser,
                        devoteeMail, devoteeId);
                  } else {
                    _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                          backgroundColor: Colors.white,
                          content: Container(
                              height: 80,
                              child: Center(
                                child: new Text(
                                  "Please enter the valid credentials",
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 20),
                                ),
                              ))),
                    );
                  }
                }
              });
            }
          }
        } else {
          //print(jsonResponse["message"]);

          setState(() {
            _state = 0;
          });

          _scaffoldKey.currentState.showSnackBar(
            new SnackBar(
                backgroundColor: Colors.white,
                content: Container(
                    height: 80,
                    child: Center(
                      child: new Text(
                        'Invalid Mobile Number/Email and Password',
                        style: TextStyle(color: Colors.red, fontSize: 20),
                      ),
                    ))),
          );
        }
      } else {
        setState(() {
          _state = 0;
        });
        _scaffoldKey.currentState.showSnackBar(
          new SnackBar(
              backgroundColor: Colors.white,
              content: Container(
                  height: 80,
                  child: Center(
                    child: new Text(
                      jsonResponse["message"],
                      style: TextStyle(color: Colors.red, fontSize: 20),
                    ),
                  ))),
        );
      }
    }
  }

  bool _isNumeric(String str) {
    if (str == null) {
      return false;
    }
    return double.tryParse(str) != null;
  }

  void moveToNextScreen(UserAuthentication userAuthenticationResponse,
      bool isAdmin, String loggedInUser, String email, String devoteeId) async {
    int stateValue = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => HomeScreen(
                isAdmin: isAdmin,
                loggedInUser: loggedInUser,
                userAuthentication: userAuthenticationResponse,
                devoteeMail: email,
                devoteeId: devoteeId,
              )),
    );

    if (stateValue == 0) {
      setState(() {
        _state = stateValue;
      });
    }
  }

  void getUserInfo(String searchString) async {
    var response = await networkHelper.getUsersInfo(
        searchString, userAuthentication.token);

    print('search response : ' + response.toString());
    SearchMaster searchMaster = SearchMaster.fromJson(response);

    setState(() {
      if (searchMaster.userList.isEmpty) {
        searchEmptyText = 'Inactive User';
      }
      searchUserList = searchMaster.userList;
    });
  }

  Widget loginWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(5),
        ),
        Center(
          child: Text('Welcome Back!',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Colors.white)),
        ),
        Center(
          child: Text(
            'Log in to get started',
            style: TextStyle(fontSize: 11, color: Colors.white60),
          ),
        ),
        Container(
          width: 600,
          padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          child: TextFormField(
            controller: adminEmailString,
            /* validator: (value) {
              return formValidations.validateEMailAndMobile(value);
            }, */
            style: TextStyle(color: Colors.white, fontSize: 14),
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 10, right: 10),
                focusedErrorBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white, width: 1.0)),
                errorBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white, width: 1.0)),
                focusedBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white60, width: 2)),
                enabledBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white60, width: 2)),
                hintText: 'Mobile Number / Email',
                errorStyle: TextStyle(color: Colors.white),
                hintStyle: TextStyle(color: Colors.white.withOpacity(0.8))),
          ),
        ),
        Container(
          width: 600,
          padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          child: TextFormField(
            controller: adminPasswordString,
            validator: (value) {
              return formValidations.validateTextField(value, 'Password');
            },
            style: TextStyle(color: Colors.white, fontSize: 14),
            obscureText: true,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 10, right: 10),
                focusedErrorBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white, width: 1.0)),
                errorBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white, width: 1.0)),
                focusedBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white60, width: 2)),
                enabledBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.white60, width: 2)),
                hintText: 'Password',
                errorStyle: TextStyle(color: Colors.white),
                hintStyle: TextStyle(color: Colors.white.withOpacity(0.8))),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          //width: MediaQuery.of(context).size.width,
          width: 320,
          padding: EdgeInsets.only(left: 20, right: 20),
          child: RaisedButton(
            elevation: 0,
            color: Colors.white,
            onPressed: () {
              setState(() {
                isAdmin = true;
                loggedInUser = 'Admin';

                if (_state == 0) {
                  animateButton(context);
                }
              });

              /*    if (_formKey.currentState.validate()) {
                setState(() {
                  isAdmin = true;
                  loggedInUser = 'Admin';

                  if (_state == 0) {
                    animateButton(context);
                  }
                });
              } */
            },
            child: setUpButtonChild(),
            textColor: Colors.black,
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
            ),
          ),
        )
      ],
    );
  }

  Widget searchDevotee() {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.all(10),
      constraints: BoxConstraints(maxWidth: 600, maxHeight: 80),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              flex: 6,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: TextFormField(
                  controller: searchController,
                  autofocus: true,
                  validator: (value) {
                    return _isNumeric(value)
                        ? formValidations.validateMobile(value)
                        : formValidations.validateEmail(value);
                  },
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10),
                      errorStyle: TextStyle(color: Colors.white),
                      focusedErrorBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide:
                              BorderSide(color: Colors.white, width: 1.0)),
                      errorBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide:
                              BorderSide(color: Colors.white, width: 1.0)),
                      focusedBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide:
                              BorderSide(color: Colors.white60, width: 2)),
                      enabledBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide:
                              BorderSide(color: Colors.white60, width: 2)),
                      hintText: 'Mobile Number / Email',
                      hintStyle:
                          TextStyle(color: Colors.white.withOpacity(0.8))),
                ),
              )),
          OutlineButton(
            borderSide: BorderSide(color: Colors.white60),
            textColor: Colors.white,
            child: Text('Search'),

            //child: Icon(Icons.search),
            onPressed: () {
              searchEmptyText = '';
              adminEmailString.clear();
              if (_formKey.currentState.validate()) {
                setState(() {
                  isAdmin = false;
                  searchUserList.clear();
                });
                userLogin(isAdmin, adminEmail, adminPassword, true, context);
              } else {
                searchUserList.clear();
              }
            },
          )
        ],
      ),
    );
  }

  Widget searchResults() {
    return Container(
        width: 600,
        child: searchUserList.isEmpty
            ? Center(
                child: Text(searchEmptyText,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                    )))
            : ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: searchUserList.length,
                itemBuilder: (context, index) {
                  return new GestureDetector(
                      onTap: () {
                        setState(() {
                          isAdmin = false;
                          loggedInUser = searchUserList[index].firstName +
                              ' ' +
                              searchUserList[index].lastName;
                          devoteeMail = searchUserList[index].email;
                          devoteeId = searchUserList[index].userId;
                          moveToNextScreen(userAuthentication, isAdmin,
                              loggedInUser, devoteeMail, devoteeId);
                          //getAnonymousToken();
                        });
                      },
                      child: Card(
                        elevation: 4,
                        margin: EdgeInsets.only(left: 20, right: 20, top: 10),
                        color: Colors.white,
                        child: Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '${searchUserList[index].firstName} ${searchUserList[index].lastName}',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12),
                                ),
                                Text(
                                  searchUserList[index].email,
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                      fontSize: 11),
                                ),
                              ],
                            )),
                      ));
                },
              ));
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return new Text(
        "Login",
        style: const TextStyle(
          color: Colors.black,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return SizedBox(
        height: 24,
        width: 24,
        child: CircularProgressIndicator(
          strokeWidth: 1.0,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.orangeAccent),
        ),
      );
    } else {
      return Icon(Icons.check, color: Colors.transparent);
    }
  }

  getAnonymousToken(bool isNextScreen) async {
    var jsonResponse = await networkHelper.getAnonymousToken();
    print(jsonResponse);

    if (jsonResponse != null) {
      userAuthentication = UserAuthentication.fromJson(jsonResponse);

      setState(() {
        userToken = 'Bearer ${userAuthentication.token}';
      });

      await saveTokenToLocal(userToken, true);
      await saveTempleSettings();

      if (isNextScreen) {
        moveToNextScreen(
            userAuthentication, isAdmin, loggedInUser, devoteeMail, devoteeId);
      }
    }
  }

  saveTokenToLocal(String userToken, bool isAnonymous) async {
    await _localStorage.setUpLocalDB();
    await _localStorage.saveTokenToLocal(userToken, isAnonymous);
    await saveTempleSettings();
  }

  saveTempleSettings() async {
    var templeResponse = await networkHelper.getTempleSettings(userToken);

    TempleSettings templeSettings = TempleSettings.fromJson(templeResponse);

    var kioskTempleSettings = {};

    templeSettings.templeSettingsData.forEach((item) async {
      print('minimum amount' + item.kiosk.cCDCMinimumProcessingAmount);

      kioskTempleSettings["IsSkipWalkin"] = item.kiosk.isSkipWalkin;
      kioskTempleSettings["IsSkipReceipt"] = item.kiosk.isSkipReceipt;
      kioskTempleSettings["IsSignatureCopy"] = item.kiosk.isSignatureCopy;
      kioskTempleSettings["ReturnRefundPrint"] = item.kiosk.returnRefundPrint;
      kioskTempleSettings["ReceiptMinimumAmount"] =
          item.kiosk.receiptMinimumAmount;
      kioskTempleSettings["ReturnRefundDuration"] =
          item.kiosk.returnRefundDuration;
      kioskTempleSettings["IsClosingReceiptGroupBy"] =
          item.kiosk.isClosingReceiptGroupBy;
      kioskTempleSettings["CCDCMinimumProcessingAmount"] =
          item.kiosk.cCDCMinimumProcessingAmount;
    });

    String kioskSettingsInfo = convert.jsonEncode(kioskTempleSettings);
    LocalStorage localStorage = LocalStorage();

    await localStorage.setUpLocalDB();
    await localStorage.saveKioskTempleSettings(kioskSettingsInfo);
    print('kiosk settings saved succesfully');
  }
}

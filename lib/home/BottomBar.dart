import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/* class CardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Padding(
          padding: new EdgeInsets.fromLTRB(8.0, 150.0, 8.0, 150.0),
          child: new Card(
            child: new Center(
              child: new Image.asset('images/hackathon_logo_0.png'),
            ),
          ),
        ),
      ),
    );
  }
} */

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        child: Container(
      height: 40,
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            _getCurrentTime(),
            style: new TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.bold,
              color: Colors.red,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Powered by '),
              Image.asset(
                'assets/chakra_logo.png',
                height: 32,
                fit: BoxFit.fitHeight,
              )
            ],
          ),
          Text('V.1.0.',
              style: new TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ))
        ],
      ),
    ));
  }

  _getCurrentTime() {
    DateTime now = DateTime.now();
    String formattedDateTime = DateFormat('MMM dd, hh:mm:ss a').format(now);
    return formattedDateTime;
    /*  setState(() {
      timeString = formattedDateTime;
    }); */
  }
}

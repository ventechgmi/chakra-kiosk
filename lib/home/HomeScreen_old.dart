import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:chakra/Login/User/LoginScreen.dart';
import 'package:chakra/MobileStripe/StripeWeb.dart';
import 'package:chakra/admin/AdminWidgets.dart';
import 'package:chakra/custom_radio/CustomRadio.dart';
import 'package:chakra/models/PlaceOrder/PlaceOrderByCard.dart';
import 'package:chakra/models/PlaceOrder/PlaceOrderByCash.dart';
import 'package:chakra/models/PlaceOrder/PlaceOrderByCheck.dart';
import 'package:chakra/models/PlaceOrder/UpdateOrderUser.dart';
import 'package:chakra/models/PlaceOrder/UpdateUserResponse.dart';
import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';
import 'package:chakra/models/Products/Timings.dart';
import 'package:chakra/models/ReturnRefunds/ResponseMessage.dart';
import 'package:chakra/models/Setup/Tenant/TenantInfo.dart' as tenant;
import 'package:chakra/models/Setup/UserAuthentication.dart';
import 'package:chakra/models/TokenItems.dart';
import 'package:chakra/models/Users/SearchMaster.dart';
import 'package:chakra/models/Users/UsersList.dart';
import 'package:chakra/models/products/Fees.dart';
import 'package:chakra/models/products/Items.dart';
import 'package:chakra/models/products/MasterItems.dart';
import 'package:chakra/models/products/ProductItems.dart';
import 'package:chakra/utils/DateHelper.dart';
import 'package:chakra/utils/FormValidations.dart';
import 'package:chakra/utils/LocalStorage.dart';
import 'package:chakra/utils/NetworkHelper.dart';
import 'package:chakra/utils/Printer.dart';
import 'package:chakra/widgets/common_components.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;
import 'package:process_run/process_run.dart';
import 'dart:math' as math;

class HomeScreen extends StatefulWidget {
  final UserAuthentication userAuthentication;
  final bool isAdmin;
  final String loggedInUser;
  final String devoteeMail;
  final String devoteeId;
  //final Function notifyParent;
  const HomeScreen({
    Key key,
    @required this.userAuthentication,
    @required this.isAdmin,
    @required this.loggedInUser,
    @required this.devoteeMail,
    @required this.devoteeId,
    //@required this.notifyParent
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String authTokenString = '';
  tenant.TenantInfo tenantInfos;
  String selectedFee = '';
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var networkHelper = new NetworkHelper();
  var dateHelper = new DateHelper();
  LocalStorage localStorage = LocalStorage();
  List<PlaceOrderItems> placeOrderList = [];
  List<TokenItems> tokenItemsList = [];
  List<String> catalogueTypes = [];
  List<String> catalogueIcons = [];
  List<String> catalogueColor = [];
  List<int> colors1 = [0xffff7d6b, 0xffffca73, 0xff24f09c, 0xff947def];
  List<int> colors2 = [0xffff3939, 0xfff39f00, 0xff0aabdf, 0xff734de6];
  int closingReceiptPrivilege = 0;

  String devotee_reg_text =
      'Enroll new user or devotee in the temple application for enabling them to receive devotee privileges';
  String return_refund_text =
      'Return bounced checks and collect repayment, or Refund collections for canceled Pujas/events, Generate corrected receipts for the past transactions';
  String closing_receipt_text =
      'Generate day end kiosk transaction details of any terminal (either all or single) collected by specified user';
  List<String> descTexts = [];
  List<Items> gridItems = [];
  List<Items> gridItemsAll = [];
  List<String> gridItemsPrice = [];
  //List<String> multiOccurenceList = [];

  final List<String> payOptions = [
    'assets/card.png',
    'assets/Apple-Pay.png',
    'assets/Amazon-Pay.png',
    'assets/gpay.png',
    'assets/paypal.png'
  ];

  /* final List<String> payOptionsA = [
    'assets/card-A.png',
    'assets/Apple-Pay-A.png',
    'assets/Amazon-Pay-A.png',
    'assets/G-Pay-A.png',
    'assets/Paypal-A.png'
  ];
 */

  int selectedLeftIndex = 0;
  int numOfItems = 1;
  List selectedGridIndex = [];
  List selectedItemTime = [];
  List selectedCartItem = [];
  List serviceType = [];
  List selectedItemPrice = [];
  List selectedItemQuantity = [];
  List isTokenNeeded = [];
  List<double> totalPriceByItem = [];
  double totalPriceOfCart = 0;
  int totalCartItems = 0;
  var timeString = '';
  bool isAdminRoleActive = false;
  bool isAppBarEnabled = false;
  String adminAction = '';
  Timer timer;

  String cdcMinAmount = '';
  bool isSkipReceipt = false;
  bool isSignatureCopy = false;
  String minAmntForReceipt = '';

  /* refresh() {
    setState(() {r
      print('refresh called');
      updateClearCart();
    });
  } */

  onSelected(int index) {
    setState(() {
      if (widget.isAdmin && index == 0) {
        if (totalCartItems > 0) {
          _showDialog(true, index);
        } else {
          selectedLeftIndex = index;
          getGridItems(index);
        }
      } else {
        isAdminRoleActive = false;
        isAppBarEnabled = false;
        selectedLeftIndex = index;
        getGridItems(index);
      }
    });
  }

  onItemSelected(int index, String fee) {
    print('index $index and fee is $fee');
    if (widget.isAdmin && selectedLeftIndex == 0) {
      setState(() {
        isAdminRoleActive = true;
        isAppBarEnabled = true;
      });
      print('selected grid' + gridItems[index].name);
      adminAction = gridItems[index].name;
    } else {
      String startTime;
      String endTime;

      gridItems[index].startTime != null
          ? startTime = gridItems[index].startTime
          : startTime = '00:00';

      gridItems[index].endTime != null
          ? endTime = gridItems[index].endTime
          : endTime = '00:00';

      if (!selectedItemTime.contains(gridItems[index].templeServiceId +
              //gridItems[index].catalogueType +
              startTime +
              endTime
          //+ fee

          )) {
        setState(() {
          selectedGridIndex.add(gridItems[index].templeServiceId);
          selectedItemTime.add(gridItems[index].templeServiceId +
                  //gridItems[index].catalogueType +
                  startTime +
                  endTime
              //+fee
              );
          isTokenNeeded.add(gridItems[index].templeToken);
          if (gridItems[index].feeDetails.length > 0) {
            for (Fees feeObj in gridItems[index].feeDetails)
              if (fee == feeObj.fee) {
                selectedCartItem.add(gridItems[index].name + ' ' + feeObj.name);
              } else if (feeObj.fee == "0.00" && double.parse(fee) > 0) {
                selectedCartItem.add(gridItems[index].name);
              }
          }
          serviceType.add(gridItems[index].serviceType);
          selectedItemPrice.add(fee);
          selectedItemQuantity.add(1);
          totalPriceByItem.add(double.parse(fee));
          updateTotalCartPrice();
          updateTotalCartItem();
        });
        print(selectedItemTime);
      } else if (selectedItemTime.contains(gridItems[index].templeServiceId +
              //gridItems[index].catalogueType +
              startTime +
              endTime
          //+ fee

          )) {
        if (gridItems[index].feeDetails.length > 0) {
          for (Fees feeObj in gridItems[index].feeDetails)
            if (fee == feeObj.fee) {
              if (!selectedCartItem
                  .contains(gridItems[index].name + ' ' + feeObj.name)) {
                setState(() {
                  selectedGridIndex.add(gridItems[index].templeServiceId);
                  selectedItemTime.add(gridItems[index].templeServiceId +
                          //gridItems[index].catalogueType +
                          startTime +
                          endTime
                      //+fee
                      );
                  isTokenNeeded.add(gridItems[index].templeToken);
                  if (gridItems[index].feeDetails.length > 0) {
                    for (Fees feeObj in gridItems[index].feeDetails)
                      if (fee == feeObj.fee) {
                        selectedCartItem
                            .add(gridItems[index].name + ' ' + feeObj.name);
                      }
                  }
                  serviceType.add(gridItems[index].serviceType);
                  selectedItemPrice.add(fee);
                  selectedItemQuantity.add(1);
                  totalPriceByItem.add(double.parse(fee));
                  updateTotalCartPrice();
                  updateTotalCartItem();
                });
                print(selectedItemTime);
              }
            }
        }
      } else {
        int currIndex = 0;

        if (selectedCartItem.contains(gridItems[index].templeServiceId +
            gridItems[index].catalogueType)) {
          currIndex = selectedCartItem.indexOf(gridItems[index].templeServiceId
              // + gridItems[index].catalogueType
              );
          print('currentIndex : $currIndex');
        }
      }
    }
  }

  onRemoveCartItem(int index) {
    setState(() {
      selectedCartItem.removeAt(index);
      serviceType.remove(index);
      selectedItemTime.removeAt(index);
      selectedItemPrice.removeAt(index);
      selectedGridIndex.removeAt(index);
      selectedItemQuantity.removeAt(index);
      totalPriceByItem.removeAt(index);
      updateTotalCartPrice();
      updateTotalCartItem();
    });
  }

  removeNumOfItems(int index) {
    if (selectedItemQuantity[index] != 0) {
      setState(() {
        selectedItemQuantity[index] = selectedItemQuantity[index] - 1;
        updateItemsPrice(index);
        updateTotalCartPrice();
        updateTotalCartItem();

        if (selectedItemQuantity[index] == 0) {
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              onRemoveCartItem(index);
            });
          });
        }
      });
    }
  }

  addNumOfItems(int index) {
    setState(() {
      selectedItemQuantity[index] = selectedItemQuantity[index] + 1;
      updateItemsPrice(index);
      updateTotalCartPrice();
      updateTotalCartItem();
    });
  }

  updateItemsPrice(int index) {
    setState(() {
      print('on updating price ${selectedItemPrice[index]}');
      totalPriceByItem[index] = selectedItemQuantity[index] *
          (double.parse(selectedItemPrice[index]));
    });
  }

  updateTotalCartPrice() {
    setState(() {
      double prices = 0;
      totalPriceByItem.forEach((item) {
        prices = prices + item;
      });
      totalPriceOfCart = prices;
    });
  }

  updateTotalCartItem() {
    setState(() {
      int items = 0;
      selectedItemQuantity.forEach((item) {
        items = items + item;
      });
      totalCartItems = items;
    });
  }

  updateClearCart() {
    selectedGridIndex.clear();
    selectedCartItem.clear();
    serviceType.clear();
    selectedItemTime.clear();
    selectedItemPrice.clear();
    selectedItemQuantity.clear();
    totalPriceByItem.clear();
    totalPriceOfCart = 0;
    totalCartItems = 0;
  }

  @override
  void initState() {
    super.initState();
    getTenantInfo();
    getAllProductItems();
    setupPayOptions();
    getTempleSettings();
    timer = new Timer.periodic(
        Duration(seconds: 1), (Timer t) => _getCurrentTime());
  }

  @override
  void dispose() {
    timer?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      //bottomNavigationBar: BottomBar(),
      appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: new IconThemeData(color: Colors.red),
          backgroundColor: Colors.white,
          title: Row(children: [
            Expanded(flex: 3, child: SizedBox()),
            Expanded(
              flex: 6,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  tenantInfos.tenantDetails.additionalProperties.receiptLogo !=
                          null
                      ? Image.network(
                          tenantInfos
                              .tenantDetails.additionalProperties.receiptLogo,
                          height: 40,
                          width: 40,
                          fit: BoxFit.fitHeight)
                      : Container(
                          height: 40,
                          width: 40,
                        ),
                  SizedBox(width: 10),
                  Text(
                    tenantInfos.tenantDetails.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.red[700],
                        fontSize: 22,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            Expanded(
                flex: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    /* IconButton(
                            icon: Icon(Icons.devices_other),
                            onPressed: () {
                              if (widget.isAdmin) {
                                showStripeDialog(true, context);
                              } else {
                                showStripeDialog(false, context);
                              }
                            },
                          ), */
                    widget.isAdmin
                        ? SizedBox(
                            width: 10,
                          )
                        : SizedBox(
                            width: 0,
                          ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'KIOSK 1',
                          style:
                              TextStyle(fontSize: 16, color: Colors.red[700]),
                        ),
                        Text(
                          'Welcome, ${toBeginningOfSentenceCase(widget.loggedInUser.toString())}',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => LoginScreen(
                                    kioskBanner: tenantInfos.tenantDetails
                                        .additionalProperties.kioskBanner)),
                            (Route<dynamic> route) => false);
                      },
                      child: Icon(
                        FontAwesomeIcons.signOutAlt,
                      ),
                    ),
                  ],
                )),
          ])),
      body: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              child: ListView.builder(
                  itemCount: catalogueTypes.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new GestureDetector(
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.height / 5,
                            width: MediaQuery.of(context).size.height / 5,
                            /* color: selectedLeftIndex != null &&
                                                      selectedLeftIndex == index
                                                  ? Colors.white
                                                  : colors[index], */

                            decoration: BoxDecoration(
                                //color: Colors.white,
                                gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: selectedLeftIndex != null &&
                                            selectedLeftIndex == index
                                        ? [Colors.white, Colors.white]
                                        : index >= 4
                                            ? [
                                                Color(colors1[index % 4]),
                                                Color(colors2[index % 4])
                                              ]
                                            : [
                                                Color(colors1[index]),
                                                Color(colors2[index])
                                              ])),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    catalogueIcons[index] == 'assets/admin.svg'
                                        ? SvgPicture.asset(
                                            catalogueIcons[index],
                                            color: selectedLeftIndex != null &&
                                                    selectedLeftIndex == index
                                                ? index >= 4
                                                    ? Color(colors2[index % 4])
                                                    : Color(colors2[index])
                                                : Colors.white,
                                            width: (Platform.isAndroid ||
                                                    Platform.isIOS)
                                                ? 24
                                                : 32,
                                            height: (Platform.isAndroid ||
                                                    Platform.isIOS)
                                                ? 24
                                                : 32,
                                          )
                                        : SvgPicture.network(
                                            catalogueIcons[index],
                                            color: selectedLeftIndex != null &&
                                                    selectedLeftIndex == index
                                                ? index >= 4
                                                    ? Color(colors2[index % 4])
                                                    : Color(colors1[index])
                                                : Colors.white,
                                            width: (Platform.isAndroid ||
                                                    Platform.isIOS)
                                                ? 32
                                                : 44,
                                            height: (Platform.isAndroid ||
                                                    Platform.isIOS)
                                                ? 32
                                                : 44,
                                          ),
                                    Text(
                                      toBeginningOfSentenceCase(
                                          '${catalogueTypes[index]}'
                                              .toLowerCase()),
                                      maxLines: 1,
                                      softWrap: true,
                                      style: TextStyle(
                                        fontSize: (Platform.isAndroid ||
                                                Platform.isIOS)
                                            ? 14
                                            : 16,
                                        color: selectedLeftIndex != null &&
                                                selectedLeftIndex == index
                                            ? index >= 4
                                                ? Color(colors2[index % 4])
                                                : Color(colors2[index])
                                            : Colors.white,
                                      ),
                                    )
                                  ]),
                            ),
                          ),
                          Visibility(
                            visible: selectedLeftIndex != null &&
                                    selectedLeftIndex == index
                                ? true
                                : false,
                            child: Container(
                              height: MediaQuery.of(context).size.height / 5,
                              width: MediaQuery.of(context).size.height / 5,
                              child: SvgPicture.asset(
                                'assets/arc-shape.svg',
                                color: index >= 4
                                    ? Color(colors2[index % 4])
                                    : Color(colors2[index]),
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          )
                        ],
                      ),
                      onTap: () {
                        onSelected(index);
                      },
                    );
                  }),
            ),
          ),
          Expanded(
            flex: isAdminRoleActive ? 8 : 5,
            child: Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                child: isAdminRoleActive
                    ? Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                                width: 80,
                                margin: EdgeInsets.only(left: 40),
                                child: FlatButton(
                                  child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: SvgPicture.asset(
                                        'assets/undo.svg',
                                        height: 32,
                                        width: 32,
                                        color: Colors.deepOrange,
                                      )),
                                  onPressed: () {
                                    setState(() {
                                      isAdminRoleActive = false;
                                    });
                                  },
                                )),
                          ),
                          Expanded(
                              child: AdminWidget(
                                  isAdminRoleActive: isAdminRoleActive,
                                  selectedAdmin: adminAction,
                                  token: authTokenString,
                                  userId: widget.userAuthentication.userId,
                                  reportPrivilege: closingReceiptPrivilege))
                        ],
                      )
                    : GridView.builder(
                        padding: EdgeInsets.all(10),
                        /*  padding: EdgeInsets.only(
                                              left: 10, right: 10, top: 2, bottom: 2), */
                        itemCount: gridItems.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3),
                        itemBuilder: (BuildContext context, int index) {
                          return new GestureDetector(
                            child: widget.isAdmin && selectedLeftIndex == 0
                                ? Card(
                                    margin: EdgeInsets.all(10),
                                    elevation: 4.0,
                                    child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Image.asset(
                                            gridItems[index].imagePath,
                                            width: 60,
                                            height: 60,
                                          ),
                                          Text(
                                            gridItems[index].name,
                                            style: TextStyle(
                                                color: Colors.red[900],
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 10, bottom: 20),
                                            width: 40,
                                            height: 1,
                                            color: Colors.blueGrey,
                                          ),
                                          Text(
                                            descTexts[index].toString(),
                                            style:
                                                TextStyle(color: Colors.grey),
                                          )
                                        ],
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  )
                                : new Container(
                                    margin: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(
                                          color: Colors.black.withOpacity(0.1),
                                          blurRadius: 8,
                                          spreadRadius: 2.4,
                                          offset: new Offset(0.0, 2.0),
                                        ),
                                      ],
                                    ),
                                    child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        child: Stack(
                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 3,
                                                  child: Container(
                                                    alignment:
                                                        Alignment.bottomRight,
                                                    padding:
                                                        new EdgeInsets.only(
                                                      right: 8.0,
                                                    ),
                                                    decoration:
                                                        new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                        image: gridItems[index]
                                                                    .imagePath !=
                                                                null
                                                            ? new NetworkImage(
                                                                gridItems[index]
                                                                    .imagePath)
                                                            : new AssetImage(
                                                                'assets/meenakshi.jpg'),
                                                        fit: BoxFit.fitHeight,
                                                      ),
                                                    ),
                                                    child: widget.isAdmin &&
                                                            isAdminRoleActive
                                                        ? Container(
                                                            height: 0,
                                                          )
                                                        : Container(
                                                            decoration:
                                                                new BoxDecoration(
                                                              color:
                                                                  Colors.green,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4.0),
                                                              border: new Border
                                                                      .all(
                                                                  width: 2,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 8,
                                                                    right: 8,
                                                                    top: 4,
                                                                    bottom: 4),
                                                            child: new Text(
                                                                '\$ ${gridItems[index].feeDetails[0].fee}',
                                                                style: new TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .italic,
                                                                    fontSize:
                                                                        12))),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Container(
                                                      padding: (Platform
                                                                  .isAndroid ||
                                                              Platform.isIOS)
                                                          ? EdgeInsets.only(
                                                              left: 5,
                                                              right: 5,
                                                              top: 2,
                                                              bottom: 2)
                                                          : EdgeInsets.all(10),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      decoration: BoxDecoration(
                                                          gradient: LinearGradient(
                                                              begin: Alignment
                                                                  .centerLeft,
                                                              end: Alignment
                                                                  .centerRight,
                                                              colors: [
                                                            Color(0xffff3939),
                                                            Color(0xffff7d6b)
                                                          ])),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Expanded(
                                                            child: Text(
                                                              toBeginningOfSentenceCase(
                                                                  gridItems[
                                                                          index]
                                                                      .name
                                                                      .toLowerCase()),
                                                              style: TextStyle(
                                                                fontSize: (Platform
                                                                            .isAndroid ||
                                                                        Platform
                                                                            .isIOS)
                                                                    ? 10
                                                                    : 14,
                                                                letterSpacing:
                                                                    0.5,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              softWrap: true,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: <
                                                                  Widget>[
                                                                (gridItems[index].startTime !=
                                                                            null) &&
                                                                        (gridItems[index].endTime !=
                                                                            null)
                                                                    ? Text(
                                                                        getFormattedTimeOnly(gridItems[index].startTime) +
                                                                            '-' +
                                                                            getFormattedTimeOnly(gridItems[index].endTime),
                                                                        style:
                                                                            TextStyle(
                                                                          color:
                                                                              Colors.white,
                                                                          fontSize: (Platform.isAndroid || Platform.isIOS)
                                                                              ? 10
                                                                              : 12,
                                                                        ),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                      )
                                                                    : SizedBox(
                                                                        height:
                                                                            0),
                                                                gridItems[index]
                                                                            .serviceType ==
                                                                        'Event'
                                                                    ? Align(
                                                                        alignment:
                                                                            Alignment.bottomRight,
                                                                        child:
                                                                            FlatButton(
                                                                          child: Text(
                                                                              'View schedule',
                                                                              style: TextStyle(
                                                                                color: Colors.white,
                                                                                fontSize: (Platform.isAndroid || Platform.isIOS) ? 8 : 10,
                                                                                decoration: TextDecoration.underline,
                                                                              )),
                                                                          onPressed:
                                                                              () {
                                                                            showScheduleForEvent(
                                                                                gridItems[index].eventTimings,
                                                                                gridItems[index].name,
                                                                                gridItems[index].catalogueType,
                                                                                gridItems[index].startDate,
                                                                                gridItems[index].endDate,
                                                                                index);
                                                                          },
                                                                        ),
                                                                      )
                                                                    : SizedBox(
                                                                        width:
                                                                            0,
                                                                      )
                                                              ],
                                                            ),
                                                          )
                                                        ],
                                                      )),
                                                )
                                              ],
                                            ),
                                            Visibility(
                                              visible:
                                                  isCartValid(gridItems[index])
                                                      ? true
                                                      : false,
                                              child: Container(
                                                child: Center(
                                                  child: Icon(
                                                    Icons.done,
                                                    size: 34,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                                color: Color(colors1[0])
                                                    .withOpacity(0.7),
                                              ),
                                            )
                                          ],
                                        )),
                                  ),
                            // ),
                            onTap: () {
                              if (widget.isAdmin && selectedLeftIndex == 0) {
                                onItemSelected(index, '0');
                              } else if (gridItems[index].feeDetails[0].fee ==
                                      '0.00' &&
                                  !selectedGridIndex.contains(
                                      gridItems[index].templeServiceId)) {
                                showZeroFeeDialog(index);
                              } else {
                                if (gridItems[index].feeDetails.length > 1) {
                                  showFeeDialog(
                                      gridItems[index].feeDetails, index);
                                } else {
                                  //print('index : ${index.toString()}');
                                  if (gridItems[index].feeDetails[0].fee !=
                                      '0.00') {
                                    onItemSelected(index,
                                        gridItems[index].feeDetails[0].fee);
                                  }
                                }
                              }
                            },
                          );
                        })),
          ),
          widget.isAdmin && selectedLeftIndex == 0
              ? Expanded(
                  flex: 1,
                  child: Container(),
                )
              : Expanded(
                  flex: 2,
                  child: Container(
                    margin: EdgeInsets.all(10),
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        //color: Colors.grey[200],
                        color: Color(0XFFFBF8E8)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Visibility(
                                  visible: selectedGridIndex.length > 0
                                      ? true
                                      : false,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Your cart',
                                      style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        //fontFamily: 'Roboto'
                                      ),
                                    ),
                                  )),
                              Visibility(
                                  visible: selectedGridIndex.length > 0
                                      ? true
                                      : false,
                                  child: OutlineButton(
                                    padding: EdgeInsets.all(4),
                                    child: Text('Clear Cart',
                                        style: TextStyle(
                                            color: Colors.deepOrange,
                                            fontSize: 12)),
                                    onPressed: () {
                                      _showDialog(false, 0);
                                    },
                                    borderSide: BorderSide(
                                      color: Colors.deepOrange,
                                    ),
                                  )

                                  /* InkWell(
                                                  onTap: () {
                                                    _showDialog(false, 0);
                                                  },
                                                  child: Container(
                                                      child: Image.asset(
                                                    'assets/clear-cart.png',
                                                    width: 24,
                                                    height: 24,
                                                  )),
                                                ) */

                                  /* Row(
                                                    children: <Widget>[
                                                      Badge(
                                                        animationType: BadgeAnimationType.scale,
                                                        badgeColor: Colors.green,
                                                        shape: BadgeShape.circle,
                                                        toAnimate: false,
                                                        badgeContent: Text(
                                                            totalCartItems.toString(),
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: 8,
                                                                fontWeight: FontWeight.w400)),
                                                        child: Image.asset('assets/cart.png'),
                                                      ),
                                                      SizedBox(
                                                        width: 20,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          _showDialog(false, 0);
                                                        },
                                                        child: Container(
                                                            child: Image.asset(
                                                          'assets/clear-cart.png',
                                                          width: 24,
                                                          height: 24,
                                                        )),
                                                      )
                                                    ],
                                                  ),
                                               */
                                  ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: selectedGridIndex.length <= 0
                                ? Center(
                                    child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    //crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Image.asset(
                                        'assets/empty_cart_color.png',
                                        fit: BoxFit.scaleDown,
                                      ),
                                      Text(
                                        'Your shopping cart is empty',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w200,
                                          color: Colors.blueGrey[300],
                                        ),
                                      )
                                    ],
                                  ))
                                : ListView.builder(
                                    itemCount: selectedCartItem.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return new Card(
                                        margin: EdgeInsets.all(2),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              top: 5,
                                              left: 10,
                                              right: 10,
                                              bottom: 5),
                                          child: Stack(
                                            alignment: Alignment.topRight,
                                            children: <Widget>[
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                      toBeginningOfSentenceCase(
                                                          selectedCartItem[
                                                                  index]
                                                              .toString()
                                                              .toLowerCase())),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Row(
                                                          children: <Widget>[
                                                            InkWell(
                                                              child: Container(
                                                                alignment:
                                                                    new FractionalOffset(
                                                                        0.0,
                                                                        0.0),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(5),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: Colors
                                                                      .amber,
                                                                  /*  border:
                                                                                        new Border
                                                                                            .all(
                                                                                      color: Colors
                                                                                          .green,
                                                                                      width: 2.0,
                                                                                    ), */
                                                                  shape: BoxShape
                                                                      .circle,
                                                                ),
                                                                child: Icon(
                                                                  Icons.remove,
                                                                  size: 18,
                                                                  color: Colors
                                                                      .black,
                                                                ),
                                                              ),
                                                              onTap: () {
                                                                removeNumOfItems(
                                                                    index);
                                                              },
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Container(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(5),
                                                              child: selectedItemQuantity
                                                                          .length >
                                                                      0
                                                                  ? Text(
                                                                      selectedItemQuantity[
                                                                              index]
                                                                          .toString(),
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              18),
                                                                    )
                                                                  : Text('0'),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            InkWell(
                                                              child: Container(
                                                                alignment:
                                                                    new FractionalOffset(
                                                                        0.0,
                                                                        0.0),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(5),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: Colors
                                                                      .amber,
                                                                  /*  border:
                                                                                        new Border
                                                                                            .all(
                                                                                      color: Colors
                                                                                          .green,
                                                                                      width: 2.0,
                                                                                    ), */
                                                                  shape: BoxShape
                                                                      .circle,
                                                                ),
                                                                child: Icon(
                                                                  Icons.add,
                                                                  size: 18,
                                                                  color: Colors
                                                                      .black,
                                                                ),
                                                              ),
                                                              onTap: () {
                                                                addNumOfItems(
                                                                    index);
                                                              },
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      totalPriceByItem.length >
                                                              0
                                                          ? Text(
                                                              '\$ ${totalPriceByItem[index].toStringAsFixed(2)}')
                                                          : Text('0')
                                                    ],
                                                  )
                                                ],
                                              ),
                                              InkWell(
                                                child: Padding(
                                                    padding: EdgeInsets.all(10),
                                                    child: Icon(
                                                      FontAwesomeIcons.trashAlt,
                                                      color: Colors.red,
                                                      semanticLabel: 'Close',
                                                      size: 14,
                                                    )),
                                                onTap: () {
                                                  onRemoveCartItem(index);
                                                },
                                              )
                                            ],
                                          ),
                                        ),
                                        color: Colors.white,
                                        elevation: 0,
                                      );
                                    }),
                          ),
                        ),
                        /*  ClipRRect(
                                            borderRadius: BorderRadius.only(
                                                bottomRight: Radius.circular(15),
                                                bottomLeft: Radius.circular(15)), */
                        //child:
                        Container(
                            //color: Colors.white,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(
                                bottom: 10, left: 10, right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Total ',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.normal),
                                      textAlign: TextAlign.left,
                                    ),
                                    Visibility(
                                      visible:
                                          totalCartItems > 0 ? true : false,
                                      child: Text(
                                        '($totalCartItems items)',
                                        style: TextStyle(
                                          fontFamily: 'Roboto',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '\$ ${totalPriceOfCart.toStringAsFixed(2)}',
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontSize: 22,
                                    fontWeight: FontWeight.w500,
                                  ),
                                )
                              ],
                            )),
                        //)
                      ],
                    ),
                  ),
                ),
          widget.isAdmin && selectedLeftIndex == 0
              ? Expanded(
                  flex: 0,
                  child: SizedBox(
                    width: 0,
                  ))
              : Expanded(
                  flex: 1,
                  child: Container(
                    color: Colors.white,
                    child: ListView.separated(
                        itemCount: payOptions.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                              height: 1.0,
                              indent: 20.0,
                              endIndent: 20.0,
                              color: Colors.orange.withOpacity(0.4),
                            ),
                        itemBuilder: (BuildContext context, int index) {
                          return new GestureDetector(
                            child: ColorFiltered(
                                colorFilter: ColorFilter.mode(
                                    totalPriceOfCart != 0
                                        ? Colors.white.withOpacity(1.0)
                                        : Colors.white.withOpacity(0.2),
                                    BlendMode.dstATop),
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.height / 7,
                                  width: MediaQuery.of(context).size.height / 7,
                                  child: Padding(
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: Image.asset(
                                        payOptions[index],
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                )),
                            onTap: totalPriceOfCart != 0
                                ? () {
                                    payItems(index, context);
                                  }
                                : () {},
                          );
                        }),
                  ),
                )
        ],
      ),
    );
  }

  isCartValid(Items gridItems) {
    bool isValid = false;

    String startTime;
    String endTime;

    gridItems.startTime != null
        ? startTime = gridItems.startTime
        : startTime = '00:00';

    gridItems.endTime != null ? endTime = gridItems.endTime : endTime = '00:00';

    if (selectedItemTime.contains(gridItems.templeServiceId +
            //gridItems.catalogueType +
            startTime +
            endTime
        //+gridItems.feeDetails[0].fee

        )) {
      isValid = true;
    } else {
      isValid = false;
    }

    /*  if (gridItems.startTime != null && gridItems.endTime != null) {
                if (selectedItemTime.contains(gridItems.templeServiceId +
                    gridItems.catalogueType +
                    gridItems.startTime +
                    gridItems.endTime)) {
                  isValid = true;
                }
              } else if (selectedItemTime
                  .contains(gridItems.templeServiceId + gridItems.catalogueType)) {
                isValid = true;
              } else {
                isValid = false;
              }
           */
    //print(isValid);
    return isValid;
  }

  void getAllProductItems() async {
    await getUserToken();

    var jsonResponse = await networkHelper.getAllProductItems(authTokenString);
    MasterItems masterItems = MasterItems.fromJson(jsonResponse);
    List<ProductItems> productItems = masterItems.products;

    if (productItems != null) {
      setState(() {
        if (widget.isAdmin) {
          catalogueTypes.insert(0, 'Admin');
          catalogueIcons.insert(0, 'assets/admin.svg');
        }

        productItems.forEach((product) {
          catalogueTypes.add(product.catalogueType);
          catalogueIcons.add(product.catalogueTypeIcon);
          catalogueColor.add(product.colorString);
          for (int i = 0; i < product.items.length; i++) {
            gridItemsAll.add(product.items[i]);
          }
        });
        print(catalogueIcons);
        getGridItems(selectedLeftIndex);
      });
    }
  }

  void getGridItems(int index) {
    setState(() {
      gridItems.clear();
      if (widget.isAdmin && index == 0) {
        Items item1 = Items();
        Items item2 = Items();
        Items item3 = Items();

        item1.name = "Devotee Enrollment";
        item2.name = "Return / Refund";
        item3.name = "Closing Receipt";
        item1.imagePath = "assets/registration.png";
        item2.imagePath = "assets/refund.png";
        item3.imagePath = "assets/reports.png";

        widget.userAuthentication.claims.forEach((item) {
          if (item.name == 'User Profile') {
            if (item.privilege == 2) {
              gridItems.add(item1);
              descTexts.add(devotee_reg_text);
            }
          }
          if (item.name == 'Kiosk Return and Refund') {
            if (item.privilege != 0) {
              gridItems.add(item2);
              descTexts.add(return_refund_text);
            }
          }

          if (item.name == 'Kiosk Closing Receipt') {
            closingReceiptPrivilege = item.privilege;
            if (item.privilege != 0) {
              gridItems.add(item3);
              descTexts.add(closing_receipt_text);
            }
          }
        });
      }

      gridItemsAll.forEach((item) {
        if (item.catalogueType == catalogueTypes[index]) {
          gridItems.add(item);
        }
      });
    });
  }

  _getCurrentTime() {
    DateTime now = DateTime.now();
    String formattedDateTime = DateFormat('MMM dd, hh:mm:ss a').format(now);

    setState(() {
      timeString = formattedDateTime;
    });
  }

  void _showDialog(bool isAdmin, int index) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            "Clear cart items?",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          content:
              new Text("This will remove all your items added to the cart"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(
                "NO",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                "YES",
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                setState(() {
                  updateClearCart();
                });

                if (isAdmin) {
                  setState(() {
                    isAdminRoleActive = false;
                    selectedLeftIndex = index;
                    getGridItems(index);
                  });
                }

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void setupPayOptions() {
    widget.userAuthentication.claims.forEach((item) {
      if (item.name == 'Pay Method' && item.privilege == 2) {
        setState(() {
          payOptions.insert(0, 'assets/cash.png');
          payOptions.insert(1, 'assets/cheque.png');
        });
      }
    });

    /* if (widget.isAdmin) {
                    setState(() {
                      payOptions.insert(0, 'assets/cash.png');
                      payOptions.insert(1, 'assets/cheque.png');
                    });
                  } */
  }

  void payItems(int index, BuildContext context) {
    if (totalCartItems == 0) {
      showSnackBar(context, 'Your cart is empty');
    } else {
      //if (widget.isAdmin) {
      switch (payOptions[index]) {
        case 'assets/cash.png':
          paymentDialog(0, totalPriceOfCart, 'Cash', authTokenString,
              widget.loggedInUser, context, '', '');
          break;
        case 'assets/cheque.png':
          paymentDialog(1, totalPriceOfCart, 'Check', authTokenString,
              widget.loggedInUser, context, '', '');
          break;
        case 'assets/card.png':
          if (totalPriceOfCart < double.parse(cdcMinAmount)) {
            showSnackBar(context,
                'Total cart amount is too low for card payment, Please choose another payment method');
          } else {
            cardPaymentDialog(totalPriceOfCart);
          }

          break;
        default:
          showSnackBar(context,
              'This payment is not supported yet, Please contact the admin');
          break;
      }
    }
  }

  void paymentDialogForCard(
    String cardNumber,
    String payeeName,
    String expiryDate,
    double totalAmount,
    String token,
    String loggedInUser,
    BuildContext scaffoldContext,
    String email,
    String devoteeId,
  ) {
    TextEditingController payeeNameController = TextEditingController();
    TextEditingController cvvText = TextEditingController();
    TextEditingController expiryController = TextEditingController();
    TextEditingController cardNumberController = TextEditingController();

    expiryController.text = expiryDate;
    payeeNameController.text = payeeName;
    cardNumberController.text = cardNumber;

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Container(
                  child: Row(
                children: [
                  Expanded(
                    child: Text('Card Payment'),
                  ),
                  IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                ],
              )),
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Container(
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text('Card Number',
                                  style: TextStyle(fontSize: 12)),
                            ),
                            Padding(
                                padding: EdgeInsets.all(10),
                                child: Container(
                                    constraints: BoxConstraints(
                                        maxHeight: 40, minWidth: 200),
                                    padding: EdgeInsets.only(left: 5, right: 5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: Colors.grey)),
                                    child: TextField(
                                        controller: cardNumberController,
                                        style: TextStyle(color: Colors.black),
                                        obscureText: true,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            border: InputBorder.none)))),
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text('Card Holder Name',
                                  style: TextStyle(fontSize: 12)),
                            ),
                            Padding(
                                padding: EdgeInsets.all(10),
                                child: Container(
                                    constraints: BoxConstraints(
                                        maxHeight: 40, minWidth: 200),
                                    padding: EdgeInsets.only(left: 5, right: 5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: Colors.grey)),
                                    child: TextField(
                                      controller: payeeNameController,
                                      enabled: false,
                                      style: TextStyle(fontSize: 12),
                                      decoration: InputDecoration(
                                          border: InputBorder.none),
                                    ))),
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text('Card Expiry',
                                  style: TextStyle(fontSize: 12)),
                            ),
                            Padding(
                              child: Container(
                                  constraints: BoxConstraints(
                                      maxHeight: 40, minWidth: 200),
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(color: Colors.grey)),
                                  child: TextField(
                                      controller: expiryController,
                                      //enabled: false,
                                      style: TextStyle(fontSize: 12),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                      ))),
                              padding: EdgeInsets.all(10),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child:
                                  Text('CVV', style: TextStyle(fontSize: 12)),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Container(
                                  constraints: BoxConstraints(
                                      maxHeight: 40, minWidth: 200),
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(color: Colors.grey)),
                                  child: TextField(
                                    controller: cvvText,
                                    style: TextStyle(fontSize: 12),
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'CVV',
                                    ),
                                  )),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Container(
                                alignment: Alignment.center,
                                child: RaisedButton(
                                    color: Colors.deepOrange,
                                    child: Text('PAY',
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.white)),
                                    onPressed: () {
                                      PlaceOrderByCard placeOrder =
                                          PlaceOrderByCard();
                                      placeOrder.amount =
                                          totalPriceOfCart.toString();
                                      placeOrder.paymentType =
                                          'Credit/Debit Card';
                                      placeOrder.cardNumber = cardNumber;
                                      placeOrder.payeeName = payeeName;
                                      placeOrder.cvv = cvvText.text;
                                      placeOrder.expiryDate =
                                          expiryController.text;
                                      placeOrder.transactionDate =
                                          DateTime.now().toString();
                                      placeOrder.notes = '';
                                      placeOrder.placeOrderItems =
                                          getOrderDetails();
                                      String ordersJson =
                                          convert.jsonEncode(placeOrder);
                                      payForCartItems(
                                          ordersJson,
                                          widget.userAuthentication,
                                          context,
                                          widget.isAdmin,
                                          widget.loggedInUser,
                                          email,
                                          widget.devoteeId,
                                          isSkipReceipt,
                                          isSignatureCopy,
                                          updateClearCart,
                                          tokenItemsList,
                                          minAmntForReceipt);
                                    }),
                              ),
                            )
                          ],
                        )),
                  ),
                );
              }));
        });
  }

  void paymentDialog(
      int index,
      double totalAmount,
      String paymentType,
      String token,
      String loggedInUser,
      BuildContext scaffoleContext,
      String email,
      String devoteeId) {
    TextEditingController cashCollectedText = TextEditingController();
    TextEditingController balanceDue = TextEditingController();
    TextEditingController notesText = TextEditingController();
    TextEditingController checkNumber = TextEditingController();
    TextEditingController payeeName = TextEditingController();
    TextEditingController bankName = TextEditingController();
    TextEditingController receiptMail = TextEditingController();

    bool _isValidPayeeName = false;
    bool _isValidCheckNumber = false;
    bool _isValidBankName = false;

    bool _isValidCashCollected = true;

    print('devotee mail : $email');

    if (email.isNotEmpty) {
      receiptMail.text = email;
    } else {
      receiptMail.text = '';
    }

    DateTime selectedCheckDate = DateTime.now();
    final _amountValidator =
        RegExp('^\$|^(0|([1-9][0-9]{0,}))(\\.[0-9]{0,})?\$');
    //String printOption = 'Print only';

    /*  if (loggedInUser != ('Admin') || loggedInUser != ('Guest')) {
                            payeeName.text = loggedInUser;
                          } */

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String errorText = '';
        return AlertDialog(
            titlePadding: EdgeInsets.all(0),
            title: Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('$paymentType payment',
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.w500)),
                    Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    )
                  ],
                )),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      constraints: BoxConstraints(minWidth: 400),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Total (\$) : ',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border:
                                              Border.all(color: Colors.grey)),
                                      child: TextField(
                                        enabled: false,
                                        style: TextStyle(fontSize: 12),
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(4),
                                            hintText:
                                                totalAmount.toStringAsFixed(2),
                                            border: InputBorder.none),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          Visibility(
                              visible:
                                  widget.isAdmin && index == 0 ? true : false,
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        'Cash collected  (\$) : *  ',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                        flex: 2,
                                        child: Container(
                                          padding: EdgeInsets.all(2),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              border: Border.all(
                                                  color: Colors.grey)),
                                          child: TextFormField(
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  8),
                                              DecimalTextInputFormatter(
                                                  decimalRange: 2),
                                            ],
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                              decimal: true,
                                              signed: false,
                                            ),
                                            style: TextStyle(fontSize: 12),
                                            controller: cashCollectedText,
                                            decoration: InputDecoration(
                                                errorText: _isValidCashCollected
                                                    ? ''
                                                    : null,
                                                errorStyle:
                                                    TextStyle(fontSize: 10),
                                                contentPadding:
                                                    EdgeInsets.all(4),
                                                hintText: '0.00',
                                                border: InputBorder.none),
                                            onChanged: (text) {
                                              double cashCollected =
                                                  double.parse(text);
                                              if (cashCollected >=
                                                  totalAmount) {
                                                balanceDue.text =
                                                    (cashCollected -
                                                            totalAmount)
                                                        .toString();
                                                setState(() {
                                                  _isValidCashCollected = false;
                                                });
                                              } else {
                                                balanceDue.text = '';
                                                //  cashCollectedText.clear();
                                                setState(() {
                                                  _isValidCashCollected = true;
                                                });
                                                /* cashCollectedText.text =
                                                                                                            'Entered value is less than '; */
                                                //cashCollected = 0;
                                              }
                                            },
                                          ),
                                        ))
                                  ],
                                ),
                              )),
                          Visibility(
                              visible:
                                  widget.isAdmin && index == 0 ? true : false,
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        'Balance Due (\$): ',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                        flex: 2,
                                        child: Container(
                                          padding: EdgeInsets.all(2),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              border: Border.all(
                                                  color: Colors.grey)),
                                          child: TextField(
                                            style: TextStyle(fontSize: 12),
                                            enabled: false,
                                            controller: balanceDue,
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.all(4),
                                                hintText: '0.00',
                                                border: InputBorder.none),
                                          ),
                                        ))
                                  ],
                                ),
                              )),
                          Visibility(
                            visible: (paymentType == 'Check') ||
                                    (paymentType == 'Card')
                                ? true
                                : false,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Payee Name *',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                      flex: 2,
                                      child: Container(
                                        padding: EdgeInsets.all(2),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            border:
                                                Border.all(color: Colors.grey)),
                                        child: TextField(
                                          onChanged: (text) {
                                            if (text.length > 0) {
                                              setState(() {
                                                _isValidPayeeName = false;
                                              });
                                            }
                                          },
                                          style: TextStyle(fontSize: 12),
                                          controller: payeeName,
                                          decoration: InputDecoration(
                                              errorText: _isValidPayeeName
                                                  ? 'Enter payee name'
                                                  : null,
                                              contentPadding: EdgeInsets.all(4),
                                              hintText: 'Payee Name',
                                              border: InputBorder.none),
                                        ),
                                      ))
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible:
                                widget.isAdmin && index == 1 ? true : false,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Check Number *',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                      flex: 2,
                                      child: Container(
                                        padding: EdgeInsets.all(2),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            border:
                                                Border.all(color: Colors.grey)),
                                        child: TextField(
                                          onChanged: (text) {
                                            if (text.length > 0) {
                                              setState(() {
                                                _isValidCheckNumber = false;
                                              });
                                            }
                                          },
                                          style: TextStyle(fontSize: 12),
                                          controller: checkNumber,
                                          keyboardType: TextInputType.number,
                                          inputFormatters: <TextInputFormatter>[
                                            WhitelistingTextInputFormatter
                                                .digitsOnly
                                          ],
                                          decoration: InputDecoration(
                                              errorText: _isValidCheckNumber
                                                  ? 'Enter check number'
                                                  : null,
                                              contentPadding: EdgeInsets.all(4),
                                              hintText: 'Check Number',
                                              border: InputBorder.none),
                                        ),
                                      ))
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible:
                                widget.isAdmin && index == 1 ? true : false,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Check Date',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: OutlineButton(
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              dateHelper.getFromattedDate(
                                                  selectedCheckDate.toString()),
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(5),
                                            child: Icon(
                                              FontAwesomeIcons.solidCalendarAlt,
                                              color: Colors.deepOrange,
                                              size: 14,
                                            ),
                                          )
                                        ],
                                      ),
                                      onPressed: () async {
                                        DateTime picked = await showDatePicker(
                                            context: context,
                                            initialDate: selectedCheckDate,
                                            firstDate: DateTime.now(),
                                            lastDate: DateTime(2100, 12));
                                        setState(() {
                                          if (picked != null) {
                                            selectedCheckDate = picked;
                                          } else {
                                            selectedCheckDate = DateTime.now();
                                          }
                                        });
                                      },
                                    ),

                                    /*  Text(
                                                                                                                          /*  DateTime.now()
                                                                                                                              .toString()
                                                                                                                              .substring(0, 10) */
                                                                                                                          dateHelper.getCurrentDate(),
                                                                                                                          style: TextStyle(fontSize: 12),
                                                                                                                        ), */
                                  )
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible:
                                widget.isAdmin && index == 1 ? true : false,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Bank Name ',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border:
                                              Border.all(color: Colors.grey)),
                                      child: TextField(
                                        style: TextStyle(fontSize: 12),
                                        controller: bankName,
                                        decoration: InputDecoration(
                                          errorText: _isValidBankName
                                              ? 'Enter bank name'
                                              : null,
                                          contentPadding: EdgeInsets.all(4),
                                          hintText: 'Bank Name',
                                          border: InputBorder.none,
                                          //errorText: 'Bank name mandatory'
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible: paymentType == 'Check' ? true : false,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Notes : ',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border:
                                              Border.all(color: Colors.grey)),
                                      child: TextField(
                                        style: TextStyle(fontSize: 12),
                                        controller: notesText,
                                        keyboardType: TextInputType.multiline,
                                        maxLines: 3,
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(4),
                                            hintText: 'Notes',
                                            border: InputBorder.none),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: RaisedButton(
                              child: Text(
                                'Pay',
                                style: TextStyle(
                                    fontSize: 13, color: Colors.white),
                              ),
                              color: Theme.of(context).primaryColor,
                              onPressed: widget.isAdmin && index == 0
                                  ? !_isValidCashCollected
                                      ? () {
                                          // if (printOption != 'Print only') {
                                          //if (receiptMail.text.isNotEmpty) {
                                          if (paymentType == 'Cash') {
                                            payForItemsByCash(
                                              notesText.text,
                                              receiptMail.text,
                                              token,
                                              scaffoleContext,
                                            );
                                          } else if (paymentType == 'Check') {
                                            if (checkNumber.text.isEmpty ||
                                                    payeeName.text
                                                        .isEmpty /* ||
                                                                                              bankName.text.isEmpty*/
                                                ) {
                                              setState(() {
                                                checkNumber.text.isEmpty
                                                    ? _isValidCheckNumber = true
                                                    : _isValidCheckNumber =
                                                        false;
                                                payeeName.text.isEmpty
                                                    ? _isValidPayeeName = true
                                                    : _isValidPayeeName = false;
                                                /* bankName.text.isEmpty
                                                                                                  ? _isValidBankName = true
                                                                                                  : _isValidBankName = false;*/
                                              });
                                            } else {
                                              payForItemsByCheck(
                                                  notesText.text,
                                                  receiptMail.text,
                                                  checkNumber.text,
                                                  bankName.text,
                                                  payeeName.text,
                                                  selectedCheckDate.toString(),
                                                  token,
                                                  scaffoleContext);
                                            }
                                          } else if (paymentType == 'Card') {
                                            payForItemsByCard(
                                              notesText.text,
                                              receiptMail.text,
                                              token,
                                              payeeName.text,
                                              scaffoleContext,
                                            );
                                          }
                                        }
                                      : null
                                  : () {
                                      if (paymentType == 'Cash') {
                                        payForItemsByCash(
                                          notesText.text,
                                          receiptMail.text,
                                          token,
                                          scaffoleContext,
                                        );
                                      } else if (paymentType == 'Check') {
                                        if (checkNumber.text.isEmpty ||
                                            payeeName.text.isEmpty) {
                                          setState(() {
                                            checkNumber.text.isEmpty
                                                ? _isValidCheckNumber = true
                                                : _isValidCheckNumber = false;
                                            payeeName.text.isEmpty
                                                ? _isValidPayeeName = true
                                                : _isValidPayeeName = false;
                                          });
                                        } else {
                                          payForItemsByCheck(
                                              notesText.text,
                                              receiptMail.text,
                                              checkNumber.text,
                                              bankName.text,
                                              payeeName.text,
                                              selectedCheckDate.toString(),
                                              token,
                                              scaffoleContext);
                                        }
                                      } else if (paymentType == 'Card') {
                                        payForItemsByCard(
                                          notesText.text,
                                          receiptMail.text,
                                          token,
                                          payeeName.text,
                                          scaffoleContext,
                                        );
                                      }
                                    },
                            ),
                          )
                        ],
                      ),
                    )),
              );
            }));
      },
    );
  }

  payForItemsByCash(
      String notesText, String email, String token, BuildContext context) {
    PlaceOrderByCash placeOrder = PlaceOrderByCash();
    placeOrder.amount = totalPriceOfCart.toString();
    placeOrder.paymentType = 'Cash';
    placeOrder.transactionDate = DateTime.now().toString();
    placeOrder.notes = notesText;
    placeOrder.approvalNumber = '';

    placeOrder.placeOrderItems = getOrderDetails();
    String ordersJson = convert.jsonEncode(placeOrder);
    print(ordersJson);

    payForCartItems(
        ordersJson,
        widget.userAuthentication,
        context,
        widget.isAdmin,
        widget.loggedInUser,
        email,
        widget.devoteeId,
        isSkipReceipt,
        isSignatureCopy,
        updateClearCart,
        tokenItemsList,
        minAmntForReceipt);
  }

  payForItemsByCheck(
      String notesText,
      String email,
      String checkNumber,
      String bankName,
      String payeeName,
      String checkDate,
      String token,
      BuildContext context) {
    PlaceOrderByCheck placeOrder = PlaceOrderByCheck();
    placeOrder.amount = totalPriceOfCart.toString();
    placeOrder.paymentType = 'Check';
    placeOrder.transactionDate = DateTime.now().toString();
    placeOrder.notes = notesText;
    placeOrder.bankName = bankName;
    placeOrder.dateOnTheCheck = checkDate;
    placeOrder.approvalNumber = checkNumber;
    placeOrder.payeeName = payeeName;
    placeOrder.placeOrderItems = getOrderDetails();
    String ordersJson = convert.jsonEncode(placeOrder);
    payForCartItems(
        ordersJson,
        widget.userAuthentication,
        context,
        widget.isAdmin,
        widget.loggedInUser,
        email,
        widget.devoteeId,
        isSkipReceipt,
        isSignatureCopy,
        updateClearCart,
        tokenItemsList,
        minAmntForReceipt);
  }

  payForItemsByCard(String notesText, String email, String token,
      String payeeName, BuildContext context) async {
    var stripeDetails = await getStripeDeviceInfo();
    var jsonResponse = convert.jsonDecode(stripeDetails);
    String deviceLabel = jsonResponse['deviceLabel'];
    String deviceId = jsonResponse['deviceId'];

    print('token in pay dialog: $token');
    PlaceOrderByCard placeOrder = PlaceOrderByCard();
    placeOrder.amount = totalPriceOfCart.toString();
    placeOrder.paymentType = 'VPOS';

    placeOrder.transactionDate = DateTime.now().toString();
    placeOrder.notes = notesText;
    placeOrder.payeeName = payeeName;
    placeOrder.placeOrderItems = getOrderDetails();

    if (Platform.isWindows) {
      await run('assets/chakra-pay/chakra-pay.exe', [
        totalPriceOfCart.toString(),
        token,
        deviceLabel,
        deviceId
      ]).then((result) {
        String chakraPayResponse = result.stdout;
        if (chakraPayResponse.contains('success:')) {
          print('succees msg: ${result.stdout}');
          String paymentIntentId = result.stdout.toString();
          paymentIntentId = paymentIntentId.replaceAll('\r', '');
          paymentIntentId = paymentIntentId.replaceAll('\n', '');
          paymentIntentId = paymentIntentId.replaceAll('success: ', '');
          print('payment intent id : $paymentIntentId');
          placeOrder.paymentIntentId = paymentIntentId;
          String ordersJson = convert.jsonEncode(placeOrder);
          print(ordersJson);
          payForCartItems(
              ordersJson,
              widget.userAuthentication,
              context,
              widget.isAdmin,
              widget.loggedInUser,
              email,
              widget.devoteeId,
              isSkipReceipt,
              isSignatureCopy,
              updateClearCart,
              tokenItemsList,
              minAmntForReceipt);
        } else {
          print('error msg: ${result.stdout}');
        }
      });
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => StripeWeb(
                    cartAmount: totalPriceOfCart,
                    userToken: authTokenString,
                  )));
    }
  }

  getOrderDetails() {
    List<PlaceOrderItems> ordersList = [];

    for (int i = 0; i < selectedCartItem.length; i++) {
      PlaceOrderItems placeOrderItems = PlaceOrderItems();
      placeOrderItems.templeServiceId = selectedGridIndex[i];
      placeOrderItems.name = selectedCartItem[i];
      placeOrderItems.quantity = int.parse(selectedItemQuantity[i].toString());
      placeOrderItems.fee = selectedItemPrice[i];
      placeOrderItems.isZeroFee = false;
      placeOrderItems.totalAmount = totalPriceByItem[i];
      placeOrderItems.serviceType = serviceType[i];
      placeOrderItems.performDate = [];

      if (isTokenNeeded[i]) {
        TokenItems tokenItems = TokenItems();
        tokenItems.name = selectedCartItem[i];
        tokenItems.quantity = int.parse(selectedItemQuantity[i].toString());
        tokenItems.price = double.parse(selectedItemPrice[i]);
        tokenItems.totalAmount = totalPriceByItem[i];
        tokenItemsList.add(tokenItems);
      }
      ordersList.add(placeOrderItems);
    }
    return ordersList;
  }

  showZeroFeeDialog(int selectedIndex) {
    TextEditingController zeroFeeText = TextEditingController();
    bool _isValidZeroFee = true;
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Column(
                children: <Widget>[
                  Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () {
                            Navigator.of(context).pop();
                          })),
                  Text('Please enter amount of your preference',
                      style: TextStyle(fontSize: 14, color: Colors.deepOrange)),
                ],
              ),
              content: Container(
                  padding: EdgeInsets.all(20),
                  width: 320,
                  height: 200,
                  constraints: BoxConstraints(maxHeight: 180),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: Colors.grey)),
                        child: TextFormField(
                          controller: zeroFeeText,
                          onChanged: (text) {
                            setState(() {
                              _isValidZeroFee =
                                  FormValidations().validateZeroFeeText(text);
                            });
                          },
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(8),
                            DecimalTextInputFormatter(decimalRange: 2),
                          ],
                          keyboardType: TextInputType.numberWithOptions(
                            decimal: true,
                            signed: false,
                          ),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(10),
                            hintText: 'Enter the amount',
                            hintStyle: TextStyle(fontSize: 13),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RaisedButton(
                          padding: EdgeInsets.all(0),
                          color: zeroFeeText.text.isNotEmpty && _isValidZeroFee
                              ? Colors.deepOrange
                              : Colors.deepOrange.withOpacity(0.7),
                          child: Text('Add',
                              style: TextStyle(
                                color: Colors.white,
                              )),
                          onPressed: (_isValidZeroFee &&
                                  zeroFeeText.text.isNotEmpty)
                              ? () {
                                  if (_isValidZeroFee &&
                                      zeroFeeText.text.isNotEmpty) {
                                    addZeroFee(selectedIndex, zeroFeeText.text);
                                  }
                                }
                              : null)
                    ],
                  )),
            );
          });
        });
  }

  Future<Null> showFeeDialog(List<Fees> feeDetails, int selectedIndex) async {
    List returnVal = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text('Choose your preference',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.deepOrange,
                      )),
                  IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              content: Container(
                padding: EdgeInsets.all(20),
                width: 320,
                height: 520,
                child: CustomRadio(
                  feeDetails: feeDetails,
                  itemIndex: selectedIndex,
                ),
              ));
        });
    selectedFee = returnVal[0];
    /*   multiOccurenceList.add(gridItems[returnVal[1]].templeServiceId +
                                                                gridItems[returnVal[1]].name +
                                                                gridItems[returnVal[1]].serviceType); */
    onItemSelected(returnVal[1], returnVal[0]);
  }

  showScheduleForEvent(List<Timings> eventTimings, String name,
      String serviceType, String startDate, String endDate, int index) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              children: [
                Expanded(
                    flex: 7,
                    child: Text('View Event Details',
                        //textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 14, color: Colors.deepOrange))),
                Expanded(
                    flex: 1,
                    child: IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        })),
              ],
            ),
            content: SingleChildScrollView(
                child: Column(
              children: [
                Column(
                  //crossAxisAlignment: CrossAxisAlignment,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Text('Service Name : ' + name,
                              overflow: TextOverflow.ellipsis,
                              softWrap: true,
                              style: TextStyle(fontSize: 13)),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(),
                        ),
                        Expanded(
                            flex: 3,
                            child: Text('Catalogue Type : ' + serviceType,
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                                style: TextStyle(fontSize: 13)))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            flex: 3,
                            child: Text('Start Date : ' + startDate,
                                style: TextStyle(fontSize: 13))),
                        Expanded(flex: 2, child: Container()),
                        Expanded(
                          flex: 3,
                          child: Text('End Date : ' + endDate,
                              style: TextStyle(fontSize: 13)),
                        )
                      ],
                    ),
                  ],
                ),
                DataTable(
                    onSelectAll: (b) {},
                    sortAscending: true,
                    columns: <DataColumn>[
                      DataColumn(
                        label: Expanded(
                          child: Center(
                              child: Text("Date",
                                  style: TextStyle(
                                      fontFamily: 'Poppins',
                                      //fontWeight: FontWeight.w500,
                                      color: Colors.deepOrange,
                                      fontSize: 12),
                                  softWrap: true)),
                        ),
                        numeric: false,
                      ),
                      DataColumn(
                        label: Expanded(
                          child: Center(
                              child: Text("Start Time",
                                  style: TextStyle(
                                      fontFamily: 'Poppins',
                                      //fontWeight: FontWeight.w500,
                                      color: Colors.deepOrange,
                                      fontSize: 12),
                                  softWrap: true)),
                        ),
                        numeric: false,
                      ),
                      DataColumn(
                        label: Expanded(
                          child: Center(
                              child: Text("End Time",
                                  style: TextStyle(
                                      fontFamily: 'Poppins',
                                      //fontWeight: FontWeight.w500,
                                      color: Colors.deepOrange,
                                      fontSize: 12),
                                  softWrap: true)),
                        ),
                        numeric: false,
                      ),
                    ],
                    rows: eventTimings
                        .map(
                          (item) => DataRow(
                            cells: [
                              DataCell(Center(
                                child: item.recurrenceDate != null
                                    ? Text(
                                        item.recurrenceDate,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontFamily: 'Poppins'),
                                      )
                                    : Text('N/A'),
                              )),
                              DataCell(
                                Center(
                                  child: item.startTime != null
                                      ? Text(
                                          item.startTime.toUpperCase(),
                                          textAlign: TextAlign.center,
                                          style:
                                              TextStyle(fontFamily: 'Poppins'),
                                        )
                                      : Text('N/A'),
                                ),
                              ),
                              DataCell(
                                Center(
                                  child: item.endTime != null
                                      ? Text(
                                          item.endTime.toUpperCase(),
                                          textAlign: TextAlign.center,
                                          style:
                                              TextStyle(fontFamily: 'Poppins'),
                                        )
                                      : Text('N/A'),
                                ),
                              ),
                            ],
                          ),
                        )
                        .toList())
              ],
            )),
          );
        });
  }

  getUserToken() async {
    if (widget.userAuthentication != null) {
      authTokenString = 'Bearer ' + widget.userAuthentication.token;
    } else {
      if (Platform.isAndroid || Platform.isIOS) {
        final storage = new FlutterSecureStorage();

        authTokenString = await storage.read(key: 'auth_token');
      } else {
        LocalStorage localStorage = LocalStorage();
        await localStorage.setUpLocalDB();
        authTokenString =
            await localStorage.getUserTokenFromLocal(widget.isAdmin);
      }
    }
  }

  Future<dynamic> getTenantInfo() async {
    await localStorage.setUpLocalDB();
    tenant.TenantInfo tenantInfo = await localStorage.getTenantInfoFull();
    setState(() {
      tenantInfos = tenantInfo;
    });
  }

  showSnackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(new SnackBar(
        backgroundColor: Colors.red,
        content: Row(
          children: <Widget>[
            Icon(Icons.warning, color: Colors.white),
            SizedBox(
              width: 20,
            ),
            Text(message)
          ],
        )));
  }

  void getTempleSettings() async {
    var kioskTempleSettings = await getTempleSettingsInfo();
    var jsonResponse = convert.jsonDecode(kioskTempleSettings);

    setState(() {
      if (jsonResponse != null) {
        cdcMinAmount = jsonResponse['CCDCMinimumProcessingAmount'];
        isSkipReceipt = jsonResponse['IsSkipReceipt'];
        isSignatureCopy = jsonResponse['IsSignatureCopy'];
        minAmntForReceipt = jsonResponse['ReceiptMinimumAmount'];
      } else {
        cdcMinAmount = '';
        isSkipReceipt = false;
        isSignatureCopy = false;
        minAmntForReceipt = '';
      }
    });
  }

  void cardPaymentDialog(double totalPriceOfCart) async {
    TextEditingController cardController = TextEditingController();
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
                contentPadding: const EdgeInsets.all(16.0),
                content: Stack(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        Container(
                          constraints: BoxConstraints(maxWidth: 300),
                          child: new TextField(
                            controller: cardController,
                            autofocus: true,
                            style: TextStyle(color: Colors.transparent),
                            onEditingComplete: () {
                              setState(() {
                                readCardDetails(cardController.text);
                              });
                            },
                          ),
                        )
                      ],
                    ),
                    Visibility(
                        visible: true,
                        child: Container(
                            width: 300,
                            height: 100,
                            color: Colors.deepOrange,
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: IconButton(
                                      icon: Icon(Icons.close),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ),
                                  Text('Swipe your card')
                                ],
                              ),
                            )))
                  ],
                ));
          });
        });
  }

  addZeroFee(int index, String text) {
    onItemSelected(index, text);
    Navigator.of(context).pop();
  }

  readCardDetails(String cardReaderString) async {
    var output = cardReaderString.split('^');
    String cardNumber = output[0].substring(2);
    String payeeName = output[1].trimRight();
    String year = output[2].substring(0, 2);
    String month = output[2].substring(2, 4);
    String cardExpiry = month + '/' + year;

    Navigator.of(context).pop();
    paymentDialogForCard(cardNumber, payeeName, cardExpiry, totalPriceOfCart,
        widget.userAuthentication.token, widget.loggedInUser, context, '', '');
  }

  String getFormattedTimeOnly(String strDate) {
    String formattedDate;
    try {
      formattedDate = DateFormat('hh:mm aa').format(DateTime.parse(strDate));
    } catch (e) {
      formattedDate = strDate;
    }

    return formattedDate;
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange})
      : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(',') ||
          value.contains('-') ||
          value.contains(' ') ||
          value.contains('..')) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value.contains(".") &&
          value.substring(value.indexOf(".") + 1).length > decimalRange) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}

payForCartItems(
    String orderJson,
    UserAuthentication authToken,
    BuildContext context,
    bool isAdmin,
    String loggedInUser,
    String email,
    String devoteeId,
    bool isSkipReceipt,
    bool isSignatureCopy,
    Function refresh,
    List<TokenItems> tokenItemsList,
    String minAmntReceipt) async {
  print(email);
  NetworkHelper networkHelper = NetworkHelper();
  //var printer = Printer();
  print('order json : $orderJson');
  var response = await networkHelper.placeOrder(orderJson, authToken.token);
  print('token auth : ${authToken.token}');

  print(response);

  ResponseMessage rsm = ResponseMessage.fromJson(response);
  if (rsm.success) {
    CommonComponents().showOverlayMessage(
        context, 'Payment success', Colors.green, Colors.white);

    LocalStorage localStorage = LocalStorage();

    await localStorage.setUpLocalDB();
    tenant.TenantInfo tenantInfo = await localStorage.getTenantInfoFull();
    Timer(Duration(seconds: 3), () {
      Navigator.of(context).pop();
      if (isAdmin) {
        refresh();
        devoteeSearchDialog(context, authToken.token, rsm.orderResponse.orderId,
            isSkipReceipt, tokenItemsList, isSignatureCopy, minAmntReceipt);
      } else {
        continueShopDialog(
            context, loggedInUser, authToken, email, devoteeId, tenantInfo);
      }
    });
  } else {
    print(rsm.message);
  }
}

/* showStripeDialog(bool isAdmin, BuildContext context) async {
  TextEditingController deviceIdController = TextEditingController();
  TextEditingController deviceLabelController = TextEditingController();

  var stripeDetails = await getStripeDeviceInfo();
  if (stripeDetails != null) {
    var jsonResponse = convert.jsonDecode(stripeDetails);
    deviceLabelController.text = jsonResponse['deviceLabel'];
    deviceIdController.text = jsonResponse['deviceId'];
  }

  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            title: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Stripe device setup',
                    textAlign: TextAlign.center,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
            content: Container(
              constraints: BoxConstraints(minWidth: 320, maxHeight: 500),
              margin: EdgeInsets.all(20),
              child: Center(
                child: Column(
                  children: <Widget>[
                    isAdmin
                        ? Container()
                        : Column(
                            children: <Widget>[
                              TextField(
                                decoration: InputDecoration(hintText: 'Email'),
                              ),
                              TextField(
                                decoration:
                                    InputDecoration(hintText: 'Password'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              OutlineButton(
                                child: Text('Submit'),
                                onPressed: () {},
                              )
                            ],
                          ),
                    SizedBox(
                      height: 60,
                    ),
                    Visibility(
                      visible: isAdmin ? true : false,
                      child: Column(
                        children: <Widget>[
                          Text('Enter Stripe device details'),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: EdgeInsets.all(16),
                            child: TextField(
                              controller: deviceLabelController,
                              decoration:
                                  InputDecoration(hintText: 'Device Label'),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16),
                            child: TextField(
                              controller: deviceIdController,
                              decoration:
                                  InputDecoration(hintText: 'Device ID'),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          OutlineButton(
                            color: Colors.deepOrange,
                            child: Text('Save'),
                            onPressed: () {
                              saveStripeDeviceInfo(deviceIdController.text,
                                  deviceLabelController.text);
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
      });
}
 */
Future<dynamic> getTempleSettingsInfo() async {
  LocalStorage localStorage = new LocalStorage();
  await localStorage.setUpLocalDB();
  return await localStorage.getKioskTempleSettings();
}

Future<dynamic> getStripeDeviceInfo() async {
  LocalStorage localStorage = new LocalStorage();
  await localStorage.setUpLocalDB();
  return await localStorage.getStripeInfo();
}

void saveStripeDeviceInfo(String deviceId, String deviceLabel) async {
  var deviceInfo = {};
  deviceInfo["deviceId"] = deviceId;
  deviceInfo["deviceLabel"] = deviceLabel;

  String deviceInfoJson = convert.jsonEncode(deviceInfo);
  LocalStorage localStorage = LocalStorage();

  await localStorage.setUpLocalDB();
  await localStorage.saveStripeInfo(deviceInfoJson);
  print('device info saved succesfully');
}

Future<void> devoteeSearchDialog(
    BuildContext context,
    String authToken,
    String orderId,
    bool isSkipReceipt,
    List<TokenItems> tokenItemsList,
    bool isSignatureCopy,
    String minAmntReceipt) async {
  LocalStorage localStorage = LocalStorage();

  await localStorage.setUpLocalDB();
  tenant.TenantInfo tenantInfo = await localStorage.getTenantInfoFull();
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertWrapper(
        authToken: authToken,
        orderId: orderId,
        isSkipPrint: isSkipReceipt,
        //notifyParent: refresh,
        tokenItemsList: tokenItemsList,
        isSignatureCopy: isSignatureCopy,
        minAmntReceipt: minAmntReceipt, tenantInfo: tenantInfo,
      );
    },
  );
}

class AlertWrapper extends StatefulWidget {
  final String authToken;
  final String orderId;
  final bool isSkipPrint;
  final bool isSignatureCopy;
  final String minAmntReceipt;
  final tenant.TenantInfo tenantInfo;
  //final Function() notifyParent;
  final List<TokenItems> tokenItemsList;

  const AlertWrapper(
      {Key key,
      @required this.authToken,
      @required this.orderId,
      @required this.isSkipPrint,
      //@required this.notifyParent,
      @required this.tokenItemsList,
      @required this.isSignatureCopy,
      @required this.minAmntReceipt,
      @required this.tenantInfo})
      : super(key: key);

  @override
  _AlertWrapperState createState() => _AlertWrapperState();
}

class _AlertWrapperState extends State<AlertWrapper> {
  final TextEditingController searchTextField = TextEditingController();
  final TextEditingController mobileNumber = TextEditingController();
  final TextEditingController emailText = TextEditingController();
  final TextEditingController devoteeName = TextEditingController();
  List<UsersList> searchUserList = [];
  String selectedDevoteeId;

  changesOnField() {
    var searchText = searchTextField.text;

    if (searchText.length > 3) {
      searchUserList.clear();

      getUserSearchResult(searchText, widget.authToken);
    } else if (searchTextField.text.isEmpty || searchText.length < 3) {
      setState(() {
        searchUserList.clear();

        if (mobileNumber.text.isEmpty) {
          mobileNumber.text = '';
        }
        if (emailText.text.isEmpty) {
          emailText.text = '';
        }
        if (devoteeName.text.isEmpty) {
          devoteeName.text = '';
        }
      });
    }
  }

  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();
    //searchTextField.addListener(changesOnField);
    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    //searchTextField.dispose();
    myFocusNode.dispose();
    super.dispose();
  }

  String value;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  'Search Devotee',
                  textAlign: TextAlign.center,
                ),
              ),
              IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
          ),
          Container(
            height: 60,
            child: TextField(
              controller: searchTextField,
              focusNode: myFocusNode,
              onChanged: (editedValue) {
                if (searchTextField.text.length > 3) {
                  searchUserList.clear();

                  getUserSearchResult(searchTextField.text, widget.authToken);
                } else if (searchTextField.text.isEmpty ||
                    searchTextField.text.length < 3) {
                  setState(() {
                    searchUserList.clear();

                    if (mobileNumber.text.isEmpty) {
                      mobileNumber.text = '';
                    }
                    if (emailText.text.isEmpty) {
                      emailText.text = '';
                    }
                    if (devoteeName.text.isEmpty) {
                      devoteeName.text = '';
                    }
                  });
                }
                /*  if (searchTextField.text.length > 3) {
                  value = editedValue;

                  getUserSearchResult(value, widget.authToken);
                } else {
                  setState(() {
                    searchUserList.clear();
                  });
                } */
              },
              decoration: InputDecoration(
                  hintText: 'Search user',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
          ),
        ],
      ),
      content: searchUserList.isEmpty
          ? Container(
              height: 350,
              width: 300,
              // constraints: BoxConstraints(minWidth: 300, minHeight: 50),
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: mobileNumber,
                    decoration: InputDecoration(
                      labelText: 'Mobile number',
                    ),
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(10),
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                  ),
                  TextField(
                    controller: emailText,
                    decoration: InputDecoration(
                      labelText: 'Email',
                    ),
                  ),
                  TextField(
                    controller: devoteeName,
                    enabled: false,
                    decoration: InputDecoration(
                      labelText: 'Devotee name',
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      OutlineButton(
                          child: Text('Print'),
                          onPressed: () async {
                            //widget.notifyParent();
                            await updateUserForOrder();
                            await Printer().checkForPrinter(
                                widget.authToken,
                                widget.orderId,
                                widget.tokenItemsList,
                                widget.isSignatureCopy,
                                widget.minAmntReceipt);
                          }),
                      widget.isSkipPrint
                          ? Row(
                              children: [
                                SizedBox(width: 20),
                                OutlineButton(
                                    child: Text('Skip Print'),
                                    onPressed: () {
                                      //widget.notifyParent();
                                      updateUserForOrder();
                                    })
                              ],
                            )
                          : SizedBox(
                              width: 0,
                            ),
                    ],
                  ),
                ],
              ),
            )
          : Container(
              margin: EdgeInsets.only(top: 20),
              width: 320,
              // constraints: BoxConstraints(minWidth: 300, minHeight: 600),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(10)),
              child: ListView.builder(
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  final item = searchUserList[index];
                  return InkWell(
                      onTap: () {
                        searchTextField.clear();
                        myFocusNode.requestFocus();
                        //print(item.mobileNumber);
                        setState(() {
                          mobileNumber.text = item.mobileNumber;
                          emailText.text = item.email;
                          devoteeName.text = item.firstName;
                          selectedDevoteeId = item.userId;
                          searchUserList.clear();
                          searchTextField.clear();
                          myFocusNode.requestFocus();
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(item.firstName + ' ' + item.lastName),
                            Text(
                              item.email,
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.grey),
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey[400],
                            ),
                          ],
                        ),
                      ));
                },
                itemCount: searchUserList.length,
              ),
            ),
    );
  }

  getUserSearchResult(
    String searchString,
    String authToken,
  ) async {
    var response = await NetworkHelper().getUsersInfo(searchString, authToken);
    SearchMaster searchMaster = SearchMaster.fromJson(response);
    setState(() {
      searchUserList = searchMaster.userList;
      //searchTextField.clear();
    });
  }

  Future<void> checkForPrinter() async {
    bool isNakshatraReceipt = false;
    NetworkHelper networkHelper = new NetworkHelper();
    var jsonResponse = await networkHelper.getPrintReceiptDetails(
        'Bearer ' + widget.authToken, widget.orderId);

    print(jsonResponse.toString());

    PrintReceipt printReceipt = PrintReceipt.fromJson(jsonResponse);
    List<OrderItemDetails> orderDetails =
        printReceipt.receiptDetails.order.orderItemDetails;

    print('Receipt for services');
    print('\n Receipt no : ' +
        printReceipt.receiptDetails.order.receiptNumber.toString() +
        '\n Date:' +
        printReceipt.receiptDetails.order.transactionDate);

    print('\n Address : ' +
        printReceipt.receiptDetails.order.user.firstName +
        printReceipt.receiptDetails.order.user.lastName +
        '\n' +
        printReceipt.receiptDetails.order.user.userAddress.address +
        '\n' +
        printReceipt.receiptDetails.order.user.userAddress.city +
        '\n' +
        printReceipt.receiptDetails.order.user.userAddress.state.name +
        '\n' +
        printReceipt.receiptDetails.order.user.userAddress.country.name);

    print('\n -----------------------------------------\n');

    orderDetails.forEach((orderItem) {
      print(orderItem.name +
          orderItem.quantity.toString() +
          orderItem.fee +
          orderItem.totalAmount.toStringAsFixed(2));
      if (orderItem.serviceType == 'Payment Service') {
        isNakshatraReceipt = true;
      }
    });

    print('\n -----------------------------------------\n');

    print('Receipt Total (\$) : ' +
        printReceipt.receiptDetails.orderInfo.receiptTotalAmount
            .toStringAsFixed(2));

    print('Payment mode : ' +
        printReceipt.receiptDetails.order.paymentDetails.paymentType);

    if (isNakshatraReceipt) {
      printNakshatraReceipt(
          printReceipt.receiptDetails.order.user.nakshatraDetails);
    }

    print('token item list: ' + widget.tokenItemsList.toString());

    if (widget.tokenItemsList.length != 0) {
      printTokenReceipt(widget.tokenItemsList);
    }
  }

  Future<void> updateUserForOrder() async {
    UpdateOrderUser updateOrderUser = new UpdateOrderUser();
    updateOrderUser.orderId = widget.orderId;
    updateOrderUser.userId = selectedDevoteeId;
    updateOrderUser.email = emailText.text;
    String jsonString = convert.jsonEncode(updateOrderUser);
    print(jsonString);

    //code to send email
    NetworkHelper networkHelper = NetworkHelper();
    var response =
        await networkHelper.updateOrderUser(jsonString, widget.authToken);
    print('Response in update order user' + response.toString());

    UpdateUserResponse usm = UpdateUserResponse.fromJson(response);

    if (usm.status) {
      Navigator.pop(context);
    }
  }

  void clearCartItems() {}

  void printNakshatraReceipt(List<NakshatraDetails> nakshatraDetails) {
    nakshatraDetails.forEach((item) {
      print('Name : ' + item.name + 'Nakshatra : ' + item.nakshatra);
    });
  }

  void printTokenReceipt(List<TokenItems> tokenItemsList) {
    tokenItemsList.forEach((item) {
      print(item.name +
          '---' +
          item.quantity.toString() +
          '*' +
          item.price.toStringAsFixed(2) +
          ' = ' +
          item.totalAmount.toStringAsFixed(2));
    });
  }

  checkForUser(String searchText, String authToken) async {
    if (searchText.length > 3) {
      var response = await NetworkHelper().getUsersInfo(searchText, authToken);
      SearchMaster searchMaster = SearchMaster.fromJson(response);
      return searchMaster.userList;
    }
  }
}

continueShopDialog(
    BuildContext context,
    String loggedInUser,
    UserAuthentication userAuthentication,
    String devoteeMail,
    String devoteeId,
    tenant.TenantInfo tenantInfo) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Continue shopping?'),
          content: Text('Would you like to continue another purchase?'),
          actions: <Widget>[
            new FlatButton(
              child: Text('No Thanks', style: TextStyle(color: Colors.red)),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LoginScreen(
                            kioskBanner: tenantInfo.tenantDetails
                                .additionalProperties.kioskBanner)));
              },
            ),
            new FlatButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => HomeScreen(
                              isAdmin: false,
                              loggedInUser: loggedInUser,
                              userAuthentication: userAuthentication,
                              devoteeMail: devoteeMail,
                              devoteeId: devoteeId,
                            )));
              },
            )
          ],
        );
      });
}

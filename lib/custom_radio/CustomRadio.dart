import 'package:chakra/custom_radio/RadioModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomRadio extends StatefulWidget {
  final feeDetails;
  final itemIndex;

  const CustomRadio(
      {Key key, @required this.feeDetails, @required this.itemIndex})
      : super(key: key);

  @override
  CustomRadioState createState() => new CustomRadioState();
}

class CustomRadioState extends State<CustomRadio> {
  List<RadioModel> sampleData = new List<RadioModel>();

  @override
  void initState() {
    super.initState();

    widget.feeDetails.forEach((item) {
      sampleData.add(new RadioModel(false, item.fee, item.name));
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: sampleData.length,
      itemBuilder: (BuildContext context, int index) {
        return new InkWell(
          splashColor: Colors.deepOrange,
          onTap: () {
            setState(() {
              sampleData.forEach((element) => element.isSelected = false);
              sampleData[index].isSelected = true;

              Navigator.pop(
                  context, [sampleData[index].buttonText, widget.itemIndex]);
            });
          },
          child: new RadioItem(sampleData[index]),
        );
      },
    );
  }
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.all(15.0),
      child: Row(
        children: <Widget>[
          Container(
            height: 24,
            width: 24,
            child: Visibility(
                visible: _item.isSelected ? true : false,
                child: Center(
                    child: Icon(
                  Icons.done,
                  size: 16,
                  color: _item.isSelected ? Colors.white : Colors.deepOrange,
                ))),
            decoration: BoxDecoration(
                color: _item.isSelected ? Colors.deepOrange : Colors.white,
                border: Border.all(color: Colors.deepOrange),
                borderRadius: BorderRadius.circular(30)),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              decoration: new BoxDecoration(
                  //color: Colors.deepOrange,
                  border: Border.all(
                color: Colors.deepOrange,
              )),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Container(
                      height: 32.0,
                      width: 80.0,
                      child: new Center(
                        child: new Text('\$ ${_item.buttonText}',
                            style: new TextStyle(
                                color: Colors.white,
                                fontSize: 13.0,
                                fontWeight: FontWeight.w500)),
                      ),
                      decoration: new BoxDecoration(
                        color: Colors.deepOrange,
                      )),
                  new Container(
                    margin: new EdgeInsets.only(left: 10.0),
                    child: new Text(
                      toBeginningOfSentenceCase(_item.text.toLowerCase()),
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

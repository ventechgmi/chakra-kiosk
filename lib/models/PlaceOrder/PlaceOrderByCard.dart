import 'package:chakra/models/PlaceOrder/PlaceOrderByCash.dart';

class PlaceOrderByCard {
  List<PlaceOrderItems> placeOrderItems;
  String amount;
  String paymentType;
  String cardNumber;
  String expiryDate;
  String cvv;
  String payeeName;
  String paymentIntentId;
  String transactionDate;
  String notes;

  PlaceOrderByCard(
      {this.placeOrderItems,
      this.amount,
      this.paymentType,
      this.transactionDate,
      this.cardNumber,
      this.expiryDate,
      this.cvv,
      this.payeeName,
      this.paymentIntentId,
      this.notes});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderItems'] = placeOrderItems;
    data['amount'] = amount;
    data['paymentType'] = paymentType;
    data['cardNumber'] = cardNumber;
    data['expiryDate'] = expiryDate;
    data['cvv'] = cvv;
    data['payeeName'] = payeeName;
    data['paymentIntentId'] = paymentIntentId;
    data['transactionDate'] = transactionDate;
    data['notes'] = notes;
    return data;
  }
}

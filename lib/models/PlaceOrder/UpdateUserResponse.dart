class UpdateUserResponse {
  bool status;
  String message;

  UpdateUserResponse({this.status, this.message});

  factory UpdateUserResponse.fromJson(Map<String, dynamic> parsedJson) {
    var status = parsedJson['status'] as bool;
    var message = parsedJson['message'];

    return UpdateUserResponse(status: status, message: message);
  }
}

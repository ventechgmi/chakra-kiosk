class PlaceOrderByCash {
  List<PlaceOrderItems> placeOrderItems;
  String amount;
  String paymentType;
  String payeeName;
  String transactionDate;
  String notes;
  String approvalNumber;

  PlaceOrderByCash(
      {this.placeOrderItems,
      this.amount,
      this.paymentType,
      this.transactionDate,
      this.notes,
      this.approvalNumber});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderItems'] = placeOrderItems;
    data['amount'] = amount;
    data['paymentType'] = paymentType;
    data['transactionDate'] = transactionDate;
    data['notes'] = notes;
    data['approvalNumber'] = approvalNumber;
    return data;
  }
}

class PlaceOrderItems {
  String templeServiceId;
  String name;
  int quantity;
  String fee;
  bool isZeroFee;
  String serviceType;
  double totalAmount;
  List<PerformedDate> performDate;
  //bool isTempleToken;

  PlaceOrderItems({
    this.templeServiceId,
    this.name,
    this.quantity,
    this.fee,
    this.isZeroFee,
    this.serviceType,
    this.totalAmount,
    this.performDate,
    //this.isTempleToken
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templeServiceId'] = templeServiceId;
    data['name'] = name;
    data['quantity'] = quantity;
    data['fee'] = fee;
    data['isZeroFee'] = isZeroFee;
    data['serviceType'] = serviceType;
    data['totalAmount'] = totalAmount;
    data['performedDate'] = performDate;
    //data['templeToken'] = isTempleToken;
    return data;
  }

  factory PlaceOrderItems.fromJson(Map<String, dynamic> parsedJson) {
    var templeServiceId = parsedJson['templeServiceId'];
    var name = parsedJson['name'];
    var quantity = parsedJson['quantity'];
    var fee = parsedJson['fee'];
    var isZeroFee = parsedJson['isZeroFee'] as bool;
    //var isTempleToken = parsedJson['templeToken'] as bool;
    var serviceType = parsedJson['serviceType'];
    var totalAmount = parsedJson['totalAmount'];
    var performedDate = parsedJson['performedDate'] as List;

    return PlaceOrderItems(
        templeServiceId: templeServiceId,
        name: name,
        quantity: quantity,
        fee: fee,
        isZeroFee: isZeroFee,
        //isTempleToken: isTempleToken,
        serviceType: serviceType,
        totalAmount: totalAmount,
        performDate: performedDate);
  }
}

class PerformedDate {
  String startDate;
  String endDate;
  String startTime;
  String endTime;

  PerformedDate({this.startDate, this.endDate, this.startTime, this.endTime});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['startDate'] = startDate;
    data['endDate'] = endDate;
    data['startTime'] = startTime;
    data['endTime'] = endTime;
    return data;
  }
}

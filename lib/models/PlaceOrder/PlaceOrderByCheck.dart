import 'package:chakra/models/PlaceOrder/PlaceOrderByCash.dart';

class PlaceOrderByCheck {
  List<PlaceOrderItems> placeOrderItems;
  String amount;
  String paymentType;
  String payeeName;
  String transactionDate;
  String dateOnTheCheck;
  String bankName;
  String approvalNumber;
  String notes;

  PlaceOrderByCheck(
      {this.placeOrderItems,
      this.amount,
      this.paymentType,
      this.transactionDate,
      this.payeeName,
      this.dateOnTheCheck,
      this.bankName,
      this.approvalNumber,
      this.notes});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderItems'] = placeOrderItems;
    data['amount'] = amount;
    data['paymentType'] = paymentType;
    data['payeeName'] = payeeName;
    data['transactionDate'] = transactionDate;
    data['dateOnTheCheck'] = dateOnTheCheck;
    data['bankName'] = bankName;
    data['approvalNumber'] = approvalNumber;
    data['notes'] = notes;
    return data;
  }
}

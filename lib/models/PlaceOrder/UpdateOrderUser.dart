class UpdateOrderUser {
  String orderId;
  String userId;
  String email;

  UpdateOrderUser({this.orderId, this.userId, this.email});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = orderId;
    data['userId'] = userId;
    data['email'] = email;
    return data;
  }
}

class PrinterItems {
  final List<PrintOrderItems> orderedItems;

  PrinterItems({this.orderedItems});

  factory PrinterItems.fromJson(Map<String, dynamic> parsedJson) {
    var products = parsedJson['OrderItems'] as List;

    List<PrintOrderItems> orderedItemList =
        products.map((i) => PrintOrderItems.fromJson(i)).toList();

    return PrinterItems(
      orderedItems: orderedItemList,
    );
  }
}

class PrintOrderItems {
  String templeServiceId;
  String name;
  int quantity;
  String fee;
  bool isZeroFee;
  String serviceType;
  double totalAmount;
  List<PrintPerformedDate> performDate;

  PrintOrderItems(
      {this.templeServiceId,
      this.name,
      this.quantity,
      this.fee,
      this.isZeroFee,
      this.serviceType,
      this.totalAmount,
      this.performDate});

  /*  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templeServiceId'] = templeServiceId;
    data['name'] = name;
    data['quantity'] = quantity;
    data['fee'] = fee;
    data['isZeroFee'] = isZeroFee;
    data['serviceType'] = serviceType;
    data['totalAmount'] = totalAmount;
    data['performedDate'] = performDate;
    return data;
  } */

  factory PrintOrderItems.fromJson(Map<String, dynamic> parsedJson) {
    var templeServiceId = parsedJson['templeServiceId'];
    var name = parsedJson['name'];
    var quantity = parsedJson['quantity'];
    var fee = parsedJson['fee'];
    var isZeroFee = parsedJson['isZeroFee'] as bool;
    var serviceType = parsedJson['serviceType'];
    var totalAmount = parsedJson['totalAmount'];
    var performedDate = parsedJson['performedDate'] as List;

    List<PrintPerformedDate> orderedItemList =
        performedDate.map((i) => PrintPerformedDate.fromJson(i)).toList();

    return PrintOrderItems(
        templeServiceId: templeServiceId,
        name: name,
        quantity: quantity,
        fee: fee,
        isZeroFee: isZeroFee,
        serviceType: serviceType,
        totalAmount: totalAmount,
        performDate: orderedItemList);
  }
}

class PrintPerformedDate {
  String startDate;
  String endDate;
  String startTime;
  String endTime;

  PrintPerformedDate(
      {this.startDate, this.endDate, this.startTime, this.endTime});

  /* Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['startDate'] = startDate;
    data['endDate'] = endDate;
    data['startTime'] = startTime;
    data['endTime'] = endTime;
    return data;
  } */

  factory PrintPerformedDate.fromJson(Map<String, dynamic> parsedJson) {
    var startDate = parsedJson['startDate'];
    var endDate = parsedJson['endDate'];
    var startTime = parsedJson['startTime'];
    var endTime = parsedJson['endTime'];

    return PrintPerformedDate(
      startDate: startDate,
      endDate: endDate,
      startTime: startTime,
      endTime: endTime,
    );
  }
}

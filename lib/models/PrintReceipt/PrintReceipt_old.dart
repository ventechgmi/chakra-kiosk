class PrintReceipt {
  bool success;
  String message;
  ReceiptDetails receiptDetails;

  PrintReceipt({this.success, this.message, this.receiptDetails});

  factory PrintReceipt.fromJson(Map<String, dynamic> parsedJson) {
    var success = parsedJson['success'] as bool;
    var message = parsedJson['message'];
    var receiptDetails = parsedJson['receiptDetails'];

    ReceiptDetails tenantDetail = ReceiptDetails.fromJson(receiptDetails);

    return PrintReceipt(
        success: success, message: message, receiptDetails: tenantDetail);
  }
}

class ReceiptDetails {
  Orders orders;
  BillOrderInfo billOrderInfo;
  BillTenantInfo billTenantInfo;
  ReceiptDetails({this.orders, this.billOrderInfo, this.billTenantInfo});

  factory ReceiptDetails.fromJson(Map<String, dynamic> parsedJson) {
    var orders = parsedJson['order'];
    var billOrderInfo = parsedJson['orderInfo'];
    var billTenantInfo = parsedJson['tenantInfo'];

    Orders billOrders = Orders.fromJson(orders);
    BillOrderInfo billOrderInfos = BillOrderInfo.fromJson(billOrderInfo);
    BillTenantInfo billTenantInfos = BillTenantInfo.fromJson(billTenantInfo);

    return ReceiptDetails(
        orders: billOrders,
        billOrderInfo: billOrderInfos,
        billTenantInfo: billTenantInfos);
  }
}

class BillOrderInfo {
  int receiptTotalAmount;

  BillOrderInfo({
    this.receiptTotalAmount,
  });

  factory BillOrderInfo.fromJson(Map<String, dynamic> parsedJson) {
    var receiptAmount = parsedJson['receiptTotalAmount'];

    return BillOrderInfo(receiptTotalAmount: receiptAmount);
  }
}

class BillTenantInfo {
  String name;
  BillTenantAddress billTenantAddres;
  TenantState tenantState;
  TenantCountry tenantCountry;

  BillTenantInfo(
      {this.name, this.billTenantAddres, this.tenantState, this.tenantCountry});

  factory BillTenantInfo.fromJson(Map<String, dynamic> parsedJson) {
    var tenantName = parsedJson['name'];
    var billTenantAddres = parsedJson['address'];
    var tenantState = billTenantAddres['state'];
    var tenantCountry = billTenantAddres['country'];

    BillTenantAddress tenantAddres =
        BillTenantAddress.fromJson(billTenantAddres);
    TenantState state = TenantState.fromJson(tenantState);
    TenantCountry country = TenantCountry.fromJson(tenantCountry);

    return BillTenantInfo(
        name: tenantName,
        billTenantAddres: tenantAddres,
        tenantState: state,
        tenantCountry: country);
  }
}

class BillTenantAddress {
  String city;
  String address;

  BillTenantAddress({
    this.city,
    this.address,
  });

  factory BillTenantAddress.fromJson(Map<String, dynamic> parsedJson) {
    var city = parsedJson['city'];
    var address = parsedJson['address'];

    return BillTenantAddress(city: city, address: address);
  }
}

class TenantCountry {
  String name;

  TenantCountry({this.name});

  factory TenantCountry.fromJson(Map<String, dynamic> parsedJson) {
    var name = parsedJson['name'];

    return TenantCountry(
      name: name,
    );
  }
}

class TenantState {
  String name;

  TenantState({this.name});

  factory TenantState.fromJson(Map<String, dynamic> parsedJson) {
    print('parsed json $parsedJson');
    var stateName = parsedJson['title'];

    return TenantState(
      name: stateName,
    );
  }
}

class Orders {
  String orderId;
  String userId;
  String transactionDate;
  int receiptNumber;
  List<OrderItemDetails> orderItemDetails;
  BillPayDetails billPayDetails;
  User user;

  Orders(
      {this.orderId,
      this.userId,
      this.transactionDate,
      this.receiptNumber,
      this.orderItemDetails,
      this.billPayDetails,
      this.user});

  factory Orders.fromJson(Map<String, dynamic> parsedJson) {
    var orderId = parsedJson['orderId'];
    var userId = parsedJson['userId'];
    var transactionDate = parsedJson['transactionDate'];
    var receiptNumber = parsedJson['receiptNumber'];
    var orderItemDetails = parsedJson['orderItemDetails'] as List;
    var billPayDetails = parsedJson['paymentDetails'];
    var user = parsedJson['user'];

    BillPayDetails billTenantAddres = BillPayDetails.fromJson(billPayDetails);

    User userInfo = User.fromJson(user);

    List<OrderItemDetails> orderItemsList =
        orderItemDetails.map((i) => OrderItemDetails.fromJson(i)).toList();

    return Orders(
        orderId: orderId,
        userId: userId,
        transactionDate: transactionDate,
        receiptNumber: receiptNumber,
        orderItemDetails: orderItemsList,
        billPayDetails: billTenantAddres,
        user: userInfo);
  }
}

class UserAddress {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  UserState state;
  UserCountry country;

  UserAddress(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.state,
      this.country});

  UserAddress.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    address = json['address'];
    stateId = json['stateId'];
    zipCode = json['zipCode'];
    countryId = json['countryId'];
    state =
        json['state'] != null ? new UserState.fromJson(json['state']) : null;
    country = json['country'] != null
        ? new UserCountry.fromJson(json['country'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['address'] = this.address;
    data['stateId'] = this.stateId;
    data['zipCode'] = this.zipCode;
    data['countryId'] = this.countryId;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    return data;
  }
}

class UserState {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  String referenceId;
  Null tenantId;

  UserState(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  UserState.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class UserCountry {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  Null referenceId;
  Null tenantId;

  UserCountry(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  UserCountry.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class User {
  String userId;
  String userAccountId;
  String firstName;
  String middleName;
  String lastName;
  String dateOfBirth;
  String nakshatra;
  String gender;
  UserAddress userAddress;
  String tenantId;
  bool isWalkinUser;
  String status;
  String mobileNumber;
  String email;
  List<NakshatraDetails> nakshatraDetails;

  User(
      {this.userId,
      this.userAccountId,
      this.firstName,
      this.middleName,
      this.lastName,
      this.dateOfBirth,
      this.nakshatra,
      this.gender,
      this.userAddress,
      this.tenantId,
      this.isWalkinUser,
      this.status,
      this.mobileNumber,
      this.email,
      this.nakshatraDetails});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    userAccountId = json['userAccountId'];
    firstName = json['firstName'];
    middleName = json['middleName'];
    lastName = json['lastName'];
    dateOfBirth = json['dateOfBirth'];
    nakshatra = json['nakshatra'];
    gender = json['gender'];
    userAddress = json['address'] != null
        ? new UserAddress.fromJson(json['address'])
        : null;
    tenantId = json['tenantId'];
    isWalkinUser = json['isWalkinUser'];
    status = json['status'];
    mobileNumber = json['mobileNumber'];
    email = json['email'];
    if (json['nakshatraDetails'] != null) {
      nakshatraDetails = new List<NakshatraDetails>();
      json['nakshatraDetails'].forEach((v) {
        nakshatraDetails.add(new NakshatraDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['userAccountId'] = this.userAccountId;
    data['firstName'] = this.firstName;
    data['middleName'] = this.middleName;
    data['lastName'] = this.lastName;
    data['dateOfBirth'] = this.dateOfBirth;
    data['nakshatra'] = this.nakshatra;
    data['gender'] = this.gender;
    if (this.userAddress != null) {
      data['address'] = this.userAddress.toJson();
    }
    data['tenantId'] = this.tenantId;
    data['isWalkinUser'] = this.isWalkinUser;
    data['status'] = this.status;
    data['mobileNumber'] = this.mobileNumber;
    data['email'] = this.email;
    if (this.nakshatraDetails != null) {
      data['nakshatraDetails'] =
          this.nakshatraDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BillPayDetails {
  String paymentType;

  BillPayDetails({this.paymentType});

  factory BillPayDetails.fromJson(Map<String, dynamic> parsedJson) {
    var paymentType = parsedJson['paymentType'];

    return BillPayDetails(paymentType: paymentType);
  }
}

class OrderItemDetails {
  String fee;
  String name;
  int quantity;
  int totalAmount;
  String serviceType;
  String processType;
  bool templeToken;

  OrderItemDetails(
      {this.fee,
      this.name,
      this.quantity,
      this.totalAmount,
      this.serviceType,
      this.processType,
      this.templeToken});

  factory OrderItemDetails.fromJson(Map<String, dynamic> parsedJson) {
    var fee = parsedJson['fee'];
    var name = parsedJson['name'];
    var quantity = parsedJson['quantity'];
    var totalAmount = parsedJson['totalAmount'];
    var serviceType = parsedJson['serviceType'];
    var processType = parsedJson['processType'];
    var templeToken = parsedJson['templeToken'];

    return OrderItemDetails(
        fee: fee,
        name: name,
        quantity: quantity,
        totalAmount: totalAmount,
        serviceType: serviceType,
        processType: processType,
        templeToken: templeToken);
  }
}

class NakshatraDetails {
  String name;
  String nakshatra;

  NakshatraDetails({this.name, this.nakshatra});

  NakshatraDetails.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    nakshatra = json['nakshatra'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['nakshatra'] = this.nakshatra;
    return data;
  }
}

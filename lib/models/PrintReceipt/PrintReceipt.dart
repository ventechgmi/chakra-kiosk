class PrintReceipt {
  bool success;
  String message;
  ReceiptDetails receiptDetails;

  PrintReceipt({this.success, this.message, this.receiptDetails});

  PrintReceipt.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    receiptDetails = json['receiptDetails'] != null
        ? new ReceiptDetails.fromJson(json['receiptDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.receiptDetails != null) {
      data['receiptDetails'] = this.receiptDetails.toJson();
    }
    return data;
  }
}

class ReceiptDetails {
  Order order;
  OrderInfo orderInfo;
  TenantInfo tenantInfo;
  String notes;

  ReceiptDetails({this.order, this.orderInfo, this.tenantInfo, this.notes});

  ReceiptDetails.fromJson(Map<String, dynamic> json) {
    order = json['order'] != null ? new Order.fromJson(json['order']) : null;
    orderInfo = json['orderInfo'] != null
        ? new OrderInfo.fromJson(json['orderInfo'])
        : null;
    tenantInfo = json['tenantInfo'] != null
        ? new TenantInfo.fromJson(json['tenantInfo'])
        : null;
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.order != null) {
      data['order'] = this.order.toJson();
    }
    if (this.orderInfo != null) {
      data['orderInfo'] = this.orderInfo.toJson();
    }
    if (this.tenantInfo != null) {
      data['tenantInfo'] = this.tenantInfo.toJson();
    }
    data['notes'] = this.notes;
    return data;
  }
}

class Order {
  String orderId;
  String userId;
  String transactionDate;
  List<OrderItemDetails> orderItemDetails;
  PaymentDetails paymentDetails;
  String tenantId;
  AdditionalDetails additionalDetails;
  bool isRefundOrder;
  String status;
  int receiptNumber;
  String amount;
  AdditionalProperties additionalProperties;
  List<TokenDetails> tokenDetails;
  String transactionDateDisplay;
  User user;
  String thankYouMessage;

  Order(
      {this.orderId,
      this.userId,
      this.transactionDate,
      this.orderItemDetails,
      this.paymentDetails,
      this.tenantId,
      this.additionalDetails,
      this.isRefundOrder,
      this.status,
      this.receiptNumber,
      this.amount,
      this.additionalProperties,
      this.tokenDetails,
      this.transactionDateDisplay,
      this.user,
      this.thankYouMessage});

  Order.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    userId = json['userId'];
    transactionDate = json['transactionDate'];
    if (json['orderItemDetails'] != null) {
      orderItemDetails = new List<OrderItemDetails>();
      json['orderItemDetails'].forEach((v) {
        orderItemDetails.add(new OrderItemDetails.fromJson(v));
      });
    }
    paymentDetails = json['paymentDetails'] != null
        ? new PaymentDetails.fromJson(json['paymentDetails'])
        : null;
    tenantId = json['tenantId'];
    additionalDetails = json['additionalDetails'] != null
        ? new AdditionalDetails.fromJson(json['additionalDetails'])
        : null;
    isRefundOrder = json['isRefundOrder'];
    status = json['status'];
    receiptNumber = json['receiptNumber'];
    amount = json['amount'];
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
    if (json['tokenDetails'] != null) {
      tokenDetails = new List<TokenDetails>();
      json['tokenDetails'].forEach((v) {
        tokenDetails.add(new TokenDetails.fromJson(v));
      });
    }
    transactionDateDisplay = json['transactionDateDisplay'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    thankYouMessage = json['thankYouMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['userId'] = this.userId;
    data['transactionDate'] = this.transactionDate;
    if (this.orderItemDetails != null) {
      data['orderItemDetails'] =
          this.orderItemDetails.map((v) => v.toJson()).toList();
    }
    if (this.paymentDetails != null) {
      data['paymentDetails'] = this.paymentDetails.toJson();
    }
    data['tenantId'] = this.tenantId;
    if (this.additionalDetails != null) {
      data['additionalDetails'] = this.additionalDetails.toJson();
    }
    data['isRefundOrder'] = this.isRefundOrder;
    data['status'] = this.status;
    data['receiptNumber'] = this.receiptNumber;
    data['amount'] = this.amount;
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    if (this.tokenDetails != null) {
      data['tokenDetails'] = this.tokenDetails.map((v) => v.toJson()).toList();
    }
    data['transactionDateDisplay'] = this.transactionDateDisplay;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['thankYouMessage'] = this.thankYouMessage;
    return data;
  }
}

class OrderItemDetails {
  String fee;
  String name;
  int quantity;
  double totalAmount;
  String serviceType;
  String processType;
  bool templeToken;

  OrderItemDetails(
      {this.fee,
      this.name,
      this.quantity,
      this.totalAmount,
      this.serviceType,
      this.processType,
      this.templeToken});

  factory OrderItemDetails.fromJson(Map<String, dynamic> parsedJson) {
    var fee = double.parse(parsedJson['fee']).toStringAsFixed(2);
    var name = parsedJson['name'];
    var quantity = parsedJson['quantity'];
    var totalAmount;
    if (parsedJson['totalAmount'] is int) {
      totalAmount = parsedJson['totalAmount'].toDouble();
    } else {
      totalAmount = parsedJson['totalAmount'];
    }
    var serviceType = parsedJson['serviceType'];
    var processType = parsedJson['processType'];
    var templeToken = parsedJson['templeToken'];

    return OrderItemDetails(
        fee: fee,
        name: name,
        quantity: quantity,
        totalAmount: totalAmount,
        serviceType: serviceType,
        processType: processType,
        templeToken: templeToken);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fee'] = this.fee;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['serviceType'] = this.serviceType;
    data['totalAmount'] = this.totalAmount;
    data['processType'] = this.processType;
    data['templeToken'] = this.templeToken;
    return data;
  }
}

class PaymentDetails {
  String bankName;
  String payeeName;
  String paymentType;
  String approvalNumber;
  String dateOnTheCheck;

  PaymentDetails(
      {this.bankName,
      this.payeeName,
      this.paymentType,
      this.approvalNumber,
      this.dateOnTheCheck});

  PaymentDetails.fromJson(Map<String, dynamic> json) {
    bankName = json['bankName'];
    payeeName = json['payeeName'];
    paymentType = json['paymentType'];
    approvalNumber = json['approvalNumber'];
    dateOnTheCheck = json['dateOnTheCheck'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bankName'] = this.bankName;
    data['payeeName'] = this.payeeName;
    data['paymentType'] = this.paymentType;
    data['approvalNumber'] = this.approvalNumber;
    data['dateOnTheCheck'] = this.dateOnTheCheck;
    return data;
  }
}

class AdditionalDetails {
  String notes;
  bool isKiosk;

  AdditionalDetails({this.notes, this.isKiosk});

  AdditionalDetails.fromJson(Map<String, dynamic> json) {
    notes = json['notes'];
    isKiosk = json['isKiosk'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notes'] = this.notes;
    data['isKiosk'] = this.isKiosk;
    return data;
  }
}

class AdditionalProperties {
  String password;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String phoneNumber;
  String receiptLogo;
  String kioskBanner;
  String shortCode;
  String createdBy;
  String modifiedBy;
  String modifiedDate;

  AdditionalProperties(
      {this.password,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.phoneNumber,
      this.receiptLogo,
      this.kioskBanner,
      this.shortCode,
      this.createdBy,
      this.modifiedBy,
      this.modifiedDate});

  factory AdditionalProperties.fromJson(Map<String, dynamic> parsedJson) {
    var password = parsedJson['password'];
    var mailFooter = parsedJson['mailFooter'];
    var mailHeader = parsedJson['mailHeader'];
    var templeMail = parsedJson['templeMail'];
    var websiteUrl = parsedJson['websiteUrl'];
    var phoneNumber = parsedJson['phoneNumber'];
    var receiptLogo = parsedJson['receiptLogo'];
    var kioskBanner = parsedJson['kioskBanner'];
    var shortCode = parsedJson['shortCode'];
    var createdBy = parsedJson['createdBy'];
    var modifiedBy = parsedJson['modifiedBy'];
    var modifiedDate = parsedJson['modifiedDate'];

    return AdditionalProperties(
      password: password,
      mailFooter: mailFooter,
      mailHeader: mailHeader,
      templeMail: templeMail,
      websiteUrl: websiteUrl,
      phoneNumber: phoneNumber,
      receiptLogo: receiptLogo,
      kioskBanner: kioskBanner,
      shortCode: shortCode,
      createdBy: createdBy,
      modifiedBy: modifiedBy,
      modifiedDate: modifiedDate,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['mailFooter'] = this.mailFooter;
    data['mailHeader'] = this.mailHeader;
    data['templeMail'] = this.templeMail;
    data['websiteUrl'] = this.websiteUrl;
    data['phoneNumber'] = this.phoneNumber;
    data['receiptLogo'] = this.receiptLogo;
    data['kioskBanner'] = this.kioskBanner;
    data['modifiedDate'] = this.shortCode;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['modifiedDate'] = this.modifiedDate;
    return data;
  }
}

class AdditionalPropertiesOld {
  String createdBy;
  String modifiedBy;
  String modifiedDate;

  AdditionalPropertiesOld({this.createdBy, this.modifiedBy, this.modifiedDate});

  AdditionalPropertiesOld.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    modifiedBy = json['modifiedBy'];
    modifiedDate = json['modifiedDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['modifiedDate'] = this.modifiedDate;
    return data;
  }
}

class TokenDetails {
  String templeServiceId;
  bool templeToken;
  bool devoteeReceipt;

  TokenDetails({this.templeServiceId, this.templeToken, this.devoteeReceipt});

  TokenDetails.fromJson(Map<String, dynamic> json) {
    templeServiceId = json['templeServiceId'];
    templeToken = json['templeToken'];
    devoteeReceipt = json['devoteeReceipt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templeServiceId'] = this.templeServiceId;
    data['templeToken'] = this.templeToken;
    data['devoteeReceipt'] = this.devoteeReceipt;
    return data;
  }
}

class User {
  String userId;
  String userAccountId;
  String firstName;
  String middleName;
  String lastName;
  String dateOfBirth;
  String nakshatra;
  String gender;
  Address userAddress;
  String tenantId;
  bool isWalkinUser;
  String status;
  String mobileNumber;
  String email;
  List<NakshatraDetails> nakshatraDetails;

  User(
      {this.userId,
      this.userAccountId,
      this.firstName,
      this.middleName,
      this.lastName,
      this.dateOfBirth,
      this.nakshatra,
      this.gender,
      this.userAddress,
      this.tenantId,
      this.isWalkinUser,
      this.status,
      this.mobileNumber,
      this.email,
      this.nakshatraDetails});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    userAccountId = json['userAccountId'];
    firstName = json['firstName'];
    middleName = json['middleName'];
    lastName = json['lastName'];
    dateOfBirth = json['dateOfBirth'];
    nakshatra = json['nakshatra'];
    gender = json['gender'];
    userAddress =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    tenantId = json['tenantId'];
    isWalkinUser = json['isWalkinUser'];
    status = json['status'];
    mobileNumber = json['mobileNumber'];
    email = json['email'];
    if (json['nakshatraDetails'] != null) {
      nakshatraDetails = new List<NakshatraDetails>();
      json['nakshatraDetails'].forEach((v) {
        nakshatraDetails.add(new NakshatraDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['userAccountId'] = this.userAccountId;
    data['firstName'] = this.firstName;
    data['middleName'] = this.middleName;
    data['lastName'] = this.lastName;
    data['dateOfBirth'] = this.dateOfBirth;
    data['nakshatra'] = this.nakshatra;
    data['gender'] = this.gender;
    if (this.userAddress != null) {
      data['address'] = this.userAddress.toJson();
    }
    data['tenantId'] = this.tenantId;
    data['isWalkinUser'] = this.isWalkinUser;
    data['status'] = this.status;
    data['mobileNumber'] = this.mobileNumber;
    data['email'] = this.email;
    if (this.nakshatraDetails != null) {
      data['nakshatraDetails'] =
          this.nakshatraDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NakshatraDetails {
  String name;
  String nakshatra;

  NakshatraDetails({this.name, this.nakshatra});

  NakshatraDetails.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    nakshatra = json['nakshatra'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['nakshatra'] = this.nakshatra;
    return data;
  }
}

class Address {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;
  StateAddress state;
  Country country;

  Address(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode,
      this.state,
      this.country});

  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    address = json['address'];
    stateId = json['stateId'];
    zipCode = json['zipCode'];
    countryId = json['countryId'];
    countryCode = json['countryCode'];
    state =
        json['state'] != null ? new StateAddress.fromJson(json['state']) : null;
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['address'] = this.address;
    data['stateId'] = this.stateId;
    data['zipCode'] = this.zipCode;
    data['countryId'] = this.countryId;
    data['countryCode'] = this.countryCode;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    return data;
  }
}

class StateAddress {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  String referenceId;
  Null tenantId;

  StateAddress(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  StateAddress.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class Country {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  Null referenceId;
  Null tenantId;

  Country(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  Country.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class OrderInfo {
  double receiptTotalAmount;
  String receiptType;
  DRRPaymentDetails dRRPaymentDetails;

  OrderInfo(
      {this.receiptTotalAmount, this.receiptType, this.dRRPaymentDetails});

  OrderInfo.fromJson(Map<String, dynamic> json) {
    if (json['receiptTotalAmount'] is double) {
      receiptTotalAmount = json['receiptTotalAmount'];
    } else if (json['receiptTotalAmount'] is String) {
      receiptTotalAmount = json['receiptTotalAmount'] as double;
    } else if (json['receiptTotalAmount'] is int) {
      receiptTotalAmount = (json['receiptTotalAmount'] as int).toDouble();
    } else {
      receiptTotalAmount = json['receiptTotalAmount'];
    }

    receiptType = json['receiptType'];
    dRRPaymentDetails = json['DRRPaymentDetails'] != null
        ? new DRRPaymentDetails.fromJson(json['DRRPaymentDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['receiptTotalAmount'] = this.receiptTotalAmount;
    data['receiptType'] = this.receiptType;
    if (this.dRRPaymentDetails != null) {
      data['DRRPaymentDetails'] = this.dRRPaymentDetails.toJson();
    }
    return data;
  }
}

class DRRPaymentDetails {
  String notes;
  String bankName;
  String payeeName;
  String paymentType;
  String repaymentMode;
  String approvalNumber;
  String dateOnTheCheck;

  DRRPaymentDetails(
      {this.notes,
      this.bankName,
      this.payeeName,
      this.paymentType,
      this.repaymentMode,
      this.approvalNumber,
      this.dateOnTheCheck});

  DRRPaymentDetails.fromJson(Map<String, dynamic> json) {
    notes = json['notes'];
    bankName = json['bankName'];
    payeeName = json['payeeName'];
    paymentType = json['paymentType'];
    repaymentMode = json['repaymentMode'];
    approvalNumber = json['approvalNumber'];
    dateOnTheCheck = json['dateOnTheCheck'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notes'] = this.notes;
    data['bankName'] = this.bankName;
    data['payeeName'] = this.payeeName;
    data['paymentType'] = this.paymentType;
    data['repaymentMode'] = this.repaymentMode;
    data['approvalNumber'] = this.approvalNumber;
    data['dateOnTheCheck'] = this.dateOnTheCheck;
    return data;
  }
}

class TenantInfo {
  String tenantId;
  String name;
  Address address;
  AdditionalProperties additionalProperties;
  String domain;
  String shortCode;

  TenantInfo(
      {this.tenantId,
      this.name,
      this.address,
      this.additionalProperties,
      this.domain,
      this.shortCode});

  TenantInfo.fromJson(Map<String, dynamic> json) {
    tenantId = json['tenantId'];
    name = json['name'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
    domain = json['domain'];
    shortCode = json['shortCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tenantId'] = this.tenantId;
    data['name'] = this.name;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    data['domain'] = this.domain;
    data['shortCode'] = this.shortCode;
    return data;
  }
}

class OrderReceiptDetails {
  bool success;
  String message;
  ReceiptDetails receiptDetails;

  OrderReceiptDetails({this.success, this.message, this.receiptDetails});

  OrderReceiptDetails.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    receiptDetails = json['receiptDetails'] != null
        ? new ReceiptDetails.fromJson(json['receiptDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.receiptDetails != null) {
      data['receiptDetails'] = this.receiptDetails.toJson();
    }
    return data;
  }
}

class ReceiptDetails {
  Order order;
  OrderInfo orderInfo;
  Tenant tenantInfo;
  String notes;

  ReceiptDetails({this.order, this.orderInfo, this.tenantInfo, this.notes});

  ReceiptDetails.fromJson(Map<String, dynamic> json) {
    order = json['order'] != null ? new Order.fromJson(json['order']) : null;
    orderInfo = json['orderInfo'] != null
        ? new OrderInfo.fromJson(json['orderInfo'])
        : null;
    tenantInfo = json['tenantInfo'] != null
        ? new Tenant.fromJson(json['tenantInfo'])
        : null;
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.order != null) {
      data['order'] = this.order.toJson();
    }
    if (this.orderInfo != null) {
      data['orderInfo'] = this.orderInfo.toJson();
    }
    if (this.tenantInfo != null) {
      data['tenantInfo'] = this.tenantInfo.toJson();
    }
    data['notes'] = this.notes;
    return data;
  }
}

class Order {
  String orderId;
  String userId;
  String transactionDate;
  List<OrderItemDetails> orderItemDetails;
  PaymentDetails paymentDetails;
  String tenantId;
  AdditionalDetails additionalDetails;
  bool isRefundOrder;
  String status;
  int receiptNumber;
  String amount;
  AdditionalProperties additionalProperties;
  String transactionDateDisplay;
  User user;
  String thankYouMessage;

  Order(
      {this.orderId,
      this.userId,
      this.transactionDate,
      this.orderItemDetails,
      this.paymentDetails,
      this.tenantId,
      this.additionalDetails,
      this.isRefundOrder,
      this.status,
      this.receiptNumber,
      this.amount,
      this.additionalProperties,
      this.transactionDateDisplay,
      this.user,
      this.thankYouMessage});

  Order.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    userId = json['userId'];
    transactionDate = json['transactionDate'];
    if (json['orderItemDetails'] != null) {
      orderItemDetails = new List<OrderItemDetails>();
      json['orderItemDetails'].forEach((v) {
        orderItemDetails.add(new OrderItemDetails.fromJson(v));
      });
    }
    paymentDetails = json['paymentDetails'] != null
        ? new PaymentDetails.fromJson(json['paymentDetails'])
        : null;
    tenantId = json['tenantId'];
    additionalDetails = json['additionalDetails'] != null
        ? new AdditionalDetails.fromJson(json['additionalDetails'])
        : null;
    isRefundOrder = json['isRefundOrder'];
    status = json['status'];
    receiptNumber = json['receiptNumber'];
    amount = json['amount'];
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
    transactionDateDisplay = json['transactionDateDisplay'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    thankYouMessage = json['thankYouMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['userId'] = this.userId;
    data['transactionDate'] = this.transactionDate;
    if (this.orderItemDetails != null) {
      data['orderItemDetails'] =
          this.orderItemDetails.map((v) => v.toJson()).toList();
    }
    if (this.paymentDetails != null) {
      data['paymentDetails'] = this.paymentDetails.toJson();
    }
    data['tenantId'] = this.tenantId;
    if (this.additionalDetails != null) {
      data['additionalDetails'] = this.additionalDetails.toJson();
    }
    data['isRefundOrder'] = this.isRefundOrder;
    data['status'] = this.status;
    data['receiptNumber'] = this.receiptNumber;
    data['amount'] = this.amount;
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    data['transactionDateDisplay'] = this.transactionDateDisplay;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['thankYouMessage'] = this.thankYouMessage;
    return data;
  }
}

class OrderItemDetails {
  String fee;
  String name;
  int quantity;
  bool isZeroFee;
  String serviceType;
  int totalAmount;
  List<Null> performedDate;
  String templeServiceId;
  String processType;

  OrderItemDetails(
      {this.fee,
      this.name,
      this.quantity,
      this.isZeroFee,
      this.serviceType,
      this.totalAmount,
      this.performedDate,
      this.templeServiceId,
      this.processType});

  OrderItemDetails.fromJson(Map<String, dynamic> json) {
    fee = json['fee'];
    name = json['name'];
    quantity = json['quantity'];
    isZeroFee = json['isZeroFee'];
    serviceType = json['serviceType'];
    totalAmount = json['totalAmount'];
    /* 	if (json['performedDate'] != null) {
			performedDate = new List<Null>();
			json['performedDate'].forEach((v) { performedDate.add(new Null.fromJson(v)); });
		} */
    templeServiceId = json['templeServiceId'];
    processType = json['processType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fee'] = this.fee;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['isZeroFee'] = this.isZeroFee;
    data['serviceType'] = this.serviceType;
    data['totalAmount'] = this.totalAmount;
    /* 	if (this.performedDate != null) {
      data['performedDate'] = this.performedDate.map((v) => v.toJson()).toList();
    } */
    data['templeServiceId'] = this.templeServiceId;
    data['processType'] = this.processType;
    return data;
  }
}

class PaymentDetails {
  String paymentType;

  PaymentDetails({this.paymentType});

  PaymentDetails.fromJson(Map<String, dynamic> json) {
    paymentType = json['paymentType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paymentType'] = this.paymentType;
    return data;
  }
}

class AdditionalDetails {
  String notes;
  bool isKiosk;

  AdditionalDetails({this.notes, this.isKiosk});

  AdditionalDetails.fromJson(Map<String, dynamic> json) {
    notes = json['notes'];
    isKiosk = json['isKiosk'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notes'] = this.notes;
    data['isKiosk'] = this.isKiosk;
    return data;
  }
}

class AdditionalProperties {
  String createdBy;
  Null modifiedBy;
  String createdDate;
  Null modifiedDate;

  AdditionalProperties(
      {this.createdBy, this.modifiedBy, this.createdDate, this.modifiedDate});

  AdditionalProperties.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    modifiedBy = json['modifiedBy'];
    createdDate = json['createdDate'];
    modifiedDate = json['modifiedDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['createdDate'] = this.createdDate;
    data['modifiedDate'] = this.modifiedDate;
    return data;
  }
}

class User {
  String userId;
  String userAccountId;
  String firstName;
  String middleName;
  String lastName;
  Null dateOfBirth;
  String nakshatra;
  Null gender;
  Address address;
  List<Roles> roles;
  AdditionalProperties additionalProperties;
  Null templeServiceWishList;
  Null relatives;
  String tenantId;
  Null companies;
  bool isWalkinUser;
  Null walkinDetails;
  String status;
  String mobileNumber;
  String email;
  List<NakshatraDetails> nakshatraDetails;

  User(
      {this.userId,
      this.userAccountId,
      this.firstName,
      this.middleName,
      this.lastName,
      this.dateOfBirth,
      this.nakshatra,
      this.gender,
      this.address,
      this.roles,
      this.additionalProperties,
      this.templeServiceWishList,
      this.relatives,
      this.tenantId,
      this.companies,
      this.isWalkinUser,
      this.walkinDetails,
      this.status,
      this.mobileNumber,
      this.email,
      this.nakshatraDetails});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    userAccountId = json['userAccountId'];
    firstName = json['firstName'];
    middleName = json['middleName'];
    lastName = json['lastName'];
    dateOfBirth = json['dateOfBirth'];
    nakshatra = json['nakshatra'];
    gender = json['gender'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    if (json['roles'] != null) {
      roles = new List<Roles>();
      json['roles'].forEach((v) {
        roles.add(new Roles.fromJson(v));
      });
    }
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
    templeServiceWishList = json['templeServiceWishList'];
    relatives = json['relatives'];
    tenantId = json['tenantId'];
    companies = json['companies'];
    isWalkinUser = json['isWalkinUser'];
    walkinDetails = json['walkinDetails'];
    status = json['status'];
    mobileNumber = json['mobileNumber'];
    email = json['email'];
    if (json['nakshatraDetails'] != null) {
      nakshatraDetails = new List<NakshatraDetails>();
      json['nakshatraDetails'].forEach((v) {
        nakshatraDetails.add(new NakshatraDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['userAccountId'] = this.userAccountId;
    data['firstName'] = this.firstName;
    data['middleName'] = this.middleName;
    data['lastName'] = this.lastName;
    data['dateOfBirth'] = this.dateOfBirth;
    data['nakshatra'] = this.nakshatra;
    data['gender'] = this.gender;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.roles != null) {
      data['roles'] = this.roles.map((v) => v.toJson()).toList();
    }
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    data['templeServiceWishList'] = this.templeServiceWishList;
    data['relatives'] = this.relatives;
    data['tenantId'] = this.tenantId;
    data['companies'] = this.companies;
    data['isWalkinUser'] = this.isWalkinUser;
    data['walkinDetails'] = this.walkinDetails;
    data['status'] = this.status;
    data['mobileNumber'] = this.mobileNumber;
    data['email'] = this.email;
    if (this.nakshatraDetails != null) {
      data['nakshatraDetails'] =
          this.nakshatraDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Address {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  State state;
  Country country;

  Address(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.state,
      this.country});

  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    address = json['address'];
    stateId = json['stateId'];
    zipCode = json['zipCode'];
    countryId = json['countryId'];
    state = json['state'] != null ? new State.fromJson(json['state']) : null;
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['address'] = this.address;
    data['stateId'] = this.stateId;
    data['zipCode'] = this.zipCode;
    data['countryId'] = this.countryId;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    return data;
  }
}

class State {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  String referenceId;
  Null tenantId;

  State(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  State.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class Country {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  Null referenceId;
  Null tenantId;

  Country(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  Country.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class Roles {
  String roleId;

  Roles({this.roleId});

  Roles.fromJson(Map<String, dynamic> json) {
    roleId = json['roleId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roleId'] = this.roleId;
    return data;
  }
}

class UserAdditionalProperties {
  String gothra;
  String createdBy;
  String modifiedBy;
  String createdDate;
  String devoteeCode;
  String modifiedDate;
  ThemeSettings themeSettings;
  //FavoriteSettings favoriteSettings;
  String userPrivilegeType;
  bool isReceiveEmailTemplate;
  bool isReceiveThankYouLetter;

  UserAdditionalProperties(
      {this.gothra,
      this.createdBy,
      this.modifiedBy,
      this.createdDate,
      this.devoteeCode,
      this.modifiedDate,
      this.themeSettings,
      //this.favoriteSettings,
      this.userPrivilegeType,
      this.isReceiveEmailTemplate,
      this.isReceiveThankYouLetter});

  UserAdditionalProperties.fromJson(Map<String, dynamic> json) {
    gothra = json['gothra'];
    createdBy = json['createdBy'];
    modifiedBy = json['modifiedBy'];
    createdDate = json['createdDate'];
    devoteeCode = json['devoteeCode'];
    modifiedDate = json['modifiedDate'];
    themeSettings = json['themeSettings'] != null
        ? new ThemeSettings.fromJson(json['themeSettings'])
        : null;
    /*   favoriteSettings = json['favoriteSettings'] != null
        ? new FavoriteSettings.fromJson(json['favoriteSettings'])
        : null; */
    userPrivilegeType = json['userPrivilegeType'];
    isReceiveEmailTemplate = json['isReceiveEmailTemplate'];
    isReceiveThankYouLetter = json['isReceiveThankYouLetter'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['gothra'] = this.gothra;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['createdDate'] = this.createdDate;
    data['devoteeCode'] = this.devoteeCode;
    data['modifiedDate'] = this.modifiedDate;
    if (this.themeSettings != null) {
      data['themeSettings'] = this.themeSettings.toJson();
    }
    /* if (this.favoriteSettings != null) {
      data['favoriteSettings'] = this.favoriteSettings.toJson();
    } */
    data['userPrivilegeType'] = this.userPrivilegeType;
    data['isReceiveEmailTemplate'] = this.isReceiveEmailTemplate;
    data['isReceiveThankYouLetter'] = this.isReceiveThankYouLetter;
    return data;
  }
}

class ThemeSettings {
  String name;
  Theme theme;
  String title;
  Tenant tenant;

  ThemeSettings({this.name, this.theme, this.title, this.tenant});

  ThemeSettings.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    theme = json['theme'] != null ? new Theme.fromJson(json['theme']) : null;
    title = json['title'];
    tenant =
        json['tenant'] != null ? new Tenant.fromJson(json['tenant']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.theme != null) {
      data['theme'] = this.theme.toJson();
    }
    data['title'] = this.title;
    if (this.tenant != null) {
      data['tenant'] = this.tenant.toJson();
    }
    return data;
  }
}

class Theme {
  String menu;
  String skin;
  String menuType;
  bool showMenu;
  bool showLogin;
  bool showMenuIcon;
  bool showSideChat;
  bool showUserInfo;
  bool footerIsFixed;
  bool navbarIsFixed;
  bool sidebarIsFixed;
  bool sideChatIsHoverable;
  bool showLoginUserFeatures;

  Theme(
      {this.menu,
      this.skin,
      this.menuType,
      this.showMenu,
      this.showLogin,
      this.showMenuIcon,
      this.showSideChat,
      this.showUserInfo,
      this.footerIsFixed,
      this.navbarIsFixed,
      this.sidebarIsFixed,
      this.sideChatIsHoverable,
      this.showLoginUserFeatures});

  Theme.fromJson(Map<String, dynamic> json) {
    menu = json['menu'];
    skin = json['skin'];
    menuType = json['menuType'];
    showMenu = json['showMenu'];
    showLogin = json['showLogin'];
    showMenuIcon = json['showMenuIcon'];
    showSideChat = json['showSideChat'];
    showUserInfo = json['showUserInfo'];
    footerIsFixed = json['footerIsFixed'];
    navbarIsFixed = json['navbarIsFixed'];
    sidebarIsFixed = json['sidebarIsFixed'];
    sideChatIsHoverable = json['sideChatIsHoverable'];
    showLoginUserFeatures = json['showLoginUserFeatures'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menu'] = this.menu;
    data['skin'] = this.skin;
    data['menuType'] = this.menuType;
    data['showMenu'] = this.showMenu;
    data['showLogin'] = this.showLogin;
    data['showMenuIcon'] = this.showMenuIcon;
    data['showSideChat'] = this.showSideChat;
    data['showUserInfo'] = this.showUserInfo;
    data['footerIsFixed'] = this.footerIsFixed;
    data['navbarIsFixed'] = this.navbarIsFixed;
    data['sidebarIsFixed'] = this.sidebarIsFixed;
    data['sideChatIsHoverable'] = this.sideChatIsHoverable;
    data['showLoginUserFeatures'] = this.showLoginUserFeatures;
    return data;
  }
}

class Tenant {
  String name;
  String domain;
  Address address;
  String tenantId;
  String shortCode;
  AdditionalProperties additionalProperties;

  Tenant(
      {this.name,
      this.domain,
      this.address,
      this.tenantId,
      this.shortCode,
      this.additionalProperties});

  Tenant.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    domain = json['domain'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    tenantId = json['tenantId'];
    shortCode = json['shortCode'];
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['domain'] = this.domain;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    data['tenantId'] = this.tenantId;
    data['shortCode'] = this.shortCode;
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    return data;
  }
}

class TenantAddress {
  String city;
  State state;
  String address;
  Country country;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;

  TenantAddress(
      {this.city,
      this.state,
      this.address,
      this.country,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode});

  TenantAddress.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    state = json['state'] != null ? new State.fromJson(json['state']) : null;
    address = json['address'];
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    stateId = json['stateId'];
    zipCode = json['zipCode'];
    countryId = json['countryId'];
    countryCode = json['countryCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    data['address'] = this.address;
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    data['stateId'] = this.stateId;
    data['zipCode'] = this.zipCode;
    data['countryId'] = this.countryId;
    data['countryCode'] = this.countryCode;
    return data;
  }
}

class TempleAdditionalProperties {
  String password;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String phoneNumber;
  String receiptLogo;

  TempleAdditionalProperties(
      {this.password,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.phoneNumber,
      this.receiptLogo});

  TempleAdditionalProperties.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    mailFooter = json['mailFooter'];
    mailHeader = json['mailHeader'];
    templeMail = json['templeMail'];
    websiteUrl = json['websiteUrl'];
    phoneNumber = json['phoneNumber'];
    receiptLogo = json['receiptLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['mailFooter'] = this.mailFooter;
    data['mailHeader'] = this.mailHeader;
    data['templeMail'] = this.templeMail;
    data['websiteUrl'] = this.websiteUrl;
    data['phoneNumber'] = this.phoneNumber;
    data['receiptLogo'] = this.receiptLogo;
    return data;
  }
}

/* class FavoriteSettings {


	FavoriteSettings({});

	FavoriteSettings.fromJson(Map<String, dynamic> json) {
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		return data;
	}
} */

class NakshatraDetails {
  String name;
  String nakshatra;

  NakshatraDetails({this.name, this.nakshatra});

  NakshatraDetails.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    nakshatra = json['nakshatra'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['nakshatra'] = this.nakshatra;
    return data;
  }
}

class OrderInfo {
  int receiptTotalAmount;
  String receiptType;
  //FavoriteSettings dRRPaymentDetails;

  OrderInfo({this.receiptTotalAmount, this.receiptType});

  OrderInfo.fromJson(Map<String, dynamic> json) {
    receiptTotalAmount = json['receiptTotalAmount'];
    receiptType = json['receiptType'];
    /*  dRRPaymentDetails = json['DRRPaymentDetails'] != null
        ? new FavoriteSettings.fromJson(json['DRRPaymentDetails'])
        : null; */
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['receiptTotalAmount'] = this.receiptTotalAmount;
    data['receiptType'] = this.receiptType;
    /* if (this.dRRPaymentDetails != null) {
      data['DRRPaymentDetails'] = this.dRRPaymentDetails.toJson();
    } */
    return data;
  }
}

class OrderAdditionalProperties {
  String password;
  String kioskImage;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String phoneNumber;
  String receiptLogo;

  OrderAdditionalProperties(
      {this.password,
      this.kioskImage,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.phoneNumber,
      this.receiptLogo});

  OrderAdditionalProperties.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    kioskImage = json['kioskImage'];
    mailFooter = json['mailFooter'];
    mailHeader = json['mailHeader'];
    templeMail = json['templeMail'];
    websiteUrl = json['websiteUrl'];
    phoneNumber = json['phoneNumber'];
    receiptLogo = json['receiptLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['kioskImage'] = this.kioskImage;
    data['mailFooter'] = this.mailFooter;
    data['mailHeader'] = this.mailHeader;
    data['templeMail'] = this.templeMail;
    data['websiteUrl'] = this.websiteUrl;
    data['phoneNumber'] = this.phoneNumber;
    data['receiptLogo'] = this.receiptLogo;
    return data;
  }
}

class PrintReceiptDetails {
  Order order;
  OrderInfo orderInfo;
  Tenant tenantInfo;
  String notes;

  PrintReceiptDetails(
      {this.order, this.orderInfo, this.tenantInfo, this.notes});

  PrintReceiptDetails.fromJson(Map<String, dynamic> json) {
    order = json['order'] != null ? new Order.fromJson(json['order']) : null;
    orderInfo = json['orderInfo'] != null
        ? new OrderInfo.fromJson(json['orderInfo'])
        : null;
    tenantInfo = json['tenantInfo'] != null
        ? new Tenant.fromJson(json['tenantInfo'])
        : null;
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.order != null) {
      data['order'] = this.order.toJson();
    }
    if (this.orderInfo != null) {
      data['orderInfo'] = this.orderInfo.toJson();
    }
    if (this.tenantInfo != null) {
      data['tenantInfo'] = this.tenantInfo.toJson();
    }
    data['notes'] = this.notes;
    return data;
  }
}

class PrintReceiptOrderInfo {
  int receiptTotalAmount;
  String receiptType;
  //FavoriteSettings dRRPaymentDetails;

  PrintReceiptOrderInfo({
    this.receiptTotalAmount,
    this.receiptType,
  });

  PrintReceiptOrderInfo.fromJson(Map<String, dynamic> json) {
    receiptTotalAmount = json['receiptTotalAmount'];
    receiptType = json['receiptType'];
    /*  dRRPaymentDetails = json['DRRPaymentDetails'] != null
        ? new FavoriteSettings.fromJson(json['DRRPaymentDetails'])
        : null; */
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['receiptTotalAmount'] = this.receiptTotalAmount;
    data['receiptType'] = this.receiptType;
    /* if (this.dRRPaymentDetails != null) {
      data['DRRPaymentDetails'] = this.dRRPaymentDetails.toJson();
    } */
    return data;
  }
}

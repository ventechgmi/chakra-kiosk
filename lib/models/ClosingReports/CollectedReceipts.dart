import 'package:chakra/models/PrintReceipt/PrintReceipt.dart';

class CollectedReceipts {
  bool success;
  String message;
  List<ClosingReceiptDetails> closingReceipt;
  TenantInfo tenantInfo;

  CollectedReceipts(
      {this.success, this.message, this.closingReceipt, this.tenantInfo});

  factory CollectedReceipts.fromJson(Map<String, dynamic> parsedJson) {
    var success = parsedJson['success'];
    var message = parsedJson['message'];
    var closingReceipt = parsedJson['closingReceiptDetails'] as List;
    List<ClosingReceiptDetails> closingReceiptsList =
        closingReceipt.map((i) => ClosingReceiptDetails.fromJson(i)).toList();

    return CollectedReceipts(
      success: success,
      message: message,
      closingReceipt: closingReceiptsList,
    );
  }
}

class TenantInfo {
  String tenantId;
  String name;
  TenantAddress address;
  AdditionalProperties additionalProperties;

  TenantInfo(
      {this.tenantId, this.name, this.address, this.additionalProperties});

  factory TenantInfo.fromJson(Map<String, dynamic> parsedJson) {
    var tenantID = parsedJson['tenantId'];
    var name = parsedJson['name'];
    var address = parsedJson['address'];
    var additionalProperties = parsedJson['additionalProperties'];

    AdditionalProperties additionaProperty =
        AdditionalProperties.fromJson(additionalProperties);

    TenantAddress addressJson = TenantAddress.fromJson(address);

    return TenantInfo(
        tenantId: tenantID,
        name: name,
        address: addressJson,
        additionalProperties: additionaProperty);
  }
}

class AdditionalProperties {
  String password;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String phoneNumber;
  String receiptLogo;
  String kioskBanner;
  String shortCode;

  AdditionalProperties(
      {this.password,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.phoneNumber,
      this.receiptLogo,
      this.kioskBanner,
      this.shortCode});

  factory AdditionalProperties.fromJson(Map<String, dynamic> parsedJson) {
    var password = parsedJson['password'];
    var mailFooter = parsedJson['mailFooter'];
    var mailHeader = parsedJson['mailHeader'];
    var templeMail = parsedJson['templeMail'];
    var websiteUrl = parsedJson['websiteUrl'];
    var phoneNumber = parsedJson['phoneNumber'];
    var receiptLogo = parsedJson['receiptLogo'];
    var kioskBanner = parsedJson['kioskBanner'];
    var shortCode = parsedJson['shortCode'];

    return AdditionalProperties(
        password: password,
        mailFooter: mailFooter,
        mailHeader: mailHeader,
        templeMail: templeMail,
        websiteUrl: websiteUrl,
        phoneNumber: phoneNumber,
        receiptLogo: receiptLogo,
        kioskBanner: kioskBanner,
        shortCode: shortCode);
  }
}

class TenantAddress {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;

  StateDetails state;

  TenantAddress(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode,
      this.state});

  factory TenantAddress.fromJson(Map<String, dynamic> parsedJson) {
    var city = parsedJson['city'];
    var address = parsedJson['address'];
    var stateId = parsedJson['stateId'];
    var zipCode = parsedJson['zipCode'];
    var countryId = parsedJson['countryId'];
    var countryCode = parsedJson['countryCode'];
    var state = parsedJson['state'];

    return TenantAddress(
        city: city,
        address: address,
        stateId: stateId,
        zipCode: zipCode,
        countryId: countryId,
        countryCode: countryCode,
        state: state);
  }
}

class StateDetails {
  String name;

  StateDetails({this.name});

  factory StateDetails.fromJson(Map<String, dynamic> parsedJson) {
    var name = parsedJson['name'];

    return StateDetails(name: name);
  }
}

class ClosingReceiptDetails {
  String collectedBy;
  List<ItemDetails> itemDetails;
  double grandTotal;
  double cashTotal;
  double checkTotal;
  double cardTotal;

  ClosingReceiptDetails(
      {this.collectedBy,
      this.itemDetails,
      this.grandTotal,
      this.cashTotal,
      this.cardTotal,
      this.checkTotal});

  factory ClosingReceiptDetails.fromJson(Map<String, dynamic> parsedJson) {
    var collectedBy = parsedJson['collectedBy'];
    var items = parsedJson['itemDetails'] as List;
    //var grandTotal = parsedJson['GrandTotal'];
    var grandTotal;
    if (parsedJson['GrandTotal'] is double) {
      grandTotal = (parsedJson['GrandTotal'] as double).toStringAsFixed(2);
    } else {
      grandTotal = parsedJson['GrandTotal'];
    }

    var cashTotal;
    if (parsedJson['CashTotal'] is double) {
      cashTotal = (parsedJson['CashTotal'] as double).toStringAsFixed(2);
    } else {
      cashTotal = parsedJson['CashTotal'];
    }

    var checkTotal = parsedJson['CheckTotal'];
    var cardTotal = parsedJson['CardTotal'];

    List<ItemDetails> itemsList =
        items.map((i) => ItemDetails.fromJson(i)).toList();

    return ClosingReceiptDetails(
        collectedBy: collectedBy,
        itemDetails: itemsList,
        grandTotal: grandTotal,
        cashTotal: cashTotal,
        cardTotal: cardTotal,
        checkTotal: checkTotal);
  }
}

class ItemDetails {
  int total;
  int itemCount = 0;
  String name = '';
  int receiptNumber = 0;

  ItemDetails({this.total, this.itemCount, this.name, this.receiptNumber});

  factory ItemDetails.fromJson(Map<String, dynamic> parsedJson) {
    var total;
    if (parsedJson['total'] is double) {
      total = parsedJson['total'].toInt();
    } else if (parsedJson['total'] is String) {
      total = double.parse(parsedJson['total']);
      total = total.toInt();
    } else {
      total = parsedJson['total'];
    }
    var itemCount = 0;
    var name = '';
    try {
      itemCount = parsedJson['count'];
      name = parsedJson['name'];
    } catch (e) {}

    var receiptNumber;
    try {
      if (parsedJson['receiptNumber'] is String) {
        receiptNumber = int.parse(parsedJson['receiptNumber']);
        print(receiptNumber);
      } else {
        receiptNumber = parsedJson['receiptNumber'];
        print(receiptNumber);
      }
    } catch (e) {}

    return ItemDetails(
        total: total,
        itemCount: itemCount,
        name: name,
        receiptNumber: receiptNumber);
  }
}

class ClosingReceipt {
  bool success;
  String message;
  ClosingReceiptDetails closingReceiptDetails;

  ClosingReceipt({this.success, this.message, this.closingReceiptDetails});

  ClosingReceipt.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    closingReceiptDetails = json['closingReceiptDetails'] != null
        ? new ClosingReceiptDetails.fromJson(json['closingReceiptDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.closingReceiptDetails != null) {
      data['closingReceiptDetails'] = this.closingReceiptDetails.toJson();
    }
    return data;
  }
}

class ClosingReceiptDetails {
  TenantInfo tenantInfo;
  List<ClosingReportDetails> closingReportDetails;
  bool closingReceiptGroupbyService;

  ClosingReceiptDetails(
      {this.tenantInfo,
      this.closingReportDetails,
      this.closingReceiptGroupbyService});

  ClosingReceiptDetails.fromJson(Map<String, dynamic> json) {
    tenantInfo = json['tenantInfo'] != null
        ? new TenantInfo.fromJson(json['tenantInfo'])
        : null;
    if (json['closingReportDetails'] != null) {
      closingReportDetails = new List<ClosingReportDetails>();
      json['closingReportDetails'].forEach((v) {
        closingReportDetails.add(new ClosingReportDetails.fromJson(v));
      });
    }
    closingReceiptGroupbyService = json['closingReceiptGroupbyService'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tenantInfo != null) {
      data['tenantInfo'] = this.tenantInfo.toJson();
    }
    if (this.closingReportDetails != null) {
      data['closingReportDetails'] =
          this.closingReportDetails.map((v) => v.toJson()).toList();
    }
    data['closingReceiptGroupbyService'] = closingReceiptGroupbyService;
    return data;
  }
}

class TenantInfo {
  String tenantId;
  String name;
  Address address;
  AdditionalProperties additionalProperties;
  String domain;
  String shortCode;

  TenantInfo(
      {this.tenantId,
      this.name,
      this.address,
      this.additionalProperties,
      this.domain,
      this.shortCode});

  TenantInfo.fromJson(Map<String, dynamic> json) {
    tenantId = json['tenantId'];
    name = json['name'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
    domain = json['domain'];
    shortCode = json['shortCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tenantId'] = this.tenantId;
    data['name'] = this.name;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    data['domain'] = this.domain;
    data['shortCode'] = this.shortCode;
    return data;
  }
}

class Address {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;
  State state;
  Country country;

  Address(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode,
      this.state,
      this.country});

  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    address = json['address'];
    stateId = json['stateId'];
    zipCode = json['zipCode'];
    countryId = json['countryId'];
    countryCode = json['countryCode'];
    state = json['state'] != null ? new State.fromJson(json['state']) : null;
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['address'] = this.address;
    data['stateId'] = this.stateId;
    data['zipCode'] = this.zipCode;
    data['countryId'] = this.countryId;
    data['countryCode'] = this.countryCode;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    return data;
  }
}

class State {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  String referenceId;
  Null tenantId;

  State(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  State.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class Country {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  Null description;
  int sortOrder;
  String status;
  Null additionalProperties;
  Null referenceId;
  Null tenantId;

  Country(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  Country.fromJson(Map<String, dynamic> json) {
    applicationCodeId = json['applicationCodeId'];
    applicationCodeType = json['applicationCodeType'];
    name = json['name'];
    title = json['title'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    status = json['status'];
    additionalProperties = json['additionalProperties'];
    referenceId = json['referenceId'];
    tenantId = json['tenantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applicationCodeId'] = this.applicationCodeId;
    data['applicationCodeType'] = this.applicationCodeType;
    data['name'] = this.name;
    data['title'] = this.title;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    data['additionalProperties'] = this.additionalProperties;
    data['referenceId'] = this.referenceId;
    data['tenantId'] = this.tenantId;
    return data;
  }
}

class AdditionalProperties {
  String password;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String kioskBanner;
  String phoneNumber;
  String receiptLogo;

  AdditionalProperties(
      {this.password,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.kioskBanner,
      this.phoneNumber,
      this.receiptLogo});

  AdditionalProperties.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    mailFooter = json['mailFooter'];
    mailHeader = json['mailHeader'];
    templeMail = json['templeMail'];
    websiteUrl = json['websiteUrl'];
    kioskBanner = json['kioskBanner'];
    phoneNumber = json['phoneNumber'];
    receiptLogo = json['receiptLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['mailFooter'] = this.mailFooter;
    data['mailHeader'] = this.mailHeader;
    data['templeMail'] = this.templeMail;
    data['websiteUrl'] = this.websiteUrl;
    data['kioskBanner'] = this.kioskBanner;
    data['phoneNumber'] = this.phoneNumber;
    data['receiptLogo'] = this.receiptLogo;
    return data;
  }
}

class ClosingReportDetails {
  String collectedBy;
  List<ItemDetails> itemDetails;
  List<RefundItemDetails> refundItemDetails;
  double grandTotal;
  double cashTotal;
  double checkTotal;
  double cardTotal;
  double totalAmountRefunded;

  ClosingReportDetails(
      {this.collectedBy,
      this.itemDetails,
      this.refundItemDetails,
      this.grandTotal,
      this.cashTotal,
      this.checkTotal,
      this.cardTotal,
      this.totalAmountRefunded});

  ClosingReportDetails.fromJson(Map<String, dynamic> json) {
    collectedBy = json['collectedBy'];
    if (json['itemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['itemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['refundItemDetails'] != null) {
      refundItemDetails = new List<RefundItemDetails>();
      json['refundItemDetails'].forEach((v) {
        refundItemDetails.add(new RefundItemDetails.fromJson(v));
      });
    }
    grandTotal = json['GrandTotal'] is int
        ? (json['GrandTotal'] as int).toDouble()
        : json['GrandTotal'] as double;
    cashTotal = json['CashTotal'] is int
        ? (json['CashTotal'] as int).toDouble()
        : json['CashTotal'] as double;
    checkTotal = json['CheckTotal'] is int
        ? (json['CheckTotal'] as int).toDouble()
        : json['CheckTotal'] as double;
    cardTotal = json['CardTotal'] is int
        ? (json['CardTotal'] as int).toDouble()
        : json['CardTotal'] as double;
    totalAmountRefunded = json['TotalAmountRefunded'] is int
        ? (json['TotalAmountRefunded'] as int).toDouble()
        : json['TotalAmountRefunded'] as double;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['collectedBy'] = this.collectedBy;
    if (this.itemDetails != null) {
      data['itemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.refundItemDetails != null) {
      data['refundItemDetails'] =
          this.refundItemDetails.map((v) => v.toJson()).toList();
    }
    data['GrandTotal'] = this.grandTotal;
    data['CashTotal'] = this.cashTotal;
    data['CheckTotal'] = this.checkTotal;
    data['CardTotal'] = this.cardTotal;
    data['TotalAmountRefunded'] = this.totalAmountRefunded;
    return data;
  }
}

class ItemDetails {
  String total;
  String receiptNumber;
  int itemCount = 0;
  String name = '';

  ItemDetails({this.total, this.receiptNumber, this.itemCount, this.name});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    try {
      if (json['total'] is String) {
        total = json['total'];

        print(total);
      } else if (json['total'] is int) {
        total = json['total'].toString();
        print(total);
      }
      if (json['receiptNumber'] is String) {
        receiptNumber = json['receiptNumber'];

        print(receiptNumber);
      } else if (json['receiptNumber'] is int) {
        receiptNumber = json['receiptNumber'].toString();
        print(receiptNumber);
      }
    } catch (e) {}
    try {
      itemCount = json['count'];
      name = json['name'];
    } catch (e) {}
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['receiptNumber'] = this.receiptNumber;
    data['count'] = this.itemCount;
    data['name'] = this.name;

    return data;
  }
}

class RefundItemDetails {
  String total;
  String receiptNumber;

  RefundItemDetails({this.total, this.receiptNumber});

  RefundItemDetails.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    receiptNumber = json['receiptNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['receiptNumber'] = this.receiptNumber;
    return data;
  }
}

class BillCollectors {
  String transactionDate;
  String userCollectedId;
  String userCollectedName;

  BillCollectors(
      {this.transactionDate, this.userCollectedId, this.userCollectedName});

  factory BillCollectors.fromJson(Map<String, dynamic> parsedJson) {
    var transactionDate = parsedJson['transactionDate'];
    var userCollectedId = parsedJson['collectedByUserId'];
    var userCollectedName = parsedJson['collectedByUserName'];

    return BillCollectors(
        transactionDate: transactionDate,
        userCollectedId: userCollectedId,
        userCollectedName: userCollectedName);
  }
}

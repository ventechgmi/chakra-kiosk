class KioskSettings {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;

  KioskSettings(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode});

  factory KioskSettings.fromJson(Map<String, dynamic> parsedJson) {
    var city = parsedJson['city'];
    var address = parsedJson['address'];
    var stateId = parsedJson['stateId'];
    var zipCode = parsedJson['zipCode'];
    var countryId = parsedJson['countryId'];
    var countryCode = parsedJson['countryCode'];

    return KioskSettings(
        city: city,
        address: address,
        stateId: stateId,
        zipCode: zipCode,
        countryId: countryId,
        countryCode: countryCode);
  }
}

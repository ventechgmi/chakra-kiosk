class TempleSettings {
  bool success;
  List<TempleSettingsData> templeSettingsData;
  TenantData tenantData;

  TempleSettings({this.success, this.templeSettingsData, this.tenantData});

  TempleSettings.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['templeSettingsData'] != null) {
      templeSettingsData = new List<TempleSettingsData>();
      json['templeSettingsData'].forEach((v) {
        templeSettingsData.add(new TempleSettingsData.fromJson(v));
      });
    }
    tenantData = json['tenantData'] != null
        ? new TenantData.fromJson(json['tenantData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.templeSettingsData != null) {
      data['templeSettingsData'] =
          this.templeSettingsData.map((v) => v.toJson()).toList();
    }
    if (this.tenantData != null) {
      data['tenantData'] = this.tenantData.toJson();
    }
    return data;
  }
}

class TempleSettingsData {
  String templeSettingId;
  Timings timings;
//Others others;
  Message message;
  Kiosk kiosk;
  ThankYouSignatureSettings thankYouSignatureSettings;
  FeedbackSettings feedbackSettings;
  String tenantId;
  AdditionalProperties additionalProperties;

  TempleSettingsData(
      {this.templeSettingId,
      this.timings,
      //    this.others,
      this.message,
      this.kiosk,
      this.thankYouSignatureSettings,
      this.feedbackSettings,
      this.tenantId,
      this.additionalProperties});

  TempleSettingsData.fromJson(Map<String, dynamic> json) {
    templeSettingId = json['templeSettingId'];
    timings =
        json['timings'] != null ? new Timings.fromJson(json['timings']) : null;
    // others =        json['others'] != null ? new Others.fromJson(json['others']) : null;
    message =
        json['message'] != null ? new Message.fromJson(json['message']) : null;
    kiosk = json['kiosk'] != null ? new Kiosk.fromJson(json['kiosk']) : null;
    thankYouSignatureSettings = json['ThankYouSignatureSettings'] != null
        ? new ThankYouSignatureSettings.fromJson(
            json['ThankYouSignatureSettings'])
        : null;
    feedbackSettings = json['feedbackSettings'] != null
        ? new FeedbackSettings.fromJson(json['feedbackSettings'])
        : null;
    tenantId = json['tenantId'];
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templeSettingId'] = this.templeSettingId;
    if (this.timings != null) {
      data['timings'] = this.timings.toJson();
    }
    /*  if (this.others != null) {
      data['others'] = this.others.toJson();
    }*/
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    if (this.kiosk != null) {
      data['kiosk'] = this.kiosk.toJson();
    }
    if (this.thankYouSignatureSettings != null) {
      data['ThankYouSignatureSettings'] =
          this.thankYouSignatureSettings.toJson();
    }
    if (this.feedbackSettings != null) {
      data['feedbackSettings'] = this.feedbackSettings.toJson();
    }
    data['tenantId'] = this.tenantId;
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    return data;
  }
}

class Timings {
  bool isBrkHours;
  String weekDayEndTime;
  String weekEndsEndTime;
  String weekDayStartTime;
  bool isWeekEndBrkHours;
  String weekEndsStartTime;
  String weekDayBreakEndTime;
  String weekEndsBreakEndTime;
  String weekDayBreakStartTime;
  String weekEndsBreakStartTime;

  Timings(
      {this.isBrkHours,
      this.weekDayEndTime,
      this.weekEndsEndTime,
      this.weekDayStartTime,
      this.isWeekEndBrkHours,
      this.weekEndsStartTime,
      this.weekDayBreakEndTime,
      this.weekEndsBreakEndTime,
      this.weekDayBreakStartTime,
      this.weekEndsBreakStartTime});

  Timings.fromJson(Map<String, dynamic> json) {
    isBrkHours = json['IsBrkHours'];
    weekDayEndTime = json['weekDayEndTime'];
    weekEndsEndTime = json['weekEndsEndTime'];
    weekDayStartTime = json['weekDayStartTime'];
    isWeekEndBrkHours = json['IsWeekEndBrkHours'];
    weekEndsStartTime = json['weekEndsStartTime'];
    weekDayBreakEndTime = json['weekDayBreakEndTime'];
    weekEndsBreakEndTime = json['weekEndsBreakEndTime'];
    weekDayBreakStartTime = json['weekDayBreakStartTime'];
    weekEndsBreakStartTime = json['weekEndsBreakStartTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IsBrkHours'] = this.isBrkHours;
    data['weekDayEndTime'] = this.weekDayEndTime;
    data['weekEndsEndTime'] = this.weekEndsEndTime;
    data['weekDayStartTime'] = this.weekDayStartTime;
    data['IsWeekEndBrkHours'] = this.isWeekEndBrkHours;
    data['weekEndsStartTime'] = this.weekEndsStartTime;
    data['weekDayBreakEndTime'] = this.weekDayBreakEndTime;
    data['weekEndsBreakEndTime'] = this.weekEndsBreakEndTime;
    data['weekDayBreakStartTime'] = this.weekDayBreakStartTime;
    data['weekEndsBreakStartTime'] = this.weekEndsBreakStartTime;
    return data;
  }
}

class Others {
  String homePageScheduleType;
  bool acceptDuplicateEmails;
  String defaultTravelTimeForOutsidePuja;
  int minimumNumberofPriestsInsideTemple;
  int maximumNumberofSchedulesOutsideTemple;

  Others(
      {this.homePageScheduleType,
      this.acceptDuplicateEmails,
      this.defaultTravelTimeForOutsidePuja,
      this.minimumNumberofPriestsInsideTemple,
      this.maximumNumberofSchedulesOutsideTemple});

  Others.fromJson(Map<String, dynamic> json) {
    homePageScheduleType = json['homePageScheduleType'];
    if (json['AcceptDuplicateEmails'] is String) {
      acceptDuplicateEmails = false;
    } else {
      acceptDuplicateEmails = json['AcceptDuplicateEmails'];
    }
    defaultTravelTimeForOutsidePuja = json['DefaultTravelTimeForOutsidePuja'];

    if (json['MinimumNumberofPriestsInsideTemple'] is String) {
      minimumNumberofPriestsInsideTemple =
          int.parse(json['MinimumNumberofPriestsInsideTemple']);
    } else {
      minimumNumberofPriestsInsideTemple =
          json['MinimumNumberofPriestsInsideTemple'];
    }
    if (json['MaximumNumberofSchedulesOutsideTemple'] is String) {
      maximumNumberofSchedulesOutsideTemple =
          int.parse(json['MaximumNumberofSchedulesOutsideTemple']);
    } else {
      maximumNumberofSchedulesOutsideTemple =
          json['MaximumNumberofSchedulesOutsideTemple'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['homePageScheduleType'] = this.homePageScheduleType;
    data['AcceptDuplicateEmails'] = this.acceptDuplicateEmails;
    data['DefaultTravelTimeForOutsidePuja'] =
        this.defaultTravelTimeForOutsidePuja;
    data['MinimumNumberofPriestsInsideTemple'] =
        this.minimumNumberofPriestsInsideTemple;
    data['MaximumNumberofSchedulesOutsideTemple'] =
        this.maximumNumberofSchedulesOutsideTemple;
    return data;
  }
}

class Message {
  String paymentReceiptMessage;
  String transactionEmailMessage;
  String kioskPaymentReceiptMessage;

  Message(
      {this.paymentReceiptMessage,
      this.transactionEmailMessage,
      this.kioskPaymentReceiptMessage});

  Message.fromJson(Map<String, dynamic> json) {
    paymentReceiptMessage = json['PaymentReceiptMessage'];
    transactionEmailMessage = json['TransactionEmailMessage'];
    kioskPaymentReceiptMessage = json['KioskPaymentReceiptMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['PaymentReceiptMessage'] = this.paymentReceiptMessage;
    data['TransactionEmailMessage'] = this.transactionEmailMessage;
    data['KioskPaymentReceiptMessage'] = this.kioskPaymentReceiptMessage;
    return data;
  }
}

class Kiosk {
  bool isSkipWalkin;
  bool isSkipReceipt;
  bool isSignatureCopy;
  int userSearchCount;
  bool returnRefundPrint;
  String receiptMinimumAmount;
  String returnRefundDuration;
  bool isClosingReceiptGroupBy;
  String cCDCMinimumProcessingAmount;

  Kiosk(
      {this.isSkipWalkin,
      this.isSkipReceipt,
      this.isSignatureCopy,
      this.userSearchCount,
      this.returnRefundPrint,
      this.receiptMinimumAmount,
      this.returnRefundDuration,
      this.isClosingReceiptGroupBy,
      this.cCDCMinimumProcessingAmount});

  Kiosk.fromJson(Map<String, dynamic> json) {
    isSkipWalkin = json['IsSkipWalkin'];
    isSkipReceipt = json['IsSkipReceipt'];
    isSignatureCopy = json['IsSignatureCopy'];
    if (json['UserSearchCount'] is String) {
      if (json['UserSearchCount'] != null && json['UserSearchCount'] != '') {
        userSearchCount = json['UserSearchCount'];
      }
    } else {
      userSearchCount = json['UserSearchCount'];
    }
    returnRefundPrint = json['ReturnRefundPrint'];
    if (json['ReceiptMinimumAmount'] is int) {
      receiptMinimumAmount = json['ReceiptMinimumAmount'].toString();
    } else {
      receiptMinimumAmount = json['ReceiptMinimumAmount'];
    }
    if (json['ReturnRefundDuration'] is int) {
      returnRefundDuration = json['ReturnRefundDuration'].toString();
    } else {
      returnRefundDuration = json['ReturnRefundDuration'];
    }

    isClosingReceiptGroupBy = json['IsClosingReceiptGroupBy'];
    cCDCMinimumProcessingAmount = json['CCDCMinimumProcessingAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IsSkipWalkin'] = this.isSkipWalkin;
    data['IsSkipReceipt'] = this.isSkipReceipt;
    data['IsSignatureCopy'] = this.isSignatureCopy;
    data['UserSearchCount'] = this.userSearchCount;
    data['ReturnRefundPrint'] = this.returnRefundPrint;
    data['ReceiptMinimumAmount'] = this.receiptMinimumAmount;
    data['ReturnRefundDuration'] = this.returnRefundDuration;
    data['IsClosingReceiptGroupBy'] = this.isClosingReceiptGroupBy;
    data['CCDCMinimumProcessingAmount'] = this.cCDCMinimumProcessingAmount;
    return data;
  }
}

class ThankYouSignatureSettings {
  String signatureSettingsMessage;

  ThankYouSignatureSettings({this.signatureSettingsMessage});

  ThankYouSignatureSettings.fromJson(Map<String, dynamic> json) {
    signatureSettingsMessage = json['SignatureSettingsMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SignatureSettingsMessage'] = this.signatureSettingsMessage;
    return data;
  }
}

class FeedbackSettings {
  bool isRating;
  bool isSuggestionBox;
  List<FeedbackQuestion> feedbackQuestion;
  List<RatingAndDescription> ratingAndDescription;
  bool isRequireFeedbackForm;

  FeedbackSettings(
      {this.isRating,
      this.isSuggestionBox,
      this.feedbackQuestion,
      this.ratingAndDescription,
      this.isRequireFeedbackForm});

  FeedbackSettings.fromJson(Map<String, dynamic> json) {
    isRating = json['isRating'];
    isSuggestionBox = json['isSuggestionBox'];
    if (json['feedbackQuestion'] != null) {
      feedbackQuestion = new List<FeedbackQuestion>();
      json['feedbackQuestion'].forEach((v) {
        feedbackQuestion.add(new FeedbackQuestion.fromJson(v));
      });
    }
    if (json['ratingAndDescription'] != null) {
      ratingAndDescription = new List<RatingAndDescription>();
      json['ratingAndDescription'].forEach((v) {
        ratingAndDescription.add(new RatingAndDescription.fromJson(v));
      });
    }
    isRequireFeedbackForm = json['isRequireFeedbackForm'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isRating'] = this.isRating;
    data['isSuggestionBox'] = this.isSuggestionBox;
    if (this.feedbackQuestion != null) {
      data['feedbackQuestion'] =
          this.feedbackQuestion.map((v) => v.toJson()).toList();
    }
    if (this.ratingAndDescription != null) {
      data['ratingAndDescription'] =
          this.ratingAndDescription.map((v) => v.toJson()).toList();
    }
    data['isRequireFeedbackForm'] = this.isRequireFeedbackForm;
    return data;
  }
}

class FeedbackQuestion {
  int id;
  String type;
  String status;
  String description;

  FeedbackQuestion({this.id, this.type, this.status, this.description});

  FeedbackQuestion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    status = json['status'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['status'] = this.status;
    data['description'] = this.description;
    return data;
  }
}

class RatingAndDescription {
  int rating;
  String description;

  RatingAndDescription({this.rating, this.description});

  RatingAndDescription.fromJson(Map<String, dynamic> json) {
    rating = json['rating'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rating'] = this.rating;
    data['description'] = this.description;
    return data;
  }
}

class AdditionalProperties {
  String createdBy;
  String modifiedBy;
  String createdDate;
  String modifiedDate;

  AdditionalProperties(
      {this.createdBy, this.modifiedBy, this.createdDate, this.modifiedDate});

  AdditionalProperties.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    modifiedBy = json['modifiedBy'];
    createdDate = json['createdDate'];
    modifiedDate = json['modifiedDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['createdDate'] = this.createdDate;
    data['modifiedDate'] = this.modifiedDate;
    return data;
  }
}

class TenantData {
  String tenantId;
  String name;
  Address address;
  AdditionalProperties additionalProperties;
  String domain;
  String shortCode;

  TenantData(
      {this.tenantId,
      this.name,
      this.address,
      this.additionalProperties,
      this.domain,
      this.shortCode});

  TenantData.fromJson(Map<String, dynamic> json) {
    tenantId = json['tenantId'];
    name = json['name'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    additionalProperties = json['additionalProperties'] != null
        ? new AdditionalProperties.fromJson(json['additionalProperties'])
        : null;
    domain = json['domain'];
    shortCode = json['shortCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tenantId'] = this.tenantId;
    data['name'] = this.name;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.additionalProperties != null) {
      data['additionalProperties'] = this.additionalProperties.toJson();
    }
    data['domain'] = this.domain;
    data['shortCode'] = this.shortCode;
    return data;
  }
}

class Address {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;

  Address(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode});

  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    address = json['address'];
    stateId = json['stateId'];
    zipCode = json['zipCode'];
    countryId = json['countryId'];
    countryCode = json['countryCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['address'] = this.address;
    data['stateId'] = this.stateId;
    data['zipCode'] = this.zipCode;
    data['countryId'] = this.countryId;
    data['countryCode'] = this.countryCode;
    return data;
  }
}

class TempleAdditionalProperties {
  String password;
  String kioskImage;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String phoneNumber;
  String receiptLogo;

  TempleAdditionalProperties(
      {this.password,
      this.kioskImage,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.phoneNumber,
      this.receiptLogo});

  TempleAdditionalProperties.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    kioskImage = json['kioskImage'];
    mailFooter = json['mailFooter'];
    mailHeader = json['mailHeader'];
    templeMail = json['templeMail'];
    websiteUrl = json['websiteUrl'];
    phoneNumber = json['phoneNumber'];
    receiptLogo = json['receiptLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['kioskImage'] = this.kioskImage;
    data['mailFooter'] = this.mailFooter;
    data['mailHeader'] = this.mailHeader;
    data['templeMail'] = this.templeMail;
    data['websiteUrl'] = this.websiteUrl;
    data['phoneNumber'] = this.phoneNumber;
    data['receiptLogo'] = this.receiptLogo;
    return data;
  }
}

import 'package:chakra/models/products/Items.dart';

class ProductItems {
  String catalogueType;
  String catalogueTypeIcon;
  String colorString;
  List<Items> items;

  ProductItems(
      {this.catalogueType,
      this.catalogueTypeIcon,
      this.colorString,
      this.items});

  factory ProductItems.fromJson(Map<String, dynamic> parsedJson) {
    var items = parsedJson['items'] as List;
    var catalogueType = parsedJson['catalogueType'];
    var catalogueTypeIcon = parsedJson['catalogueTypeIcon'];
    var colorString = parsedJson['color'];

    List<Items> itemsList = items.map((i) => Items.fromJson(i)).toList();

    return ProductItems(
        items: itemsList,
        catalogueType: catalogueType,
        catalogueTypeIcon: catalogueTypeIcon,
        colorString: colorString);
  }
}

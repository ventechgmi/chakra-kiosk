import 'package:chakra/models/Products/Timings.dart';
import 'package:chakra/models/products/Fees.dart';

class Items {
  String templeServiceId;
  String name;
  List<Fees> feeDetails;
  List<Timings> eventTimings;
  String imageName;
  String imagePath;
  String startDate;
  String endDate;
  String startTime;
  String endTime;
  String catalogueType;
  String serviceType;
  bool templeToken;

  Items(
      {this.templeServiceId,
      this.name,
      this.feeDetails,
      this.eventTimings,
      this.imageName,
      this.imagePath,
      this.startDate,
      this.endDate,
      this.startTime,
      this.endTime,
      this.catalogueType,
      this.serviceType,
      this.templeToken});

  factory Items.fromJson(Map<String, dynamic> parsedJson) {
    var templeServiceId = parsedJson['templeServiceId'];
    var name = parsedJson['name'];
    var feeDetails = parsedJson['feeDetails'] as List;
    var eventTimings =
        parsedJson['timings'] != null ? parsedJson['timings'] as List : [];
    var imageName = parsedJson['imageName'];
    var imagePath = parsedJson['imagePath'];
    var startDate = parsedJson['startDate'];
    var endDate = parsedJson['endDate'];
    var startTime = parsedJson['startTime'];
    var endTime = parsedJson['endTime'];
    var catalogueType = parsedJson['catalogueType'];
    var serviceType = parsedJson['serviceType'];
    var templeToken = parsedJson['templeToken'];

    List<Fees> feeList = feeDetails.map((i) => Fees.fromJson(i)).toList();
    List<Timings> eventList =
        eventTimings.map((i) => Timings.fromJson(i)).toList();

    return Items(
        templeServiceId: templeServiceId,
        name: name,
        feeDetails: feeList,
        eventTimings: eventList,
        imageName: imageName,
        imagePath: imagePath,
        startDate: startDate,
        endDate: endDate,
        startTime: startTime,
        endTime: endTime,
        catalogueType: catalogueType,
        serviceType: serviceType,
        templeToken: templeToken);
  }
}

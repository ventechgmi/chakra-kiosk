class Timings {
  String recurrenceDate;
  String startTime;
  String endTime;

  Timings({this.recurrenceDate, this.startTime, this.endTime});

  factory Timings.fromJson(Map<String, dynamic> parsedJson) {
    var recurrenceDate = parsedJson['recurrenceDate'];
    var startTime = parsedJson['startTime'];
    var endTime = parsedJson['endTime'];

    return Timings(
        recurrenceDate: recurrenceDate, startTime: startTime, endTime: endTime);
  }
}

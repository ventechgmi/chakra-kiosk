import 'package:chakra/models/products/ProductItems.dart';

class MasterItems {
  bool success;
  String message;
  final List<ProductItems> products;

  MasterItems({this.success, this.message, this.products});

  factory MasterItems.fromJson(Map<String, dynamic> parsedJson) {
    var products = parsedJson['productItems'] as List;
    var success = parsedJson['success'] as bool;
    var message = parsedJson['message'];

    List<ProductItems> productsList =
        products.map((i) => ProductItems.fromJson(i)).toList();

    return MasterItems(
      products: productsList,
      success: success,
      message: message,
    );
  }
}

class Fees {
  String fee;
  String name;

  Fees({this.fee, this.name});

  factory Fees.fromJson(Map<String, dynamic> parsedJson) {
    var fee = parsedJson['fee'];
    var name = parsedJson['name'];

    return Fees(
      fee: fee,
      name: name,
    );
  }
}

class DevoteeRegResponse {
  bool success;
  String message;
  //OrderResponse orderResponse;

  DevoteeRegResponse({
    this.success,
    this.message,
  });

  factory DevoteeRegResponse.fromJson(Map<String, dynamic> parsedJson) {
    var success = parsedJson['success'] as bool;
    var message = parsedJson['message'];

    return DevoteeRegResponse(
      success: success,
      message: message,
    );
  }
}

/* class OrderResponse {
  String orderId;

  OrderResponse({this.orderId});

  factory OrderResponse.fromJson(Map<String, dynamic> parsedJson) {
    var orderId = parsedJson['orderId'];

    return OrderResponse(
      orderId: orderId,
    );
  } 
}
*/

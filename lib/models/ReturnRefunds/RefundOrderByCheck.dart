import 'package:chakra/models/ReturnRefunds/RefundOrderByCash.dart';

class RefundOrderByCheck {
  String orderId;
  String refundPaymentType;
  String refundedOn;
  String refundAmount;
  String reason;
  String payeeName;
  String refundCheckNumber;
  String bankName;
  String dateOnTheCheck;
  String refundReason;
  List<RefundOrderDetails> refundDetails;

  RefundOrderByCheck(
      {this.orderId,
      this.refundPaymentType,
      this.refundedOn,
      this.refundAmount,
      this.reason,
      this.payeeName,
      this.bankName,
      this.dateOnTheCheck,
      this.refundCheckNumber,
      this.refundDetails});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['refundPaymentType'] = this.refundPaymentType;
    data['refundedOn'] = this.refundedOn;
    data['refundAmount'] = this.refundAmount;
    data['refundReason'] = this.reason;
    data['payeeName'] = this.payeeName;
    data['bankName'] = this.bankName;
    data['dateOnTheCheck'] = this.dateOnTheCheck;
    data['refundCheckNumber'] = this.refundCheckNumber;
    data['refundDetails'] = this.refundDetails;
    return data;
  }
}

class Donations {
  bool success;
  String message;
  List<DonationItems> donationItems;

  Donations({this.success, this.message, this.donationItems});

  factory Donations.fromJson(Map<String, dynamic> parsedJson) {
    var donationItems = parsedJson['DonationsList'] as List;
    var success = parsedJson['success'] as bool;
    var message = parsedJson['message'];

    List<DonationItems> donationList =
        donationItems.map((i) => DonationItems.fromJson(i)).toList();

    return Donations(
        success: success, message: message, donationItems: donationList);
  }
}

class DonationItems {
  String orderId;
  String subCategoryCode;
  String transactionDate;
  String templeServiceId;
  String amount;
  int quantity;
  String totalAmount;
  int receiptNumber;
  String type;
  bool isRefundOrder;
  String firstName;
  String lastName;
  String mobileNumber;
  String paymentType;
  String serviceType;
  String serviceName;
  String approvalNumber;
  RefundDetails refundDetails;
  String createdBy;
  String modifiedBy;
  bool isKiosk;

  DonationItems(
      {this.orderId,
      this.subCategoryCode,
      this.transactionDate,
      this.templeServiceId,
      this.amount,
      this.quantity,
      this.totalAmount,
      this.receiptNumber,
      this.type,
      this.isRefundOrder,
      this.firstName,
      this.lastName,
      this.mobileNumber,
      this.paymentType,
      this.serviceType,
      this.serviceName,
      this.approvalNumber,
      this.refundDetails,
      this.createdBy,
      this.modifiedBy,
      this.isKiosk});

  factory DonationItems.fromJson(Map<String, dynamic> parsedJson) {
    var orderId = parsedJson['orderId'];
    var subCategoryCode = parsedJson['subCategoryCode'];
    var transactionDate = parsedJson['transactionDate'];
    var templeServiceId = parsedJson['templeServiceId'];
    var amount = parsedJson['amount'];
    var quantity = parsedJson['quantity'];
    var totalAmount = parsedJson['totalAmount'];
    var receiptNumber = parsedJson['receiptNumber'];
    var type = parsedJson['type'];
    var isRefundOrder = parsedJson['isRefundOrder'];
    var firstName = parsedJson['firstName'];
    var lastName = parsedJson['lastName'];
    var mobileNumber = parsedJson['mobileNumber'];
    var paymentType = parsedJson['paymentType'];
    var serviceType = parsedJson['serviceType'];
    var serviceName = parsedJson['serviceName'];
    var approvalNumber = parsedJson['approvalNumber'];
    var refundDetails = parsedJson['refundDetails'] != null
        ? RefundDetails.fromJson(parsedJson['refundDetails'])
        : parsedJson['refundDetails'];
    var createdBy = parsedJson['createdBy'];
    var modifiedBy = parsedJson['modifiedBy'];
    var isKiosk = parsedJson['isKiosk'];

    return DonationItems(
        orderId: orderId,
        subCategoryCode: subCategoryCode,
        transactionDate: transactionDate,
        templeServiceId: templeServiceId,
        amount: amount,
        quantity: quantity,
        totalAmount: totalAmount,
        receiptNumber: receiptNumber,
        type: type,
        isRefundOrder: isRefundOrder,
        firstName: firstName,
        lastName: lastName,
        mobileNumber: mobileNumber,
        paymentType: paymentType,
        serviceType: serviceType,
        serviceName: serviceName,
        approvalNumber: approvalNumber,
        refundDetails: refundDetails,
        createdBy: createdBy,
        modifiedBy: modifiedBy,
        isKiosk: isKiosk);
  }
}

class RefundDetails {
  String reason;
  String bankName;
  String payeeName;
  String bankCharges;
  String paymentType;
  String approvalNumber;
  String dateOnTheCheck;
  String checkReturnDate;

  RefundDetails(
      {this.reason,
      this.bankName,
      this.payeeName,
      this.bankCharges,
      this.paymentType,
      this.approvalNumber,
      this.dateOnTheCheck,
      this.checkReturnDate});

  factory RefundDetails.fromJson(Map<String, dynamic> parsedJson) {
    var reason = parsedJson['reason'];
    var bankName = parsedJson['bankName'];
    var payeeName = parsedJson['payeeName'];
    var bankCharges = parsedJson['bankCharges'];
    var paymentType = parsedJson['paymentType'];
    var approvalNumber = parsedJson['approvalNumber'];
    var dateOnTheCheck = parsedJson['dateOnTheCheck'];
    var checkReturnDate = parsedJson['checkReturnDate'];

    return RefundDetails(
        reason: reason,
        bankCharges: bankCharges,
        bankName: bankName,
        payeeName: payeeName,
        paymentType: paymentType,
        approvalNumber: approvalNumber,
        dateOnTheCheck: dateOnTheCheck,
        checkReturnDate: checkReturnDate);
  }
}

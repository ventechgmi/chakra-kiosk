import 'RepaymentByCash.dart';

class RepaymentByCheck {
  String orderId;
  String paymentType;
  String transactionDate;
  String amount;
  String bankcharges;
  String notes;
  String checkReturnedDate;
  String reason;
  String payeeName;
  String bankName;
  String dateOnTheCheck;
  String approvalNumber;
  List<RepaymentDetails> repaymentDetails;

  RepaymentByCheck(
      {this.orderId,
      this.paymentType,
      this.transactionDate,
      this.amount,
      this.bankcharges,
      this.notes,
      this.checkReturnedDate,
      this.reason,
      this.payeeName,
      this.bankName,
      this.dateOnTheCheck,
      this.approvalNumber,
      this.repaymentDetails});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['paymentType'] = this.paymentType;
    data['transactionDate'] = this.transactionDate;
    data['amount'] = this.amount;
    data['bankcharges'] = this.bankcharges;
    data['notes'] = this.notes;
    data['checkReturnedDate'] = this.checkReturnedDate;
    data['reason'] = this.reason;
    data['payeeName'] = this.payeeName;
    data['bankName'] = this.bankName;
    data['dateOnTheCheck'] = this.dateOnTheCheck;
    data['approvalNumber'] = this.approvalNumber;
    data['repaymentDetails'] = this.repaymentDetails;
    return data;
  }
}

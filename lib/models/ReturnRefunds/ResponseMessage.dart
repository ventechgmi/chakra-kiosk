class ResponseMessage {
  bool success;
  String message;
  OrderResponse orderResponse;

  ResponseMessage({this.success, this.message, this.orderResponse});

  factory ResponseMessage.fromJson(Map<String, dynamic> parsedJson) {
    var success = parsedJson['success'] as bool;
    var message = parsedJson['message'];
    var orderResponse = parsedJson['order'];

    OrderResponse orderMsg = OrderResponse.fromJson(orderResponse);

    return ResponseMessage(
        success: success, message: message, orderResponse: orderMsg);
  }
}

class OrderResponse {
  String orderId;

  OrderResponse({this.orderId});

  factory OrderResponse.fromJson(Map<String, dynamic> parsedJson) {
    var orderId = parsedJson['orderId'];

    return OrderResponse(
      orderId: orderId,
    );
  }
}

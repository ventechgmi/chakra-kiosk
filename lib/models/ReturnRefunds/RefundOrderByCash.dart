class RefundOrderByCash {
  String orderId;
  String refundPaymentType;
  String refundedOn;
  String refundAmount;
  String reason;
  List<RefundOrderDetails> refundDetails;

  RefundOrderByCash(
      {this.orderId,
      this.refundPaymentType,
      this.refundedOn,
      this.refundAmount,
      this.reason,
      this.refundDetails});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['refundPaymentType'] = this.refundPaymentType;
    data['refundedOn'] = this.refundedOn;
    data['refundAmount'] = this.refundAmount;
    data['refundReason'] = this.reason;
    data['refundDetails'] = this.refundDetails;
    return data;
  }
}

class RefundOrderDetails {
  String templeServiceId;
  String amount;

  RefundOrderDetails({this.templeServiceId, this.amount});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templeServiceId'] = this.templeServiceId;
    data['totalAmount'] = this.amount;
    return data;
  }
}

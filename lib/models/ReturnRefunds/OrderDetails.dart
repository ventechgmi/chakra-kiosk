import 'Donations.dart';

class OrderDetails {
  bool success;
  String message;
  List<OrderItems> orderDetails;

  OrderDetails({this.success, this.message, this.orderDetails});

  factory OrderDetails.fromJson(Map<String, dynamic> parsedJson) {
    var orderDetails = parsedJson['orderDetails'] as List;
    var success = parsedJson['success'] as bool;
    var message = parsedJson['message'];

    List<OrderItems> ordersList =
        orderDetails.map((i) => OrderItems.fromJson(i)).toList();

    return OrderDetails(
        success: success, message: message, orderDetails: ordersList);
  }
}

class OrderItems {
  String orderId;
  String subCategoryCode;
  String transactionDate;
  String templeServiceId;
  String amount;
  int quantity;
  String totalAmount;
  int receiptNumber;
  String type;
  bool isRefundOrder;
  String firstName;
  String lastName;
  String mobileNumber;
  String paymentType;
  String serviceType;
  String serviceName;
  String receiptTotalAmount;
  PaymentDetails paymentDetails;
  String approvalNumber;
  String returnRefundDate;
  RefundDetails refundDetails;
  String extraPoojaFee;

  OrderItems(
      {this.orderId,
      this.subCategoryCode,
      this.transactionDate,
      this.templeServiceId,
      this.amount,
      this.quantity,
      this.totalAmount,
      this.receiptNumber,
      this.type,
      this.isRefundOrder,
      this.firstName,
      this.lastName,
      this.mobileNumber,
      this.paymentType,
      this.serviceType,
      this.serviceName,
      this.receiptTotalAmount,
      this.paymentDetails,
      this.approvalNumber,
      this.returnRefundDate,
      this.refundDetails,
      this.extraPoojaFee});

  factory OrderItems.fromJson(Map<String, dynamic> parsedJson) {
    var orderId = parsedJson['orderId'];
    var subCategoryCode = parsedJson['subCategoryCode'];
    var transactionDate = parsedJson['transactionDate'];
    var templeServiceId = parsedJson['templeServiceId'];
    var amount = parsedJson['amount'];
    var quantity = parsedJson['quantity'];
    var totalAmount = parsedJson['totalAmount'];
    var receiptNumber = parsedJson['receiptNumber'];
    var type = parsedJson['type'];
    var isRefundOrder = parsedJson['isRefundOrder'];
    var firstName = parsedJson['firstName'];
    var lastName = parsedJson['lastName'];
    var mobileNumber = parsedJson['mobileNumber'];
    var paymentType = parsedJson['paymentType'];
    var serviceType = parsedJson['serviceType'];
    var serviceName = parsedJson['serviceName'];
    var receiptTotalAmount = parsedJson['receiptTotalAmount'];
    var payDetails = PaymentDetails.fromJson(parsedJson['paymentDetails']);
    var approvalNumber = parsedJson['approvalNumber'];
    var returnRefundDate = parsedJson['returnRefundDate'];
    var refundDetails = parsedJson['refundDetails'] != null
        ? RefundDetails.fromJson(parsedJson['refundDetails'])
        : parsedJson['refundDetails'];
    var extraPoojaFee = parsedJson['extraPoojaFee'];

    return OrderItems(
        orderId: orderId,
        subCategoryCode: subCategoryCode,
        transactionDate: transactionDate,
        templeServiceId: templeServiceId,
        amount: amount,
        quantity: quantity,
        totalAmount: totalAmount,
        receiptNumber: receiptNumber,
        type: type,
        isRefundOrder: isRefundOrder,
        firstName: firstName,
        lastName: lastName,
        mobileNumber: mobileNumber,
        paymentType: paymentType,
        serviceType: serviceType,
        serviceName: serviceName,
        receiptTotalAmount: receiptTotalAmount,
        paymentDetails: payDetails,
        approvalNumber: approvalNumber,
        returnRefundDate: returnRefundDate,
        refundDetails: refundDetails,
        extraPoojaFee: extraPoojaFee);
  }
}

class PaymentDetails {
  String bankName;
  String payeeName;
  String paymentType;
  String approvalNumber;
  String dateOnTheCheck;

  PaymentDetails(
      {this.bankName,
      this.payeeName,
      this.paymentType,
      this.approvalNumber,
      this.dateOnTheCheck});

  factory PaymentDetails.fromJson(Map<String, dynamic> parsedJson) {
    var bankName = parsedJson['bankName'];
    var payeeName = parsedJson['payeeName'];
    var paymentType = parsedJson['paymentType'];
    var approvalNumber = parsedJson['approvalNumber'];
    var dateOnTheCheck = parsedJson['dateOnTheCheck'];

    return PaymentDetails(
        bankName: bankName,
        payeeName: payeeName,
        paymentType: paymentType,
        approvalNumber: approvalNumber,
        dateOnTheCheck: dateOnTheCheck);
  }
}

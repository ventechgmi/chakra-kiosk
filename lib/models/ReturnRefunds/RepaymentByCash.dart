class RepaymentByCash {
  String orderId;
  String paymentType;
  String transactionDate;
  String amount;
  String bankcharges;
  String notes;
  String checkReturnedDate;
  String reason;
  List<RepaymentDetails> repaymentDetails;

  RepaymentByCash(
      {this.orderId,
      this.paymentType,
      this.transactionDate,
      this.amount,
      this.bankcharges,
      this.notes,
      this.checkReturnedDate,
      this.reason,
      this.repaymentDetails});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['paymentType'] = this.paymentType;
    data['transactionDate'] = this.transactionDate;
    data['amount'] = this.amount;
    data['bankcharges'] = this.bankcharges;
    data['notes'] = this.notes;
    data['checkReturnedDate'] = this.checkReturnedDate;
    data['reason'] = this.reason;
    data['repaymentDetails'] = this.repaymentDetails;
    return data;
  }
}

class RepaymentDetails {
  String templeServiceId;
  String totalAmount;

  RepaymentDetails({this.templeServiceId, this.totalAmount});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templeServiceId'] = this.templeServiceId;
    data['totalAmount'] = this.totalAmount;
    return data;
  }
}

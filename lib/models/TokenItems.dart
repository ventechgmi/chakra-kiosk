class TokenItems {
  String name;
  int quantity;
  double price;
  double totalAmount;

  TokenItems({this.name, this.quantity, this.price, this.totalAmount});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['name'] = name;
    data['quantity'] = quantity;
    data['price'] = price;
    data['totalAmount'] = totalAmount;

    return data;
  }

  factory TokenItems.fromJson(Map<String, dynamic> parsedJson) {
    var name = parsedJson['name'];
    var quantity = parsedJson['quantity'];
    var price = parsedJson['price'];
    var totalAmount = parsedJson['totalAmount'];

    return TokenItems(
        name: name, quantity: quantity, price: price, totalAmount: totalAmount);
  }
}

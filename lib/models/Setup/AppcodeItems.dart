class AppcodeItems {
  String applicationCodeId;
  String applicationCodeType;
  String name;
  String title;
  String description;
  int sortOrder;
  String status;
  String additionalProperties;
  String referenceId;
  String tenantId;

  AppcodeItems(
      {this.applicationCodeId,
      this.applicationCodeType,
      this.name,
      this.title,
      this.description,
      this.sortOrder,
      this.status,
      this.additionalProperties,
      this.referenceId,
      this.tenantId});

  factory AppcodeItems.fromJson(Map<String, dynamic> parsedJson) {
    var appCodeId = parsedJson['applicationCodeId'];
    var appCodeType = parsedJson['applicationCodeType'];
    var name = parsedJson['name'];
    var title = parsedJson['title'];
    var description = parsedJson['description'];
    var sortOrder = parsedJson['sortOrder'];
    var status = parsedJson['status'];
    var additionalProperties = parsedJson['additionalProperties'];
    var referenceId = parsedJson['referenceId'];
    var tenantId = parsedJson['tenantId'];

    return AppcodeItems(
        applicationCodeId: appCodeId,
        applicationCodeType: appCodeType,
        name: name,
        title: title,
        description: description,
        sortOrder: sortOrder,
        status: status,
        additionalProperties: additionalProperties,
        referenceId: referenceId,
        tenantId: tenantId);
  }
}

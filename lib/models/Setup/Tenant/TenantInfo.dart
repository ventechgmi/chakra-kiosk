class TenantInfo {
  bool success;
  TenantDetails tenantDetails;

  TenantInfo({this.success, this.tenantDetails});

  factory TenantInfo.fromJson(Map<String, dynamic> parsedJson) {
    var success = parsedJson['success'] as bool;
    var tenantDetails =
        parsedJson['tenantDetails'] != null ? parsedJson['tenantDetails'] : '';

    TenantDetails tenantDetail = TenantDetails.fromJson(tenantDetails);

    return TenantInfo(tenantDetails: tenantDetail, success: success);
  }
}

class TenantDetails {
  String tenantId;
  String name;
  String shortCode;
  TenantAddress address;
  AdditionalProperties additionalProperties;

  TenantDetails(
      {this.tenantId,
      this.name,
      this.shortCode,
      this.address,
      this.additionalProperties});

  factory TenantDetails.fromJson(Map<String, dynamic> parsedJson) {
    var tenantID = parsedJson['tenantId'];
    var name = parsedJson['name'];
    var shortCode = parsedJson['shortCode'];
    var address = parsedJson['address'];
    var additionalProperties = parsedJson['additionalProperties'];

    AdditionalProperties additionaProperty =
        AdditionalProperties.fromJson(additionalProperties);

    TenantAddress addressJson = TenantAddress.fromJson(address);

    return TenantDetails(
        tenantId: tenantID,
        name: name,
        shortCode: shortCode,
        address: addressJson,
        additionalProperties: additionaProperty);
  }
}

class AdditionalProperties {
  String password;
  String mailFooter;
  String mailHeader;
  String templeMail;
  String websiteUrl;
  String phoneNumber;
  String receiptLogo;
  String kioskBanner;
  String shortCode;

  AdditionalProperties(
      {this.password,
      this.mailFooter,
      this.mailHeader,
      this.templeMail,
      this.websiteUrl,
      this.phoneNumber,
      this.receiptLogo,
      this.kioskBanner,
      this.shortCode});

  factory AdditionalProperties.fromJson(Map<String, dynamic> parsedJson) {
    var password = parsedJson['password'];
    var mailFooter = parsedJson['mailFooter'];
    var mailHeader = parsedJson['mailHeader'];
    var templeMail = parsedJson['templeMail'];
    var websiteUrl = parsedJson['websiteUrl'];
    var phoneNumber = parsedJson['phoneNumber'];
    var receiptLogo = parsedJson['receiptLogo'];
    var kioskBanner = parsedJson['kioskBanner'];
    var shortCode = parsedJson['shortCode'];

    return AdditionalProperties(
        password: password,
        mailFooter: mailFooter,
        mailHeader: mailHeader,
        templeMail: templeMail,
        websiteUrl: websiteUrl,
        phoneNumber: phoneNumber,
        receiptLogo: receiptLogo,
        kioskBanner: kioskBanner,
        shortCode: shortCode);
  }
}

class TenantAddress {
  String city;
  String address;
  String stateId;
  String zipCode;
  String countryId;
  String countryCode;

  TenantAddress(
      {this.city,
      this.address,
      this.stateId,
      this.zipCode,
      this.countryId,
      this.countryCode});

  factory TenantAddress.fromJson(Map<String, dynamic> parsedJson) {
    var city = parsedJson['city'];
    var address = parsedJson['address'];
    var stateId = parsedJson['stateId'];
    var zipCode = parsedJson['zipCode'];
    var countryId = parsedJson['countryId'];
    var countryCode = parsedJson['countryCode'];

    return TenantAddress(
        city: city,
        address: address,
        stateId: stateId,
        zipCode: zipCode,
        countryId: countryId,
        countryCode: countryCode);
  }
}

class StripeInfo {
  String deviceId;
  String deviceLabel;

  StripeInfo({this.deviceId, this.deviceLabel});

  factory StripeInfo.fromJson(Map<String, dynamic> parsedJson) {
    var deviceId = parsedJson['deviceId'];
    var deviceLabel = parsedJson['deviceLabel'];

    return StripeInfo(deviceId: deviceId, deviceLabel: deviceLabel);
  }

  Map<String, dynamic> toMap() {
    return {
      'deviceId': deviceId,
      'deviceLabel': deviceLabel,
    };
  }
}

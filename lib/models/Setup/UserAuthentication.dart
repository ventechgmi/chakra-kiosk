class UserAuthentication {
  bool success;
  String token;
  List<Claims> claims;
  List<String> roles;
  String lastLoginTime;
  String userId;
  String loggedinUser;

  UserAuthentication(
      {this.success,
      this.token,
      this.claims,
      this.roles,
      this.lastLoginTime,
      this.userId,
      this.loggedinUser});

  UserAuthentication.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    token = json['token'];
    userId = json['userId'];
    loggedinUser = json['name'];
    if (json['claims'] != null) {
      claims = new List<Claims>();
      json['claims'].forEach((v) {
        claims.add(new Claims.fromJson(v));
      });
    }

    if (json['roles'] != null) {
      roles = json['roles'].cast<String>();
    }

    if (json['lastLoginTime'] != null) {
      lastLoginTime = json['lastLoginTime'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['token'] = this.token;
    if (this.claims != null) {
      data['claims'] = this.claims.map((v) => v.toJson()).toList();
    }
    data['roles'] = this.roles;
    data['lastLoginTime'] = this.lastLoginTime;
    data['userId'] = this.userId;
    data['name'] = this.loggedinUser;
    return data;
  }
}

class Claims {
  int privilege;
  String name;

  Claims({this.privilege, this.name});

  Claims.fromJson(Map<String, dynamic> json) {
    privilege = json['privilege'];
    name = json['name'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['privilege'] = this.privilege;
    data['name'] = this.name;
    return data;
  }
}

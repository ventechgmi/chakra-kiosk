class UsersList {
  String userId;
  String firstName;
  String middleName;
  String lastName;
  String address;
  String city;
  String state;
  String stateId;
  String country;
  String countryId;
  String mobileNumber;
  String email;

  UsersList(
      {this.userId,
      this.firstName,
      this.middleName,
      this.lastName,
      this.address,
      this.city,
      this.state,
      this.stateId,
      this.country,
      this.countryId,
      this.mobileNumber,
      this.email});

  factory UsersList.fromJson(Map<String, dynamic> parsedJson) {
    var userId = parsedJson['userId'];
    var firstName = parsedJson['firstName'];
    var middleName = parsedJson['middleName'];
    var lastName = parsedJson['lastName'];
    var address = parsedJson['address'];
    var city = parsedJson['city'];
    var state = parsedJson['state'];
    var stateId = parsedJson['stateId'];
    var country = parsedJson['country'];
    var countryId = parsedJson['countryId'];
    var mobileNumber = parsedJson['mobileNumber'];
    var email = parsedJson['email'];

    return UsersList(
        userId: userId,
        firstName: firstName,
        middleName: middleName,
        lastName: lastName,
        address: address,
        city: city,
        state: state,
        stateId: stateId,
        country: country,
        countryId: countryId,
        mobileNumber: mobileNumber,
        email: email);
  }
}

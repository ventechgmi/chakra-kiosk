import 'package:chakra/models/Users/UsersList.dart';

class SearchMaster {
  bool success;
  String message;
  List<UsersList> userList;

  SearchMaster({this.success, this.message, this.userList});

  factory SearchMaster.fromJson(Map<String, dynamic> parsedJson) {
    var success = parsedJson['success'];
    var message = parsedJson['message'];
    var users = parsedJson['userList'] as List;

    List<UsersList> usersList =
        users.map((i) => UsersList.fromJson(i)).toList();

    return SearchMaster(
        success: success, message: message, userList: usersList);
  }
}

import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'dart:io' show Platform;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'Login/Tenant/TenantVerifyScreen.dart';
import 'Login/User/LoginScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Database db;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        checkLogin();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
    );
  }

  void checkLogin() async {
    if (Platform.isAndroid || Platform.isIOS) {
      final storage = new FlutterSecureStorage();
      String tenantCode = await storage.read(key: 'code');
      String pswd = await storage.read(key: 'pswd');

      if (tenantCode != null || pswd != null) {
        Navigator.of(context)
            .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
          return new LoginScreen(
            tenantCode: tenantCode,
          );
        }));
      } else {
        Navigator.of(context)
            .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
          return new TenantVerifyScreen();
        }));
      }
    } else {
      String dbPath = 'sample.db';
      DatabaseFactory dbFactory = databaseFactoryIo;

      //We use the database factory to open the database
      db = await dbFactory.openDatabase(dbPath);
      var store = StoreRef.main();
      var code = await store.record('code').get(db) as String;
      var pswd = await store.record('pswd').get(db) as String;
/* 
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        return new LoginScreen();
      })); */

      if (code != null || pswd != null) {
        Navigator.of(context)
            .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
          return new LoginScreen(
            tenantCode: code,
          );
        }));
      } else {
        Navigator.of(context)
            .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
          return new TenantVerifyScreen();
        }));
      }
    }
  }
}

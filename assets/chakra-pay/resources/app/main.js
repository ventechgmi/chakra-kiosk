const { app, BrowserWindow } = require('electron');

let mainWindow;

//global.sharedObj = { myvar: "hellofrommainjs" };

app.on('ready', function () {
  mainWindow = new BrowserWindow({
    height: 360,
    width: 360,
    frame: false,
    webPreferences: {
      nodeIntegration: true
    }
  });
  mainWindow.autoHideMenuBar = true;
  mainWindow.loadURL('http://192.168.10.6:3001/test1.html');
});
